module Modulo_n_Counter(
input clk, Reset, Load, countUp, 
output reg [4:0] Count,
input [4:0]data
);
always @(posedge clk)
begin
		if(Load)
				Count <= data;
		else if (Reset)
				Count <= 0; //reset
		else
				Count <= Count + (countUp ? 1 : -1) ; //count down
end
endmodule

//	mod_counter(
//		.clk(clk), .Load, .countUp(1), .restart(1), .enCounting(1), .dataLoad, .detectReLoad,.count, 
//	);
module mod_counter(
input clk, Load, countUp, loop, enCounting,
input [n:0] dataLoad, detectReLoad,
output reg [n:0] count 
);
parameter n=4;
always @(posedge clk)
begin
	if(Load)
		count = dataLoad;
	else if(detectReLoad==count)
			if(loop)
				count = dataLoad;
			else
				count = 0;
	else
		if(enCounting)
			count = count + (countUp ? 1 : -1);
end
endmodule //mod_counter