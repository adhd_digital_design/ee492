module MasterVerilog( 
	input clk,
	input [17:0] switch,
	output [7:0] green,
	output [17:0] red,
	input [3:0] pushBut,
	output [6:0] Hex0,
	output [6:0] Hex1,
	output [6:0] Hex2,
	output [6:0] Hex3,
	output [6:0] Hex4,
	output [6:0] Hex5,
	output [6:0] Hex6
);
	wire [4:0] twoCompInput;
		assign twoCompInput = switch[4:0];
	reg oneComp;
	reg stepped;
	wire [2:0] bitSize; assign bitSize= 4; // 0 is not an option, start counting at 1
	wire [1:0] displayType; //0=signed bit, 1=one's complement, 2=two's complement... representations
//Hex display
	reg [4:0] displayAtHex0, displayAtHex1, displayAtHex2, displayAtHex3, displayAtHex4, displayAtHex5,displayAtHex6; // {isHexValue_L, 4'b_HEX_REPRESENTATION}
		buf(red[0], 0);
		buf(red[1], 0);
		buf(red[2], 0);
		buf(red[3], 0);
		buf(red[4], 0);
		buf(red[5], detectedBin);
		buf(red[6], resetCounter);
		buf(red[7], 0);
	
	always @(pushBut[2]) // display when push button 2 is pressed
	begin
		//test to display (-14)
		if(pushBut[2])
			begin
			displayAtHex0=counterValue; // 4
			displayAtHex1=5'h0; // 1
			displayAtHex2=5'h0; // -
			displayAtHex3=5'h0;
			end
		else
			begin
			displayAtHex0=5'hE; // 
			displayAtHex1=5'hE; // -
			displayAtHex2=5'hE; // debug
			displayAtHex3=5'hE;
			end
	end

wire resetCounter, detectedBin;
wire [4:0] counterValue; 
assign green[4:0] = counterValue[4:0];
	
//and(detectedBin, !counterValue[3], counterValue[2], counterValue[1], counterValue[0]); 
//or(resetCounter, detectedBin);
//Modulo_n_Counter stepCounter(.clk(!pushBut[1]), .Reset(switch[17]), .Count(counterValue[4:0]),.countUp(switch[16]),.Load(switch[15]),.data(switch[4:0]));									
mod_counter(.clk(!pushBut[1]), .Load(switch[17]), .countUp(switch[16]),
.dataLoad(switch[4:0]), .detectReLoad(switch[9:5]),.count(counterValue[4:0]));
//	defparam stepCounter.max = 15;

//Display reg displayAtHex to SevenSegmentDisplayDecoder module 
SevenSegmentDisp LED0(.ssOut_L(Hex0[6:0]), .nIn(displayAtHex0[3:0]), .isHex_L(displayAtHex0[4]));
SevenSegmentDisp LED1(.ssOut_L(Hex1[6:0]), .nIn(displayAtHex1[3:0]), .isHex_L(displayAtHex1[4]));
SevenSegmentDisp LED2(.ssOut_L(Hex2[6:0]), .nIn(displayAtHex2[3:0]), .isHex_L(displayAtHex2[4]));
SevenSegmentDisp LED3(.ssOut_L(Hex3[6:0]), .nIn(displayAtHex3[3:0]), .isHex_L(displayAtHex3[4]));	

function [4:0] onesCompTOsignedRep;
		input [4:0] onesComp;
		begin
			if(onesComp[4]==1)
				begin
					onesCompTOsignedRep = ~onesComp;
					onesCompTOsignedRep[4]=1;
				end
			else
				onesCompTOsignedRep = onesComp;
		end
endfunction


endmodule //end MAIN

module Modulo_n_Counter(
input clk, Reset, Load, countUp, 
output reg [4:0] Count,
input [4:0]data
);
always @(posedge clk)
begin
		if(Load)
				Count <= data;
		else if (Reset)
				Count <= 0; //reset
		else
				Count <= Count + (countUp ? 1 : -1) ; //count down
end
endmodule

module mod_counter(
input clk, Load, countUp,
input [4:0] dataLoad, detectReLoad,
output reg [4:0] count
);
always @(posedge clk)
begin
	if(Load)
		count = dataLoad;
	else if(detectReLoad==count)
		count = dataLoad;
	else
		count = count + (countUp ? 1 : -1);
end
endmodule //mod_counter




/*
always @(posedge clk)
begin
	case(bitSize)
		1:			
					SevenSegmentDisplayDecoder(.ssOut_L(Hex0[6:0]), .nIn(bit0[3:0]));
		2:		begin
					SevenSegmentDisplayDecoder(.ssOut_L(Hex0[6:0]), .nIn(bit0[3:0]));
					SevenSegmentDisplayDecoder(.ssOut_L(Hex1[6:0]), .nIn(bit1[3:0]));
				end
		3:		begin
					SevenSegmentDisplayDecoder(.ssOut_L(Hex0[6:0]), .nIn(bit0[3:0]));
					SevenSegmentDisplayDecoder(.ssOut_L(Hex1[6:0]), .nIn(bit1[3:0]));
					SevenSegmentDisplayDecoder(.ssOut_L(Hex2[6:0]), .nIn(bit2[3:0]));
				end
		4:		begin	//four bits will be displayed on the 7seg display 
					SevenSegmentDisplayDecoder(.ssOut_L(Hex0[6:0]), .nIn(bit0[3:0]));
					SevenSegmentDisplayDecoder(.ssOut_L(Hex1[6:0]), .nIn(bit1[3:0]));
					SevenSegmentDisplayDecoder(.ssOut_L(Hex2[6:0]), .nIn(bit2[3:0]));
					SevenSegmentDisplayDecoder(.ssOut_L(Hex3[6:0]), .nIn(bit3[3:0]));
				end
	endcase 
	//this can be expanded for our final project to include all 7 bits. Eventually this should be contained within a module  along with displayType and its case
//Display Type
	
end //posedge clk always
*/

// Attempt to create a task.




	/*
	buf(green[0], my_inReg[0]); // n
	buf(green[1], my_inReg[1]); // d
	buf(green[2], my_inReg[2]); // q
	buf(green[3], my_inReg[3]); // S0
	buf(green[4], my_inReg[4]); // S1
	buf(green[5], my_inReg[5]); // S2
	buf(green[6], my_inReg[6]); // S3
	buf(green[7], clk); //
	counterValue[3], !counterValue[2], counterValue[1],!counterValue[0])
		//buf(red[0], my_s[0]);
		//buf(red[1], my_s[0]);
		//buf(red[2], my_s[0]);
		buf(red[3], 1);
		buf(red[4], my_s[0]);
		buf(red[5], my_s[1]);
		buf(red[6], my_s[2]);
		buf(red[7], my_s[3]);
	buf(red[8], 1);
		buf(red[9], switch[0]);
		buf(red[10], switch[1]);
		buf(red[11], switch[2]);
	buf(red[12], 1);
		buf(red[13], my_dS[0]);
		buf(red[14], my_dS[1]);
		buf(red[15], my_dS[2]);
		buf(red[16], my_dS[3]);
	buf(red[17], 1);
	*/

	
//Control 7segment individually. 
	/*
	always @(switch[7:0])
		begin
			Hex0[0] = switch[0];
			Hex0[1] = switch[1];
			Hex0[2] = switch[2];
			Hex0[3] = switch[3];
			Hex0[4] = switch[4];
			Hex0[5] = switch[5];
			Hex0[6] = switch[6];			
		end
		*/
//	decode2to4(.x(switch[1:0]),.y(red[3:0]),.EN(switch[17]),.y_L(green[3:0]));
//	decode3to8_L(.y_L(red[17:9]), .x(switch[3:0]),.EN(switch[16]), .EN_L(switch[15]), .EN2_L(switch[14]));
//	encode8to3_prio(.y(red[2:0]),.x(switch[7:0]),.z(green[0]),.EN(1));
//	multiplexer2to1(.f(red[0]),.x(switch[0:1]),.S(switch[2]));
//	multiplexer4to1(.f(red[1]),.x(switch[3:7]),.S(switch[8:9]));
// pop_machine(.clk(clk), .n(switch[0]), .d(switch[1]), .q(switch[2]),.reset(switch[4]),.out(red[0]), .n_change(red[1]),.d_change(red[2]),.s(my_s),.inReg(my_inReg));
// pop_machineSam(.reset(switch[4]), .clk(clk), .n(switch[0]), .d(switch[1]), .q(switch[2]), .out(red[0]), .n_change(red[1]), .d_change(red[2]),.s(my_s),.inReg(my_inReg));

//	SevenSegmentDisplayDecoder(.ssOut_L(Hex0[6:0]), .nIn(switch[3:0]));
//	SevenSegmentDisplayDecoder(.ssOut_L(Hex1[6:0]), .nIn(switch[7:4]));
//	SevenSegmentDisplayDecoder(.ssOut_L(Hex2[6:0]), .nIn(switch[11:8]));
//	SevenSegmentDisplayDecoder(.ssOut_L(Hex3[6:0]), .nIn(switch[15:12]));



