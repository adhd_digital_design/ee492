module decimalOnly(
	input [4:0] twosCompBin4b,
	output reg [4:0] onesLED,
	output reg [4:0] tensLED,
	output reg [4:0] signLED
);

	reg [3:0]magnitude;
	always @(twosCompBin4b)
	begin
	// Negative Values
		if(twosCompBin4b[4]) //twos comp number
		begin
			signLED = 5'b1_0001; // '-'
			magnitude = ~twosCompBin4b +1;  //find absolute value
			if((magnitude[3:0]-9)>0)
				  begin
					signLED = 5'b1_0000;
					tensLED = 5'b0_0001; // '1'
					onesLED = magnitude[3:0]-10; //'remainder'
				  end
			  else //no tens digit
				  begin
					signLED = 5'b1_0000;// ' '
					tensLED = 5'b1_0000; //' '
					onesLED = magnitude[3:0]; //'number'
				  end
		end
	//Positive Values
		else   // positive value
		begin
			if((twosCompBin4b[3:0]-9)>0)
			  begin
				signLED = 5'b1_0000;
				tensLED = 5'b0_0001; // '1'
				onesLED = twosCompBin4b[3:0]-10; //'remainder'
			  end
		  else //no tens digit
			  begin
				signLED = 5'b1_0000; //' '
				tensLED = 5'b1_0000; //' '
				onesLED = twosCompBin4b[3:0]; //'number'
			  end
		end
	end	

endmodule