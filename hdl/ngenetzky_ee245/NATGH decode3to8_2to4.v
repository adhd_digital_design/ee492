module decode2to4(
	input EN,
	input [1:0]x,
	output reg [3:0]y,
	output reg [3:0]y_L
	);
	
	always @(x,EN)
		begin
			case ({EN, x})
					3'b100: y= 4'b0001;// this takes []
					3'b101: y= 4'b0010;
					3'b110: y= 4'b0100;
					3'b111: y= 4'b1000;
				default: y= 4'b0000; // if EN is 0 then make no one hot.
			endcase
			y_L = ~y;
		end
	 // pg352 
endmodule

module decode3to8_L(y_L, y, x,EN, EN_L, EN2_L);
	input EN, EN_L, EN2_L;
	input [2:0]x;
	output [7:0]y_L; //active low outputs.
	output [7:0]y;
	
	wire [3:0]M;
	wire ONx2;
	wire NOTx2;
	wire EN_all;
	
	assign EN_all = EN & !EN_L & !EN2_L;
	assign NOTx2 = !x[2] & EN_all;
	assign ONx2 = x[2] & EN_all;
				decode2to4 (.x(x[1:0]),.y_L(y_L[7:4]),.EN(ONx2));// MSB is 1 so 4<OneHot<8
				decode2to4 (.x(x[1:0]),.y_L(y_L[3:0]),.EN(NOTx2));// MSB is 0 so 0<OneHot<3
	assign y= ~y_L; //makes it active low.
endmodule