module Stepper(
    input in, clk,
    output out, waiting,
    output reg latch = 0, debounceLoad = 0
    //,output reg clkLED
);
    parameter ticksToWait = 32'h02FAF080; // 50 million clock cycles
    always @(posedge(clk)) begin
        if (in && !waiting) begin
					latch = 1;
               debounceLoad = 1;
        end
        if (!in && !waiting) begin
					latch = 0;
               debounceLoad = 0;
        end
     end
     assign out = latch;

     // >>> UNCOMMENT IF YOU WANT TO TEST THE HARDWARE CLOCK
     //reg [63:0] clkCount = 0;
     //always @(posedge(clk)) begin
     //    clkCount = clkCount + 1;
     //    if(clkCount % 50000000 == 0)
     //       clkLED = !clkLED;
     //end


Debouncer Debouncer_my(.clk(clk),
                    .en(latch),
                    .load(debounceLoad),
                    .ticksToWait(ticksToWait),
                    .waiting(waiting)
						  );
endmodule


module Debouncer_my(
    input clk, en, load,
    input [32:0] ticksToWait,
    output reg waiting
);
    reg [32:0] countdown = 0; // initialize to zero

    always @(posedge(clk)) begin
        if (load && countdown == 0) begin
            countdown = ticksToWait;
            waiting = 1;
        end
        else if (en) begin
            countdown = countdown - 1;
            // waiting = countdown > 0 ?; // <<< HAVEN'T TESTED THIS BUT I'M GUESSING THIS WORKS, TOO
            waiting = countdown > 0 ? 1 : 0;
        end
          else if (!en)
              waiting = 0;
    end
endmodule
