/*
module SamProcessor(
    input              clk,                    // HW clock
                       Ain, Gin, Gout, Extern, // from Control circuit
    input       [6:0]  select,                 // from Control circuit
    input       [15:0] data,                   // from FPGA switches
    output wire [15:0] bus                     // will be declared internally later (same as B)
);
    wire [15:0] A_out, ALU_out, G_out;

    TriState Data_TS    (.en(Extern),
                         .in(data),
                         .out(bus)
                         );
    Register A          (.clk(clk),
                         .en(Ain),
                         .in(bus),
                         .out(A_out)
                         );
    ALU      alu        (.select(select),
                         .A(A_out),
                         .B(bus),
                         .G(ALU_out)
                         );
    Register G          (.clk(clk),
                         .en(Gin),
                         .in(ALU_out),
                         .out(G_out)
                         );
    TriState G_TS       (.en(Gout),
                         .in(G_out),
                         .out(bus)
                         );
endmodule

// inspired by figure 7.66, pg 447
module Register(
    input                clk, en,
    input      [15:0] in,
    output reg [15:0] out
);
    always @(posedge clk)
        if (en)
            out <= in;
endmodule

// inspired by figure 7.67, pg 447
module TriState(
    input en,
    input  [15:0] in,
    output [15:0] out
);
    assign out = en ? in : 'bz;
endmodule

// my own creation
module ALU(
    output reg [15:0] G,
    input wire [15:0] A,
    input wire [15:0] B,
    input wire [6:0] select
);
    always @(select)
    case (select)
        // load: load from 'Data' into register XXX
        7'b0000000: ;
        // move: move from register XXX to YYY
        7'b0000001: ;
        // add
        7'b1000010: G = A + B;
        // subtract
        7'b1000011: G = A - B;
        // OR
        7'b1000100: G = A | B;
        // AND
        7'b1000101: G = A & B;
        // bitwise complement
        7'b1000110: G = ~B;
        // two's complement
        7'b1000111: G = ~B + 1;
    endcase
endmodule
*/