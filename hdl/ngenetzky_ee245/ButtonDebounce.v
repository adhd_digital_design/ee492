module ButtonDebounce(
    input in, clk,
    output out, waiting,
    output reg latch = 0, debounceLoad = 0
    //,output reg clkLED
);
    parameter ticksToWait = 32'h00989_680;	//10 million clock cycles
									   //017D_7840;	//25 million clock cycles
									   //02FA_F080; // 50 million clock cycles 
    always @(posedge(clk)) begin
        if (in && !waiting) begin
					latch = 1;
               debounceLoad = 1;
        end
		  
		  if (waiting) // once we start waiting, we set debounceLoad to 0 -- this happens every time we debounce
				debounceLoad = 0;
        
		  if (!in && !waiting)
				 latch = 0;
		  //end
     end
     assign out = latch;

     // >>> UNCOMMENT IF YOU WANT TO TEST THE HARDWARE CLOCK
     //reg [63:0] clkCount = 0;
     //always @(posedge(clk)) begin
     //    clkCount = clkCount + 1;
     //    if(clkCount % 50000000 == 0)
     //       clkLED = !clkLED;
     //end


DebounceCounter Debouncer(.clk(clk),
                          .en(latch),
                          .load(debounceLoad),
                          .ticksToWait(ticksToWait),
                          .waiting(waiting)
			  			       );
endmodule


module DebounceCounter(
    input clk, en, load,
    input [32:0] ticksToWait,
    output reg waiting
);
    reg [32:0] countdown; //= 0; // initialize to zero

    always @(posedge(clk)) begin
        if (en) begin
		      if (load/* && countdown == 0*/) begin
               countdown = ticksToWait;
               waiting = 1;
            end
				else if (countdown > 0)
					countdown = countdown - 1;
				else
					waiting = 0;
        end
          else if (!en)
              waiting = 0;
    end
endmodule
