//module Master(clk,in1,in2,in3,in4,out1,out2,out3,out4,out5,out6, out7);
module MasterVerilog( 
	input clk,
	input [0:9] switch,
	output [0:7] green,
	output [0:17] red,
	input [0:3] pushBut
);
	wire [3:0] my_s;
	wire [1:2] my_x; // REMOVE WHEN FINISHED
	wire [3:0] my_dS;

//	buf(green[0], pushBut[0]);
//	buf(green[1], pushBut[1]);
//	buf(green[2], pushBut[2]);
//	buf(green[3], pushBut[3]);
	
	buf(green[4], my_s[0]); // S0
	buf(green[5], my_s[1]); // S1
	buf(green[6], my_s[2]); // S2
	buf(green[7], my_s[3]); // S3
	
//		.out(red[0]),
//	.n_change(red[2]),
//	.d_change(red[3]),
	buf(red[9], 1);
	buf(red[10], my_x[2]); // REMOVE WHEN FINISHED
	buf(red[11], my_x[1]); // REMOVE WHEN FINISHED
	//buf(red[6], switch[0]);
	//buf(red[7], switch[1]);
	//buf(red[8], switch[2]);
	//buf(red[9], switch[4]);
	buf(red[12], 1);
	buf(red[13], my_dS[0]);
	buf(red[14], my_dS[1]);
	buf(red[15], my_dS[2]);
	buf(red[16], my_dS[3]);
	buf(red[17], 1);

pop_machine U1(
	.reset(switch[4]),
	.clk(clk), 
	.n(switch[0]),
	.d(switch[1]),
	.q(switch[2]),
	.out(red[0]),
	.n_change(red[2]),
	.d_change(red[3]),
	.s(my_s),
	.x(my_x),
	.dS(my_dS)
);

	//wire [1:2] my_x;	
	//buf(0, my_x[1]); buf(0, my_x[2]);
	//FSMlab07(.n(in1),.d(in2),.q(in3),.clk(clk),.pop(out1),.s0(out2), .s1(out3), .s2(out4));
 // 1-D, 2-D_MS, 3-T
//Flp_D(.d(), .clk(clk), .q(out1), .NOTq());
//Flp_MS_D MS_D(.d(), .clk(clk), .q(out2), .NOTq());
//Flp_T Toggle(.t(in3), .clk(clk), .q(out3), .NOTq());
//Flp_JK(.J(in1),.K(in2),.clk(clk), .q(out4), .NOTQ());
//BinCounter_Moore(.CNT(in4), .clk(clk), .Q1(out6), .Q0(out5), .OUT(out7)); // binary number of form 'Q1''Q2'
endmodule
//module pop_machine(
//  input reset, clk, n, d, q,
//  output out, n_change, d_change
//`ifdef TEST
//  ,output [3:0] s,
//  output [1:2] x
//`endif
//);
module Flp_D(
	input d, clk,
	output q, NOTq
);
	wire NOTd, qloop, NOTqloop;
	wire clkD, clkNOTD;

	not(NOTd, d);
	nand(clkD,d,clk);
	nand(clkNOTD, NOTd,clk);

	nand(qloop, clkD, NOTqloop);
	nand(NOTqloop, clkNOTD, qloop);

	and(q, qloop, 1);
	and(NOTq, NOTqloop, 1);

endmodule
// d_flipflop    in Sams
module Flp_MS_D(
	input d, clk,
	output q, NOTq
);
	wire qNextState, NOTclk, NOTnotclk;
	not(NOTclk,clk);
	not(NOTnotclk,NOTclk);
	Flp_D Master(.d(d), .clk(NOTclk), .q(qNextState), .NOTq());
	Flp_D Slave(.d(qNextState), .clk(NOTnotclk), .q(q), .NOTq(NOTq));
	
endmodule
 // encoder4to2 in Sams
module prioEncoder4to2(x0,x1,x2,x3,y1,y0);
	// x0 is MSB with x3 LSB
	input x0,x1,x2,x3;
	output y0,y1;
	wire NOTx3, NOTx2,NOTx1, Px2,Px1;
	not(NOTx3,x3); not(NOTx2,x2); not(NOTx1,x1);
	and(Px2, NOTx3,x2);
	and(Px1, NOTx3, NOTx2, x1);
	or(y1, x3, Px2);
	or(y0, x3, Px1);
endmodule  //PrioEncoder ends
//
//module Flp_T(t, clk, q, NOTq);
//input t, clk;
//output q, NOTq;
//wire differ, Q_NOTt, t_NOTQ, Qstate, NOTQstate, NOTt;
//not(NOTt, t);
//
//and(Q_NOTt, NOTt, Qstate);
//and(t_NOTQ, NOTQstate, t);
//or notSame(differ, Q_NOTt, t_NOTQ);
//
//Flp_D U1(.d(differ), .clk(clk), .q(Qstate), .NOTq(NOTQstate));
//assign q = Qstate;
//assign NOTq = NOTQstate;
//
//endmodule //end Flp_T
//
//module Flp_JK(J,K,clk, q, NOTQ);
//input J,K,clk;
//output q,NOTQ;
//wire Qstate, NOTQstate, J_NOTQ, Q_NOTK, T, NOTK, differ;
//not(NOTK,K);
//and(Q_NOTK, Qstate, NOTK);
//and(J_NOTQ, J, NOTQstate);
//
//or different(differ, Q_NOTK, J_NOTQ);
//Flp_MS_D EdgeTrig(.d(differ), .clk(clk), .q(Qstate), .NOTq(NOTQstate));
//assign q = Qstate;
//assign NOTQ = NOTQstate;
//endmodule
//

//
//
//
