//module decode2to4(y,x,EN);
//	input EN;
//	input [1:0]x;
//	output reg [0:3]y;
//	
//	always @(x,EN)
//		case ({x, EN})
//			3'b100: y= 4'b1000;// this takes []
//			3'b101: y= 4'b0100;
//			3'b110: y= 4'b0010;
//			3'b111: y= 4'b0001;
//		default: y= 4'b0000; // if EN is 0 then make no one hot.
//		endcase
//	 // pg352 
//endmodule
//
//module decode3to8_L(y_L, x,EN, EN_L, EN2_L);
//	input EN, EN_L, EN2_L;
//	input [2:0]x;
//	output [0:7]y_L; //active low outputs.
//	wire [0:3]M;
//	wire NOTx2;
//	wire EN_all
//	assign NOTx2 = !x[2];
//	always @(x, EN)
//		begin
//			if(x[2])
//				decode2to4 MSB1(.x(x[1:0]),.y(y_L[4:8]),.EN(1);// MSB is 1 so 4<OneHot<8
//			else 
//				decode2to4 MSB0(.x(x[1:0]),.y(y_L[0:3]),.EN(1);// MSB is 1 so 0<OneHot<3
//		end
//	assign y_L= ~y_L; //makes it active low.
//endmodule

//module encode8to2_prio(y,x,z, EN);
//	input EN;
//	input [8:0]x;
//	output reg [2:0]y;
//	output reg z;
//	always @(x)
//		begin
//			z=1;
//			casex(x)
//				8'b1xxx_xxxx: y =7;
//				8'b01xx_xxxx: y =6;
//				8'b001x_xxxx: y =5;
//				8'b0001_xxxx: y =4;
//				8'b0000_1xxx: y =3;
//				8'b0000_01xx: y =2;
//				8'b0000_001x: y =1;
//				8'b0000_0001: y =0;
//			default:
//					begin
//						z=0;
//						y=3'bx;
//					end
//			endcase
//		end
//endmodule 

//module multiplexer2to1(f,x,S);
//	input S; 
//	input[0:1]x; 
//	output reg f;
//	always @(x[0],x[1],S)
//		f = S ? x[1] : x[0];
//endmodule
//
//module multiplexer4to1(f,x,S);
//	input [0:3]x;
//	output [0:1]S;
//	output reg f;
//	
//	always @(x,S)
//		case (S)
//			0:f=x[0];
//			1:f=x[1];
//			2:f=x[2];
//			3:f=x[3];
//		endcase
//endmodule 
