//shift register
module shiftr(	//pg 448 referenced from book.
input Reset_L, in, Clock,
output reg [1:m] Q);
integer k;
parameter m=4;
always @(negedge Reset_L, posedge Clock)
	if(!Reset_L)
		Q<=0;
	else
	begin
		for(k=m; k>1; k=k-1) //de-increment until 1
			Q[k]<=Q[k-1];
	Q[1]<=in;
	end
endmodule 