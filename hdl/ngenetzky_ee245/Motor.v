module Motor(
    input reset, en, clkwise,
    output reg [1:4] Q	//why is it 1:4????
);
    parameter   S1 = 4'b1010,
                S2 = 4'b1001,
                S3 = 4'b0101,
                S4 = 4'b0110;

    always @(posedge(reset), posedge(en))
        if (reset)
            Q = S1; // put Q into state 1
        else
            case (Q)
                S1: if (clkwise) Q = S2; else Q = S4;
                S2: if (clkwise) Q = S3; else Q = S1;
                S3: if (clkwise) Q = S4; else Q = S2;
                S4: if (clkwise) Q = S1; else Q = S3;
            endcase

endmodule
