`define TEST

module pop_machineSam(
    input reset, clk, n, d, q,
    output reg out, n_change, d_change,
	 output reg inReg,
`ifdef TEST
    output reg [3:0] s
`endif
);
`ifndef TEST
    reg [3:0] s;
`endif
    // parameters {{{
    parameter [3:0]
        have_0  = 4'b0000,
        have_5  = 4'b0001,
        have_10 = 4'b0010,
        have_15 = 4'b0011,
        have_20 = 4'b0100,
        have_25 = 4'b0101,
        have_30 = 4'b0110,
        have_35 = 4'b0111,
        have_40 = 4'b1000,
        have_45 = 4'b1001,
        have_50 = 4'b1010;
    // coin input
    parameter [2:0] // done by priority
        nothing = 3'b000,
        nickel  = 3'b001,
        dime    = 3'b010,
        quarter = 3'b100;
		  
    // change current state {{{
    always @(negedge clk)
begin
	inReg ={s, q, d, n};
	if(reset)
			begin
			  out = 1'b0;
			  n_change = 1'b0;
			  d_change = 1'b0;
			  s = 4'b0000;
			end
	else
			case({s, q, d, n})
            { have_0,  nothing }:    s = have_0;
            { have_0,  nickel }:     s = have_5;
            { have_0,  dime }:       s = have_10;
            { have_0,  quarter }:    s = have_25;

            { have_5,  nothing }:    s = have_5;
            { have_5,  nickel }:     s = have_10;
            { have_5,  dime }:       s = have_15;
            { have_5,  quarter }:    s = have_30;

            { have_10, nothing }:    s = have_10;
            { have_10, nickel }:     s = have_15;
            { have_10, dime }:       s = have_20;
            { have_10, quarter }:    s = have_35;

            { have_15, nothing }:    s = have_15;
            { have_15, nickel }:     s = have_20;
            { have_15, dime }:       s = have_25;
            { have_15, quarter }:    s = have_40;

            { have_20, nothing }:    s = have_20;
            { have_20, nickel }:     s = have_25;
            { have_20, dime }:       s = have_30;
            { have_20, quarter }:    s = have_45;

            { have_25, nothing }:    s = have_25;
            { have_25, nickel }:     s = have_30;
            { have_25, dime }:       s = have_35;
            { have_25, quarter }:    s = have_50;

            { have_30, nothing }:    s = have_30;
            { have_30, nickel }:     s = have_35;
            { have_30, dime }:       s = have_40;
            { have_30, quarter }:    s = have_50;

            { have_35, nothing }:    s = have_0;
            { have_35, nickel }:     s = have_5;
            { have_35, dime }:       s = have_10;
            { have_35, quarter }:    s = have_25;

            { have_40, nothing }:    s = have_0;
            { have_40, nickel }:     s = have_5;
            { have_40, dime }:       s = have_10;
            { have_40, quarter }:    s = have_25;

            { have_45, nothing }:    s = have_0;
            { have_45, nickel }:     s = have_5;
            { have_45, dime }:       s = have_10;
            { have_45, quarter }:    s = have_25;

            { have_50, nothing }:    s = have_0;
            { have_50, nickel }:     s = have_5;
            { have_50, dime }:       s = have_10;
            { have_50, quarter }:    s = have_25;
			endcase
			case(s)
            have_0: begin out = 0; n_change = 0; d_change = 0; end
            have_5: begin out = 0; n_change = 0; d_change = 0; end
            have_10: begin out = 0; n_change = 0; d_change = 0; end
            have_15: begin out = 0; n_change = 0; d_change = 0; end
            have_20: begin out = 0; n_change = 0; d_change = 0; end
            have_25: begin out = 0; n_change = 0; d_change = 0; end
            have_30: begin out = 0; n_change = 0; d_change = 0; end
            have_35: begin out = 1; n_change = 0; d_change = 0; end
            have_40: begin out = 1; n_change = 1; d_change = 0; end
            have_45: begin out = 1; n_change = 0; d_change = 1; end
            have_50: begin out = 1; n_change = 1; d_change = 1; end
        endcase
end

endmodule 