// Z indicates that some input is HI

module encode8to3_prio(
	input EN,
	input [7:0]x,
	output reg [2:0]y,
	output reg z
	);
	always @(x)
		begin
			z=1;
			casex(x)
				8'b1xxx_xxxx: y =7;
				8'b01xx_xxxx: y =6;
				8'b001x_xxxx: y =5;
				8'b0001_xxxx: y =4;
				8'b0000_1xxx: y =3;
				8'b0000_01xx: y =2;
				8'b0000_001x: y =1;
				8'b0000_0001: y =0;
			default:
					begin
						z=0;
						y=3'bx;
					end
			endcase
		end
endmodule