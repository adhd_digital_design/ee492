//
//Project2Processor(Data,Reset,w,Clock,F,Rx,Ry,Done,BusWires); //	referenced pg461 from Book
//wire ClearCounter= Reset |Done| (~w & ~Count [1] &~Count[0]);
//counter(.upCount(1), .reset(ClearCounter),.clk(clk), .count(StepCount));
//...
//
//I= register[1:2} // holds opcode
//always @(Count, I, Xreg, Y)
//begin
//	Extern=1’b0; Done=1’b0; Ain=1’b0; Gin=1’b0; Gout=1’b0; AddSub=1’b0; Rin=4’b0; Rout=1’b0; 
//	
//case(StepCount)
//	2’b00: //No signals asserted (time to load OP_code)
//	2’b01: 	//embeded case, T1
//	case(I) //  I is the OP_code
//2’b00: 	begin //Load
//		Extern=1b’1; Rin = Xreg; Done=1’b1
//		end
//	2’b01	begin //Move
//		Rout = Y; Rin =Xreg; Done =1b’1;
//		end
//	2’b10:	begin //add sub
//			Rout=Xreg; Ain=1’b1
//		end
//endcase
//	 2'b10 //processor step T2
//case(I)
//  2'b10:  begin //add
//   Rout = Y; Gin =1'b1;
//   end
//  2'b11: begin //Sub
//   Rout =Y; AddSub = 1'b1; Gin=1'b1
//   end
//  default:
// endcase
// 2'b11
// case(I)
//  2'b10,2'b11: begin
//   Gout=1'b1; Rin =Xreg; Done=1'b1;
//   end
//  default;
// endcase
// endcase //StepCount Case
//regn reg_0(BusWires, Rin[0],Clock, R0) // make another for each Rin[n] R’n’= Rin[1], R1
//
////alu
//always @(I,A,Buswires);
//begin
//	case (I) //case of opcode
//	2’b10: //add
//		Result = A+BusWires;
//	endcase
//end
//
//regn reg_G(Result, Gin, Clock, G)
//
////Load the proper registry onto the bus.
//assign Sel = {Rout, Gout, Extern}; //one hot
//always @(Sel,R0,R1,R2,R3,G,Data)
//begin 
//	case(Sel) //one hot
//	6’b10_00_00:
//		BusWires =R0;
//	6’b01_00_00:
//		BusWires =R1;
//	6’b00_10_00:
//		BusWires =R2;
//	6’b00_01_00:
//		BusWires =R3;
//	6’b00_00_10:
//		BusWires =G;
//	6’b00_00_01:	//Not in book
//		BusWires =R5;
//	default:
//		BusWires=Data;
//endcase
//endmodule
