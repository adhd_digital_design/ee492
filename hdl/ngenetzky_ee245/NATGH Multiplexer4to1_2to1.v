module multiplexer2to1(
input [0:1]x,S,
output reg f
);
	//input S; 
	//input[0:1]x; 
	//output reg f;
	always @(x[0],x[1],S)
		f = S ? x[1] : x[0];
endmodule

//module pop_machine(
//  input reset, clk, n, d, q,
//  output out, n_change, d_change
//`ifdef TEST
//  ,output [3:0] s,
//  output [1:2] x,
//  output [3:0] dS

// {S0,S1}
module multiplexer4to1(
input [3:0]x,
output reg f,
input [0:1]S
);
	always @(x,S)
		case ({S[1],S[0]})
			2'b00:f=x[0];
			2'b01:f=x[1];
			2'b10:f=x[2];
			2'b11:f=x[3];
		endcase
endmodule 

module multiplexer16to1(
input [15:0]x,
output reg f,
input [3:0]S
);
always @(x,S)
		case (S)
			4'b0000: f <= x[0];
			4'b0001: f <= x[1];	
			4'b0010: f <= x[2];		
			4'b0011: f <= x[3];
			
			4'b0100: f <= x[4];	
			4'b0101: f <= x[5];	
			4'b0110: f <= x[6];
			4'b0111: f <= x[7];
			
			4'b1000: f <= x[8];	
			4'b1001: f <= x[9];
			4'b1010: f <= x[10];	
			4'b1011: f <= x[11];
			
			4'b1100: f <= x[12];	
			4'b1101: f <= x[13];
			4'b1110: f <= x[14];	
			4'b1111: f <= x[15];
		default:
			f <= 'bz;
		endcase
endmodule 

