module trin(
input en,
input [15:0]in,
output [15:0]out
);
assign out = en ? in : 'bz;
endmodule