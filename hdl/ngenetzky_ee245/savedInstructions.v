/*
module savedInstructions(
	input en, load, clk,  usePreset,
	input wire [15:0] newOpcode,
	input wire [2:0] opLoc,
	output reg [15:0] functionReg
	
);
reg [7:0] regIn, regOut;
wire [15:0] IR_0,IR_1,IR_2,IR_3,IR_4,IR_5,IR_6,IR_7;
always @(posedge clk)
	if(en)begin
		if(((opLoc)!=0) & (!load)) 
			begin
				regOut[opLoc] = 1;		//output instruction @address on opLoc
			end
		else if (usePreset)
			begin			//Set preset instructions
			//			16'b-op_code_XXX_YYY_UUU
			case (opLoc)
				0 :	begin
							regIn[0]= 1;
							newOpcode = 16'b100_0010_000_000_000;
						end
				1 :	begin
							regIn[1]= 1;
							newOpcode = 16'b100_0010_100_011_000;
						end
				2 :	begin
							regIn[2]= 1;
							newOpcode = 16'b100_0010_010_001_000;
						end
				3 :	begin
							regIn[3]= 1;
							newOpcode = 16'b100_0010_011_010_000;
						end
				4 :	begin
							regIn[4]= 1;
							newOpcode = 16'b100_0010_100_011_000;
						end
				5 :	begin
							regIn[5]= 1;
							newOpcode = 16'b100_0010_101_100_000;
						end
				6 :	begin
							regIn[6]= 1;
							newOpcode = 16'b100_0010_110_101_000;
						end
				7 :	begin
							regIn[7]= 1;
							newOpcode = 16'b100_0010_111_110_000;
						end
			//			performs running total on the registers 
			endcase
			end
		else if((opLoc !=0)& (load))
			regIn[opLoc] = 1;			//input instruction into reg @address
		else if(load)
			regIn[currentIndex] = 1;			//other wise input instruction into reg0
	end

Register16Bit Ireg0(.Load(opLoc[0]),  // {{{	Instruction 0
		 .In(newOpcode),
		 .Out(functionReg),
		 .Clock(clk)
	);
Register16Bit Ireg1(.Load(opLoc[1]),  // {{{	Instruction 1
		 .In(newOpcode),
		 .Out(functionReg),
		 .Clock(clk)
	);
Register16Bit Ireg2(.Load(opLoc[2]),  // {{{	Instruction 2
		 .In(newOpcode),
		 .Out(functionReg),
		 .Clock(clk)
	);
Register16Bit Ireg3(.Load(opLoc[3]),  // {{{	Instruction 3
		 .In(newOpcode),
		 .Out(functionReg),
		 .Clock(clk)
	);
Register16Bit Ireg4(.Load(opLoc[4]),  // {{{	Instruction 4
		 .In(newOpcode),
		 .Out(functionReg),
		 .Clock(clk)
	);
Register16Bit Ireg5(.Load(opLoc[5]),  // {{{	Instruction 5
		 .In(newOpcode),
		 .Out(functionReg),
		 .Clock(clk)
	);
Register16Bit Ireg6(.Load(opLoc[6]),  // {{{	Instruction 6
		 .In(newOpcode),
		 .Out(functionReg),
		 .Clock(clk)
	);
Register16Bit Ireg7(.Load(opLoc[7]),  // {{{	Instruction 7
		 .In(newOpcode),
		 .Out(functionReg),
		 .Clock(clk)
	);

TriStateBuffer16Bit out0(.Enable(regOut[0]),.In(IR_0[15:0]), // {{{
    .Out(functionReg)
);
TriStateBuffer16Bit out1(.Enable(regOut[1]),.In(IR_1[15:0]), // {{{
    .Out(functionReg)
);
TriStateBuffer16Bit out2(.Enable(regOut[2]),.In(IR_2[15:0]), // {{{
    .Out(functionReg)
);
TriStateBuffer16Bit out3(.Enable(regOut[3]),.In(IR_3[15:0]), // {{{
    .Out(functionReg)
);
TriStateBuffer16Bit out4(.Enable(regOut[4]),.In(IR_4[15:0]), // {{{
    .Out(functionReg)
);
TriStateBuffer16Bit out5(.Enable(regOut[5]),.In(IR_5[15:0]), // {{{
    .Out(functionReg)
);
TriStateBuffer16Bit out6(.Enable(regOut[6]),.In(IR_6[15:0]), // {{{
    .Out(functionReg)
);
TriStateBuffer16Bit out7(.Enable(regOut[7]),.In(IR_7[15:0]), // {{{
    .Out(functionReg)
);

endmodule
*/
	
	