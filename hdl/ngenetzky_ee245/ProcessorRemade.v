`define FPGA

// Figure 7.78
module ProcessorRemade (
    input       [15:0] ExternalData,
    input       [0:0]  ProcessorReset, ProcessorEnable, ClockButton, ViewRegister,
`ifdef FPGA
                       DebounceClock,
`endif
	 
    output wire [15:0] HexDisplay, // contents of bus or register
    output      [0:0]  OperationFinished,
	 output wire [3:0]  TimeStep
);
	 wire [0:0] Clock;
`ifndef FPGA
	 assign  Clock = ClockButton;
`endif
    reg  [15:0]  EnableRegIn, EnableRegOut;
		//@Nate I think this needs to be a while. The enable in and enable out are staying high. Is what I THINK is happening.
    wire [0:0]   Clear, ExternalDataEnable, LoadRegisterA, LoadRegisterG,
                 OutputRegisterG, LoadFunctionRegister;
    wire [1:0]   Count;
    //wire [3:0]   TimeStep;
    wire [6:0]   ALUSelect;
    wire [7:0]   Instruction, Xreg, Y;
    wire [15:0]  ALUResult, Bus, RegA_Output, RegG_Output, FunctionRegisterInput, FunctionRegisterOutput,
                 Reg0_Output, Reg1_Output, Reg2_Output, Reg3_Output,
                 Reg4_Output, Reg5_Output, Reg6_Output, Reg7_Output;

    // Debouncer {{{
`ifdef FPGA
    wire [0:0] Waiting;
    reg  [0:0] Latch = 0, DebounceLoad = 0;
    parameter TicksToWait = 32'hFFFFFFFF; // 50 million clock cycles
    always @(posedge(DebounceClock)) begin		// DebounceClock =50MHz
        if (ClockButton && !Waiting) begin
            Latch = 1;
            DebounceLoad = 1;
        end
        if (!ClockButton && !Waiting) begin
            Latch = 0;
            DebounceLoad = 0;
        end
		  if (ClockButton && Waiting)		//hit the clock button while debouncer is downcounting
				DebounceLoad = 0 ;
    end
    assign Clock = Latch;
    Debouncer ClockDebouncer(.clk(ClockButton),
                             .en(Latch),
                             .load(DebounceLoad),
                             .ticksToWait(TicksToWait),
                             .waiting(Waiting));
`endif
    // }}}

    // TODO: Add an actual Hex Decoder here
    assign HexDisplay = ViewRegister == 1
                        ? ExternalData[2:0] == 0
								? Reg0_Output
								: ExternalData[2:0] == 1
                        ? Reg1_Output
                        : ExternalData[2:0] == 2
                        ? Reg2_Output
                        : ExternalData[2:0] == 3
                        ? Reg3_Output
                        : ExternalData[2:0] == 4
                        ? Reg4_Output
                        : ExternalData[2:0] == 5
                        ? Reg5_Output
                        : ExternalData[2:0] == 6
                        ? Reg6_Output
                        : /* FunctionRegisterOutput[2:0] == 7, so */ Reg7_Output
                        : Bus;

    UpCounter TimeStepCounter (.Clear(Clear),
                               .Clock(Clock),
                               .Out(Count)
                              );
    Decoder2to4 TimeStepDecoder (.In(Count),
                                 .Enable(1'b1),
                                 .Out(TimeStep)
                                );
    assign Clear = ProcessorReset     // Reset is applied to processor
                 | OperationFinished  // Done with operation
                 | (~ProcessorEnable & TimeStep[0]); // Processor is disabled and
                                                     // no operation is being executed

    assign FunctionRegisterInput = Bus;
				
			//@Nate this is redudant. why not just have the function register as an INPUT!
    assign LoadFunctionRegister  = ProcessorEnable & TimeStep[0];

    Register16Bit FunctionRegister (.In(FunctionRegisterInput),
                                    .Load(LoadFunctionRegister),
                                    .Clock(Clock),
                                    .Out(FunctionRegisterOutput)
                                   );
    InstDecoder InstructionDecoder (.In(FunctionRegisterOutput[15:9]),  // Opcode
                                    .Enable(1'b1),                      // Always enabled
                                    .Out(Instruction)                   // Instruction
                                   );
    Decoder3to8 OperandAddressXDecoder (.In(FunctionRegisterOutput[8:6]),   // Address of operand X
                                        .Enable(1'b1),                      // Always enabled
                                        .Out(Xreg)  // address of register to relay to X
                                       );
    Decoder3to8 OperandAddressYDecoder (.In(FunctionRegisterOutput[5:3]),   // Address of operand Y
                                        .Enable(1'b1),                      // Always enabled
                                        .Out(Y) // address of register to relay to wire B
                                       );
    assign ExternalDataEnable = (Instruction[0] & TimeStep[1]) | (TimeStep[0]);
    assign OperationFinished = // { 0: Load } and { 1: Move } only take one time step
                               (TimeStep[1] & (Instruction[0] | Instruction[1]))
                               // { 2: Add }, { 3: Subtract }, { 4: OR }, { 5: AND },
                               // { 6: 1's Comp }, and { 7: 2's Comp } take three time steps
                             | (TimeStep[3] & (Instruction[2] | Instruction[3] | Instruction[4] |
                                 Instruction[5] | Instruction[6] | Instruction[7])
                                  );
    assign LoadRegisterA = // { 2: Add }, { 3: Subtract }, { 4: OR }, { 5: AND }, { 6: 1's Comp }, and
                           // { 7: 2's Comp } require reg A to be loaded from bus on step 1
                           ((Instruction[2] | Instruction[3] | Instruction[4] |
                             Instruction[5] | Instruction[6] | Instruction[7])
                              & TimeStep[1]);
    assign LoadRegisterG = // { 2: Add }, { 3: Subtract }, { 4: OR }, { 5: AND }, { 6: 1's Comp }, and
                           // { 7: 2's Comp } require reg G to be loaded from ALU on step 2
                           ((Instruction[2] | Instruction[3] | Instruction[4] |
                             Instruction[5] | Instruction[6] | Instruction[7])
                              & TimeStep[2]);
    assign OutputRegisterG = // { 2: Add }, { 3: Subtract }, { 4: OR }, { 5: AND }, { 6: 1's Comp }, and
                             // { 7: 2's Comp } require reg G to output to bus on step 3
                             ((Instruction[2] | Instruction[3] | Instruction[4] |
                               Instruction[5] | Instruction[6] | Instruction[7])
                                & TimeStep[3]);
    assign ALUSelect = FunctionRegisterOutput[15:9];

    // Register Control
    always @(Instruction, TimeStep, Xreg, Y)
        begin
            // Register 0 {{{
            EnableRegIn[0] = ((Instruction[0] | Instruction[1])
                                & TimeStep[1] & Xreg[0])
                           | ((Instruction[2] | Instruction[3] | Instruction[4] |
									    Instruction[5] | Instruction[6] | Instruction[7])
                                & (TimeStep[3] & Xreg[0]));
            EnableRegOut[0] = (Instruction[1] & TimeStep[1] & Y[0]) // Move Y to X
                            | ((Instruction[2] | Instruction[3] | Instruction[4] | Instruction[5] | Instruction[6] | Instruction[7])
                                 & ((TimeStep[1] & Xreg[0]) | (TimeStep[2] & Y[0])));
            // }}}
            // Register 1 {{{
            EnableRegIn[1] = ((Instruction[0] | Instruction[1])
                                & TimeStep[1] & Xreg[1])
                           | ((Instruction[2] | Instruction[3] | Instruction[4] |
                               Instruction[5] | Instruction[6] | Instruction[7])
                                & (TimeStep[3] & Xreg[1]));
            EnableRegOut[1] = (Instruction[1] & TimeStep[1] & Y[1])
                            | ((Instruction[2] | Instruction[3] | Instruction[4] |
                                Instruction[5] | Instruction[6] | Instruction[7])
                                 & ((TimeStep[1] & Xreg[1]) | (TimeStep[2] & Y[1])));
            // }}}
            // Register 2 {{{
            EnableRegIn[2] = ((Instruction[0] | Instruction[1])
                                & TimeStep[1] & Xreg[2])
                           | ((Instruction[2] | Instruction[3] | Instruction[4] |
                               Instruction[5] | Instruction[6] | Instruction[7])
                                & (TimeStep[3] & Xreg[2]));
            EnableRegOut[2] = (Instruction[1] & TimeStep[1] & Y[2])
                            | ((Instruction[2] | Instruction[3] | Instruction[4] |
                                Instruction[5] | Instruction[6] | Instruction[7])
                                 & ((TimeStep[1] & Xreg[2]) | (TimeStep[2] & Y[2])));
            // }}}
            // Register 3 {{{
            EnableRegIn[3] = ((Instruction[0] | Instruction[1])
                                & TimeStep[1] & Xreg[3])
                           | ((Instruction[2] | Instruction[3] | Instruction[4] |
                               Instruction[5] | Instruction[6] | Instruction[7])
                                & (TimeStep[3] & Xreg[3]));
            EnableRegOut[3] = (Instruction[1] & TimeStep[1] & Y[3])
                            | ((Instruction[2] | Instruction[3] | Instruction[4] |
                                Instruction[5] | Instruction[6] | Instruction[7])
                                 & ((TimeStep[1] & Xreg[3]) | (TimeStep[2] & Y[3])));
            // }}}
            // Register 4 {{{
            EnableRegIn[4] = ((Instruction[0] | Instruction[1])
                                & TimeStep[1] & Xreg[4])
                           | ((Instruction[2] | Instruction[3] | Instruction[4] |
                               Instruction[5] | Instruction[6] | Instruction[7])
                                & (TimeStep[3] & Xreg[4]));
            EnableRegOut[4] = (Instruction[1] & TimeStep[1] & Y[4])
                            | ((Instruction[2] | Instruction[3] | Instruction[4] |
                                Instruction[5] | Instruction[6] | Instruction[7])
                                 & ((TimeStep[1] & Xreg[4]) | (TimeStep[2] & Y[4])));
            // }}}
            // Register 5 {{{
            EnableRegIn[5] = ((Instruction[0] | Instruction[1])
                                & TimeStep[1] & Xreg[5])
                           | ((Instruction[2] | Instruction[3] | Instruction[4] |
                               Instruction[5] | Instruction[6] | Instruction[7])
                                & (TimeStep[3] & Xreg[5]));
            EnableRegOut[5] = (Instruction[1] & TimeStep[1] & Y[5])
                            | ((Instruction[2] | Instruction[3] | Instruction[4] |
                                Instruction[5] | Instruction[6] | Instruction[7])
                                 & ((TimeStep[1] & Xreg[5]) | (TimeStep[2] & Y[5])));
            // }}}
            // Register 6 {{{
            EnableRegIn[6] = ((Instruction[0] | Instruction[1])
                                & TimeStep[1] & Xreg[6])
                           | ((Instruction[2] | Instruction[3] | Instruction[4] |
                               Instruction[5] | Instruction[6] | Instruction[7])
                                & (TimeStep[3] & Xreg[6]));
            EnableRegOut[6] = (Instruction[1] & TimeStep[1] & Y[6])
                            | ((Instruction[2] | Instruction[3] | Instruction[4] |
                                Instruction[5] | Instruction[6] | Instruction[7])
                                 & ((TimeStep[1] & Xreg[6]) | (TimeStep[2] & Y[6])));
            // }}}
            // Register 7 {{{
            EnableRegIn[7] = ((Instruction[0] | Instruction[1])
                                & TimeStep[1] & Xreg[7])
                           | ((Instruction[2] | Instruction[3] | Instruction[4] |
                               Instruction[5] | Instruction[6] | Instruction[7])
                                & (TimeStep[3] & Xreg[7]));
            EnableRegOut[7] = (Instruction[1] & TimeStep[1] & Y[7])
                            | ((Instruction[2] | Instruction[3] | Instruction[4] |
                                Instruction[5] | Instruction[6] | Instruction[7])
                                 & ((TimeStep[1] & Xreg[7]) | (TimeStep[2] & Y[7])));
            // }}}
        end

    TriStateBuffer16Bit ExternalDataTSB (.In(ExternalData),
                                         .Enable(ExternalDataEnable),
                                         .Out(Bus)
                                        );
    // Registers {{{1
    // Operand Registers {{{2
    // Register 0 {{{3
    Register16Bit       Register0    (.In(Bus),
                                      .Load(EnableRegIn[0]),
                                      .Clock(Clock),
                                      .Out(Reg0_Output)
                                     );
    TriStateBuffer16Bit Register0TSB (.In(Reg0_Output),
                                      .Enable(EnableRegOut[0]),
                                      .Out(Bus)
                                     ); // }}}
    // Register 1 {{{3
    Register16Bit       Register1    (.In(Bus),
                                      .Load(EnableRegIn[1]),
                                      .Clock(Clock),
                                      .Out(Reg1_Output)
                                     );
    TriStateBuffer16Bit Register1TSB (.In(Reg1_Output),
                                      .Enable(EnableRegOut[1]),
                                      .Out(Bus)
                                     ); // }}}
    // Register 2 {{{3
    Register16Bit       Register2    (.In(Bus),
                                      .Load(EnableRegIn[2]),
                                      .Clock(Clock),
                                      .Out(Reg2_Output)
                                     );
    TriStateBuffer16Bit Register2TSB (.In(Reg2_Output),
                                      .Enable(EnableRegOut[2]),
                                      .Out(Bus)
                                     ); // }}}
    // Register 3 {{{3
    Register16Bit       Register3    (.In(Bus),
                                      .Load(EnableRegIn[3]),
                                      .Clock(Clock),
                                      .Out(Reg3_Output)
                                     );
    TriStateBuffer16Bit Register3TSB (.In(Reg3_Output),
                                      .Enable(EnableRegOut[3]),
                                      .Out(Bus)
                                     ); // }}}
    // Register 4 {{{3
    Register16Bit       Register4    (.In(Bus),
                                      .Load(EnableRegIn[4]),
                                      .Clock(Clock),
                                      .Out(Reg4_Output)
                                     );
    TriStateBuffer16Bit Register4TSB (.In(Reg4_Output),
                                      .Enable(EnableRegOut[4]),
                                      .Out(Bus)
                                     ); // }}}
    // Register 5 {{{3
    Register16Bit       Register5    (.In(Bus),
                                      .Load(EnableRegIn[5]),
                                      .Clock(Clock),
                                      .Out(Reg5_Output)
                                     );
    TriStateBuffer16Bit Register5TSB (.In(Reg5_Output),
                                      .Enable(EnableRegOut[5]),
                                      .Out(Bus)
                                     ); // }}}
    // Register 6 {{{3
    Register16Bit       Register6    (.In(Bus),
                                      .Load(EnableRegIn[6]),
                                      .Clock(Clock),
                                      .Out(Reg6_Output)
                                     );
    TriStateBuffer16Bit Register6TSB (.In(Reg6_Output),
                                      .Enable(EnableRegOut[6]),
                                      .Out(Bus)
                                     ); // }}}
    // Register 7 {{{3
    Register16Bit       Register7    (.In(Bus),
                                      .Load(EnableRegIn[7]),
                                      .Clock(Clock),
                                      .Out(Reg7_Output)
                                     );
    TriStateBuffer16Bit Register7TSB (.In(Reg7_Output),
                                      .Enable(EnableRegOut[7]),
                                      .Out(Bus)
                                     ); // }}}
    // }}}
    // ALU Registers {{{2
    // Register A {{{3
    Register16Bit       RegisterA    (.In(Bus),
                                      .Load(LoadRegisterA),
                                      .Clock(Clock),
                                      .Out(RegA_Output)
                                     );
    // No Tri-State Buffer for Register A }}}
    // Register G {{{3
    Register16Bit       RegisterG    (.In(ALUResult),
                                      .Load(LoadRegisterG),
                                      .Clock(Clock),
                                      .Out(RegG_Output)
                                     );
    TriStateBuffer16Bit RegisterGTSB (.In(RegG_Output),
                                      .Enable(OutputRegisterG),
                                      .Out(Bus)
                                     ); // }}}
    // }}}
    // }}}

    ALU alu (.select(ALUSelect),
             .A(RegA_Output),
             .B(Bus),
             .G(ALUResult)
            );

endmodule

// Modified Figure 6.35
module Decoder2to4 ( // {{{
    input [1:0] In,
    input Enable,
    output reg [3:0] Out
);
    always @(In, Enable)
        case ({Enable, In})
            3'b100:  Out = 4'b0001;
            3'b101:  Out = 4'b0010;
            3'b110:  Out = 4'b0100;
            3'b111:  Out = 4'b1000;
            default: Out = 4'b0000;
        endcase
endmodule // }}}

module Decoder3to8 ( // {{{
    input [2:0] In,
    input Enable,
    output reg [7:0] Out
);
    always @(In, Enable)
        case ({Enable, In})
            4'b1000:  Out = 8'b00000001;
            4'b1001:  Out = 8'b00000010;
            4'b1010:  Out = 8'b00000100;
            4'b1011:  Out = 8'b00001000;
            4'b1100:  Out = 8'b00010000;
            4'b1101:  Out = 8'b00100000;
            4'b1110:  Out = 8'b01000000;
            4'b1111:  Out = 8'b10000000;
            default:  Out = 8'b00000000;
        endcase
endmodule // }}}

// Used for convenience in rest of processor
module InstDecoder ( // {{{
    input [6:0] In,
    input Enable,
    output reg [7:0] Out
);
    always @(In, Enable)
        case ({Enable, In})
            8'b10000000: Out = 8'b00000001; // { 0: Load }
            8'b10000001: Out = 8'b00000010; // { 1: Move }
            8'b11000010: Out = 8'b00000100; // { 2: Add }
            8'b11000011: Out = 8'b00001000; // { 3: Subtract }
            8'b11000100: Out = 8'b00010000; // { 4: OR }
            8'b11000101: Out = 8'b00100000; // { 5: AND }
            8'b11000110: Out = 8'b01000000; // { 6: 1's Comp }
            8'b11000111: Out = 8'b10000000; // { 7: 2's Comp }
        endcase
endmodule

// Modified Figure 7.76
module UpCounter ( // {{{
    input Clear, Clock,
    output reg [1:0] Out
);
    always @(posedge Clock)
        if (Clear)
            Out <= 0;
        else
            Out <= Out + 1;
endmodule // }}}

// Modified Figure 7.66
module Register16Bit ( // {{{
    input [15:0] In,
    input Load, Clock,
    output reg [15:0] Out
);
    always @(posedge Clock)
        if (Load)
            Out <= In;
endmodule // }}}

// Modified Figure 7.67
module TriStateBuffer16Bit ( // {{{
    input [15:0] In,
    input Enable,
    output wire [15:0] Out
);
    assign Out = Enable ? In : 'bz;
endmodule // }}}

module ALU (
    output reg [15:0] G,
    input wire [15:0] A,
    input wire [15:0] B,
    input wire [6:0] select
);
    always @(select)
        case (select)
            // add
            7'b1000010: G = A + B;
            // subtract
            7'b1000011: G = A - B;
            // OR
            7'b1000100: G = A | B;
            // AND
            7'b1000101: G = A & B;
            // one's complement
            7'b1000110: G = ~B;
            // two's complement
            7'b1000111: G = ~B + 1;
        endcase
endmodule

`ifdef FPGA
module Debouncer( // {{{
    input clk, en, load, //load=debounceLoad
    input [32:0] ticksToWait,
    output reg waiting
);
    reg [32:0] countdown;

    always @(posedge(clk)) begin
        if (load) begin	//only happen once, right away
            countdown = ticksToWait;
            waiting = 1;
        end
        else if (en) begin
            countdown = countdown - 1;
            waiting = countdown > 0 ? 1 : 0;
        end
          else if (!en)
              waiting = 0;
    end
endmodule // }}}
`endif
