//Regn for making Q
module regn (R, Rin, Clock, Q); // reference: pg447 book
input [15:0] R;
input Rin, Clock;
output reg[15:0] Q;
always @(posedge Clock)
 if(Rin)
  Q<=R;
  else
	Q<='bz;
endmodule //regn

module Register(
    input                clk, en,
    input      [15:0] in,
    output reg [15:0] out
);
    always @(posedge clk)
        if (en)
            out <= in;
endmodule