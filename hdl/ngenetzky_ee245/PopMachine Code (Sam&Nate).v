
// 35-CENT POP MACHINE W/ CHANGE {{{1
// Implemented using D flip-flops
// {{{2 S0
// S0 = S3' * S0' * X1' * X2
//    + S3' * S1' * S0 * X2'
//    + S3' * S2' * S0 * X2'
//    + S2' * S0' * X2
//    + S3' * S2 * S1 * S0 * X2
//    + S3 * S2' * S1' * X2
//    + S3' * S1' * S0' * X2
// }}}
// {{{2 S1
// S1 = S3 * S2' * S0' * X1 * X2'
//    + S2' * S1' * X1 * X2'
//    + S3' * S1 * S0' * X1'
//    + S3' * S1' * S0 * X2
//    + S3' * S2' * S1 * X1' * X2'
//    + S3' * S1 * S0' * X2
//    + S3' * S2 * S0 * X1 * X2'
//    + S3' * S1' * X1 * X2'
// }}}
// {{{2 S2
// S2 = S2' * S0' * X1 * X2
//    + S2' * S1' * X1 * X2
//    + S3' * S2 * S0' * X1'
//    + S3' * S2 * S1' * X2'
//    + S3' * S2' * S1 * X1 * X2'
//    + S3' * S2' * S1 * S0 * X1' * X2
//    + S3' * S2 * S1' * X1'
//    + S3' * S2 * S1 * S0 * X1 * X2
// }}}
// {{{2 S3
// S3 = S3' * S2' * S1 * S0 * X1 * X2
//    + S3' * S2 * S1 * S0' * X1
//    + S3' * S2 * S1' * X1 * X2
// }}}
// {{{2 n_change
// n_change = S3' * S2 * S1 * S0' * X1
//          + S3' * S2 * S1' * S0 * X1 * X2
//          + S3' * S2' * S1 * S0 * X1 * X2
// }}}
// {{{2 d_change
// d_change = S3' * S2 * S0' * X1 * X2
//          + S3' * S2 * S1' * X1 * X2
// }}}
// {{{2 out
// out = S3' * S2 * S0' * X1 * X2
//     + S3' * S2 * S1' * S0 * X1
//     + S3' * S2 * S1 * S0' * X2
//     + S3' * S2' * S1 * X1 * X2
//     + S3' * S2 * S1 * S0' * X1
// }}}
// }}}
module pop_machine(
	input clk, n, d, q, reset,
	output reg out, n_change, d_change,
	output reg [3:0] s,
	output [1:2] x,
	output reg [6:0] inReg
);
	//	reg coinInput;

		parameter [3:0]
				got_0  = 4'b0000,
				got_5  = 4'b0001,
				got_10 = 4'b0010,
				got_15 = 4'b0011,
				got_20 = 4'b0100,
				got_25 = 4'b0101,
				got_30 = 4'b0110,
				got_35 = 4'b0111,
				got_40 = 4'b1000,
				got_45 = 4'b1001,
				got_50 = 4'b1010; 
		parameter [2:0]
				nickle 	=3'b001,
				dime 		=3'b010,
				quarter 	=3'b100;
//always @(posedge clk)
//	s<=s;

always @(posedge reset)
	if (reset)
		s = 4'b0000;
//always @(clk, q, d, n)
//		coinInput={q,d,n};

//assign inReg = {s,coinInput};
always @(posedge clk)
begin
	inReg = {s,q,d,n};//{s,coinInput};
		case({s,q,d,n})
			{got_0,nickle}:		s= got_5;
			{got_0,dime}:			s= got_10;
			{got_0,quarter}:		s= got_25;
			
			{got_5,nickle}:		s= got_10;
			{got_5,dime}:			s= got_15;
			{got_5,quarter}:		s= got_30;
			
			{got_10,nickle}:		s= got_15;
			{got_10,dime}:			s= got_20;
			{got_10,quarter}:		s= got_35;
			
			{got_15,nickle}:		s= got_20;
			{got_15,dime}:			s= got_30;
			{got_15,quarter}:		s= got_40;
			
			{got_20,nickle}:		s= got_25;
			{got_20,dime}:			s= got_20;
			{got_20,quarter}:		s= got_45;
			
			{got_25,nickle}:		s= got_30;
			{got_25,dime}:			s= got_35;
			{got_25,quarter}:		s= got_50;
			
			{got_30,nickle}:		s= got_35;
			{got_30,dime}:			s= got_40;
			{got_30,quarter}:		s= got_50;
			
			{got_35,nickle}:		s= got_5;
			{got_35,dime}:			s= got_10;
			{got_35,quarter}:		s= got_25;
			
			{got_40,nickle}:		s= got_5;
			{got_40,dime}:			s= got_10;
			{got_40,quarter}:		s= got_25;
			
			{got_45,nickle}:		s= got_5;
			{got_45,dime}:			s= got_10;
			{got_45,quarter}:		s= got_25;
			
			{got_50,nickle}:		s= got_5;
			{got_50,dime}:			s= got_10;
			{got_50,quarter}:		s= got_25;
			default:
				s= got_35;
		endcase
		
		case(s)
			got_0: begin out = 0; n_change = 0; d_change = 0; end
			got_5: begin out = 0; n_change = 0; d_change = 0; end
			got_10: begin out = 0; n_change = 0; d_change = 0; end
			got_15: begin out = 0; n_change = 0; d_change = 0; end
			got_20: begin out = 0; n_change = 0; d_change = 0; end
			got_25: begin out = 0; n_change = 0; d_change = 0; end
			got_30: begin out = 0; n_change = 0; d_change = 0; end
			got_35: begin out = 1; n_change = 0; d_change = 0; end
			got_40: begin out = 1; n_change = 1; d_change = 0; end
			got_45: begin out = 1; n_change = 0; d_change = 1; end
			got_50: begin out = 1; n_change = 1; d_change = 1; end
		default: 
			begin out = 0; n_change = 1; d_change = 1; end
		endcase
end

endmodule



/*
always@(x)
case (s[3:0])
	4'b0000:
		if(x==001)			s = 0001;
		else if(x==010)		s = 0010;
		else if(x==100)		s = 0011;
endcase;
*/


/*
module pop_machine(
  input reset, clk, n, d, q,
  output out, n_change, d_change
`ifdef TEST
  ,output [3:0] s,
  output [1:2] x,
  output [3:0] dS
`endif
);
  // states (wires for flip-flop outputs)
  wire [3:0] s_not;
  wire [1:2] x_not;
`ifndef TEST
  wire [3:0] s;
  wire [1:2] x;
`endif
  // encoding for nickel, dime, and quarter inputs
  //prioEncoder4to2 e42(.x0(q), .x1(d), .x2(n), .x3(0), .y1(x[1]), .y0(x[2]) );
  encoder4to2 e42(.w3(q), .w2(d), .w1(n), .w0(0), .y1(x[1]), .y0(x[2]));
  // {{{1 complements
  wire n_not, d_not, q_not;
  not(n_not, n);
  not(d_not, d);
  not(q_not, q);
  not(s_not[0], s[0]);
  not(s_not[1], s[1]);
  not(s_not[2], s[2]);
  not(s_not[3], s[3]);
  not(x_not[1], x[1]);
  not(x_not[2], x[2]);
  // }}}
  // {{{1 terms for flip-flop inputs
  wire [6:0] d0_terms; // S0*
  wire [7:0] d1_terms; // S1*
  wire [7:0] d2_terms; // S2*
  wire [2:0] d3_terms; // S3*
  
  wire [0:0] dn_terms; // n_change
  wire [1:0] dd_terms; // d_change
  wire [2:0] dy_terms; // out
    // {{{2 S0*
    and(d0_terms[0], s_not[3], s_not[0], x_not[1], x[2]); //
    and(d0_terms[1], s_not[3], s_not[1], s[0], x_not[2]);
    and(d0_terms[2], s_not[3], s_not[2], s[0], x_not[2]);
    and(d0_terms[3], s_not[2], s_not[0], x[2]);
    and(d0_terms[4], s_not[3], s[2], s[1], s[0], x[2]);
    and(d0_terms[5], s[3], s_not[2], s_not[1], x[2]);
    and(d0_terms[6], s_not[3], s_not[1], s_not[0], x[2]);
    // }}}
    // {{{2 S1*
    and(d1_terms[0], s[3], s_not[2], s_not[0], x[1], x_not[2]);
    and(d1_terms[1], s_not[2], s_not[1], x[1], x_not[2]);
    and(d1_terms[2], s_not[3], s[1], s_not[0], x_not[1]);
    and(d1_terms[3], s_not[3], s_not[1], s[0], x[2]); //W
    and(d1_terms[4], s_not[3], s_not[2], s[1], x_not[1], x_not[2]);
    and(d1_terms[5], s_not[3], s[1], s_not[0], x[2]);
    and(d1_terms[6], s_not[3], s[2], s[0], x[1], x_not[2]);
    and(d1_terms[7], s_not[3], s_not[1], x[1], x_not[2]);
    // }}}
    // {{{2 S2*
    and(d2_terms[0], s_not[2], s_not[0], x[1], x[2]);
    and(d2_terms[1], s_not[2], s_not[1], x[1], x[2]);
    and(d2_terms[2], s_not[3], s[2], s_not[0], x_not[1]);
    and(d2_terms[3], s_not[3], s[2], s_not[1], x_not[2]);
    and(d2_terms[4], s_not[3], s_not[2], s[1], x[1], x_not[2]);
    and(d2_terms[5], s_not[3], s_not[2], s[1], s[0], x_not[1], x[2]);
    and(d2_terms[6], s_not[3], s[2], s_not[1], x_not[1]);
    and(d2_terms[7], s_not[3], s[2], s[1], s[0], x[1], x[2]);
    // }}}
    // {{{2 S3*
    and(d3_terms[0], s_not[3], s_not[2], s[1], s[0], x[1], x[2]);
    and(d3_terms[1], s_not[3], s[2], s[1], s_not[0], x[1]);
    and(d3_terms[2], s_not[3], s[2], s_not[1], x[1], x[2]);
    // }}}
    // {{{2 n_change
    and(dn_terms[0], s[3], s_not[2], s_not[0]);
    // }}}
    // {{{2 d_change
    and(dd_terms[0], s[3], s_not[2], s[1], s_not[0]);
    and(dd_terms[1], s[3], s_not[2], s_not[1], s[0]);
    // }}}
    // {{{2 out
    and(dy_terms[0], s_not[3], s[2], s[1], s[0]);
    and(dy_terms[1], s[3], s_not[2], s_not[0]);
    and(dy_terms[2], s[3], s_not[2], s_not[1]);
    // }}}
  // }}}
  // {{{1 flip-flop inputs
  wire d0_in, d1_in, d2_in, d3_in, dn_in, dd_in, dy_in;
  or(d0_in, // {{{2
     d0_terms[0],
     d0_terms[1],
     d0_terms[2],
     d0_terms[3],
     d0_terms[4],
     d0_terms[5],
     d0_terms[6]); // }}}
  or(d1_in, // {{{2
     d1_terms[0],
     d1_terms[1],
     d1_terms[2],
     d1_terms[3],
     d1_terms[4],
     d1_terms[5],
     d1_terms[6],
     d1_terms[7]); // }}}
  or(d2_in, // {{{2
     d2_terms[0],
     d2_terms[1],
     d2_terms[2],
     d2_terms[3],
     d2_terms[4],
     d2_terms[5],
     d2_terms[6],
     d2_terms[7]); // }}}
  or(d3_in, // {{{2
     d3_terms[0],
     d3_terms[1],
     d3_terms[2]); // }}}
	 // end state change
  buf(dn_in, // {{{2
      dn_terms[0]); // }}}
  or(dd_in, // {{{2
     dd_terms[0],
     dd_terms[1]); // }}}
  or(dy_in, // {{{2
     dy_terms[0],
     dy_terms[1],
     dy_terms[2]); // }}}
  // }}}
  // flip-flop inputs with active-high reset
  wire d0_in_r, d1_in_r, d2_in_r, d3_in_r, dy_in_r, reset_not; wire dd_in_r, dn_in_r;
  not(reset_not, reset);
  and(d0_in_r, d0_in, reset_not);
  and(d1_in_r, d1_in, reset_not);
  and(d2_in_r, d2_in, reset_not);
  and(d3_in_r, d3_in, reset_not);
  and(n_change, dn_in, reset_not);
  and(d_change, dd_in, reset_not);
  and(out, dy_in, reset_not);
  // flip-flops (same clock for both d flip flops)
  `ifdef TEST
  buf(dS[0],d0_in_r);
  buf(dS[1],d1_in_r);
  buf(dS[2],d2_in_r);
  buf(dS[3],d3_in_r);
  `endif
  d_flipflop d0(d0_in_r, clk, s[0]);
  d_flipflop d1(d1_in_r, clk, s[1]);
  d_flipflop d2(d2_in_r, clk, s[2]);
  d_flipflop d3(d3_in_r, clk, s[3]);
  //d_flipflop dn(dn_in_r, clk, n_change);
  //d_flipflop dd(dd_in_r, clk, d_change);
  //d_flipflop dy(dy_in_r, clk, out);

endmodule
*/
