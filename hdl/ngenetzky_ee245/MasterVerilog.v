module MasterVerilog(
	input clk, clk_27, clk_50,
	input [17:0] switch,
	output [8:0] green,
	output [17:0] red,
	input [3:0] pushBut,
	output [6:0] Hex0,Hex1,Hex2,Hex3,Hex4,Hex5,Hex6,Hex7,
	output [3:0] serialPins
);

//Hex display regs
	reg [4:0] displayAtHex0, displayAtHex1, displayAtHex2, displayAtHex3, displayAtHex4, displayAtHex5,displayAtHex6, displayAtHex7; // {isHexValue_L, 4'b_HEX_REPRESENTATION}
		wire [15:0] hexCurrentDisplay;
	///		IO Control						START
assign red[15:0] = hexCurrentDisplay;	wire [15:0] bus;
assign red[16] = key3isPressed;
assign red[17] = clk_50;
assign green[8]=Done; 	wire Done;

wire key3isPressed;
	assign key3isPressed= !pushBut[3];
wire [2:0]XXX, YYY, UUU;
	assign XXX = switch[8:6];
	assign YYY = switch[5:3];
	assign UUU = switch[2:0];
	assign opcode = switch[15:9];
wire [6:0] opcode;
wire [3:0] ProcStep;
	assign green[3:0] = ProcStep;
wire DebouncedClk;
	assign green[7] = DebouncedClk;
///
///	Button Debouncer
///	->
	ButtonDebounce(
		.in(!clk), 
		.clk(clk_50),
		.out(), 
		.waiting(),
		.latch(DebouncedClk), 
		.debounceLoad()
    //,output reg clkLED
);
	

	

///		The Processor.						FINISHED
// 		Instantiate Processor
ProcessorRemade (
    .ExternalData(switch[15:0]), //)input       [15:0] ,
    .ProcessorReset(switch[16]), 		//We arent suppose to have this.
	 .ProcessorEnable(switch[17]), 	// run switch 
	 .ClockButton(DebouncedClk), 
	 .ViewRegister(key3isPressed), // If ViewRegister = 1, then the contents of the register at address RegisterSelect
											// is assigned to HexDisplay; else the contents of the bus is assigned to HexDisplay
	 .DebounceClock(clk_50),
			
    .HexDisplay(hexCurrentDisplay), // contents of bus or register
    .OperationFinished(Done),
	 .TimeStep(ProcStep)
);


				//#TODO make more generic.
//Insta Decoder. To decimal
    always @(opcode)
        case ({1'b1, opcode})
            8'b10000000: displayAtHex7=0;// { 0: Load }
            8'b10000001: displayAtHex7=1; // { 1: Move }
            8'b11000010: displayAtHex7=2; // { 2: Add }
            8'b11000011: displayAtHex7=3; // { 3: Subtract }
            8'b11000100: displayAtHex7=4; // { 4: OR }
            8'b11000101: displayAtHex7=5; // { 5: AND }
            8'b11000110: displayAtHex7=6; // { 6: 1's Comp }
            8'b11000111: displayAtHex7=7; // { 7: 2's Comp }
				default:
					displayAtHex7 = 4'hF;
        endcase	
			///		///
		///		///				All purpose LED display
	///		///	
	
					// 	LED DISPLAY AND HEX DISPLAY
					//  Finish hexdisplay
						always @(hexCurrentDisplay)
							begin
										displayAtHex0 = {1'b0,hexCurrentDisplay[3:0]};
										displayAtHex1 = {1'b0,hexCurrentDisplay[7:4]};
										displayAtHex2 = {1'b0,hexCurrentDisplay[11:8]};
										displayAtHex3 = {1'b0,hexCurrentDisplay[15:12]};
							end
					//		Display hexCurrentDisplay to SevenSegmentDisplayDecoder module 
						SevenSegmentDisp LED0	(.ssOut_L(Hex0[6:0]), .nIn(displayAtHex0[3:0]), .isHex_L(displayAtHex0[4]));
						SevenSegmentDisp LED1	(.ssOut_L(Hex1[6:0]), .nIn(displayAtHex1[3:0]), .isHex_L(displayAtHex1[4]));
						SevenSegmentDisp LED2	(.ssOut_L(Hex2[6:0]), .nIn(displayAtHex2[3:0]), .isHex_L(displayAtHex2[4]));
						SevenSegmentDisp LED3	(.ssOut_L(Hex3[6:0]), .nIn(displayAtHex3[3:0]), .isHex_L(displayAtHex3[4]));
						//SevenSegmentDisp LED7	(.ssOut_L(Hex7[6:0]), .nIn(displayAtHex7[3:0]), .isHex_L(displayAtHex3[4]));
						
						//regular JUST HEX displays
		 SevenSegmentDisplayDecoder		uDisplay4(.ssOut(),.ssOut_L(Hex4[6:0]) , .nIn({1'b0,YYY}));		//disp location of Rx	XXX
		 SevenSegmentDisplayDecoder		uDisplay5(.ssOut(),.ssOut_L(Hex5[6:0]) , .nIn({1'b0, XXX}));	//disp location of Ry	YYY
		 SevenSegmentDisplayDecoder		uDisplay6(.ssOut(),.ssOut_L(Hex6[6:0]) , .nIn({1'b0, UUU})); 	//connect to hex 6
		 SevenSegmentDisplayDecoder		uDisplay7(.ssOut(),.ssOut_L(Hex7[6:0]) , .nIn(displayAtHex7)); 	//connect to hex 6
			///		///
		///		///				FINISH	All purpose LED display
	///		///
	
			///		///
		///		///				START 	Instruction memory
	///		///	
/*	wire Mclock;
	reg [2:0] savedInstructionSteps;
	reg useSavedInstructions, saveIR, loadSavedIR, presetSavedIR, countSavedIR, getAll;
	reg [15:0] inputIR;
	reg [2:0] addressIR;
	wire nextStep;
		assign nextStep = useSavedInstructions & done;
savedInstructions(
	.en(useSavedInstructions), 
	.load(loadSavedIR), 
	.usePreset(presetSavedIR),
	.clk(clk_50), 
	.newOpcode(bus),
	.opLoc(addressIR),
	.functionReg()
);
//savedInstructionSteps counterValue
mod_counter(
	.clk(clk), 
	.Load(0), 	
	.countUp(1), 
	.loop(1), 
	.enCounting(nextStep),
	.dataLoad(0), 
	.detectReLoad(8),
	.count(savedInstructionSteps)
);
always @(posedge clk_50)
begin
	if((useSavedInstructions)&(presetSavedIR))
		begin
			if(savedInstructionSteps==0)
				countSavedIR = 1;
			addressIR = savedInstructionSteps;
			loadSavedIR =0;
		end
	else if ((useSavedInstructions)&(loadSavedIR))
		addressIR = switch[2:0];
		
	else if ((useSavedInstructions)&getAll)
		begin
			if(savedInstructionSteps==0)
				countSavedIR = 1;
			address = savedInstructionSteps;
		end
	else 
		countSavedIR =0;	
		
		
		
end

*/
			///		///
		///		///				FINISH 	Instruction memory
	///		///	
	
	/*
///
/// scrapped Processor that I worked hours and hours on... 	
///	
	reg Extern, inPreTemp, inPostTemp,outPostTemp, AddSub, Done;
	wire [1:0] StepCount;
	reg [2:0] XXX, YYY, UUU;
	reg [3:0] dispRegLoc, reg2bus_loc, bus2reg_loc;
	reg [6:0] opcode;
	wire [7:0] inGate, outGate, inOtherGate, outOtherGate;
	wire [15:0] bus;
	wire [15:0] reg0, reg1,reg2,reg3,reg4,reg5,reg6,reg7, preTemp, postTemp, dataEX;
	reg [15:0] hexCurrentDisplay;
	
	wire error[10:0]; 
		or(error[0],error[1],error[2]); assign error[2] =0;
		
parameter [3:0] 	loc_reg0= 4'b0000, loc_reg1= 4'b0001, 
						loc_reg2= 4'b0010, loc_reg3= 4'b0011, 
						loc_reg4= 4'b0100,  loc_reg5= 4'b0101, 
						loc_reg6= 4'b0110, loc_reg7= 4'b0111, 
										loc_Pre = 4'b1000, loc_Post=4'b1001, 			// do not need to reference them this way
										loc_Data=4'b0010, loc_Instruct=4'b1011;		// do not need to reference them this way
							

		///		///
		///		///				Start Processor
		///		///
		
// Debounce the manual clock input
		// Stepper( .in(), .clk(),.out(), .waiting(),.latch(), .debounceLoad());		
		//I think we should also debounce the other pushBut just to be safe.
		
// ALU with contained A and G element 		
SamProc(	.clk(clk),
			.Ain(inPreTemp), 
			.Gin(inPostTemp), 
			.Gout(outPostTemp), 
			.select(opcode),
			.bus(bus)
			);  


///
				// 			Bus Control						HUGE SECTION				//
///				
//Decide the what is connected to the bus.
///
///					Decode the inGate / outGate loc code from control center
///
		//Decode the loc to find the INPUT to the BUS			VERIFIED TO ASSERT EACH
				decode3to8_L inLocDecodeMSB0(.y_L(), .EN_L(0), .EN2_L(0),
												.y(inGate[7:0]), 
												.x(bus2reg_loc[2:0]),
												.EN(!bus2reg_loc[3]));	
//				decode3to8_L inLocDecodeMSB1(.y_L(), .EN_L(0), .EN2_L(0),
//												.y(inOtherGate[7:0]), 
//												.x(bus2reg_loc[2:0]),
//												.EN(bus2reg_loc[3]));
					//always @(inOtherGate)
						//inPreTemp = inOtherGate[0]; // 4'b1000
												
		//Decode the loc to find the OUTPUT to the BUS			VERIFIED TO ASSERT EACH
				decode3to8_L outLocDecodeMSB0(.y_L(), .EN_L(0), .EN2_L(0),
												.y(outGate[7:0]), 
												.x(reg2bus_loc[2:0]),
												.EN(!reg2bus_loc[3]));	
//				decode3to8_L outLocDecodeMSB1(.y_L(),.EN_L(0), .EN2_L(0), 		
//												.y(outOtherGate[7:0]), 
//												.x(reg2bus_loc[2:0]),
//												.EN(reg2bus_loc[3]));
//					//always @(outOtherGate)
						//begin
						//outPostTemp = outOtherGate[1]; // 4'b1001
						//end
///
///					Control whether Bus goes into the reg
///
	//Regn Controls whether the singal can get into the gate, GATE IN	Loads bus into reg if Rin is asserted.
		//Data Registers
			Register R0(.in(bus),.out(reg0),
						.en(inGate[0]), 
						.clk(clk)
							);
			Register R1(.in(bus),.out(reg1),
						.en(inGate[1]),.clk(clk), 
						 );
			Register R2(.in(bus),.out(reg2),
						 .en(inGate[2]),
						 .clk(clk)
						 );
			Register R3(.in(bus),.out(reg3),
						.en(inGate[3]), 
						.clk(clk)
						);
			Register R4(.in(bus),.out(reg4),
						.en(inGate[4]), 
						.clk(clk)
						);
			Register R5(.in(bus),.out(reg5),
						.en(inGate[5]), 
						.clk(clk)
						);
			Register R6(.in(bus),.out(reg6),
						.en(inGate[6]), 
						.clk(clk)
						);
			Register R7(.in(bus), .out(reg7),
						.en(inGate[7]), 
						.clk(clk)
						);

///
///					Control whether bus goes onto the BUS
///
trin	trin0( .en(outGate[0]),.in(reg0),.out(bus) );
trin	trin1( .en(outGate[1]),.in(reg1),.out(bus) );
trin	trin2( .en(outGate[2]),.in(reg2),.out(bus) );
trin	trin3( .en(outGate[3]),.in(reg3),.out(bus) );
trin	trin4( .en(outGate[4]),.in(reg4),.out(bus) );
trin	trin5( .en(outGate[5]),.in(reg5),.out(bus) );
trin	trin6( .en(outGate[6]),.in(reg6),.out(bus) );
trin	trin7( .en(outGate[7]),.in(reg7),.out(bus) );

trin	external( .en(extern),.in(DataEX),.out(bus) );
///
				// 			FINISH Bus Control				HUGE SECTION				//
///

///
///					Processor steps
///

mod_counter ProccessorCount(
				.clk(clk), 
				.Load(0), 
				.countUp(1), 
				.enCounting(1), 
				.dataLoad(0), 
				.detectReLoad(4),
				.count(StepCount)
				);
				defparam ProccessorCount.n=2;
parameter [6:0]	op_Load	= 000_0000,
						op_Move	= 000_0001,
						op_Add 	= 100_0010,
						op_Sub 	= 100_0011,
						op_Or  	= 100_0100,
						op_And 	= 100_0101,
						op_onesC	= 100_0110,
						op_twosC	= 100_0111;
						
// Instruct format:  op-c_ode_XXX_YYY_UUU    (op-code is 7 bits long)
wire [15:0]Instruct;
assign Instruct = dataEX;
always @(StepCount, Instruct)
	begin
		opcode = Instruct[15:9];
		XXX =Instruct[8:6];	//temporary
		YYY = Instruct[5:3];	//temporary
		UUU = Instruct[2:0];
		case(StepCount)
			2'b00: //No signals asserted (time to load OP_code)
						;
			2'b01: 	//embeded case, T1
						case(Instruct[15:9]) //  I is the OP_code
							//Open channel from external switch to XXX
							op_Load: 	
								begin //Load
										Extern=1; 				//Data should be on the switches
										bus2reg_loc = {1'b0,XXX}; 	//Data Moves into XXX
										Done=1;					//its Done!
								end
							//Open channel from XXX to YYY
							op_Move:	
								begin //Move
										bus2reg_loc = YYY;  			//  to YYY
										reg2bus_loc ={1'b0,XXX}; 	// from XXX
										Done =1;
								end
							//Load XXX into Temp register
							op_Add | op_Sub: //either add or subtract
								begin //add sub
										bus2reg_loc={1'b0,XXX};
										inPreTemp=1;
								end
						endcase
			 2'b10: //processor step T2
						case(Instruct[15:9])
							op_Add:  
								begin //add
										bus2reg_loc = {1'b0,YYY};
										AddSub =0;		//never passed into ALU
										inPostTemp =1;
								end
							op_Sub: 
								begin //Sub
										bus2reg_loc ={1'b0,YYY}; 
										AddSub = 1; 	//never passed into ALU
										inPostTemp=1;
								end
						 endcase
			 2'b11:	// T3
						 case(Instruct[15:9])
							op_Add | op_Sub: 
								begin
										outPostTemp=1; 
										bus2reg_loc ={1'b0,XXX}; 
										Done=1;
								end
						 endcase
		endcase
	end
		///		///
		///		///				FINISH Processor I worked hours and hours on...
		///		///
	*/




		/*
		///		///
		///		///				Start stepper
		///		///
wire [4:0] twoCompInput;
	assign twoCompInput = switch[4:0];
	reg isNegative;
	always @(twoCompInput)
		isNegative = twoCompInput[4];
wire stepperOnesDigit, stepperTensDigit, stepperSignDigit;
wire [4:0] counterValue; 
assign green[4:0] = counterValue[4:0];
//wire detectThis = 5b' 1_1111
//and(detectedBin, !counterValue[3], counterValue[2], counterValue[1], counterValue[0]); 
//or(resetCounter, detectedBin);
//Modulo_n_Counter stepCounter(.clk(!pushBut[1]), .Reset(switch[17]), .Count(counterValue[4:0]),.countUp(switch[16]),.Load(switch[15]),.data(switch[4:0]));	

mod_counter StepperCount(
	.clk(!pushBut[1]), .Load(restartStepper), .countUp(0), .loop(restartStepper), .enCounting(1),
	 .dataLoad(twoCompInput[3:0]), .detectReLoad(4'b0000),.count(counterValue[3:0]), 
);	
								wire done = (counterValue[3:0]==0);
								wire restartStepper = done & switch[17]; 
decimalOnly(	.twosCompBin4b(StepCount),.onesLED(stepperOnesDigit),.tensLED(stepperTensDigit),.signLED(stepperSignDigit)	);
 Motor(.reset(), .en(!done), .clkwise(!isNegative), .Q(red[3:0])
);
 
//	defparam stepCounter.max = 15;
//LED HEX display
	always @(stepperOnesDigit,stepperTensDigit,stepperSignDigit)
		begin
				// Start display of stepper
				//stepperOnesDigit, stepperTensDigit, stepperSignDigit
			//		Stepper Module Display ONLY
					
					displayAtHex0=stepperOnesDigit; // 4
					displayAtHex1=stepperTensDigit; // 1
					displayAtHex2=stepperSignDigit; // -
					displayAtHex3=1'h0;
		end
		///		///
		///		///				FINISH stepper
		///		///	
		*/

	
	/*	
function [4:0] onesCompTOsignedRep; //Must be within Main
		input [4:0] onesComp;
		begin
			if(onesComp[4]==1)
				begin
					onesCompTOsignedRep = ~onesComp;
					onesCompTOsignedRep[4]=1;
				end
			else
				onesCompTOsignedRep = onesComp;
		end
endfunction
	*/



endmodule //end MAIN


//Begin processor components in mai

// Attempt to create a task.




	/*
	buf(green[0], my_inReg[0]); // n
	buf(green[1], my_inReg[1]); // d
	buf(green[2], my_inReg[2]); // q
	buf(green[3], my_inReg[3]); // S0
	buf(green[4], my_inReg[4]); // S1
	buf(green[5], my_inReg[5]); // S2
	buf(green[6], my_inReg[6]); // S3
	buf(green[7], clk); //
	counterValue[3], !counterValue[2], counterValue[1],!counterValue[0])
		//buf(red[0], my_s[0]);
		//buf(red[1], my_s[0]);
		//buf(red[2], my_s[0]);
		buf(red[3], 1);
		buf(red[4], my_s[0]);
		buf(red[5], my_s[1]);
		buf(red[6], my_s[2]);
		buf(red[7], my_s[3]);
	buf(red[8], 1);
		buf(red[9], switch[0]);
		buf(red[10], switch[1]);
		buf(red[11], switch[2]);
	buf(red[12], 1);
		buf(red[13], my_dS[0]);
		buf(red[14], my_dS[1]);
		buf(red[15], my_dS[2]);
		buf(red[16], my_dS[3]);
	buf(red[17], 1);
	*/
//Control 7segment individually. 
	/*
	always @(switch[7:0])
		begin
			Hex0[0] = switch[0];
			Hex0[1] = switch[1];
			Hex0[2] = switch[2];
			Hex0[3] = switch[3];
			Hex0[4] = switch[4];
			Hex0[5] = switch[5];
			Hex0[6] = switch[6];			
		end
		*/
//	decode2to4(.x(switch[1:0]),.y(red[3:0]),.EN(switch[17]),.y_L(green[3:0]));
//	decode3to8_L(.y_L(red[17:9]), .x(switch[3:0]),.EN(switch[16]), .EN_L(switch[15]), .EN2_L(switch[14]));
//	encode8to3_prio(.y(red[2:0]),.x(switch[7:0]),.z(green[0]),.EN(1));
//	multiplexer2to1(.f(red[0]),.x(switch[0:1]),.S(switch[2]));
//	multiplexer4to1(.f(red[1]),.x(switch[3:7]),.S(switch[8:9]));
// pop_machine(.clk(clk), .n(switch[0]), .d(switch[1]), .q(switch[2]),.reset(switch[4]),.out(red[0]), .n_change(red[1]),.d_change(red[2]),.s(my_s),.inReg(my_inReg));
// pop_machineSam(.reset(switch[4]), .clk(clk), .n(switch[0]), .d(switch[1]), .q(switch[2]), .out(red[0]), .n_change(red[1]), .d_change(red[2]),.s(my_s),.inReg(my_inReg));

//	SevenSegmentDisplayDecoder(.ssOut_L(Hex0[6:0]), .nIn(switch[3:0]));
//	SevenSegmentDisplayDecoder(.ssOut_L(Hex1[6:0]), .nIn(switch[7:4]));
//	SevenSegmentDisplayDecoder(.ssOut_L(Hex2[6:0]), .nIn(switch[11:8]));
//	SevenSegmentDisplayDecoder(.ssOut_L(Hex3[6:0]), .nIn(switch[15:12]));



