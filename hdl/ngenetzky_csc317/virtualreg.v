module virtualreg(
	input [15:0] inputData,
	input MSB_orLSB_L, clock,
	output reg [31:0]vir_reg
);

always @ (posedge clock)
begin 
	if(MSB_orLSB_L==1)
		begin
			vir_reg[31:16] = inputData;
		end
	else
		begin
			vir_reg[15:0] = inputData;
		end
end
endmodule 

