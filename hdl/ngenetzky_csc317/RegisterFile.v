/* RegisterFile (Intial Design) 
	CSC-317
	10/11/2014
	Written by: Drake Jeno
					Jeremy Laird
*/
//Edited By TEAM GAMMA - Jordan D. Ulmer And Patrick Schroeder
//11/01/2014
	//Added RF_WRITE Control Signal

/******BRIEF DESCRIPTION****************
	REFERENCE: Figure 5.8, "Computer Organization and Embedded Systems, 6th Ed."
	
	Rdst:		5 bit address of the destination register
	Rsrc1:	5 bit address of the first register in the opperand
					-This register is written to RA
	Rsrc2:	5 bit address of the second register in the opperand
					-This register goes to RB
	RA,RB:	are 32 bit outputs and should portmapped to the respective registers of the processor
	RY:		32 bit input and should be portmapped to RY 
	Comment:  To implement in a piplined processor Rsrc1 and Rsrc2 will not come from the same instruction
				 as Rdst.
*/
 
/********************BEGIN MODULE*****************/

module RegisterFile(Rdst,Rsrc1,Rsrc2,RA,RB,RY,clk,RF_WRITE);
	input wire [4:0]Rdst,Rsrc1,Rsrc2;
	input wire clk, RF_WRITE;
	input wire [31:0]RY;
	output wire [31:0]RA,RB;
	
	//Initialize 32 32-bit registers
		reg[31:0]R[31:0]; 												
	
	//Fill some spaces
//		initial 
//		begin
//			R[0][31:0] = 0;
//			R[1][31:0] = 1;
//			R[2][31:0] = 2;
//			R[3][31:0] = 3;
//			R[4][31:0] = 4;
//			R[5][31:0] = 5;
//			R[6][31:0] = 6;
//			R[7][31:0] = 7;
//			R[8][31:0] = 8;
//			R[9][31:0] = 9;
//			R[10][31:0] = 10;
//		end
		
	//RA and RB are assigned the contents of the general purpose registers specified by Rsrc1(5 bits) and Rsrc2 
		assign RA[31:0]=R[Rsrc1][31:0];									
		assign RB[31:0]=R[Rsrc2][31:0];
	
	//At each clock the contents of RY are written to the general purpose register specified by Rdst(5 bits)
		always@(posedge clk)
			begin
				if(RF_WRITE)begin
					R[Rdst][31:0]<=RY[31:0];
				end
			end
		
endmodule 

/*
10/11/2014
	RegisterFile that was updated to include RF_write. The 2nd version of the RegisterFile.
	Written by: Drake Jeno
				Jeremy Laird
module RegisterFile(AddressC,AddressA,AddressB,A,B,RY,clk,RF_write);
	input [4:0]AddressC,AddressA,AddressB;
	input clk,RF_write;
	input [31:0]RY;
	output wire [31:0]A,B;
	
	//Initialize 32 32-bit registers
		reg[31:0]R[31:0]; 												
	
	//A and B are assigned the contents of the general purpose registers specified by AddressA(5 bits) and AddressB 
		assign A[31:0]=R[AddressA][31:0];									
		assign B[31:0]=R[AddressB][31:0];
	
	//At each clock the contents of RY are written to the general purpose register specified by Rdst(5 bits) if RF_write is enabled
		always@(posedge clk)
			begin
				case(RF_write)
					1: R[AddressC][31:0]<=RY[31:0];
					default: ;
				endcase
			end
		
endmodule 
*/
		