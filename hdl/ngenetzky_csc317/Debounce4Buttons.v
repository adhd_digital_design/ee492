module Debounce4Buttons(
	input clk_27, in0,in1,in2,in3,
	output out0,out1,out2,out3
);
PushButton_Debouncer uDebounce0(
	 .clock27MHz(clk_27),
	 .PB(in0),  // "PB" is the glitchy, asynchronous to clk, active low push-button signal
	 // from which we make three outputs, all synchronous to the clock
	 .PB_state(out0)	//Clock is connected to Push Button 3 
	 );
PushButton_Debouncer uDebounce1(
	 .clock27MHz(clk_27),
	 .PB(in1),  // "PB" is the glitchy, asynchronous to clk, active low push-button signal
	 // from which we make three outputs, all synchronous to the clock
	 .PB_state(out1)	//Clock is connected to Push Button 3 
	 );
PushButton_Debouncer uDebounce2(
	 .clock27MHz(clk_27),
	 .PB(in2),  // "PB" is the glitchy, asynchronous to clk, active low push-button signal
	 // from which we make three outputs, all synchronous to the clock
	 .PB_state(out2)	//Clock is connected to Push Button 3 
	 );
PushButton_Debouncer uDebounce3(
	 .clock27MHz(clk_27),
	 .PB(in3),  // "PB" is the glitchy, asynchronous to clk, active low push-button signal
	 // from which we make three outputs, all synchronous to the clock
	 .PB_state(out3)	//Clock is connected to Push Button 3 
	 );
endmodule