module Counter_5Stage(
	input clk, enable,
	output reg [2:0] stage
);
//reg [2:0] dumbStage;
initial stage = 1;
always @(posedge clk)
begin
if(enable)
	begin
	case(stage)
		1:stage = 2;
		2:stage = 3;
		3:stage = 4;
		4:stage = 5;
		5:stage = 1;
	endcase
	end
//	if(dumbStage <= 3'd4)
//		begin
//		dumbStage = 0;
//		end
//	else
//		begin
//		dumbStage = dumbStage + 1;
//		end
//	end
//	stage = dumbStage +1;
end
endmodule



/*
module Modulo_n_Counter(
input clk, Reset, Load, countUp, 
output reg [4:0] Count,
input [4:0]data
);
always @(posedge clk)
begin
		if(Load)
				Count <= data;
		else if (Reset)
				Count <= 0; //reset
		else
				Count <= Count + (countUp ? 1 : -1) ; //count down
end
endmodule
*/