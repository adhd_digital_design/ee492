module Mux4to1(
    input [31:0] w0, w1,w2, w3,
	 input [1:0]s,
    output reg [31:0] f
	 );
    always@(w0 or w1 or w2 or w3 or s)
    begin
        case(s)
            'b00:f=w0;
            'b01:f=w1;
            'b10:f=w2;
            'b11:f=w3;
			endcase
    end
endmodule 