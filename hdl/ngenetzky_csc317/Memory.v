// Memory
// Last Modified 10-15-2014

module Memory(
	input [31:0] dataIn,
	input [31:0] address,
	input read_orWrite_L,
	input clock,
	output [31:0] dataOut
	);
	
assign dataOut = (address[6] ? 
						data_RAM // RAM is least significant half
						: data_ROM);  	// ROM is least significant half
	
//START ROM
wire [31:0] data_ROM;
ROM uROM(
	.address(address[5:0]), // From MuxMA
	.clken(!address[6]),
	.clock(clock),	// Clock
	.q(data_ROM)			// To IR
	);
//END ROM
//START RAM
wire [31:0] data_RAM;
RandomAccessMemory RAM1(
		.MEM_Address(address[6:0]), // From RZ or PC // WORD ADDRESSABLE
		.Read_H_Write_L(read_orWrite_L), // Memory Read (1) Write (0)
		.MemoryFunctionComplete(),  // MemoryFunctionComplete
		.MEM_In(dataIn), // Input Data
		.MEM_Out(data_RAM) // Output Data
		);
		
//END RAM
endmodule

	