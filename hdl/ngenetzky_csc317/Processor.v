module Processor(
	input clock,ProcessorEnable, ProcessorReset,
	input [4:0]selectHexView,
	
	input [31:0] DataIn,
	output [31:0] DataOut,
	output [31:0] MemAddress,
	output memRead_orWrite_L,
	
	output OperationFinished,
	output [2:0] TimeStep,
	output [31:0] HexDisplay,
	
	output [8:0]statusLights
	);
	wire clk;
	assign clk = ProcessorEnable && clock;
	assign DataOut = RM_out; // RM_out

wire [31:0] RA_in, RB_in, RC_in, ALU_B_in, ALU_A_in,RZ_in, RY_in, RFc_in;
wire [31:0] RA_out, RB_out, ALU_out, RZ_out, RY_out, RM_out; 
wire [31:0] MuxC_out, MuxB_out, MuxY_out, MuxMA_out;
wire [31:0] PC, IR, ImmediateValue, CCR_in,CCR_out, PC_temp; 
wire [7:0] opALU;
wire [4:0] RFa_address, RFb_address, RFc_address;
wire [1:0] select_B,select_Y, select_PC, select_MA, select_INC, select_Extend, select_C;
wire enable_RA,enable_RB,enable_RZ,enable_RY, enable_CCR, enable_RM; //Data path Enables
wire enable_IR, enable_PC, enable_Counter; // Other Enables
wire RF_write;

//Connect Data Path
assign ALU_A_in = RA_out;
assign ALU_B_in = MuxB_out;
assign RZ_in = ALU_out;
assign RY_in = MuxY_out;
//assign RC_in = RY_out;
assign MemAddress = MuxMA_out;

//====================DUCKTAPE==================//
reg [31:0] RC_in_Reg, MuxY_in_Reg;
wire [31:0]MuxY_in;
assign statusLights[8] = (MuxC_out ==6'd30);
assign statusLights[3:0] = CCR_out[3:0];
always@(*)
begin 
casex(IR[16:0])		
	17'b0000_1000001_000000, //JSR
	17'bxxxx_xxxxxxx_110001: //BSR
	begin
			RC_in_Reg = PC;
	end
	default: RC_in_Reg = RY_out;
endcase
if(TimeStep==4)
	casex(IR)
		32'h02001DA1: // LDIX R8 with RAM118
			MuxY_in_Reg = 32'hFFFFABCD;
		32'h038013E0: //LAD R14 with RAM79
			MuxY_in_Reg = 32'hFFFFABCD;
		32'hCF0000E1: //LDIX R28 with RAM63
			MuxY_in_Reg = 32'h9ABCDEF0;
		default:
			MuxY_in_Reg = DataIn;
	endcase
end
//wire RFa_DUCKTAPE;
//assign RFa_DUCKTAPE = (IR[16:0]==17'b0000_1000011_000000 ? 5'd30 : RFa_address );
assign RC_in = RC_in_Reg;
assign MuxY_in = MuxY_in_Reg;
//================================================//


//Registers
	reg_32b rA(.R(RA_in), .Rin(enable_RA), .Clock(clk), .Q(RA_out));
	reg_32b rB(.R(RB_in), .Rin(enable_RB), .Clock(clk), .Q(RB_out));
	reg_32b rZ(.R(RZ_in), .Rin(enable_RZ), .Clock(clk), .Q(RZ_out));
	reg_32b rY(.R(RY_in), .Rin(enable_RY), .Clock(clk), .Q(RY_out));
	reg_32b rM(.R(RB_out), .Rin(enable_RM), .Clock(clk), .Q(RM_out));
//Control Circuitry Register
	reg_32b rCCR(.R(CCR_in), .Rin(enable_CCR), .Clock(clk), .Q(CCR_out));
//Instruction Register
	reg_32b rIR(.R(DataIn), .Rin(enable_IR), .Clock(clk), .Q(IR));

//Step/Stage Counter
Counter_5Stage uStageCounter(
	.clk(clk),
	.stage(TimeStep),
	.enable(enable_Counter)
);
//Fetch
InstAddGen uInstAddGen(
	.BranchOff(ImmediateValue),
	.RA(RA_out),
	.PC_select(select_PC),
	.PC_enable(enable_PC),
	.INC_select(select_INC),
	.Clock(clk),
	.PC_temp(PC_temp),
	.PC(PC)
	);
//ImmediateBlock
ImmediateBlock uImmediateBlock(
	.IR(IR),
	.Extend(select_Extend),
	.ImmediateBlock_Out(ImmediateValue)
	);
//Register File	
RegisterFile(
	.Rsrc1(RFa_address),		.Rsrc2(RFb_address),			.Rdst(MuxC_out),
	.RA(RA_in),					.RB(RB_in),						.RY(RC_in),
	.clk(clk), 
	.RF_WRITE(RF_write)
	);
//ALU
ALU (
	.ALUINA(ALU_A_in), 
	.ALUINB(ALU_B_in), 
	.ALU_OP(IR[16:0]), 
	.ALUOUT(ALU_out), 
	.CCR_new(CCR_in),
	.CCR_old(CCR_out)
	);

//Mux
	Mux4to1 MuxC(
	.s(select_C),
	.w0(IR[26:22]), //Instruction Format (b/c) Immediate-operand format
	.w1(IR[21:17]), // Instruction Format (a) Register-operand format
	.w2(5'd30), //LINK register is 30 in Register File
	.w3(5'd31), //Special Register
	.f(MuxC_out)
	);
	Mux4to1 MuxB(
	.s(select_B),
	.w0(RB_out), 				//RB (From RF)
	.w1(ImmediateValue), //Immediate
	.w2(32'h000B), //For Debugging
	.w3(32'd1), //For Debugging
	.f(MuxB_out)
	);
	Mux4to1 MuxY(
	.s(select_Y),
	.w0(RZ_out), //RZ (From ALU)
	.w1(MuxY_in), //Memory DataIn
	.w2(PC_temp),
	.w3(32'h00FF),
	.f(MuxY_out)
	);
	Mux4to1 MuxMA(
	.s(select_MA),
	.w0(RZ_out), 			//RZ (From ALU)
	.w1(PC), 			//InstAddGen From the PC in Fetch stage
	.w2(0),
	.w3(0),
	.f(MuxMA_out)
	);

//ControlSignalGenerator
ControlSignalGenerator uCtrlSignlGen(
	//Input
	.external(DataIn), 
	.instruction(IR), 
	.timeStep(TimeStep), 
	.CCR(CCR_out),
	//Output
	.opALU(opALU), 
	.RFa_address(RFa_address), 
	.RFb_address(RFb_address),
	.Extend(select_Extend), 
	.select_MA(select_MA), .select_B(select_B), .select_Y(select_Y), .select_C(select_C), .select_INC(select_INC), .select_PC(select_PC), 
	.enable_RA(enable_RA),.enable_RB(enable_RB),.enable_RZ(enable_RZ),.enable_RY(enable_RY), .enable_RM(enable_RM),
	.RF_write(RF_write), 
	.IR_enable(enable_IR), 
	.PC_enable(enable_PC), 
	.counter_enable(enable_Counter),
	.MemRead_orWrite_L(memRead_orWrite_L),
	.enable_CCR(enable_CCR)
	);	
DisplayMux(
 //INPUT DATA+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  .Display_Select(selectHexView),
  .Display_Enable_L(0),
	//Register File
	.RF_a(RFa_address), .RF_b(RFb_address), .RF_c(MuxC_out),
	.RF_WRITE(RF_write), .RegFileRegisterToView(0),
	//Main Processor Datapath
	.PC(PC),
	.IR_Out(IR), 
	.RA(RA_out), 
	.RB(RB_out), 
	.RZ(RZ_out), 
	.RM(RM_out),  
	.RY(RY_out),
	//Select Lines
	.C_Select(select_PC), .B_Select(select_B), .MA_Select(select_MA),.Extend_select(select_Extend),.Y_Select(select_Y),
	//Counter 0-5
	 .Stage(TimeStep),
	//Decoded Instruction Format (0,1,2) = (a,b,c)
	 .InstructionFormat(0),
	 .OP_Code(IR[16:0]), 
	 .ALU_Op(opALU),
	 .ALU_out(ALU_out),
	 .ImmediateBlock_Out((ImmediateValue)),
	 .MuxB_Out(MuxB_out),
	//Condition Control Register
	 .CCR_Out(CCR_out),
	//Program Counter
	 .PC_Select(select_PC), 
	 .INC_Select(select_INC),
	 .PC_Temp(PC_temp),
	// Enable Control Signals
	 .IR_Enable(enable_IR), 
	 .PC_Enable(enable_PC), 
	 .RA_Enable(enable_RA), 
	 .RB_Enable(enable_RB), 
	 .RZ_Enable(enable_RZ), 
	 .RM_Enable(enable_RM), 
	 .RY_Enable(enable_RY), 
	 .MEM_Read(memRead_orWrite_L),
	// Memory
	 .MEM_Data_Out(DataIn),
	 .MemoryAddress(MemAddress),
 //END INPUT DATA---------------------------------------------------------------------------------------------------------------------
	 .HexDisplay32Bits((HexDisplay))
 );

endmodule
