module ControlSignalGenerator(
 //Input
 input [31:0] external, instruction, CCR,
 input [2:0] timeStep,
 //Output
 output reg [7:0] opALU,
 output reg [5:0] RFa_address, RFb_address,
 output reg [1:0] Extend, select_MA, select_B, select_Y, select_C, select_INC, select_PC,
 output reg enable_RA,enable_RB,enable_RZ,enable_RY,enable_RM,IR_enable, PC_enable, counter_enable, enable_CCR,
 output reg RF_write, MemRead_orWrite_L
 ); 
 
 wire CARRY,OVERFLOW, ZERO, NEG; 
 assign CARRY = CCR[1];
 assign OVERFLOW = CCR[4];
 assign ZERO = CCR[2];
 assign NEG = CCR[3];
 
always @(*) // instruction, timeStep
 begin
 
 //Default values for most control signals and selects are set so that if no 
 //other value is chosen then they will have a certain default
 
 //Control Signals that do not change in the current phase
 counter_enable = 1;
 //select_PC = 1;
 
 //Initial State. Procedural nature of always means these will be overwriten later.
 //RFa_address = instruction[31:27]; //Assume Register Operand format
 //RFb_address = instruction[26:22]; //Assume Register Operand format
 Extend=0; select_MA=1; select_B=0; select_Y=0; select_C=0; select_INC=0;
 RF_write=0; IR_enable=0; PC_enable=0;
 MemRead_orWrite_L = 1;
 enable_RA=0; enable_RB=0; enable_RZ=0; enable_RY=0; enable_RM=0;
 
 
 case(timeStep)// Case that depends ONLY on timestep
  1:
  begin
		RF_write =0;
		// Always read from Data to PC in step 1
		select_PC = 1;
		// Read data at Memory Address into Memory Data at the next clock cycle
		 MemRead_orWrite_L = 1;
		// The Memory Address should be the PC counter
		// select_MA: 0 (RZ), 1 (PC)
		 select_MA = 1; // MEM_Address <- PC_Counter
		// Allow Memory Data (at Memory Address) to enter the IR at next clock
		 IR_enable = 1;
		// Allow the PC to increment at the end of the stage
		 PC_enable = 1;
		// By how much should the PC increment?
		// 0 (+4), 1 (BranchOffSet) Defined in InstAddGen
		select_INC = 0;
		select_Y = 0;
  end
  // IR <- Memory Data  (IR_enable was on)
	2:
  begin
   // PC should only increment once.
    PC_enable = 0;
    //The Instruction should not change in T3-T5
    // Memory Data may be read accidently and placed on the input.
	 MemRead_orWrite_L = 0;
	 IR_enable = 0;
	 enable_RA=1;//Enable
	 enable_RB=1;//Enable
  end
  3:
  begin
	 enable_RA=0;
	 enable_RB=0;
	 enable_RZ=1;//Enable
	 enable_RM=1;//Enable
	 enable_CCR=1;
  end
  4:
  begin
   enable_CCR=0;
	enable_RZ=0;
	enable_RM=0;
	enable_RY=1; //Enable
  end
  5:
  begin
	enable_RY=0;
	PC_enable=0;
  end
endcase // End the case that depends ONLY on timestep
 
 // Case that depends on instruction
casex(instruction[16:0])
 //     _____________________(NOP)________________________ 
 //     (NOP)DESCRIPTION:
 //      (1.) No Operation  // Stall but take 5 cycles to do it...
 //     ____________________________________________________ 
17'bxxxx_xxxxxxx_111111:  
	begin  
		/*enable_RZ=0;
		enable_RM=0;
		enable_RY=0;
		enable_RY=0;*/
   end
 //     _____________________(ADD)________________________
 //     (ADD)DESCRIPTION:
 //      (1.) Addition
			
 //     ____________________________________________________

  17'b0000_0000001_000000: 
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    // What is put into LnB of the ALU?
     // select_B: 0(RB/From Register File), 1(ImmediateValue)
     select_B = 0;
     
    // What do we want to write back to the Register File?
     //select_Y: 0(RZ/ALU result), 1(Memory Data), 2 (Return Address)
     select_Y = 0; // RZ - ALU result
     
    // Where does the destination need to be written to?
     //select_C: 0(IR26-22), 1(IR21-17), 2(LINK)
     select_C = 1; 
	  if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
	  end
  end  
			/*_____________________(SUB)________________________
			(SUB)DESCRIPTION:
			 (1.) Subtraction
			____________________________________________________*/
  17'b0000_0000100_000000:
   begin
    // Instruction Format (a) Register-operand format
	  RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    //
    // What is put into LnB of the ALU?
     // select_B: 0(RB/From Register File), 1(ImmediateValue)
     select_B = 0;
     
    // What do we want to write back to the Register File?
     //select_Y: 0(RZ/ALU result), 1(Memory Data), 2 (Return Address)
     select_Y = 0; // RZ - ALU result
	  // Where does the destination need to be written to?
     //select_C: 0(IR26-22), 1(IR21-17), 2(LINK)
     select_C = 1;
    
    if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
	  end
     
    
	end
			/*_____________________(AnD)________________________
			(AnD)DESCRIPTION:
			 (1.) Bitwise AnD
			____________________________________________________*/
  17'b0000_0001000_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];

    // What is put into LnB of the ALU?
     // select_B: 0(RB/From Register File), 1(ImmediateValue)
     select_B = 0;
     
		// What do we want to write back to the Register File?
		//select_Y: 0(RZ/ALU result), 1(Memory Data), 2 (Return Address)
		select_Y = 0; // RZ - ALU result
    
		if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end
     
    // Where does the destination need to be written to?
     //select_C: 0(IR26-22), 1(IR21-17), 2(LINK)
     select_C = 1;
     /*AnD*/;// Bitwise AnD "camel_backed" to keep seperate from ADDITION
      end
/*OR*/
			/*_____________________(OR)________________________
			(OR)DESCRIPTION:
			 (1.) Bitwise OR
			____________________________________________________*/
  17'b0000_0001001_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    //
    // What is put into LnB of the ALU?
     // select_B: 0(RB/From Register File), 1(ImmediateValue)
     select_B = 0;
     
    // What do we want to write back to the Register File?
     //select_Y: 0(RZ/ALU result), 1(Memory Data), 2 (Return Address)
     select_Y = 0; // RZ - ALU result
    
		if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end
     
    // Where does the destination need to be written to?
     //select_C: 0(IR26-22), 1(IR21-17), 2(LINK)
     select_C = 1;
      end
			/*_____________________(NEG)________________________
			(NEG)DESCRIPTION:
			 (1.) (Two's) Complement  //To get the two's complement of a binary number,
			 the bits are inverted, or "flipped", by using the bitwise NOT operation;
			 the value of 1 is then added to the resulting value,
			 ignoring the overflow which occurs when taking the two's complement of 0.
			____________________________________________________*/     
  17'b0000_0001010_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    //
    // What is put into LnB of the ALU?
     // select_B: 0(RB/From Register File), 1(ImmediateValue)
     select_B = 0;
     
    // What do we want to write back to the Register File?
     //select_Y: 0(RZ/ALU result), 1(Memory Data), 2 (Return Address)
     select_Y = 0; // RZ - ALU result
    
		if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end
     
    // Where does the destination need to be written to?
     //select_C: 0(IR26-22), 1(IR21-17), 2(LINK)
     select_C = 1;
   end
			/*_____________________(XOR)________________________
			(XOR)DESCRIPTION:
			 (1.) Bitwise Exclusive OR // Exclusive OR is a logical operation that 
			 outputs true whenever both inputs differ (one is true, the other is false).
			____________________________________________________*/
  17'b0000_0001011_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    //
    // What is put into LnB of the ALU?
     // select_B: 0(RB/From Register File), 1(ImmediateValue)
     select_B = 0;

     
    // What do we want to write back to the Register File?
     //select_Y: 0(RZ/ALU result), 1(Memory Data), 2 (Return Address)
     select_Y = 0; // RZ - ALU result
    
		if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end
     
    // Where does the destination need to be written to?
     //select_C: 0(IR26-22), 1(IR21-17), 2(LINK)
     select_C = 1;
   end
			/*_____________________(COMP)________________________
			(COMP)DESCRIPTION:
			 (1.) Bitwise (One's) Complement
			____________________________________________________*/  
  17'b0000_0001100_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    //
    // What is put into LnB of the ALU?
     // select_B: 0(RB/From Register File), 1(ImmediateValue)
     select_B = 0;
     
    // What do we want to write back to the Register File?
     //select_Y: 0(RZ/ALU result), 1(Memory Data), 2 (Return Address)
     select_Y = 0; // RZ - ALU result
    
		if(timeStep == 5) 
		begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end
     
    // Where does the destination need to be written to?
     //select_C: 0(IR26-22), 1(IR21-17), 2(LINK)
     select_C = 1;
   end
			/*_____________________(LSR)________________________
			(LSR)DESCRIPTION:
			 (1.) Logical Shift Right. Shift One Bit Position Only
			____________________________________________________*/
  17'b0000_0010000_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    //
    // What is put into LnB of the ALU?
     // select_B: 0(RB/From Register File), 1(ImmediateValue)
     select_B = 0;
     
    // What do we want to write back to the Register File?
     //select_Y: 0(RZ/ALU result), 1(Memory Data), 2 (Return Address)
     select_Y = 0; // RZ - ALU result
    
		if(timeStep == 5) 
		begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end
     
    // Where does the destination need to be written to?
     //select_C: 0(IR26-22), 1(IR21-17), 2(LINK)
     select_C = 1;
   end 
			/*_____________________(ASR)________________________
			(ASR)DESCRIPTION:
			 (1.) Arithmetic Shift Right  // Arithmetic Shifts Sign Extend // Shift One Bit Position Only
			____________________________________________________*/
  17'b0000_0010001_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    //
    // What is put into LnB of the ALU?
     // select_B: 0(RB/From Register File), 1(ImmediateValue)
     select_B = 0;
     
    // What do we want to write back to the Register File?
     //select_Y: 0(RZ/ALU result), 1(Memory Data), 2 (Return Address)
     select_Y = 0; // RZ - ALU result
    
		if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end
     
    // Where does the destination need to be written to?
     //select_C: 0(IR26-22), 1(IR21-17), 2(LINK)
     select_C = 1;
   end
/*LSL_ASL*/  // LSL Is The Same As ASL , Back Fills With Zeros
			/*_____________________(LSL_ASL)________________________
			(LSL_ASL)DESCRIPTION:
			 (1.) Logical/Arithmatical Shift Left  // Shift One Bit Position Only
			____________________________________________________*/   
  17'b0000_0010011_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
    //
    // What is put into LnB of the ALU?
     // select_B: 0(RB/From Register File), 1(ImmediateValue)
     select_B = 0;
     
    // What do we want to write back to the Register File?
     //select_Y: 0(RZ/ALU result), 1(Memory Data), 2 (Return Address)
     select_Y = 0; // RZ - ALU result
    
		if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end
     
    // Where does the destination need to be written to?
     //select_C: 0(IR26-22), 1(IR21-17), 2(LINK)
     select_C = 1;  
   end
 
/*ROR*/
			/*_____________________(ROR)________________________
			(ROR)DESCRIPTION:
			 (1.) Rotate Right  // By One Bit Position
			____________________________________________________*/    
  17'b0000_0011001_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    //   
    // What is put into LnB of the ALU?
     // select_B: 0(RB/From Register File), 1(ImmediateValue)
     select_B = 0;
     
    // What do we want to write back to the Register File?
     //select_Y: 0(RZ/ALU result), 1(Memory Data), 2 (Return Address)
     select_Y = 0; // RZ - ALU result
    
		if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end
     
    // Where does the destination need to be written to?
     //select_C: 0(IR26-22), 1(IR21-17), 2(LINK)
     select_C = 1; 
   end
  
/*ROL*/
			/*_____________________(ROL)________________________
			(ROL)DESCRIPTION:
			 (1.) Rotate Left  // By One Bit Position
			____________________________________________________*/   
  17'b0000_0011010_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    //    
    // What is put into LnB of the ALU?
     // select_B: 0(RB/From Register File), 1(ImmediateValue)
     select_B = 0;

     
    // What do we want to write back to the Register File?
     //select_Y: 0(RZ/ALU result), 1(Memory Data), 2 (Return Address)
     select_Y = 0; // RZ - ALU result
    
		if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end
     
    // Where does the destination need to be written to?
     //select_C: 0(IR26-22), 1(IR21-17), 2(LINK)
     select_C = 1;
   end

/*MOVE*/
			/*_____________________(MOVE)________________________
			(MOVE)DESCRIPTION:
			 (1.) Move // Copy
			____________________________________________________*/   
  17'b0000_0100000_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    
    select_B =0;   // No Immediate block
    select_Y =0;   // No memory operation
    select_C =1; // The values of Rscr1 is copied into Rdst and in format A, Rdst is in IR(21-17)
    
		if(timeStep == 5) begin //This is the 'MOVE' Instruction
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end     
	end
/*LBI*/
			/*_____________________(LBI)________________________
			(LBI)DESCRIPTION:
			 (1.) Load Base With Index
			In the case of a Load instruction, the data read from the memory are selected by multiplexer
			MuxY and placed in register RY, to be transferred to the register file in the next clock cycle.

			For Load and Store instructions, the effective address of the memory operand is computed by the
			ALU in step 3 and loaded into register RZ. From there, it is sent to the memory,which is stage 4.
			Example:
			• Fetch the instruction from the memory.
			• Increment the program counter.
			• Decode the instruction to determine the operation to be performed.
			• Read register R7.
			• Add the immediate value X to the contents of R7.
			• Use the sum X + [R7] as the effective address of the source operand, and read the
			contents of that location in the memory.
			• Load the data received from the memory into the destination register, R5.
			____________________________________________________*/   
  17'b0000_0100001_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    //    
    Extend = 1; // Zero Extend for the Immediate value of base
    select_B =1; //The base is an immediate value which is loaded via MuxB  
    select_C =1; // The contents of EA is copied into Rdst and in format A, Rdst is in IR(21-17)
    
    case(timeStep)
     4:  
	  begin
			MemRead_orWrite_L=1;  // To read the content of EA from memory in stage 4
			select_MA = 0; //We have to calculate effective address in the ALU and put it through RZ
			select_Y =1; //Since we are getting data from memory 
	  end
	  5:
	  begin
		  RF_write=1;  // We have to write the value in Rdst
		  select_Y =0; //Since we are getting data from memory
	  end
    endcase
   end  
/*LDRI*/
			/*_____________________(LDRI)________________________
			(LDRI)DESCRIPTION:
			 (1.) Load Register Indirect
			In the case of a Load instruction, the data read from the memory are selected by multiplexer
			MuxY and placed in register RY, to be transferred to the register file in the next clock cycle.

			For Load and Store instructions, the effective address of the memory operand is computed by the
			ALU in step 3 and loaded into register RZ. From there, it is sent to the memory, which is stage 4.
			____________________________________________________*/
  17'b0000_0100010_000000:
   begin
    // Instruction Format (a) Register-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22];
     //RFc_address = instruction[21:17];
    //    
    select_B =0; // we dont need MuxB since RB is not being used  
    select_C =1; // Rscr2 contains a pointer to Rdst and in format A, Rdst is in IR(21-17)
		// What OP will be performed by the ALU?
	   // ALU OP codes are numbered sequentially and should be obtained from ALU.v
    case(timeStep)
     4:  
	  begin
			select_MA = 0; // The data is read from address in RZ
			MemRead_orWrite_L=1;  // To read the content of EA from memory in stage 4
			select_Y =1; //Since we are getting data from memory 
	  end
     5:  RF_write=1;  // We have to write the value to Rdst   
    endcase
   end
/*JMP*/
					/*_____________________(JMP)________________________
					(JMP)DESCRIPTION:
						(1.) Jump //Place the contents of Rsrc1 into the PC
					____________________________________________________	
					(JMP)RTL EQUIVELENT:
						(1.) RZ<-0 //Treat as NOP in ALU
							//DO NOT WRITE BACK... // ADD CONTROL SIGNAL RF_WILL_WRITE and send to StageTracker
						(2.) PC<-Rsrc1	
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) NONE
					____________________________________________________*/
17'b0000_1000000_000000: 
	begin				
		// Instruction Format (a) Register-operand format
		RFa_address = instruction[31:27];
		RFb_address = instruction[26:22];
		RF_write=0; // Will not write result
		enable_RZ=0; // ALU result does not go into 
		

		if(timeStep == 3) //executeStage
		begin
			PC_enable = 1;
			select_PC=0; // PC <=RA
		end
		if(timeStep == 4) //memoryStage
		begin
			PC_enable = 0;
			select_PC=1; // PC <=Memory
		end
	end
/*JSR*/
					/*_____________________(JSR)________________________
					(JSR)DESCRIPTION:
						(1.) Jump to Subroutine //Address of subroutine in Register Rsrc1, 
														//store return address in LINK register, 
														//which is always R30.
					____________________________________________________	
					(JSR)RTL EQUIVELENT:
						(1.) RZ<-0 //Treat as NOP in ALU
							//DO NOT WRITE BACK... // ADD CONTROL SIGNAL RF_WILL_WRITE and send to StageTracker
							//But Write Return Address To Memory
						(2.) PC<-Rsrc1	
						(2.) R[30]<-Return_Address // Save PC_Temp
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) NONE
					____________________________________________________*/
17'b0000_1000001_000000: 
	begin
		// Instruction Format (a) Register-operand format
		RFa_address = instruction[31:27];
		RFb_address = instruction[26:22];
		enable_RZ=0; // ALU result does not go into RZ
		
		select_Y = 2; // RY <= PC_Temp
		select_C=2; // R30 (LINK) <= RY
		if(timeStep == 3) //executeStage
		begin
			PC_enable = 1;
			RF_write = 1; // R30 (LINK) <= RY  //DUCKTAPE
			select_PC=0; // PC <=RA
		end
		if(timeStep == 4) //memoryStage
		begin
			RF_write =0; // save PC temp
			PC_enable = 0;
			select_PC=1; // PC <=Memory
		end
		
					
	end
/*RTS*/
					/*_____________________(RTS)________________________
					(RTS)DESCRIPTION:
						(1.) Return from Subroutine //Rsrc1 contains the register number
															 // for the link register (R30).
															 // Rsrc1=30
					____________________________________________________	
					(RTS)RTL EQUIVELENT:
						(3.) RZ<-0	//Treat as ALU NOP
						(4.) PC<-[RA]// [LINK] // Return Address(Stage 4)
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) NONE
					____________________________________________________*/
17'b0000_1000011_000000: 
	begin	
		// Instruction Format (a) Register-operand format
		RFa_address = 5'd30; // RA <= R30 (LINK)
		RFb_address = instruction[26:22];

		enable_RZ=0; // ALU result does not go to RZ
		if(timeStep == 4) //executeStage
		begin
			PC_enable = 1;	
			select_PC = 0; // PC <= RA
		end
		if(timeStep == 5) //memoryStage
		begin
			PC_enable = 0;
			select_PC = 1;

		end
	end
  //17'bxxxx_xxxxxxx_		//Instruction Format (b) Immediate-operand format
			  /*LD#*/
			/*_____________________(LD#)________________________
			(LD#)DESCRIPTION:
			 (1.) Load Immediate  The value in the immediate field is sign extended and placed in the Rdst.
			In the case of a Load instruction, the data read from the memory are selected by multiplexer
			MuxY and placed in register RY, to be transferred to the register file in the next clock cycle.

			For Load and Store instructions, the effective address of the memory operand is computed by the
			ALU in step 3 and loaded into register RZ. From there, it is sent to the memory,which is stage 4.
			____________________________________________________*/
  17'bxxxx_xxxxxxx_100010:
   begin
    //Instruction Format (b) Immediate-operand format
	RFa_address = instruction[31:27];
	RFb_address = 5'd31;
    // Extract the correct Immediate Value
    // 0 (Sign Extend), 1 (Zero Extend): Defined in ImmeadiateBlock
	Extend = 0; // sign Extend
	select_B =1;  //To select the Immediate block as input to MuxB
	select_Y =0;  // No memory operation. RZ to RY
	select_C =0; // Dest Reg for (b) Immediate-operand format
    
    if(timeStep == 5) 
		begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end
   end
			/*LDU#*/
			/*_____________________(LDU#)________________________
			(LDU#)DESCRIPTION:
			 (1.) Load Unsigned Immediate
			In the case of a Load instruction, the data read from the memory are selected by multiplexer
			MuxY and placed in register RY, to be transferred to the register file in the next clock cycle.

			For Load and Store instructions, the effective address of the memory operand is computed by the
			ALU in step 3 and loaded into register RZ. From there, it is sent to the memory,which is stage 4.
			0010 0011
			____________________________________________________*/    
  17'bxxxx_xxxxxxx_100011:
   begin
    //Instruction Format (b) Immediate-operand format
     RFa_address = instruction[31:27];
     RFb_address = 5'd31;
     //RFc_address = instruction[26:22];
    // Extract the correct Immediate Value
    // 0 (Sign Extend), 1 (Zero Extend): Defined in ImmeadiateBlock
    Extend = 1; //zero extend
    select_B =1;  //To select the Immediate block as input to MuxB
    select_Y =0;  // No memory operation. RZ to RY
    select_C =0; // Dest Reg for (b) Immediate-operand format
    if(timeStep == 5) 
		begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end
   end
/*ADD #*/
					/*_____________________(ADD #)________________________
					(ADD #)DESCRIPTION:
						(1.) Add Immediate //The immediate value is sign extended	//format (b)
												 //and added to the contents of Rsrc.
												 //The result is stored in Rdst
					____________________________________________________	
					(ADD #)RTL EQUIVELENT:
						(1.) RZ<-RA+RB	//Treat as ALU Addition // Is a "2's comp addition" a subtraction ??????????????
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) OVERFLOW_FLAG
							// if((RA>0 && RB>0 && RZ<0)||(RA<0 && RB<0 && RZ>0)) //Overflow Occurs When (Adding) Two (Positives) And Get A (Negative) or (Adding) Two (Negatives) And Get A (Positive)
								// OVERFLOW_FLAG=1;
						(2.) ZERO_FLAG // Continuously Assigned Using An Internal Register
							//ZERO_FLAG=(RZ==0);
						(3.) NEGATIVE_FLAG
							//NEGATIVE_FLAG=RZ[31];
						(4.) CARRY_FLAG
							//CARRY_FLAG = (RA+RB)[32];
					____________________________________________________*/
17'bxxxx_xxxxxxx_000001: 
	begin
		//Instruction Format (b) Immediate-operand format
		RFa_address = instruction[31:27];
		RFb_address = 5'd31;	
		select_C =0; // Dest Reg for (b) Immediate-operand format
		
		Extend = 0;//Sign extend format b.	
		select_B =1; // Immeadiate value on ALU_LineB
		select_Y =0;  // No memory operation. RZ to RY
		if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end 
		
	end									
/*SUB #*/
					/*_____________________(SUB #)________________________
					(SUB #)DESCRIPTION:
						(1.) Subtract Immediate //The immediate value is sign extended
														// and subtracted from the contents of Rsrc.
														//The result is stored in Rdst
					____________________________________________________	
					(SUB #)RTL EQUIVELENT:
						(1.) RZ<-RA-RB	//Treat as ALU Subtraction // Is a "2's comp addition" a subtraction ??????????????
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) OVERFLOW_FLAG
							// if((RA>0 && RB<0 && RZ<0)||(RA<0 && RB>0 && RZ>0)) //Overflow Occurs When (Subtracting) A (Positive By A Negative) And Getting A (Negative) or (Subtracting) A (Negative By A Positive) And Getting A (Positive)
								//Then// OVERFLOW_FLAG=1;
						(2.) ZERO_FLAG // Continuously Assigned Using An Internal Register
							//ZERO_FLAG=(RZ==0);
						(3.) NEGATIVE_FLAG
							//NEGATIVE_FLAG=RZ[31];
						(4.) CARRY_FLAG
							//CARRY_FLAG = (RA-RB)[32];
					____________________________________________________*/				
17'bxxxx_xxxxxxx_000100: 
	begin
    //Instruction Format (b) Immediate-operand format
		RFa_address = instruction[31:27];
		RFb_address = 5'd31;
		select_C =0; // Dest Reg for (b) Immediate-operand format
		Extend = 0; // Sign Extend the Immediate Value	
		select_B =1; // Immeadiate value on ALU_LineB
		select_Y =0;  // No memory operation. RZ to RY
		
		if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end					
	end

/*AnD #*/
// Bitwise AnD Immediate "camel_backed" to keep seperate from ADDITION
					/*_____________________(AnD #)________________________
					(AnD #)DESCRIPTION:
						(1.) Bitwise AnD Immediate //The immediate value is padded with zeros
															//on the left and ANDed with the contents of Rsrc.
															//The result is place in Rdst
					____________________________________________________	
					(AnD #)RTL EQUIVELENT:
						(1.) RZ<- [RA]&[RB] //ALU Bitwise AnD
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) ZERO_FLAG // Continuously Assigned Using An Internal Register
							//ZERO_FLAG=(RZ==0);
					____________________________________________________*/	
17'bxxxx_xxxxxxx_001000: 
	begin
    //Instruction Format (b) Immediate-operand format
		RFa_address = instruction[31:27];
		RFb_address = 5'd31;		
		select_C =0; // Dest Reg for (b) Immediate-operand format
		Extend = 1; // Zero Extend the Immediate Value	 
			 
		select_B =1; // Immeadiate value on ALU_LineB
		select_Y =0;  // No memory operation. RZ to RY	
		
		
		if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end				
	end
	
/*OR #*/
					/*_____________________(OR #)________________________
					(OR #)DESCRIPTION:
						(1.) Bitwise OR Immediate 
					____________________________________________________	
					(OR #)RTL EQUIVELENT:
						(1.) RZ<- [RA]|[RB] //ALU Bitwise OR
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) NONE
					____________________________________________________*/				
17'bxxxx_xxxxxxx_001001: 
	begin
    //Instruction Format (b) Immediate-operand format
		RFa_address = instruction[31:27];
		RFb_address = 5'd31;
		select_B =1; // Immeadiate value on ALU_LineB
		select_Y =0;  // No memory operation. RZ to RY
		select_C =0; // Dest Reg for (b) Immediate-operand format
		Extend = 1; // Zero Extend the Immediate Value
		
		if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end					
	end

/*XOR #*/
					/*_____________________(XOR #)________________________
					(XOR #)DESCRIPTION:
						(1.) Bitwise Exclusive OR Immediate //Exclusive OR
																		// is a logical operation
																		// that outputs true whenever
																		// both inputs differ ie:
																		//(one is true, the other is false).
					____________________________________________________	
					(XOR #)RTL EQUIVELENT:
						(1.) RZ<- [RA]^[RB]
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) NONE
					____________________________________________________*/	
17'bxxxx_xxxxxxx_001011: 
	begin
    //Instruction Format (b) Immediate-operand format
		RFa_address = instruction[31:27];
		RFb_address = 5'd31;	
		select_C =0; // Dest Reg for (b) Immediate-operand format
		
		Extend = 1; // Zero Extend the Immediate Value
		select_B =1; // Immeadiate value on ALU_LineB
		select_Y =0;  // No memory operation. RZ to RY
	  if(timeStep == 5) begin
			//Do I need to write back to the Register File?
			RF_write = 1; //<-Depends on operations
		end				
	end

/*BEQ*/
					/*_____________________(BEQ)________________________
					(BEQ)DESCRIPTION:
						(1.) Branch if EQual //If the contents of the two registers 
													// are equal, add the 2's complement
													// immediate value to the PC
					____________________________________________________	
					(BEQ)RTL EQUIVELENT:
						(1.) RZ<-RA-RB //Treat As A Subtraction and Check The Zero Flag // ASSUMING UNSIGNED RA and RB...
						(2.) if(ZERO_FLAG==1)//RA=RB then RA-RB=0...
								PC<- PC+Immediate // Is a "2's comp addition" a subtraction ??????????????
					____________________________________________________

					____________________________________________________*/	
17'bxxxx_xxxxxxx_001100: 
	begin
	// Subtract(A-B) is performed in ALU
    //Instruction Format (b) Immediate-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22]; //Rdest for Instruction Format (b/c) Immediate-operand format		 
		if(ZERO == 1)// Result of A-B is 0 if A==B
			begin
				Extend = 1; // Sign(0) Zero(1) for format (b)
				if(timeStep == 4) //memoryStage
				begin
					PC_enable = 1;
					select_INC = 1; // Branch Offset of Immediate value
				end	
				if(timeStep == 5) //memoryStage
				begin
					PC_enable = 0;
					select_INC = 0;
				end
			end
	end
/*BNE*/
					/*_____________________(BNE)________________________
					(BNE)DESCRIPTION:
						(1.) Branch if Not Equal //If the contents of the two registers 
														 // are NOT equal, add the 2's complement
														 // immediate value to the PC
					____________________________________________________	
					(BNE)RTL EQUIVELENT:
						(1.) RZ<-RA-RB  //Treat As A Subtraction and Check The Zero Flag // ASSUMING UNSIGNED RA and RB...
						(2.) if(ZERO_FLAG==0)//RA!=RB then RA-RB!=0...
								PC<- PC+Immediate // Is a "2's comp addition" a subtraction ??????????????
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) OVERFLOW_FLAG
							// if((RA>0 && RB<0 && RZ<0)||(RA<0 && RB>0 && RZ>0)) //Overflow Occurs When (Subtracting) A (Positive By A Negative) And Getting A (Negative) or (Subtracting) A (Negative By A Positive) And Getting A (Positive)
								//Then// OVERFLOW_FLAG=1;
						(2.) ZERO_FLAG // Continuously Assigned Using An Internal Register
							//ZERO_FLAG=(RZ==0);
						(3.) NEGATIVE_FLAG
							//NEGATIVE_FLAG=RZ[31];
						(4.) CARRY_FLAG
							//CARRY_FLAG = (RA-RB)[32];
					____________________________________________________*/	
17'bxxxx_xxxxxxx_001010: 
	begin
	// Subtract (A-B) is performed in ALU
    //Instruction Format (b) Immediate-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22]; //Rdest for Instruction Format (b/c) Immediate-operand format			
		if(ZERO == 0)// Result of A-B is 1 if A!=B
			begin
				Extend = 0; // Sign(0) Zero(1) for format (b)
				if(timeStep == 4) //memoryStage
				begin
					PC_enable = 1;
					select_INC = 1; // Branch Offset of Immediate value
				end	
				if(timeStep == 5) //memoryStage
				begin
					PC_enable = 0;
					select_INC = 0;
				end
			end	
	end

/*BLT*/
					/*_____________________(BLT)________________________
					(BLT)DESCRIPTION:
						(1.) Bacon Lettuce Tomato // If the unsigned contents of
							  Branch if Less Than  // Rsrc is less than the contents of Rdst,
														  //add the 2's complement immediate value to the PC
					____________________________________________________	
					(BLT)RTL EQUIVELENT:
						(1.) RA<-RSRC1
						(2.) RB<-RSRC2
						(3.) RZ<-RA-RB //Treat As A Subtraction and Check The Zero Flag // ASSUMING UNSIGNED RA and RB...
						(4.) if(NEGATIVE_FLAG==1) //RA<RB then RA-RB=(-)|x|
								PC<-PC+IMMEDIATE_BLOCK_OUT // Is a "2's comp addition" a subtraction ??????????????
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) OVERFLOW_FLAG
							// if((RA>0 && RB<0 && RZ<0)||(RA<0 && RB>0 && RZ>0)) //Overflow Occurs When (Subtracting) A (Positive By A Negative) And Getting A (Negative) or (Subtracting) A (Negative By A Positive) And Getting A (Positive)
								//Then// OVERFLOW_FLAG=1;
						(2.) ZERO_FLAG // Continuously Assigned Using An Internal Register
							//ZERO_FLAG=(RZ==0);
						(3.) NEGATIVE_FLAG
							//NEGATIVE_FLAG=RZ[31];
						(4.) CARRY_FLAG
							//CARRY_FLAG = (RA-RB)[32];
					____________________________________________________*/					
17'bxxxx_xxxxxxx_001111: 
	begin
	//Subtract A-B
    //Instruction Format (b) Immediate-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22]; //Rdest for Instruction Format (b/c) Immediate-operand format
		if(NEG == 0)// Result of A-B is NEG only if A<B
			begin
				Extend = 0; // Sign(0) Zero(1) for format (b)
				if(timeStep == 4) //memoryStage
				begin
					PC_enable = 1;
					select_INC = 1; // Branch Offset of Immediate value
				end	
				if(timeStep == 5) //memoryStage
				begin
					PC_enable = 0;
					select_INC = 0;
				end
			end	
					
	end
				
/*LDA*/
					/*_____________________(LDA)________________________
					(LDA)DESCRIPTION:
						(1.) Load Absolute // The immediate value is zero-filled
												 // to the left and used as an address. 
												 // Rdst is then loaded from this address.
												 // This requires the adder in Figure 5.10
												 // to be able to just pass the immediate value
												 // through (without adding to the PC),
												 // which requires an additional control line
												 // that is not implied by Figure 5.10
					____________________________________________________	
					​i think Load absolute would just take the immediate value from 
					muxb pass through the alu and to Memory address, then y_select 
					would get set to "1" storing the contents of memory into RY.
					for this instruction i would also set PC = Pc Temp so that PC 
					"didn't change"
					
					(LDA)RTL EQUIVELENT:
						(0.) NOTE We Used RZ as the address instead of loading PC with the immediate value.
						(1.) RZ<-RB //Treat as LOAD in ALU
							//DO NOT WRITE BACK... // ADD CONTROL SIGNAL RF_WILL_WRITE and send to StageTracker
						(2.) PC<-{6'b000000,Instruction[31:6]} // Via MuxPC
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) NONE
					____________________________________________________*/									
17'bxxxx_xxxxxxx_100000: 
	begin
    //Instruction Format (b) Immediate-operand format
		RFa_address = instruction[31:27];
		RFb_address = instruction[26:22];
		Extend = 3;//sign(2), zero(3) Extend for Instruction Format (c)
		select_B =1;	// Alu_LineB = Immeadiate Value
		select_Y =1;  // Memory Read. RY <= MemData
		
		//if(timeStep==3)
			//enable_RM = 1; // RM <= RB		WHY, its a load
		if(timeStep==4)
		begin
			//enable_RM = 0; // RM <= RB	 WHY, its a load
			select_MA = 0;
			MemRead_orWrite_L =1; //Read Memory in stage 4
		end
		if(timeStep==5)
			RF_write = 1;	
	end

/*STA*/
					/*_____________________(STA)________________________
					(STA)DESCRIPTION:
						(1.) STore Absolute // The immediate value is zero-filled
												  // to the left and used as an address.
												  // Rdst (yes, Rdst!) is then stored to
												  // this address. Requires modifications
												  // similar to the LDA instruction
					____________________________________________________	
					(STA)RTL EQUIVELENT:
						(1.) RZ<- RB // {6'b0,Instruction[31:6]} // ALU Pass RB just Like other Loads...
						(2.) (RZ) <-[RM] // RDST...
					____________________________________________________*/	
17'bxxxx_xxxxxxx_010000: 
	begin
    //Instruction Format (b) Immediate-operand format
		RFa_address = instruction[26:22];
		RFb_address = instruction[26:22]; //Instruction Format (b/c) Immediate-operand format
		Extend = 3;//sign(2), zero(3) Extend for Instruction Format (c)
		select_B =1; // Immeadiate value on ALU_LineB
		Extend = 3;//sign(2), zero(3) Extend for Instruction Format (c)		
		enable_RY = 0; // No Memory Read
		
		if(timeStep==3)
			enable_RM = 1; // RM <= RB	
		if(timeStep==4)
		begin
			select_MA = 0; //DataAddress = RZ
			MemRead_orWrite_L=0;
		end		
	end

/*LDIX*/
					/*_____________________(LDIX)________________________
					(LDIX)DESCRIPTION:
						(1.) LoaD IndeXed // The unsigned immediate value is added
												// to the contents of Rsrc to obtain the EA.
												// Rdst is then loaded from the memory location EA
					____________________________________________________	
					(LDIX)RTL EQUIVELENT:
						(1.) RZ<-RA+RB //Treat like an ADD //{6'b000000,Instruction[31:6]}
						(2.) RY<-MEMORY //EA=[RZ]
					____________________________________________________*/	
17'bxxxx_xxxxxxx_100001: 
	begin
    //Instruction Format (b) Immediate-operand format
		RFa_address = instruction[31:27];
		RFb_address = 5'd31;
		select_B = 1; //Immeadiate Value
		Extend = 1; // Zero extend for (b) Immediate-operand format 
		select_Y = 1;	//RY <= Memory
		if(timeStep == 4)
		begin
			select_MA = 0; // Data Address is set by RZ
			MemRead_orWrite_L=1; // Read data
		end
		if(timeStep==5)
		begin
			RF_write =1;
		end
	end
					
/*STIX*/
					/*_____________________(STIX)________________________
					(STIX)DESCRIPTION:
						(1.) STore IndeXed // The unsigned immediate value is added
											    // to the contents of Rsrc to obtain the EA.
												 // Rdst is then stored to the memory location EA
					____________________________________________________	
					(STIX)RTL EQUIVELENT:
						(1.) RZ<- RA + RB // TREAT AS ADD // RA+{6{Instruction[31]},Instruction[31:6]} 
						(2.) (RZ)<-[RM] // RDST...
					____________________________________________________*/									
17'bxxxx_xxxxxxx_010001: 
	begin
    //Instruction Format (b) Immediate-operand format
     RFa_address = instruction[31:27];
     RFb_address = instruction[26:22]; //Instruction Format (b/c) Immediate-operand format	
		select_B = 1; //Immeadiate Value
		enable_RY = 0; // No Memory Read
		Extend = 1; // Zero extend for (b) Immediate-operand format 
		if(timeStep==3)
		begin
			enable_RM = 1; // RM <= RB
			select_MA = 0; // Data Address is set by RZ
		end
		if(timeStep == 4)
		begin
			select_MA = 0; // Data Address is set by RZ
			MemRead_orWrite_L=0; // Write data
		end							
	end
/*BRA*/
					/*_____________________(BRA)________________________
					(BRA)DESCRIPTION:
						(1.) Unconditional Branch // Add the 2's complement immediate value to the PC.
														  // (wider address range than Bxx, probably more than
														  // we'll actually be able to test)
															
					____________________________________________________	
					(BRA)RTL EQUIVELENT:
						(1.) RZ<- 0 //Treat as NOP
						(2.) PC<- PC+IMMEDIATE_BLOCK_OUT // Is a "2's comp addition" a subtraction ??????????????
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) NONE
					____________________________________________________*/
17'bxxxx_xxxxxxx_110000: 
	begin
    //Instruction Format (c)
     RFa_address = instruction[31:27];
     RFb_address = 5'd31;	
		
		Extend = 0; // Sign(0) Zero(1) for format (b)	
		if(timeStep == 4) //memoryStage
		begin
			PC_enable = 1;
			select_INC = 1; // Branch Offset of Immediate value
		end	
		if(timeStep == 5) //memoryStage
		begin
			PC_enable = 0;
			select_INC = 0;
		end	
		
	end
/*BSR*/
					/*_____________________(BSR)________________________
					(BSR)DESCRIPTION:
						(1.) Unconditional Branch to SubRoutine // Add the 2's complement immediate value to the PC
																			 // and store return address in the LINK register,
																			 // which is always R30.// We HAVE TO REMEMBER R30 IN OUR INSTRUCTION!!!!!!!!!!!!!!!
																			
					____________________________________________________	
					(BSR)RTL EQUIVELENT:
						(1.) RZ<- 0 // ALU NOP 
							//But Still Write Back LINK REGISTER To Reg File RF_WRITE 
						(2.) PC<- PC+IMMEDIATE_BLOCK_OUT 
						(3.) RY<- Return_Address //Via Y_Select // PC_Temp // LINK REGISTER = RDST = 30;
					____________________________________________________
					FLAGS TO UPDATE FOR THIS OPPERATION:
						(1.) NONE
					____________________________________________________*/				
17'bxxxx_xxxxxxx_110001: 
	begin
    //Instruction Format (c) 
     RFa_address = instruction[31:27];
     RFb_address = 5'd31;
	  //#TODO
	  // Should the branch offset be magnitude addition or two's comp addition.
		Extend =3;//sign(2), zero(3) Extend for Instruction Format (c) 
		
		select_Y = 2;	// get PC_temp into RY
		select_C =2; // Set the destination register as R30
	if(timeStep==3)
	begin
		RF_write =1; // save PC temp
	end
	if(timeStep == 4) //memoryStage
		begin
			PC_enable = 1;
			select_INC = 1; // Branch Offset of Immediate value
			RF_write =0; // save PC temp
		end	
		if(timeStep == 5) //memoryStage
		begin
			PC_enable = 0;
			select_INC = 0;
		end
	end
/*LDU#*/
					/*_____________________(LR31#1)________________________
					(LDU#)DESCRIPTION:
					 (1.) Load 1 into R31
					In the case of a Load instruction, the data read from the memory are selected by multiplexer
					MuxY and placed in register RY, to be transferred to the register file in the next clock cycle.

					For Load and Store instructions, the effective address of the memory operand is computed by the
					ALU in step 3 and loaded into register RZ. From there, it is sent to the memory,which is stage 4.
					____________________________________________________*/
17'b0000_0000000_000000:
	begin
		// STRICTLY A DEBUG CASE (EMPTY MIF or all 0 instruction)
		  RFa_address = 5'd31;
		  RFb_address = 5'd31;
		  //RFc_address = instruction[26:22];
		 // Extract the correct Immediate Value
		 // 0 (Sign Extend), 1 (Zero Extend): Defined in ImmeadiateBlock
		 Extend = 1; //zero extend
		 select_B =3;  //To select the constant #32'd1
		 select_Y =0;  // No memory operation
		 select_C =3; 	// Load into R31
		 case(timeStep) //This is the 'Load Immediate' Instruction
		  5:  RF_write=1;  // We have to write the value in Rdst
		 endcase
	end	
			/*ERROR*/
			/*_____________________(ERROR)______________________
			(ERROR)DESCRIPTION:
			 (1.) INSTRUCTION NOT RECOGNIZED
			____________________________________________________*/
  default:
	begin
		;
	end
 endcase
 end // Right after casex(instruction[17:0]);
endmodule
