//ImmediateBlock
//Written by: Drake Jeno
//Edited By TEAM GAMMA - Jordan D. Ulmer And Patrick Schroeder
//11/01/2014
	// Added INSTRUCTION FORMATS:
	// Instruction Format (a) (RSRC1[31:27])(RSRC2[26:22])(RDST[21:17])(OPCODE[16:0])
	// Instruction Format (b) (RSRC1[31:27])(RDST[26:22])(IMMEDIATE_OPPERAND[21:6])(OPCODE[5:0])
	// Instruction Format (c) (IMMEDIATE_VALUE[31:6])(OPCODE[5:0])
module ImmediateBlock (IR, Extend, ImmediateBlock_Out);
	input [1:0] Extend;
	input [31:0] IR;
	output reg [31:0] ImmediateBlock_Out;// To MUX_C and To MUX_INC

always @(Extend, IR)
	begin
		case(Extend)
			//In Instruction Format (a) We Do Not Look at ImmediateBlock_Out
			0: ImmediateBlock_Out[31:0] <= {{16{IR[21]}}, IR[21:6]};	//sign Extend // Instruction Format (b) (RSRC1[31:27])(RDST[26:22])(IMMEDIATE_OPPERAND[21:6])(OPCODE[5:0])
			1: ImmediateBlock_Out[31:0] <= {16'd0, IR[21:6]};			//zero Extend // Instruction Format (b) (RSRC1[31:27])(RDST[26:22])(IMMEDIATE_OPPERAND[21:6])(OPCODE[5:0])
			2: ImmediateBlock_Out[31:0] <= {{6{IR[31]}}, IR[31:6]};	//sign Extend // Instruction Format (c) (IMMEDIATE_VALUE[31:6])(OPCODE[5:0])
			3: ImmediateBlock_Out[31:0] <= {6'd0, IR[31:6]};			//zero Extend // Instruction Format (c) (IMMEDIATE_VALUE[31:6])(OPCODE[5:0])
	default:	ImmediateBlock_Out[31:0] <= 32'hFFFFFFFF; // All F To Help Indicate An Error
		endcase
	end

endmodule

/*
Before 11/03/2014
	Initial ImmediateBlock posted on OneDrive. This was found to have extra components. Decided to replace this with the ImmeadiateBlock created by Jordan.
//Written by: Drake Jeno

module ImmediateBlock (address, RZ, PC, MA_select, IR_Enable, Extend, Immediate_Out, Immediate_In, IR, MEM_Write, MEM_Read, RM);
	input IR_Enable, MA_select, MEM_Write, MEM_Read;
	input [1:0] Extend;
	input [31:0] RZ, PC, RM, Immediate_In;
	output reg [31:0] Immediate_Out, IR;
	output reg [31:0] address;
	wire [31:0] NewIR;
	
	
always@(MA_select)
begin
	
	if(MA_select == 0)
		address = RZ;
	else	
		address = PC;
	end
always@(Extend)
begin
	if(Extend == 2'b01)
		Immediate_Out = {{16{Immediate_In[21]}}, Immediate_In[21:6]};	//sign Extend
	else if(Extend == 2'b10)
		Immediate_Out = {16'b0, Immediate_In[21:6]};			//zero Extend
	else if(Extend == 2'b11)
		Immediate_Out = {PC[31:28], Immediate_In[31:6], 2'b00};		//4 most sig bits of PC on left & 2 zeros on right
	else
		Immediate_Out = {{16{Immediate_In[21]}}, Immediate_In[21:6]};	//sign Extend by default
end
endmodule
*/
	