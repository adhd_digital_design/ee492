module ALU (ALUINA, ALUINB, ALU_OP, ALUOUT, CCR_old, CCR_new);
input wire [31:0] ALUINA, ALUINB;
input [16:0] ALU_OP;
reg [31:0] TempRegAll;
output reg [31:0] ALUOUT; 
output reg [31:0] CCR_new;
input [31:0] CCR_old;
// [0] = carry flag, [1] = V, [2] = z, [3] = N, [4] = C
//	CARRY_FLAG 		= CCR[0];
//	OVERFLOW_FLAG 	= CCR[1];
//	ZERO_FLAG 		= CCR[2];CCR_new[2]
//	NEG_FLAG 		= CCR[3]; CCR_new[3]
//[5:31] = empty
always@(*)
begin
	casex(ALU_OP)
		17'bXXXXXXXXXXX111111, 		//NOP	  No OPeration
		17'b0000_1000011_000000, 	// RTS
		17'b0000_1000001_000000, 	//JSR
		17'b0000_1000000_000000, 	//JMP
		17'bxxxx_xxxxxxx_110000,	//BRA
		17'bxxxx_xxxxxxx_110001:  	//BSR
			//CCR_new = CCR_old;
			;
		default:
			begin
				if((ALUINB[31:0] - ALUINA[31:0]) == 32'b0000_0000_0000_0000_0000_0000_0000_0000)begin
					CCR_new[2] = 1;
					end
				else if(ALUOUT==32'd0)
					CCR_new[2] = 1;
				else begin
					CCR_new[2] = 0;
					end
				if(ALUOUT[31] ==1 ) //Negative
					CCR_new[3] = 1;
				else
					CCR_new[3] = 0;
			end
	endcase
end

always@(*)
begin
		//Format A
		//Arithmetic
	casex(ALU_OP)
		17'b00000000001000000,		//ADD
		17'bxxxx_xxxxxxx_000001, 	//ADD #
		17'b0000_0100001_000000, 	// LBI	The Add operation is done between base & index and '0' selects the Add operation in ALU
		17'bxxxx_xxxxxxx_100001,   // LDIX
		17'bxxxx_xxxxxxx_010001:   //STIX
			begin
			//Can set carry bit
			{CCR_new[0],ALUOUT[31:0]} = ALUINA[31:0] + ALUINB[31:0];   		
			end
		17'b00000000100000000,		//SUB A-B
		17'bxxxx_xxxxxxx_000100,   //sub # (A-Immediate)
		17'bxxxx_xxxxxxx_001100,   //BEQ  zero flag 1  
		17'bxxxx_xxxxxxx_001010:   //BNE  zero flag 0 
		
			begin
			//Can set carry bit, and zero flag
			ALUOUT[31:0] = ALUINA[31:0] - ALUINB[31:0];
//					if(ALUOUT == 32'b0) begin
//						CCR_new[2] = 1;
//					end
//					if(ALUINA > ALUINB) begin // if a is smaller than b
//						CCR_new[3] = 1;
//					end
			end
		17'bxxxx_xxxxxxx_001111:	//BLT  negative flag 1
			ALUOUT[31:0] = ALUINB[31:0] - ALUINA[31:0]; //DUCKTAPE
		17'b00000001000000000,			//AND
		17'bxxxx_xxxxxxx_001000: 		//AND #
			ALUOUT[31:0] = ALUINA[31:0] & ALUINB[31:0]; 	  					
		17'b00000001001000000,			//OR
		17'bxxxx_xxxxxxx_001001: 		//OR#
			ALUOUT[31:0] = ALUINA[31:0] | ALUINB[31:0];   					
		17'b00000001010000000:			//NEG
			begin
				 ALUOUT[31:0] = ~ALUINA[31:0];
				 ALUOUT[31:0] = ALUOUT[31:0] + 1;
			end	
		17'b00000001011000000,			//XOR
		17'bxxxx_xxxxxxx_001011:		//XOR# 
			ALUOUT[31:0] = ALUINA[31:0] ^ ALUINB[31:0];      				
		17'b00000001100000000:
			ALUOUT[31:0] = ~ALUINA[31:0];     		//1's Complement
									 
		//Shift/Rotate		
		
		17'b00000010000000000:		//LSR	  Logical Shift Right (shift one bit position only)
			begin		
				 {ALUOUT[30:0],CCR_new[0]} <= ALUINA[31:0];
				 ALUOUT[31] <= 1'b0;
			end
		17'b00000010001000000:		//ASR	  Arithmetic Shift Right (shift one bit position only)
			begin	   
				 {ALUOUT[30:0],CCR_new[0]} <= ALUINA[31:0];
				 ALUOUT[31] <= ALUOUT[30];
			end
		17'b00000010011000000:
	 //17'b00000010011000000:;		      //ASL	  Arithmetic Shift Left (shift one bit position only) 
			begin						//LSL	  Logical Shift Left (shift one bit position only)
		 
									 {CCR_new[0],ALUOUT[31:1]} <= ALUINA[31:0];
									 ALUOUT[0] <= 1'b0; 
			end
		17'b00000011001000000:begin		//ROR	  Rotate Right (by one bit position) rotates through the carry flag
									 ALUOUT[31] = CCR_old[0]; 
									 {ALUOUT[30:0],CCR_new[0]} = ALUINA[31:0];
									 end
		17'b00000011010000000:begin		//ROL	  Rotate Left (by one bit position)
									 ALUOUT[0] = CCR_old[0];
									 {CCR_new[0],ALUOUT[31:1]} = ALUINA[31:0];
									 end
		//Load/copy		
		
		17'b00000100000000000,		//MOVE  (Copy)
		17'b0000_0100010_000000: 	//LDRi  Load Register Indirect
			ALUOUT[31:0] = ALUINA[31:0];		
			
		//17'b00000100001000000:
		//	{CCR_new[0],ALUOUT} <= ALUINA[31:0] + ALUINB[31:0];		//LBI	  Load Base with Index	
		
		17'bXXXXXXXXXXX111111, 		//NOP	  No OPeration
		17'b0000_1000011_000000, 	// RTS
		17'b0000_1000001_000000, 	//JSR
		17'b0000_1000000_000000, 	//JMP
		17'bxxxx_xxxxxxx_110000:  	//BRA/BSR
			;
		//Format B Loads		
		17'bXXXXXXXXXXX100010,  	//LD #	Load immediate Deal with the extend bits in control logic < and V
		17'bXXXXXXXXXXX100011,		//LDU #	Load unsigned immediate
		17'b0000_0000000_000000,  	//DEBUG LOAD 1 into R31 if MIF is done (instruction is 0)
		17'bxxxx_xxxxxxx_100000,   //LDA	
		17'bxxxx_xxxxxxx_010000:	//STA  
			ALUOUT[31:0] <= ALUINB[31:0];
			
		//17'b0000_0000000_000000:
		//	ALUOUT = 32'b1; //LR31#1
		default:
			;//no op default 
	endcase
end
endmodule


/*
//ALU - Arithmatical Logical Unit
module ALU(
input [7:0] op,
input [31:0] A, B,
output wire[31:0] Z ,CCR
);
reg ZERO_FLAG, OVERFLOW_FLAG, NEG_FLAG, CARRY_FLAG;
reg [31:0] out; assign Z = out;

always @(op, A, B)
begin
	case(op)
		32'd0: //ADD A+B				
			{OVERFLOW_FLAG, out} = A+B;
		32'd1: //SUB B-A
			{OVERFLOW_FLAG, out} = B-A;
		32'd2://SUB A-B
			{OVERFLOW_FLAG, out} = A-B;
		32'd3: //AND (Bitwise) A&B
			out = A&B;
		32'd4: //OR (Bitwise) A|B
			out = A|B;
		32'd5: //NEG = 2's complement of A
			begin
			out = ~A;
			out = out+1;
			end
		32'd6: //XOR = exclusive or
			out = A^B;
		32'd7: //COMP = the Ones's complement 
			out = ~A;
		32'd8: //MOVE = copy
			out = A;
//Shifts
		32'd9: //Shift left
			begin
			CARRY_FLAG = A[31];
			out = (A<<1);
			out[0] = 0;
			end
		32'd10://Arithmatical Shift Left
			begin
			CARRY_FLAG = A[31];
			out = (A<<1);
			out[0] = 0;
			//CARRY_FLAG = A[31];
			//out = (A<<<1);
			end
		32'd11://Rotate Left 
			begin
			CARRY_FLAG = out[31]; //Save the farthest left;
			out =(A<<<1);	
			out[0] = CARRY_FLAG; // And put it on the farthest right
			end
		32'd12: //Logical Shift Right
			begin
			CARRY_FLAG = A[0];
			out = (A>>1);
			out[31] = 0;
			end
		32'd13://Arithmatical Shift Right 
			begin
			CARRY_FLAG = A[0];
			out = (A>>>1);
			out[31] = out[30];
			end
		32'd14://Rotate Right 
			begin
			CARRY_FLAG = out[0]; //Save the farthest right;
			out = (A>>>1);	
			out[31] = CARRY_FLAG; // And put it on the farthest 
			end
		32'd15://OP_NOP
			out = 0;//Do nothing
		32'd16: // Pass A to Z
			out = A;
		32'd17: // Pass B to Z
			out = B;
		default://OP_NOP
			out = 0;//Do nothing
	endcase
end // END computational always

always @(out)
begin
	if(op != 15)// OP_NOP
		begin
		ZERO_FLAG = (out == 32'd0);
		NEG_FLAG = out[31]; //(out < 32'd0); // I don't think it can tell if its negative.
		end
	else //Do not change for NOP
		begin
			ZERO_FLAG = ZERO_FLAG;
			NEG_FLAG = NEG_FLAG;
		end
end



endmodule //END MODULE



0000_0000001_000000   	ADD	
0000_0000100_000000	SUB	
0000_0001000_000000   	AND	
0000_0001001_000000   	OR	

0000_0001010_000000   	NEG	
0000_0001011_000000  	 XOR	
0000_0001100_000000   	COMP	
0000_0010000_000000	LSR	Logical Shift Right (shift one bit position only)
0000_0010001_000000	ASR	Arithmetic Shift Right (shift one bit position only)
0000_0010011_000000	LSL	Logical Shift Left (shift one bit position only)
0000_0010011_000000	ASL	Arithmetic Shift Left (shift one bit position only)
0000_0011001_000000	ROR	Rotate Right (by one bit position)
0000_0011010_000000	ROL	Rotate Left (by one bit position)
0000_0100000_000000	MOVE	(Copy)
0000_0100001_000000	LBI	Load Base with Index
0000_0100010_000000	LDRi	Load Register Indirect
XXXX_XXXXXXX_111111	NOP	No OPeration
		
100010	LD #	Load immediate
100011	LDU #	Load unsigned immediate

*/