//Authors: Jordan D. Ulmer , Nathan Genetzky
//Purpose: Monitor the output of a LFSR.
//Date Created : 2017-01-27

// Timescale
`timescale 1ms/1ns

// //TODO : USE UVM - http://www.verificationguide.com/p/uvm-monitor.html
// https://www.vmmcentral.org/uvm_vmm_ik/files3/reg/uvm_mem-svh.html
// class lfsr_monitor#(parameter STAGES=8); // [ab72ff2-jdulmer-2017-01-2017] // (vlog-2894) Class 'lfsr_monitor' is parameterized.  Parameterized nested classes currently not supported.
class lfsr_monitor;
localparam STAGES=8;
localparam EXPECTED_NUM_INST_BEFORE_SEQ_REPEAT=2**8;
// localparam MSB_SR=(2**STAGES)*STAGES;

// Member Variables
int index_counter;
// reg [MSB_SR-1:0] previous_lfsr_values;
reg [STAGES-1:0] previous_lfsr_values[$];
reg [STAGES-1:0] check_value;


 
function new();
    // previous_lfsr_values = 0;
    index_counter = 0;
endfunction

// //TODO :  write a loop that stores 255 values and then restarts at 0 and checks that the values are the same...
task run_phase(reg [STAGES-1:0] current_value);
    // Check Value         // ORDER MATTERS - Check 1st
    check_for_repeats(current_value);
    
    // Update Array Value  // ORDER MATTERS - UPDATE 2nd
    previous_lfsr_values.push_front(current_value);// Shift current value on queue
    $display("No Duplicates Found: New LFSR Value [0x%h] -- [%d] Unique Values Generated", current_value, previous_lfsr_values.size());
    

    index_counter = index_counter + 1;   // Post Increment

endtask

task check_for_repeats(reg [STAGES-1:0] current_value);
    // $display("check_for_repeats: previous_lfsr_values.size()=%d",previous_lfsr_values.size());


    // Check for current_value anywhere in the sequence
    for (int i = 0; i < previous_lfsr_values.size(); i++) begin
        // Cast Check value
        check_value = previous_lfsr_values[i];

        // Check for value anywhere in the sequence
        // $display("Checking : lfsr_values[%d]= 0x%h == 0x%h", i, check_value, current_value);

        // Does the current value = previous value
        if(check_value==current_value) begin
            $display("Success - Linear Shift Feedback Register repeats after generating (%d) unique values",i+1);
            $finish;
        end

        // Are we within range
        if(i>EXPECTED_NUM_INST_BEFORE_SEQ_REPEAT) begin
            $warning("No repeat value found within the previous_lfsr_values[0:%d], sequence length is outside of expected range [0:%d]",i,EXPECTED_NUM_INST_BEFORE_SEQ_REPEAT);
        end

        // STOP simulation if we are greater than TWICE the expected range...
        if(i>2*EXPECTED_NUM_INST_BEFORE_SEQ_REPEAT) begin
            $error("No repeat value found within the previous_lfsr_values[0:%d], sequence length is now outside of two-times the expected range [0:%d]",i,EXPECTED_NUM_INST_BEFORE_SEQ_REPEAT);
            $finish;
        end


    end

endtask : check_for_repeats



endclass : lfsr_monitor 
