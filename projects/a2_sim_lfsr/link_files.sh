#! /usr/bin/env bash
# Expects to be in root of Modelsim Project.

# Link firmware in ./src
cd ./src

################################################################################
# Cheatsheet on ln
################################################################################
# ln {target_file} {symbolic_name} # symbolic_name should be "./linkname"
# ln -s {source} {destination} # Symbolic link
# symbolic links: Refer to a symbolic path indicating the abstract location of another file
# hard links : Refer to the specific location of physical data.

################################################################################
# Testbench Firmware
################################################################################


################################################################################
# DUT Firmware
################################################################################

# DUT Firmware
ln  -s  ../../../firmware/A2/linear_feedback_sr.v  ./linear_feedback_sr.v

# Common Firmware required for DUT.

################################################################################
# Helpful commands/information
################################################################################

# Format:
#+echo "this is some bash command"
# the output.
# until the next empty line.

#+cd src/ && find ../../../firmware/A2/
# ../../../firmware/A2/
# ../../../firmware/A2/linear_feedback_sr.v

#+cd src/ && find ../../../../firmware/common/

