module labPrct03 (clock, pbIn_L, dbncd, cntOut);
	input clock, pbIn_L;
	output dbncd;
	output [3:0] cntOut;
	wire resetPre_L, preOut, pbIn;
	
	assign resetPre_L = 1'b1;
	assign pbIn =!pbIn_L;
	
// For Part Two, uncomment these two lines and comment out line below these: 
	unbouncer unbnc (.reset_L(pbIn), .enable(letRun), .clock(clock), .rco(preOut));
	assign letRun = pbIn & !preOut;
//	

// for Part Two, comment this line (after uncommenting two lines above)	
	// unbouncer unbnc (.reset_L(pbIn), .enable(pbIn), .clock(clock), .rco(preOut));

	assign dbncd = preOut;
	
	counter unitCount (.reset_L(1'b1), .clock(dbncd), .enable(1'b1), .Q(cntOut));

endmodule

module counter  (reset_L, clock, enable, Q, rco);
	parameter n=4;
	input reset_L, clock,enable;
	output reg [n-1:0] Q;
	output reg rco;
	
	always @( negedge reset_L, posedge clock) begin
		if (!reset_L)
			Q<= 0;
		else if (enable)
			Q<= Q+ 1;
			
		rco <= &Q;
		end 
endmodule
module unbouncer  (reset_L, clock, enable, Q, rco);
	parameter n=19;   // and change this parameter to n = 19 for Part Two
	input reset_L, clock,enable;
	output reg [n-1:0] Q;
	output reg rco;
	
	always @( negedge reset_L, posedge clock) begin
		if (!reset_L)
			Q<= 0;
		else if (enable)
			Q<= Q+ 1;
			
		rco <= &Q;
		end 
endmodule