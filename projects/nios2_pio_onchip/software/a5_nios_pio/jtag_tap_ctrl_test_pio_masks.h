// jtag_tap_ctrl_test

#pragma once

    // top_spi2jtag.sv
    // ,.pi_export({ {31-11 - 1 {1'b0}},
    //               tap_inf.tdo          // 11     
    //              ,tap_inf.tdi          // 10     
    //              ,tap_inf.tdo_oe       // 9         
    //              ,tap_inf.sdr          // 8     
    //              ,tap_inf.cdr          // 7     
    //              ,tap_inf.udr          // 6    
    //              ,jtag_tap_ctrl_state  // 5:3 // 4-bit state         
    //              ,tap_inf.trst_L       // 2         
    //              ,tap_inf.tms          // 1     
    //              ,tap_inf.tck          // 0         
    //              }) // pi.export [31:0]
    // ,.po_export({ 
    //               tap_inf.trst_L       // 2         
    //              ,tap_inf.tms          // 1     
    //              ,tap_inf.tck          // 0 
    //              })  // po.export [31:0] // float unused output

const unsigned PI_EXPORT_JTAG_TAP_CTRL_STATE_ADDR = 0x0003;
const unsigned PI_EXPORT_TRST_L_ADDR = 0x0002;
const unsigned PI_EXPORT_TMS_ADDR = 0x0001;
const unsigned PI_EXPORT_TCK_ADDR = 0x0000;

const unsigned PO_EXPORT_TRST_L_ADDR = 0x0002;
const unsigned PO_EXPORT_TMS_ADDR = 0x0001;
const unsigned PO_EXPORT_TCK_ADDR = 0x0000;

const unsigned PI_EXPORT_JTAG_TAP_CTRL_STATE_MASK = 0x0078;
const unsigned PI_EXPORT_TRST_L_MASK = 0x0004;
const unsigned PI_EXPORT_TMS_MASK = 0x0002;
const unsigned PI_EXPORT_TCK_MASK = 0x0001;

const unsigned PO_EXPORT_TRST_L_MASK = 0x0004;
const unsigned PO_EXPORT_TMS_MASK = 0x0002;
const unsigned PO_EXPORT_TCK_MASK = 0x0001;
