// #include "altera_avalon_pio_regs.h"
// #include "system.h"
// #include "a5.h"
#include "jtag_tap_ctrl_test.h"

int setup();
void loop();

int main()
{
	// test_pio();// Never exit.

    setup();
    while (1) { // Never exit.
        loop();
    }
    return 0; // Never reached.
}

int setup(){
    // print_hello_world();
    // print_square_and_cube_1toN(25);
    // print_prime_2toN(19);
    // // delay_s(1);
    // usleep(1*10^6);
    // print_hello_world();
    // // delay_s(5);
    // usleep(5*10^6);
    // print_hello_world();
    return 0;
}

void loop(){
//    assign_leds_to_switches();
//    do_delay();
//	assign_leds_to_twos_comp_switches();
// blink_twos_comp_of_switches();
	check_jtag_tap_ctrl_state();
}

// int test_pio()
// {
//   int in,  out;
//   while (1)
//   {
//    in = IORD_ALTERA_AVALON_PIO_DATA(PIO_0_BASE);
// 	  out = in;
// 	  IOWR_ALTERA_AVALON_PIO_DATA(PIO_0_BASE, out);
//   }
// }
