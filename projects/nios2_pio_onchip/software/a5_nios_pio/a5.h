
#pragma once

#include <stdio.h>
#include <unistd.h>
#include "system.h"
#include "altera_avalon_pio_regs.h"

#define IF_DEBUG(N) N
// #define IF_DEBUG(N)

const size_t MAX_PRIME_NUM = 100;

void print_hello_world(){
  printf("Hello from Nios II!\n");
}

void print_square_and_cube(int x){
    printf("%d, %d, %d\n", x, x*x, x*x*x);
}

void print_square_and_cube_1toN(int N){
    for( int i=1; i<=N; i++){
        print_square_and_cube(i);
    }
}

void print_prime_2toN(int N){
    const int NOT_PRIME = 0;
    int is_prime[MAX_PRIME_NUM];

    if( MAX_PRIME_NUM < N) {
        printf("ERROR: Adjust MAX_PRIME_NUM to be larger than N");
    }

    for( int i=2; i<=N; i++){
        is_prime[i] = ! NOT_PRIME; // First assume everything is prime.
    }

    // Remove those that are multiples numbers.
    for( int i=2; i<=N; i++){
        for( int multiple = i+i; multiple <= N; multiple += i ){
            is_prime[multiple] = NOT_PRIME;
        }
    }

    // Print if it is not NOT_PRIME
    for( int i=2; i<=N; i++){
        if( NOT_PRIME != is_prime[i] ){
            printf("%d\n", i);
        }
    }
}

alt_u32 twos_comp(alt_u32 value){
    return (~value)+1;
}

void delay_ms(unsigned nmilisec) {
    unsigned milisec_count = 0;
    while(milisec_count < nmilisec)
    {
        usleep(1000);
        milisec_count++;
    }
}

void do_delay_from_sw(unsigned value){
    unsigned sw_21 = (value) & 0x0003;// (2^(32-1) + 2^(32-2)) ; // Look at two lsb switches

    IF_DEBUG(printf("%x=sw_21\n", sw_21));

    switch (sw_21)
    {
         case 1: delay_ms(1000);
            IF_DEBUG(printf("delay_ms(1000)\n"));
         break ;
         case 2: delay_ms(2000);
            IF_DEBUG(printf("delay_ms(2000)\n"));
         break ;
         case 3: delay_ms(4000);
            IF_DEBUG(printf("delay_ms(4000)\n"));
         break ;
         // // case 0:
         // default: delay_ms(500);
         default: delay_ms(500);
            IF_DEBUG(printf("delay_ms(500)\n"));
         break ;
    }
}

void set_leds(alt_u32 value){
    IOWR_ALTERA_AVALON_PIO_DATA(PIO_OUT_0_BASE,value);
}

unsigned get_switches(){
    return IORD_ALTERA_AVALON_PIO_DATA(PIO_IN_0_BASE);
}

void blink_twos_comp_of_switches(){
    // unsigned sw_value = (unsigned int *) SWITCHES_BASE_ADDRESS;
    alt_u32 sw_value = get_switches();
    alt_u32 sw_twos_comp = twos_comp(sw_value);
    IF_DEBUG(printf("%x=switches\n", sw_value));
    IF_DEBUG(printf("%x=sw_twos_comp\n", sw_twos_comp));

    set_leds(sw_value);
    do_delay_from_sw(sw_value);
    set_leds(sw_twos_comp);
    do_delay_from_sw(sw_value);
}

// Before attempting this assignment you should have previously performed Assignment 5a.
// After that, you are ready to consult this video: �Altera FPGA tutorial - Controlling LEDs
// with switches using NIOSII Processor (DE2 Board)� which is found at:
// https://www.youtube.com/watch?v=w82xFo1RNmo
// and develop an FPGA project which uses the C language via a Nios II processor on an
// FPGA dev board to respond to switch and blink LEDs. Namely, use two of the switches
// to establish the blink rate (approximately 1/2, 1 second, 2 seconds, and four seconds).
// Look at four to six additional switches to establish the blink pattern. The switch input
// and the two�s complement of the switch input should alternate at the rate defined by the
// first two switches.
// Do this by following the directions in this video, and stealing reverse engineering as
// needed. There may be some hints in Chapter 10 of Chu�s book, but getting his exact code
// to work required more changes than we want to try for our first Nios hardware project.
// His code should work, but the header files we are using have defined things differently.
// This video is fairly straight-forward, especially after completing Assignment 5a, but
// I�ll add some observations:

// const unsigned T_DELAY_MS[4] = { 500, 1000, 2000, 4000 };
// const unsigned SW_MASK_DELAY = 0x0003; // two LSB switches

// // alternate between twos comp and value;
// unsigned a5_get_pattern(unsigned value){
//     static int use_twos_comp=0;
//     use_twos_comp = ! use_twos_comp;
//     if(use_twos_comp){
//         return twos_comp(value);
//     } else {
//         return value
//     }
// }

// void loop(){
//     unsigned sw = SW;
//     unsigned time_delay = T_DELAY_MS[ sw && SW_MASK_DELAY ]
//     LED = a5_get_pattern(sw);
//     delay_ms(time_delay);
// }


