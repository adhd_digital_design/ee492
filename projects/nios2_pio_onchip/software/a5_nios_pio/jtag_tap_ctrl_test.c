// jtag_tap_ctrl_test.c

#include <stdio.h>
#include <unistd.h>
#include "system.h"

#include "altera_avalon_pio_regs.h"

#include "jtag_tap_ctrl_test_pio_masks.h"
#include "jtag_tap_ctrl_test.h"

#define DEBUG
#include "IF_DEBUG.h"

int check_jtag_tap_ctrl_state(){
    IF_DEBUG(printf("reset_jtag_tap_ctrl()\n"));
    reset_jtag_tap_ctrl();
    return 0;
}

void set_pio(unsigned value, unsigned mask){
    unsigned old_value = get_pio();
    unsigned value_masked = old_value | (value & mask); // TODO bug. set(0x00, 0xFF) doesn't work if value=0xFF.
    IOWR_ALTERA_AVALON_PIO_DATA(PIO_OUT_0_BASE,value_masked);
}

unsigned get_pio(){
    return IORD_ALTERA_AVALON_PIO_DATA(PIO_IN_0_BASE);
}

void set_tms(bool value){
    if(value == 1) set_pio(PI_EXPORT_TMS_MASK, PI_EXPORT_TMS_MASK);
    else           set_pio(0, PI_EXPORT_TMS_MASK);
}

bool get_tms(){
    bool value = PI_EXPORT_TMS_MASK && get_pio();
    return value;
}

void set_tck(bool value){
    if(value == 1) set_pio(PI_EXPORT_TCK_MASK, PI_EXPORT_TCK_MASK);
    else           set_pio(0, PI_EXPORT_TCK_MASK);
}

bool get_tck(){
    bool value = PI_EXPORT_TCK_MASK && get_pio();
    return value;
}

void set_trst_L(bool value){
    if(value == 1) set_pio(PI_EXPORT_TRST_L_MASK, PI_EXPORT_TRST_L_MASK);
    else           set_pio(0, PI_EXPORT_TRST_L_MASK);
}

bool get_trst_L(){
    bool value = PI_EXPORT_TRST_L_MASK && get_pio();
    return value;
}

unsigned get_state(){
    return IORD_ALTERA_AVALON_PIO_DATA(PIO_IN_0_BASE);
}


void reset_jtag_tap_ctrl(){    
    set_trst_L(1);
    IF_DEBUG(printf("%d=get_trst_L()\n",get_trst_L()));
    set_trst_L(0); // Reset @ negedge
    IF_DEBUG(printf("%d=get_trst_L()\n",get_trst_L()));
    set_trst_L(1);
    IF_DEBUG(printf("%d=get_trst_L()\n",get_trst_L()));
}
