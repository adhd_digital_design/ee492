// jtag_tap_ctrl_test.h

#pragma once
#include <stdbool.h>

int check_jtag_tap_ctrl_state();

void set_pio(unsigned value, unsigned mask);

unsigned get_pio();

void set_tms(bool value);

bool get_tms();

void set_tck(bool value);

bool get_tck();

void set_trst_L(bool value);

bool get_trst_L();

unsigned get_state();


void reset_jtag_tap_ctrl();
