#!/usr/bin/env bash

set -e # Exit if any command returns with an error.

################################################################################
# Setup.Environment
################################################################################
if [ -d "/c/cygwin/" ]; then
    # Windows environment (Jordalenovo)
    # /cygdrive/c/altera/16.1/nios2eds/nios2_command_shell.sh
    QUARTUS_ROOTDIR=/c/altera/16.1/quartus
    SOPC_KIT_NIOS2=/c/altera/16.1/nios2eds
    QSYS_ROOTDIR=/c/altera/16.1/quartus/sopc_builder/bin
    PATH=/c/cygwin/bin:$PATH

    # Add gnu tools to PATH
    PATH=${SOPC_KIT_NIOS2}/bin/gnu/H-i686-mingw32/bin:$PATH
elif [ -d "/c/cygwin64/" ]; then
    # Windows environment (PLEX).
    # Overwrite the windows environment variables.
    QUARTUS_ROOTDIR=/c/altera_lite/16.0/quartus
    SOPC_KIT_NIOS2=/c/altera_lite/16.0/nios2eds
    QSYS_ROOTDIR=/c/altera_lite/16.0/quartus/sopc_builder/bin

    # Add cywgin tools to PATH
    PATH=/c/altera_lite/16.0/quartus/bin/cygwin/bin:$PATH
    # Add gnu tools to PATH
    PATH=${SOPC_KIT_NIOS2}/bin/gnu/H-i686-mingw32/bin:$PATH
elif [ -d "/c/" ]; then
    # Windows environment (Lab PC).
    # Overwrite the windows environment variables.
    QUARTUS_ROOTDIR=/c/altera/13.0sp1/quartus
    SOPC_KIT_NIOS2=/c/altera/13.0sp1/nios2eds
    QSYS_ROOTDIR=/c/intelFPGA_lite/16.1/quartus/sopc_builder/bin

    # Add cywgin tools to PATH
    PATH=/c/altera/13.0sp1/quartus/bin/cygwin/bin:$PATH
    # Add gnu tools to PATH
    PATH=${SOPC_KIT_NIOS2}/bin/gnu/H-i686-mingw32/bin:$PATH
elif [ -d "/home/caeuser/altera/13.0sp1/" ]; then
    # Linux environment (caeuser).
    export QUARTUS_ROOTDIR=~/altera/13.0sp1/quartus
    export SOPC_KIT_NIOS2=~/altera/13.0sp1/nios2eds
    export QSYS_ROOTDIR=~/altera/13.0sp1/quartus/sopc_builder/bin
elif [ -d "/home/hipe/altera_lite/16.0/" ]; then
    # Linux environment (hipe).
    export QUARTUS_ROOTDIR=~/altera_lite/16.0/quartus
    export SOPC_KIT_NIOS2=~/altera_lite/16.0/nios2eds
    export QSYS_ROOTDIR=~/altera_lite/16.0/quartus/sopc_builder/bin
fi

# Add quartus/bin to PATH
PATH=${QUARTUS_ROOTDIR}/bin:$PATH
# Add nios2eds/bin to PATH
PATH=${SOPC_KIT_NIOS2}/bin:$PATH
# Add nios2eds//sdk/bin to PATH
PATH=${SOPC_KIT_NIOS2}/sdk2/bin:$PATH

################################################################################
# Setup.Configure
################################################################################
project_dir=$(pwd)

# qsys_name=nios2_spi_pio_q13
qsys_name=nios2_spi_pio_q16
cpu_name=nios2_qsys_0
sopc_file=${project_dir}/${qsys_name}.sopcinfo

app_name=${qsys_name}
app_dir=${project_dir}/software/${app_name}
bsp_name=${app_name}_bsp
bsp_dir=${project_dir}/software/${bsp_name}

################################################################################
# Main
################################################################################
mkdir -p ${app_dir}
mkdir -p ${bsp_dir}

command=$1
case $command in
new) # Create nios2 projects from template, "Hello World".
    cd $project_dir
    nios2-swexample-create \
        --sopc-file=${sopc_file} \
        --type=hello_world_small \
        --elf-name=${app_name}.elf \
        --app-dir=${app_dir} \
        --bsp-dir=${bsp_dir}

    cd ${bsp_dir}
    ./create-this-bsp --cpu-name $cpu_name --no-make

    cd ${app_dir}
    ./create-this-app --no-make
    ;;

new_bsp)
    cd ${bsp_dir}
    nios2-bsp hal ${bsp_dir} $sopc_file --cpu-name $cpu_name
    ;;

bsp)
    cd ${bsp_dir}
    nios2-bsp-update-settings --settings ${bsp_dir}/settings.bsp \
        --bsp-dir ${bsp_dir} --sopc ${sopc_file} --cpu-name ${cpu_name} \
        --script ${project_dir}/software/settings.bsp.tcl

    nios2-bsp-generate-files --bsp-dir ${bsp_dir} \
        --settings ${bsp_dir}/settings.bsp \
        --sopc ${sopc_file} --cpu-name ${cpu_name} \
    ;;

new_app)
    cd ${app_dir}
    nios2-app-generate-makefile --app-dir ${app_dir} --bsp-dir ${bsp_dir} \
        --elf-name ${app_name}.elf \
        --set QUARTUS_PROJECT_DIR=${project_dir} \
        --set OBJDUMP_INCLUDE_SOURCE 1 \
        --src-rdir ${project_dir}/software/spi2jtag/ \
        --src-rdir ${app_dir}
    ;;

app) # "make" app.
    cd ${app_dir}
    # nios2-app-update-makefile --app-dir ${app_dir} \
    make
    ;;

run) # Download (elf) to a Nios2 instance. Then connect stdin/stdout/stderr
    cd ${app_dir}
    nios2-download -g ${app_name}.elf
    nios2-terminal
    ;;

run0) # Download (elf) to a Nios2 instance. Then connect stdin/stdout/stderr
    # quartus_pgm -l
    # 1) DE-SoC on 192.168.0.204 [USB-2]
    # 2) USB-Blaster on 192.168.0.204 [USB-0]
    # 3) USB-Blaster on 192.168.0.204 [USB-1]
    # 4) USB-BlasterII on 192.168.0.204 [USB-1]
    cd ${app_dir}
    cable="USB-Blaster on 192.168.0.204 [USB-0]"
    nios2-download -g ${app_name}.elf --cable "${cable}"
    nios2-terminal --cable "$cable"

esac
