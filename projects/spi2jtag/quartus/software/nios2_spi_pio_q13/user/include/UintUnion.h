
#pragma once

typedef union {
    struct {
        unsigned int b0 : 1; // Could be MSb or LSb
        unsigned int b1 : 1;
        unsigned int b2 : 1;
        unsigned int b3 : 1;
        unsigned int b4 : 1;
        unsigned int b5 : 1;
        unsigned int b6 : 1;
        unsigned int b7 : 1; // Could be MSb or LSb
    };
    struct {
        unsigned int u4a : 4; // Could be MSB or LSB
        unsigned int u4b : 4; // Could be MSB or LSB
    };
} UnionU8;

typedef union{
    uint8_t u8[4];
    uint8_t u16[2];
    uint32_t u32;
} UnionU32;
