
#pragma once
//  alt_printf       Only supports %s, %x, and %c ( < 1 Kbyte)
//  alt_putstr       Smaller overhead than puts with direct drivers
//                   Note this function doesn't add a newline.
//  alt_putchar      Smaller overhead than putchar with direct drivers
//  alt_getchar      Smaller overhead than getchar with direct drivers
#include "sys/alt_stdio.h"

alt_u8 scanf_x_4bit() {
    while(1){
        alt_u8 c = alt_getchar();
        if(c<'0')        { continue; }
        else  if(c<':') { return c-'0'; }
        else  if(c<'A')  { continue; }
        else  if(c<'G')  { return 10+(c-'A'); }
        else  if(c<'a')  { continue; }
        else  if(c<'g')  { return 10+(c-'a'); }
        else             { continue; }
    };
}

alt_u8 scanf_x_8bit() {
    alt_u8 lsb = scanf_x_4bit();
    alt_u8 msb = scanf_x_4bit();
    return lsb & (msb << 4);
}
