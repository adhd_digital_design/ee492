
#pragma once
#include "BinaryLiteral.h"
#include "JtagTapState.h"
#include "altera_stdint.h"
#include "bsp_system.h"
#include "IF_DEBUG.h"

#define CS_TAP 0
#ifndef SPI_BASE
require "#define SPI_BASE"
#endif

// uint8_t jtag_tms(JtagTapState a, JtagTapState b); // c++11
inline uint8_t jtag_tms(JtagTapStateEnum a, JtagTapStateEnum b){
    switch(a){
        case(JtagTapState::TEST_LOGIC_RESET):
            switch(b){
                //case(CURRENT_STATE):                 return  B(76543210);  //last_first
                case(JtagTapState::TEST_LOGIC_RESET):  return  B(11111111);  //1
                case(JtagTapState::RUN_TEST_IDLE):     return  B(01111111);  //0
                case(JtagTapState::SHIFT_DR):          return  B(00101111);  //0010
                case(JtagTapState::SHIFT_IR):          return  B(00110111);  //00110
                case(JtagTapState::PAUSE_DR):          return  B(01010111);  //01010
                case(JtagTapState::PAUSE_IR):          return  B(01011011);  //010110
                
                case(JtagTapState::SELECT_DR):         return  B(10111111);  //10
                case(JtagTapState::CAPTURE_DR):        return  B(01011111);  //010
                case(JtagTapState::EXIT1_DR):          return  B(10101111);  //1010
                case(JtagTapState::EXIT2_DR):          return  B(10101011);  //101010
                case(JtagTapState::UPDATE_DR):         return  B(11010111);  //11010
                
                case(JtagTapState::SELECT_IR):         return  B(11011111);  //110
                case(JtagTapState::CAPTURE_IR):        return  B(01101111);  //0110
                case(JtagTapState::EXIT1_IR):          return  B(10110111);  //10110
                case(JtagTapState::EXIT2_IR):          return  B(10101101);  //1010110
                case(JtagTapState::UPDATE_IR):         return  B(11011011);  //110110
                default:                               return  B(11111111);
            }
            
        case(JtagTapState::RUN_TEST_IDLE):
            switch(b){
                case(JtagTapState::TEST_LOGIC_RESET):  return  B(11100000);  //111
                case(JtagTapState::RUN_TEST_IDLE):     return  B(00000000);  //
                case(JtagTapState::SHIFT_DR):          return  B(00100000);  //001
                case(JtagTapState::SHIFT_IR):          return  B(00110000);  //0011
                case(JtagTapState::PAUSE_DR):          return  B(01010000);  //0101
                case(JtagTapState::PAUSE_IR):          return  B(01011000);  //01011
                default:                               return  B(11111111);
            }
        default: return B(11111111);
    }
}

// Assumes LSB is shifted first.
inline uint8_t jtag_transition(JtagTapStateEnum a, JtagTapStateEnum b){
    using namespace JtagTapState;
    switch(b){
        default:
        case(TEST_LOGIC_RESET): return B(11111111);
        case(RUN_TEST_IDLE):    return B(01111111);
        case(SHIFT_DR):
            switch(a){
                default:
                case(TEST_LOGIC_RESET): return  B(00101111);  //<CDR<SDR<RTI<4TLR
                case(RUN_TEST_IDLE):    return  B(00100000);  //<CDR<SDR<6RTI
                case(SHIFT_DR):         return  B(01000001);  //<E2DR<6PDR<E2DR
                case(SHIFT_IR):         return  B(00100011);  //<CDR<SDRS<RTI<UIR<E1IR
            }
        case(SHIFT_IR):
            switch(a){
                default:
                case(TEST_LOGIC_RESET): return  B(00110111);  //<CDR<SDR<RTI<4TLR
                case(RUN_TEST_IDLE):    return  B(00110000);  //<SIRS<CDR<SDR<6RTI
                case(SHIFT_IR):         return  B(01000001);  //<E2DR<6PDR<E2DR
                case(SHIFT_DR):         return  B(00110011);  //<CIR<SIRS<RTI<UDR<E1DR
            }
    }
}

JtagTapStateEnum jtag_next_stable(JtagTapStateEnum next_state){
    using namespace JtagTapState;
    switch(next_state){ // Constrain next_state options.
        default:
        case(TEST_LOGIC_RESET):
        case(UPDATE_DR):
        case(UPDATE_IR):
        case(RUN_TEST_IDLE):
            return RUN_TEST_IDLE;

        case(SELECT_DR):
        case(CAPTURE_DR):
        case(EXIT2_DR):
        case(SHIFT_DR):
            return SHIFT_DR;

        case(SELECT_IR):
        case(CAPTURE_IR):
        case(EXIT2_IR):
        case(SHIFT_IR):
            return SHIFT_IR;

        case(EXIT1_DR):
        case(PAUSE_DR):
            return PAUSE_DR;

        case(EXIT1_IR):
        case(PAUSE_IR):
            return PAUSE_IR;
    }
}

struct JtagTap {
    const unsigned cs;
    JtagTapStateEnum state;
    
    JtagTap(
        unsigned cs = CS_TAP,
        JtagTapStateEnum state = JtagTapState::TEST_LOGIC_RESET
        )
        : cs(cs)
        , state(state)
    {
    }
    
    alt_u8 transfer(alt_u8 x){
        return spi_transfer_u8( SPI_BASE, this->cs, x, 0);
    }
    
    void goto_state(JtagTapStateEnum next_state){
        JtagTapStateEnum next_stable_state = jtag_next_stable(next_state);
        // uint8_t tms = jtag_transition(this->state, next_stable_state);
        uint8_t tms = jtag_tms(this->state, next_stable_state);
        printf("%x=JtagTap.goto_state.tms\n", tms);
        this->transfer(tms);
    }

};

