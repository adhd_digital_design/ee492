
#pragma once
#include "bsp_system.h"
#include "altera_spi.h"
#include "altera_stdint.h"

#ifndef SPI_BASE
throw "#define SPI_BASE"
#else

#define CS_TAP 0
#define CS_JTAG 1

#include "JtagTdr.h"
#include "JtagTap.h"
#include "JtagDevice.h"

inline void transfer(alt_u32 base, alt_u32 slave, JtagTdr tdr, alt_u32 flags = 0){
    alt_avalon_spi_command(
        base, slave, tdr.len, tdr.write_p(), tdr.len, tdr.read_p(), 0
        );
}

inline void transfer(JtagDevice &jd, JtagTdr &tdr, alt_u32 flags = 0){
    jd.dr_shift( tdr.len, tdr.write_p(), tdr.len, tdr.read_p(), flags);
}

#endif
