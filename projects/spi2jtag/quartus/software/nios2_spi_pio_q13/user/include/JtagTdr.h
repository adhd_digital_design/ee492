
#pragma once
#include "altera_stdint.h"

#define TDR_BYTES(bits) (bits/8)
#define TDR_MAX_LEN 8
#define TDR_MAX_BYTES TDR_BYTES(TDR_MAX_LEN)

struct JtagTdr {
    const unsigned len;

    static unsigned validate_len(unsigned len){ 
        return (( TDR_MAX_LEN < len ) ? TDR_MAX_LEN : len);
    }

    JtagTdr(unsigned len) : len(validate_len(len))
    {
    }

    uint8_t *read_p() { return &this->bytes_read[0]; }
    uint8_t *write_p() { return &this->bytes_written[0]; };

    // Accessing bytes_read (value of capture register).
    uint8_t capture_u8() { return this->bytes_read[0]; }
    // uint8_t capture_u16(); // TODO
    // uint8_t capture_u32(); // TODO
    // void capture( uint8_t* data, unsigned len ): //TODO

    // Accessing bytes_written (value of update register).
    void update_u8( uint8_t u8 ) { this->bytes_written[0] = u8; }
    // void update_u16( uint16_t u ); //TODO
    // void update_u32( uint32_t u ); //TODO
    // void update( uint8_t* data, unsigned len ): //TODO

    private:
        uint8_t bytes_read[TDR_MAX_BYTES];
        uint8_t bytes_written[TDR_MAX_BYTES];
};
