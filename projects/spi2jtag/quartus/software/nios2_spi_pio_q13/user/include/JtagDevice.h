
#pragma once
#include "JtagTap.h"
#include "altera_stdint.h"
#include "altera_avalon_spi.h"
#include "bsp_system.h"

#define IR_LEN 8
#define CS_JTAG 1
#ifndef SPI_BASE
throw "#define SPI_BASE"
#endif

struct JtagDevice {
    JtagTap tap;
    const unsigned cs;
    uint32_t ir;
    
    JtagDevice( JtagTap tap = JtagTap(), unsigned cs = CS_JTAG, uint32_t ir = 0 )
        : tap(tap), cs(cs), ir(ir)
    {
    }
    
    void dr_shift(
        uint32_t write_length, const uint8_t * write_data,
        uint32_t read_length, uint8_t * read_data,
        uint32_t flags = 0
        )
    {
        // if( JtagTapState::SHIFT_DR != this->tap.state ){
        //     this->tap.goto_state(JtagTapState::SHIFT_DR);
        // }
        alt_avalon_spi_command(
                SPI_BASE, this->cs,
                write_length, write_data,
                read_length, read_data,
                0);
    }
    
    uint32_t ir_shift( uint32_t new_ir ){
        // if( JtagTapState::SHIFT_IR != this->tap.state ){
        //     this->tap.goto_state(JtagTapState::SHIFT_IR);
        // }
        this->ir = new_ir;
        uint32_t old_ir = 0;
        uint8_t *new_lsb = (uint8_t*)new_ir;
        uint8_t *old_lsb = (uint8_t*)old_ir;
        alt_avalon_spi_command(
            SPI_BASE, this->cs, IR_LEN, new_lsb, IR_LEN, old_lsb, 0
            );
        return old_ir;
    }
    
};

