
#pragma once
#include "alt_types.h"
// #include "altera_avalon_spi_regs.h"
#include "altera_avalon_spi.h"

inline alt_u8 spi_transfer_u8(
    alt_u32 base, alt_u32 slave, alt_u8 x, alt_u32 flags
    )
{
    alt_u8 rv;
    alt_avalon_spi_command( base, slave, 8, &x, 8, &rv, 0);
    return rv;
}

// TODO: Maybe
// inline alt_u32 spi_transfer_u32(
//     alt_u32 base, alt_u32 slave, alt_u32 x, alt_u32 flags
//     )
// {
//     alt_u32 rv;
//     alt_avalon_spi_command( base, slave, 32, &x, 32, &rv, 0);
//     return rv;
// }

