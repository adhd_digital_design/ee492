
#include <cstdio>
#include <stdio.h>
#include "alt_types.h"
// #include "system.h"
#include "spi2jtag.h"
// #include "JtagTapState.h"
#include "altera_stdio.h"
#include "JtagTap.h"
#include "JtagDevice.h"
#include "UintUnion.h"
#include "bsp_system.h"

unsigned user_in;
JtagTap tap = JtagTap(CS_TAP);
// JtagDevice jd = JtagDevice(JtagTap(CS_TAP), CS_JTAG);
// JtagTdr reg0 = JtagTdr(8);

void jtag_tap_debug(unsigned x){
    switch(x){
        default:
            break;
        case(0):
            tap.transfer(0xFF);
            break;
        case(1):
            tap.transfer(0x00);
            break;
        case(2):
            tap.transfer(jtag_transition(JtagTapState::TEST_LOGIC_RESET,
                        JtagTapState::SHIFT_DR));
            break;
        case(5):
            tap.transfer(B(00100000));
            break;
    }
}

int setup()
{
    printf("Hello from Nios II!\n");
    return 0;
}

int loop()
{
    user_in = scanf_x_4bit(); // aka scanf("%1x", &user_in);
    printf("### %x=loop.user_in ###\n", user_in);

    jtag_tap_debug(user_in);

    // spi_transfer_u8( SPI_BASE, 0, user_in, 0);

    // printf("goto(%x)\n", user_in);
    // jd.tap.goto_state(JtagTapStateEnum(user_in));

    // alt_u32 ir_read = jd.ir_shift(user_in);
    // printf("ir=(%lx)\n", ir_read);

    // reg0.update_u8(user_in);
    // transfer( SPI_BASE, 1, reg0 );
    // printf("%x=r0.shift_dr(%x)\n", reg0.capture_u8(), user_in);

    return 0;
}

