
#pragma once
#include "alt_types.h"
#include "altera_avalon_pio_regs.h"

#define PIO_IN(base)                IORD_ALTERA_AVALON_PIO_DATA(base)

#define PIO_OUT(base, data)         IOWR_ALTERA_AVALON_PIO_DATA(base, data)
#define PIO_BITSET(base, data)      IOWR_ALTERA_AVALON_PIO_SET_BITS(base, data)
#define PIO_BITCLEAR(base, data)    IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(base, data)
