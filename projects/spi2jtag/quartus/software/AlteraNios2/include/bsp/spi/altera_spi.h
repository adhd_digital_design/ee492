
#pragma once
#include "alt_types.h"
#include "altera_avalon_spi_regs.h"
#include "altera/altera_stdio.h"
#include "IF_DEBUG.h"

// #define SPI_CS(slave) 
// TXRDY when txdata register is empty
#define SPI_TX_RDY(status) (status & ALTERA_AVALON_SPI_STATUS_TRDY_MSK)
// RXRDY when rxdata register is full
#define SPI_RX_RDY(status) (status & ALTERA_AVALON_SPI_STATUS_RRDY_MSK)
#define SPI_TX_EMPTY(status) (status & ALTERA_AVALON_SPI_STATUS_TMT_MSK)
#define SPI_STATUS IORD_ALTERA_AVALON_SPI_STATUS(base)

inline int altera_spi_transfer(
      alt_u32 base , alt_u32 slave
    , alt_u32 num_of_bytes
    , const alt_u8 * write_data
    , alt_u8 * read_data
    , alt_u32 flags = 0
    )
{
    // #DO_IF_DEBUG( APRINTF("altera_spi_transfer(%xbytes)\n", num_of_bytes); );
    IOWR_ALTERA_AVALON_SPI_SLAVE_SEL(base, 1 << slave); // CS=(1<<slave)
    IORD_ALTERA_AVALON_SPI_RXDATA(base); // rx=0;
    // DO_IF_DEBUG( APRINTF("%x=cs\n", IORD_ALTERA_AVALON_SPI_SLAVE_SEL(base) ););
    
    alt_u8* read_begin = read_data;
    alt_u8* read_end = read_data + num_of_bytes;
    const alt_u8* write_end = write_data + num_of_bytes;
    
    alt_u8 done = 0xFF;
    while( done ) // wait until we have read expected num_of_bytes.
    {
        if( write_data < write_end ) {
            while( !(SPI_TX_RDY( SPI_STATUS ) ))  { }
            // DO_IF_DEBUG( alt_putchar('w'); );
            IOWR_ALTERA_AVALON_SPI_TXDATA(base, *write_data++);
        } else { done &= 0xF0; };
        
        if( read_data < read_end ) {
            while( !(SPI_RX_RDY( SPI_STATUS ) ))  { }
            *read_data++ = (alt_u8) (IORD_ALTERA_AVALON_SPI_RXDATA(base)) ;
            // DO_IF_DEBUG( alt_putchar('r'); );
        } else { done &= 0x0F; };
        // TODO check errors
    }
    // DO_IF_DEBUG( alt_putchar('\n'); );
    
    // Wait until transmitt buffer is empty.
    while( !(SPI_TX_EMPTY( SPI_STATUS ) ))  { }
    
    IOWR_ALTERA_AVALON_SPI_CONTROL(base, 0);
    return ((read_end-read_begin) / sizeof(alt_u8));
}

inline alt_u8 spi_transfer_u8(
      alt_u32 base , alt_u32 slave , alt_u8 x , alt_u32 flags=0
    )
{
    alt_u8 rv=0;
    altera_spi_transfer(base, slave, 1, &x, &rv, flags=0 );
    return rv;
}

inline alt_u32 spi_transfer_u32(
      alt_u32 base , alt_u32 slave , alt_u32 x , alt_u32 flags=0
    )
{
    alt_u32 rv=0;
    altera_spi_transfer(base, slave, 4, (alt_u8*)(&x), (alt_u8*)(&rv), flags=0 );
    return rv;
}