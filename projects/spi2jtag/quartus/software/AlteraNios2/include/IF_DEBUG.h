
#pragma once

#ifdef DEBUG
    #define IF_DEBUG(X) X
    // Code is compiled but optimized out.
    #define DO_IF_DEBUG(X) do { X } while (0);
#else
    #define IF_DEBUG(X)
    #define DO_IF_DEBUG(X)
#endif
