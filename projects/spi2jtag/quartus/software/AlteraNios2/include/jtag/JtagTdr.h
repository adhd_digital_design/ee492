
#pragma once
#include "altera/altera_stdint.h"

// TDR
#ifndef TDR_BITS_TO_BYTES
    #define TDR_BITS_TO_BYTES(bits) (((bits-1)/8)+1)
#endif
#define TDR_MAX_LEN 32
#define TDR_MAX_BYTES TDR_BITS_TO_BYTES(TDR_MAX_LEN)
#if (TDR_MAX_BYTES*8 > (TDR_MAX_LEN))
#error "(TDR_MAX_BYTES bytes)*8 != (TDR_MAX_LEN bits)"
#endif


struct JtagTdr {
    const unsigned len; // length in bits.

    static unsigned validate_len(unsigned len){ 
        if( TDR_MAX_LEN < len ){
            return TDR_MAX_LEN;
        } else {
            return len;
        }
    }

    JtagTdr(unsigned len)
    : len(validate_len(len))
    {
        for( unsigned i=0; i<TDR_MAX_BYTES; i++)
        {
            this->bytes_read[i] = 0x00;
            this->bytes_written[i] = 0x00;
        }
    }

    uint8_t *read_p() { return this->bytes_read; }
    const uint8_t *write_p() { return this->bytes_written; };

    // Accessing bytes_read (value of capture register).
    uint8_t capture_u8() { return this->bytes_read[0]; }
    // uint8_t capture_u16(); // TODO
    uint32_t capture_u32()
    {
        uint32_t rv = 0;
        capture_bits( (uint8_t*) (&rv) , this->len);
        return rv;
        
        // const uint32_t *read32_p = (uint32_t*) this->bytes_read;
        // return (*read32_p);
        // return (this->bytes_read[0] << 0) 
        //     + (this->bytes_read[1] << 8) 
        //     + (this->bytes_read[2] << 16)
        //     + (this->bytes_read[3] << 24);
    }

    void capture_bytes( uint8_t* data, unsigned num_of_bytes )
    {
        for( unsigned i=0; i<num_of_bytes; i++)
        {
            data[i] = this->bytes_read[i];
        }
    }
    
    // Limited to num_of_bits<32.
    void capture_bits( uint8_t* data, unsigned num_of_bits )
    {
        unsigned whole_bytes = (num_of_bits/8);
        unsigned extra_bits = num_of_bits - whole_bytes*8; 
        
        capture_bytes(data, whole_bytes + 1 );
        
        // Don't care about some (extra_bits) of the most significant bits.
        // assume LSB_FIRST
        data[whole_bytes] &= (0xFF>>extra_bits);
    }
    
    // Accessing bytes_written (value of update register).
    void update_u8( uint8_t u8 ) { this->bytes_written[0] = u8; }
    // void update_u16( uint16_t u ); //TODO
    void update_u32( uint32_t u )
    {
        update_bits( (uint8_t*) (&u) , this->len);
        
        // #ifdef UPDATE_BITS_UPTO32
        // this->bytes_written[0] = (u >> 0) & 0xFF;
        // this->bytes_written[1] = (u >> 8) & 0xFF;
        // this->bytes_written[2] = (u >> 16) & 0xFF;
        // this->bytes_written[3] = (u >> 24) & 0xFF;
        // #endif
        
    };
    
    void update_bytes( uint8_t * const data, unsigned num_of_bytes )
    {
        for( unsigned i=0; i<num_of_bytes; i++)
        {
            this->bytes_written[i] = data[i];
        }
    }
    
    // Limited to num_of_bits<32.
    void update_bits( uint8_t* const data, unsigned num_of_bits )
    {
        unsigned whole_bytes = (num_of_bits/8);
        unsigned extra_bits = num_of_bits - whole_bytes*8; 
        // APRINTF("\tupdate_bits( data{%x bytes, %x bits} )\n", whole_bytes, extra_bits);
        
        // #ifdef UPDATE_BITS_UPTO32
        // // Don't care about some (extra_bits) of the least significant bits.
        // // assume LSB_FIRST
        // uint32_t *u32_ptr = (uint32_t*) data;
        // uint32_t u32_shifted = (*u32_ptr<<extra_bits);
        // update_u32(u32_shifted);
        // APRINTF("\t{%x}=update_bits(%x)\n", u32_shifted, *u32_ptr);
        // #endif
        
        // Doesn't handle lower than 8
        update_bytes(data, whole_bytes+1);
        for( unsigned i = whole_bytes; 0<i; i--)
        {
            uint16_t *shift16_p = (uint16_t*) (&this->bytes_written[i-1]);
            *shift16_p = ((*shift16_p)<<extra_bits);
        }
    }
    
    void debug(){
        APRINTF("\t{%x,%x,%x,%x}=bytes_written\n"
            , this->bytes_written[3]
            , this->bytes_written[2]
            , this->bytes_written[1]
            , this->bytes_written[0]
            );
        // APRINTF("{%x}=(uint32_t*)bytes_written\n"
        //     , *((uint32_t*)this->bytes_written)
        //     );
        APRINTF("\t{%x,%x,%x,%x}=bytes_read\n"
            , this->bytes_read[3]
            , this->bytes_read[2]
            , this->bytes_read[1]
            , this->bytes_read[0]
            );
        // APRINTF("{%x}=capture_u32\n"
        //     , this->capture_u32()
        //     );
    }
    
    private:
        uint8_t bytes_read[TDR_MAX_BYTES];
        uint8_t bytes_written[TDR_MAX_BYTES];
};