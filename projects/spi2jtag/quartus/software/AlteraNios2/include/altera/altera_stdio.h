
#pragma once

#ifndef ALT_STDIO_EN

#include <cstdio>
// #include <stdio.h>

// printf
// Does the printf routine in the Nios embedded processor software development kit (SDK) support floating-point data types?
// The Nios SDK has its own implementation of the printf routine and does not include support for floating-point types.
// https://www.altera.com/support/support-resources/knowledge-base/solutions/rd01152001_4558.html
#define APRINTF printf
#define alt_printf printf

#define alt_putstr putstr
#define alt_putchar putchar
#define alt_getchar getchar

#else

#ifdef printf
throw "#undef ALT_STDIO_EN to use stdio.h."
#else
#define APRINTF alt_printf
#endif

//  alt_printf       Only supports %s, %x, and %c ( < 1 Kbyte)
//  alt_putstr       Smaller overhead than puts with direct drivers
//                   Note this function doesn't add a newline.
//  alt_putchar      Smaller overhead than putchar with direct drivers
//  alt_getchar      Smaller overhead than getchar with direct drivers
#include "sys/alt_stdio.h"

#endif
