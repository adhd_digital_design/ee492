
#pragma once
#include "altera/altera_stdio.h" // alt_getchar
#include "alt_types.h" // alt_u8

#define TDR_BITS_TO_NIBBLE(bits) (((bits-1)/4)+1)

alt_u8 scanf_x_4bit() {
    while(1){
        alt_u8 c = alt_getchar();
        if(c<'0')        { continue; }
        else  if(c<':')  { return c-'0'; }
        else  if(c<'A')  { continue; }
        else  if(c<'G')  { return 10+(c-'A'); }
        else  if(c<'a')  { continue; }
        else  if(c<'g')  { return 10+(c-'a'); }
        else             { continue; }
    };
    return 0;
}

alt_u8 scanf_x_8bit() {
    alt_u8 msb = scanf_x_4bit();
    alt_u8 lsb = scanf_x_4bit();
    return (msb << 4) | lsb ;
}

alt_u32 scanf_x_12bit() {
    alt_u32 msb = scanf_x_4bit();
    alt_u32 mid = scanf_x_4bit();
    alt_u32 lsb = scanf_x_4bit();
    return (msb << 8) | (mid << 4) | lsb ;
}

// void scanf_x_nibbles( uint8_t* data, unsigned num_of_nibbles )
// {
//     for( unsigned i=0; i<num_of_nibbles; i++)
//     {
//         data[i] = scanf_x_4bit();
//     }
// }

// // Limited to num_of_bits<32.
// void scanf_x_bits( uint8_t* data, unsigned num_of_bits )
// {
//     unsigned whole_nibbles = (num_of_bits/4);
//     // Don't care about some of the least significant bits.
//     // assume LSB_FIRST
//     unsigned extra_bits = num_of_bits - whole_nibbles*4; 
    
//     capture_nibbles(data, whole_nibbles + 1 );
//     data[whole_nibbles] &= (0xFF>>extra_bits);
// }

