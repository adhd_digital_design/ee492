#pragma once

#include "alt_types.h"

#ifndef uint8_t
#define uint8_t alt_u8
#endif

#ifndef uint16_t
#define uint16_t alt_u16
#endif

#ifndef uint32_t
#define uint32_t alt_u32
#endif

#ifndef uint64_t
#define uint64_t alt_u64
#endif


