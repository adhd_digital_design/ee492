
#pragma once
#include "altera_stdint.h" // uint8_t
#include "spi2jtag/pio.h" // pio::*

// https://en.wikipedia.org/wiki/Serial_Peripheral_Interface_Bus
// At CPOL=0 the base value of the clock is zero, i.e. the idle state is 0 and active state is 1.
//     For CPHA=0, data are captured on the clock's rising edge (low→high transition) and data is output on a falling edge (high→low clock transition).
//     For CPHA=1, data are captured on the clock's falling edge and data is output on a rising edge.
// At CPOL=1 the base value of the clock is one (inversion of CPOL=0), i.e. the idle state is 1 and active state is 0.
//     For CPHA=0, data are captured on clock's falling edge and data is output on a rising edge.
//     For CPHA=1, data are captured on clock's rising edge and data is output on a falling edge.

// Purely for information. TODO for implementation.
#define SPI_NUMSLAVES 2
// Defines active low (0, false) or high (1,true) chip select.
#define CS_ACTIVE false
#define SPI_LSB_FIRST false
#define SPI_CLOCKPHASE 0
#define SPI_CLOCKPOLARITY 0

// TODO: Idea for settings:
// SPISettings settings = SPISettings(15*MHZ, LSBFIRST, SPI_MODE0);

// Simultaneously transmit and receive a byte on the SPI.
// 
// Polarity and phase are assumed to be both 0, i.e.:
//   - input data is captured on rising edge of SCLK.
//   - output data is propagated on falling edge of SCLK.
// 
// Returns the received byte.
// Cited: https://en.wikipedia.org/wiki/Serial_Peripheral_Interface_Bus
uint8_t spi_transfer(uint8_t byte_out)
{
    uint8_t byte_in = 0x00;

    for (uint8_t bit = 0x80; (0!=bit); bit >>= 1)
    {
        pio::mosi(byte_out & bit); // Shift-out a bit to the MOSI line
        // delay(SPI_SCLK_LOW_TIME); // Delay for at least the peer's setup time
        pio::sck(true); // Pull the clock line high

        if (pio::miso()) { byte_in |= bit; } // Shift-in a bit from the MISO line
        // delay(SPI_SCLK_HIGH_TIME); // Delay for at least the peer's hold time
        // Pull the clock line low
        pio::sck(false);
    }

    return byte_in;
}

uint8_t spi_transfer_u8(uint8_t cs, uint8_t tx)
{
    pio::cs(cs, CS_ACTIVE);
    uint8_t rx = spi_transfer( tx );
    pio::cs(cs, !CS_ACTIVE);
    return rx;
}

struct Pio2Spi {
    const uint8_t cs;
    Pio2Spi(uint8_t cs): cs(cs) { }
    void begin(){ pio::cs(this->cs, false); }
    void end(){ pio::cs(this->cs, true); }
    uint8_t transfer(uint8_t tx) { return spi_transfer( tx ); }
};
