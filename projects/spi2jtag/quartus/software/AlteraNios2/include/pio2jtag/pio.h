
#pragma once
#include "bsp/pio/altera_pio.h" // PIO_IN
#include "altera_stdint.h" // uint8_t
#include "bsp/pio/system.h"

#define S2J_PIO PIO_BASE

namespace pio {
    
//******************************************************************************
// Parallel Input
//******************************************************************************

// assign pio_in = {
//     {31-19 - 1 {1'b0}},
//      led_reg                 // 17:26  // 10-bit data register
//     ,jtag_ir_out             // 6:17  // 12-bit data register
//     ,tap_state               // 5     // 4-bit data register
//     ,spi_slave_wires
//     };

uint8_t spi_slave_wires()
{
    return ( (PIO_IN(S2J_PIO) >> 0) & 0x1F );
}

uint8_t tap_state()
{
    return ( (PIO_IN(S2J_PIO) >> 5) & 0xF );
}

uint16_t jtag_ir_out()
{
    return ( (PIO_IN(S2J_PIO) >> (5+4)) & 0xFFF );
}

bool cs(uint8_t n)  // read
{
	switch(n){
		default:
		case(0): return ( (PIO_IN(S2J_PIO) >> (2)) & 0x1 );
		case(1): return ( (PIO_IN(S2J_PIO) >> (3)) & 0x1 );
	};
}

bool miso()  // read
{
	return ( (PIO_IN(S2J_PIO) >> (1)) & 0x1 );
}

//******************************************************************************
// Parallel Output
//******************************************************************************

// assign pio_out = {
//      ~spi_nios2_pio.cs[1]        // 3
//     ,~spi_nios2_pio.cs[0]        // 2
//     ,spi_nios2_pio.sclk          // 1
//     ,spi_nios2_pio.mosi          // 0
//     };

void cs(uint8_t n, bool active) // write
{
	uint32_t bits = 0x00;
	switch(n){
		case(0): bits = (1<<2); break;
		case(1): bits = (1<<3); break;
		default: bits = bits && 0x00;
	};
	if(active){
		PIO_BITSET(S2J_PIO, bits);
	} else {
		PIO_BITCLEAR(S2J_PIO, bits);
	}
}

void sck(bool active) // write
{
	if(active){
		PIO_BITSET(S2J_PIO, (1<<1));
	} else {
		PIO_BITCLEAR(S2J_PIO, (1<<1));
	}
}

void mosi(bool active) // write
{
	if(active){
		PIO_BITSET(S2J_PIO, (1<<0));
	} else {
		PIO_BITCLEAR(S2J_PIO, (1<<0));
	}
}

}

//******************************************************************************
// Debugging
//******************************************************************************

void printf_pio_in()
{
    printf("%x=PIO_IN\n", PIO_IN(PIO_BASE));
    printf("%x=jtag_ir_out\n", pio::jtag_ir_out());
    printf("%x=tap_state\n", pio::tap_state());
    printf("%x=spi_slave_wires\n", pio::spi_slave_wires());
}

void debug_cs( uint8_t x )
{
	switch(x){
		case 0:
			pio::cs(0, false);
			pio::cs(1, false);
			break;
		case 1:
			pio::cs(0, true);
			pio::cs(1, false);
			break;
		case 2:
			pio::cs(0, false);
			pio::cs(1, true);
			break;
		case 3:
			pio::cs(0, true);
			pio::cs(1, true);
			break;
		case 4:
			if(pio::cs(0)){ printf("1=cs(0)"); }
			if(pio::cs(1)){ printf("1=cs(1)"); }
			break;
		case 5:
			printf("%x=spi_slave_wires\n", pio::spi_slave_wires());
			printf("%x=spi_slave_wires\n", pio::spi_slave_wires());
			break;
	}
}
