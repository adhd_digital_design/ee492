
#pragma once

// #include "bsp/spi/system.h"
// #include "bsp/pio/system.h"
#define SPI_BASE SPI_0_BASE
#define PIO_BASE PIO_0_BASE

#define CS_TAP 0
#define CS_JTAG 1
#define CS_LOOPBACK 2
