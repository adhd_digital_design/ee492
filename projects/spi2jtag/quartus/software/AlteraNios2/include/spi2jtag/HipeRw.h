#pragma once
#include "jtag/JtagTdr.h"
#include "spi2jtag/JtagDevice.h"
#include "altera/altera_stdint.h"

#define HIPE_IR_WE(base) (base+0)
#define HIPE_IR_DATA(base) (base+1)
// const unsigned DR_LEN = 8;

JtagTdr tdr(DR_LEN);

uint8_t hipe_rw_write_enable(
    JtagDevice &jd, unsigned address, uint8_t x
    )
{
    tdr.update_u8(x);
    jd.ir_shift(HIPE_IR_WE(address));
    jd.dr_shift( tdr );
    return tdr.capture_u8();
}

uint8_t hipe_rw_data(
    JtagDevice &jd, unsigned address, uint8_t x
    )
{
    tdr.update_u8(x);
    jd.ir_shift(HIPE_IR_DATA(address));
    jd.dr_shift( tdr );
    return tdr.capture_u8();
}

// struct HipeRw {
//     const unsigned base;
//     JtagTdr data_tdr;
//     JtagTdr we_tdr;
    
//     HipeRw( const unsigned base )
//         : base(base)
//         , data(JtagTdr(DR_LEN))
//         , write_enable(JtagTdr(DR_LEN));
//     {
        
//     }
    
//     void write_enable_out( uint8_t x ) { this->write_enable.update_u8(x); } 
//     uint8_t in() { return this->data.capture_u8(); } 
//     void out( uint8_t x ) { this->data.update_u8(x); }
    
//     void transfer(JtagDevice &jd)
//     {
//         jd.ir_shift(HIPE_IR_WE(this->base_address));
//         jd.dr_shift( this->we_tdr );
//         jd.ir_shift(HIPE_IR_DATA(this->base_address));
//         jd.dr_shift( this->data_tdr );
//     }
    
// }

