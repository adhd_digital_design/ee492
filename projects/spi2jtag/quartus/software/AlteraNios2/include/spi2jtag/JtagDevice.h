#pragma once
#include "spi2jtag/JtagTap.h"
#include "jtag/JtagTdr.h"
#include "spi2jtag/system.h"

#include "altera/altera_stdint.h"
// #include "bsp/spi/altera_spi_transfer.h"
#include "bsp/spi/altera_spi.h"

#ifndef CS_JTAG
    #error "#define CS_JTAG"
#endif
#ifndef SPI_BASE
    #error "#define SPI_BASE"
#endif

struct JtagDevice {
    JtagTap tap;
    // uint32_t ir;
    JtagTdr ir;
    const unsigned cs;
    
    JtagDevice(
          JtagTap tap = JtagTap()
        , JtagTdr ir = JtagTdr(32)
        , unsigned cs = CS_JTAG
        )
        : tap(tap)
        , ir(ir)
        , cs(cs)
    {
    }
    
    void transfer(JtagTdr &tdr, alt_u32 flags = 0){
        unsigned bytes = TDR_BITS_TO_BYTES(tdr.len);
        // APRINTF("JtagDevice.transfer(%x=tdr.len, %x=tdr.bytes)\n"
        //     , tdr.len ,bytes);
        altera_spi_transfer(
            SPI_BASE, this->cs, bytes,
            tdr.write_p(), tdr.read_p()
            );
            
        // uint8_t rx = spi_transfer_u8(
        //     SPI_BASE, this->cs,
        //     (*tdr.write_p())
        //     );
        // (*tdr.read_p()) = rx;
    }
    
    
    void dr_shift(
          uint32_t num_of_bytes
        , const uint8_t * write_data 
        , uint8_t * read_data
        )
    {
        this->tap.goto_state(JtagTapState::SHIFT_DR);
        altera_spi_transfer( SPI_BASE, this->cs, num_of_bytes, write_data, read_data);
        this->tap.goto_state(JtagTapState::RUN_TEST_IDLE);
    }
    
    void dr_shift( JtagTdr &tdr )
    {
        this->tap.goto_state(JtagTapState::SHIFT_DR);
        this->transfer(tdr);
        this->tap.goto_state(JtagTapState::RUN_TEST_IDLE);
    }
    
    uint32_t ir_shift( uint32_t new_ir ){
        this->tap.goto_state(JtagTapState::SHIFT_IR);
        
        uint8_t* p_ir = (uint8_t*) (&new_ir);
        this->ir.update_bits( p_ir, this->ir.len);
        // this->ir.update_u32( new_ir );
        
        this->transfer(this->ir);
        // this->ir.debug();
        
        uint32_t old_ir = this->ir.capture_u32();
        
        APRINTF("%x=JtagDevice.ir_shift(%x)\n", old_ir, new_ir);
        this->tap.goto_state(JtagTapState::RUN_TEST_IDLE);
        return old_ir;
    }
    
};


