
#pragma once
#include "spi2jtag/JtagDevice.h"
#include "jtag/JtagTdr.h"

const unsigned DR_LEN =  8;

struct HipeJtag {
    JtagDevice *jd;
    JtagTdr dr;
    
    HipeJtag(JtagDevice *jd)
        : jd(jd)
        , dr( JtagTdr(DR_LEN) )
    {
    }
    
    uint8_t write_enable(unsigned rw_index, uint8_t x ) {
        this->dr.update_u8(x);
        jd->ir_shift( (rw_index*2) );
        jd->dr_shift( this->dr );
        return this->dr.capture_u8();
    } 
    
    uint8_t data(unsigned rw_index, uint8_t x ) {
        this->dr.update_u8(x);
        jd->ir_shift( (rw_index*2)+1 );
        jd->dr_shift( this->dr );
        return this->dr.capture_u8();
    } 
    
    uint8_t rw(unsigned rw_index, uint8_t dout, uint8_t we_out) {
        this->write_enable(rw_index, we_out);
        return this->data(rw_index, dout);
    } 
    
};