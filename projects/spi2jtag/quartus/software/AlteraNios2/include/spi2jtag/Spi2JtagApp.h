#pragma once

#define DEBUG 1

// Altera Helpers
#define ALT_STDIO_EN
#include "altera/altera_stdio.h"
#include "altera/scanf_x.h"

#include "spi2jtag.h"
#include "spi2jtag/PioTdr.h"
#include "altera/scanf_x.h"

#include "spi2jtag/spi_test.h"

struct Spi2JtagApp {
    JtagDevice jd;
    HipeJtag hj;
    PioTdr pio32;
    unsigned a, b, c, d;
    
    Spi2JtagApp()
        : jd( JtagDevice( JtagTap(CS_TAP) , JtagTdr(12), CS_JTAG ) )
        , hj( &this->jd )
        , pio32( &this->hj )
    {}
    
    int setup(){ return 0; }
    int loop() { return this->interactive(); }
    
    int a_4bit()
    {
        APRINTF( "\nSpi2JtagApp.a=4'h" );
        this->a = scanf_x_4bit();        
        return this->a;
    }
    int b_8bit()
    {
        APRINTF( "\nSpi2JtagApp.b=8'h" );
        this->b = scanf_x_8bit();        
        return this->b;
    }
    int c_8bit()
    {
        APRINTF( "\nSpi2JtagApp.c=8'h" );
        this->c = scanf_x_8bit();        
        return this->c;
    }
    int d_32bit()
    {
        APRINTF( "\nSpi2JtagApp.c=32'h" );
        this->d = 0x00000000; 
        for(unsigned shift=(32-4); shift<=32; shift=shift-4)
        {
            uint32_t newbits = scanf_x_4bit();
            this->d |= ( newbits << shift);
        }
        return this->d;
    }
    int spi_transfer_8bit(unsigned cs, unsigned tx)
    {
        uint8_t rv = spi_transfer_u8(SPI_BASE, cs, tx, 0x00 );
        APRINTF("%x=spi_transfer_u8(%x=tx,%x=cs)\n", rv, tx, cs);
        return rv;
    }
    
    int test( unsigned test_id )
    {
        switch(test_id) {
            default:
            case(0): return pio32.test_with_constants();
            case(1): return pio32.test_pseudo_rand(4);
            case(2): return spi_test();
            case(3): return jd.tap.test_transitions();
        }
    }
    
    int interactive()
    {
        APRINTF(
            "\n\nSpi2JtagApp.action={4bit}\n{"
            "\n 0: return"
            "\n 1: test(test_id=a)"
            "\n 2: pio32.tdr(ho=d)"
            "\n 3: pio32.tdr_wen(wen=d)"
            "\n 4: pio32.pio(po=d)"
            "\n 5: hipe.write_enable(addr=a, data=b)"
            "\n 6: hipe.data(addr=a, data=b)"
            "\n 7: jtag.ir_shift(ir=b)"
            "\n 8: tap.goto_state(state=a)"
            "\n 9: spi_transfer_u8(cs=a, tx=b)"
            "\n a: a=4'h%x"
            "\n b: b=8'h%x"
            "\n c: c=8'h%x"
            "\n d: d=32'h%x"
            "\n} action=4'h"
            , this->a, this->b, this->c, this->d
        );
        unsigned action = scanf_x_4bit(); alt_putchar('\n');
        switch(action)
        {
            default:
            case(0x0): return 0;
            case(0x1): return this->test(this->a);
            case(0x2): return this->pio32.tdr(this->d);
            case(0x3): return this->pio32.tdr_wen(this->d);
            case(0x4): return this->pio32.pio(this->d);
            case(0x5): return this->hj.write_enable(this->a, this->b);
            case(0x6): return this->hj.data(this->a, this->b);
            case(0x7): return this->jd.ir_shift(this->b);
            case(0x8): return this->jd.tap.goto_state(JtagTapStateEnum(this->a));
            case(0x9): return this->spi_transfer_8bit(this->a, this->b);
            
            case(0xA): return this->a_4bit();
            case(0xB): return this->b_8bit();
            case(0xC): return this->c_8bit();
            case(0xD): return this->d_32bit();
        }
    }
};