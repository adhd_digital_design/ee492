
#pragma once
#include "altera/altera_stdint.h"
#include "bsp/pio/altera_pio.h"
#include "jtag/JtagTdr.h"
#include "spi2jtag/HipeJtag.h"

struct PioTdr {
    HipeJtag *hj;
    uint32_t wen; // write enable
    uint32_t po; // pio out
    uint32_t ho; // hipe out
    uint32_t hi; // hipe out

    PioTdr( HipeJtag *hj )
        : hj(hj), wen(0), po(0), ho(0)
    {
    }

    uint32_t pio_in(){
        return PIO_IN(PIO_BASE);
    }
    void pio_out(uint32_t data)
    {
        this->po = data;
        PIO_OUT(PIO_BASE, data);
    }
    
    uint32_t pio( uint32_t x )
    {
        this->pio_out(x);
        uint32_t rv = this->pio_in();
        DO_IF_DEBUG( APRINTF("%x=PioTdr::pio(po=%x)\n", rv, x); );
        return rv;
    }
    
    uint32_t tdr( uint32_t x)
    {
        this->ho = x;       
        uint8_t* x_p = (uint8_t*) (&x);
        
        uint32_t rv = 0;
        uint8_t* rv_p = (uint8_t*) (&rv);
        for( unsigned i=0; i<4; i++)
        {
            rv_p[i] = this->hj->data( i, x_p[i] );
        }
        DO_IF_DEBUG( APRINTF("%x=PioTdr::tdr(ho=%x)\n", rv, x); );
        return rv;
    }
    
    uint32_t tdr_wen( uint32_t x )
    {
        this->wen = x;       
        uint8_t* x_p = (uint8_t*) (&x);
        
        uint32_t rv = 0;
        uint8_t* rv_p = (uint8_t*) (&rv);
        for( unsigned i=0; i<4; i++)
        {
            rv_p[i] = this->hj->write_enable( i, x_p[i] );
        }
        DO_IF_DEBUG( APRINTF("%x=PioTdr::tdr_wen(wen=%x)\n", rv, x); );
        return rv;
    }
    
    void setup_pio_loopback(){
        this->tdr_wen(0x00);
    }
    void setup_ho_to_pi(){
        this->tdr_wen(0xFFFFFFFF);
    }
    
    uint32_t check_pi()
    {
        uint32_t expected = (this->po & (~this->wen)) | (this->ho & this->wen);
        uint32_t pi = this->pio_in();
        uint32_t bad = pi ^ expected;
        if( bad ){
            APRINTF("ERROR: PioTdr::check_pi(){"
                "\n\texpected=%x\n\tpi=%x\n\tbad=%x\n}\n"
                ,expected ,pi ,bad);
        }
        return bad;
    }
    
    uint32_t check_hi()
    {
        uint32_t expected = this->po;
        uint32_t hi = this->tdr(this->ho);
        uint32_t bad = hi ^ expected;
        if( bad ){
            APRINTF("ERROR: PioTdr::check_hi(){"
                "\n\texpected=%x\n\tpi=%x\n\tbad=%x\n}\n"
                ,expected ,hi ,bad);
        }
        return bad;
    }
    
    int test_with_constants()
    {
        this->setup_pio_loopback();
        this->pio(0x89ABCDEF);
        this->tdr(0x01234567);
        
        this->setup_ho_to_pi();
        this->check_pi();
        this->check_hi();
        
        this->tdr_wen(0x0F0F0F0F);
        this->check_pi();
        this->check_hi();
        
        return this->check_pi();
    }
        
    int test_pseudo_rand(unsigned seed)
    {
        const uint32_t PO_0=0x89ABCDEF;
        const uint32_t HO_0=0x01234567;
        const uint32_t WEN_0=0x0F0F0F0F;
        uint32_t v = seed*PO_0-HO_0;
        
        this->setup_pio_loopback();
        this->pio(PO_0+v);
        this->tdr(HO_0+v);
        this->check_pi();
        
        this->setup_ho_to_pi();
        this->check_pi();
        this->check_hi();
        
        this->tdr_wen(WEN_0+v);
        this->check_pi();
        this->check_hi();
        return this->check_pi();
    }
    
};