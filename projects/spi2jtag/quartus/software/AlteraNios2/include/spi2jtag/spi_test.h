#pragma once
#include "altera/altera_stdint.h"
#include "altera/altera_stdio.h"
#include "bsp/spi/altera_spi.h"

#include "IF_DEBUG.h"

#ifndef SPI_BASE
    #error "#define SPI_BASE"
#endif
#ifndef CS_TAP
    #error "#define CS_TAP"
#endif
#ifndef CS_JTAG
    #error "#define CS_JTAG"
#endif
#ifndef CS_LOOPBACK
    #error "#define CS_LOOPBACK"
#endif

uint8_t spi_loopback( uint8_t tx ){
    return spi_transfer_u8(SPI_BASE, CS_LOOPBACK, tx, 0x00 );
}

int spi_count_test(uint8_t cs) {
    int rtn;
    rtn = 0;
    for (uint8_t cnt = 0; cnt < 0xFF; cnt++)
    {
        // TODO: use altera_spi_transfer_bit
        uint8_t data_rv;
        data_rv = spi_transfer_u8(SPI_BASE, cs, cnt,0x00 );
        if (data_rv != cnt) { 
            APRINTF("ERROR: (%x)=spi_transfer_u8(%x=cs), expected (%x)\n",data_rv,cs,cnt);
            rtn = -1; // ERROR
        } else {
            // IF_DEBUG(APRINTF("Success: (%x)=spi_transfer_u8(%x=cs), expected (%x)\n",data_rv,cs,cnt);)
        }
    }
    return rtn ;
}

int spi_test () {
    spi_count_test(CS_LOOPBACK);
    return 0;
};

void debug_spi_transaction(uint8_t tx = 0xBA, unsigned cs = CS_LOOPBACK){
    uint8_t rv = spi_transfer_u8(SPI_BASE, cs, tx, 0x00 );
    IF_DEBUG(APRINTF("%x=debug_spi_transaction(%x=tx,%x=cs)", rv, tx, cs));
}
