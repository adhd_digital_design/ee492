//#include <stdio.h>
//#include "alt_types.h"
#include "altera_stdint.h"
#include "system.h"
#include "altera_avalon_spi.h"
#include "altera_spi_transfer.h"
#include "sys/alt_stdio.h"
#include "lib/BinaryLiteral.h"
#define  ALT_STDIO_EN
#include "altera_stdio.h"
#include "altera_spi.h"
#include "JtagDevice.h"

void spi_send_ui() {
	//	uint8_t rtn;
	alt_printf("Waiting...\n");
	uint8_t x = scanf_x_8bit();
		// alt_printf("Sending...\n");
	//	rtn = 1;
	spi_transfer_u8(SPI_0_BASE, 0x00, x, 0x00);
	// alt_printf("%x=spi_transfer_u8(%x=x)\n", rtn, x);
}

void spi_send_test(uint8_t cs) {
	uint8_t x = scanf_x_4bit();
	// alt_printf("spi_transfer_u8(alt_u32 base, alt_u32 slave, uint8_t x, alt_u32 flags)\n");
	for (uint8_t cnt = 0; cnt < 0xFF; cnt++)
	{
		uint8_t data_rv;
		data_rv = spi_transfer_u8(SPI_0_BASE, cs, cnt,0x00 );
		alt_printf("%x=spi_transfer_u8(SPI_0_BASE, %x %x, 0x00);\n",data_rv,cs,cnt);
		// x = scanf_x_4bit();
	}
}

void spi_count_tests(){
	alt_printf("Send cnt to Loopback...\n");
	uint8_t c = alt_getchar();
	spi_send_test(0x02);
	alt_printf("Send cnt to TAPP...\n");
	c = alt_getchar();
	spi_send_test(0x00);
	alt_printf("Send cnt to HIPE...\n");
	spi_send_test(0x01);
}

int main() {
	while (true) {
		// spi_count_tests();

	}
}
