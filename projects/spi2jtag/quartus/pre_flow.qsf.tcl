

################################################################################
# Developer notes
################################################################################

# Use this with a quartus project. Can add in qsf as:
# set_global_assignment -name PRE_FLOW_SCRIPT_FILE "quartus_sh:pre_flow.qsf.tcl"

################################################################################
# See Quartus II Tcl Example: Automatic Script Execution
################################################################################
# https://www.altera.com/support/support-resources/design-examples/design-software/tcl/auto_processing.html

# The Quartus II software executes the scripts as shown here:

# <executable> -t <script name> <flow or module name> <project name> <revision name>
# The first argument passed in the quartus(args) variable is the name of the
# flow or module being executed, depending on the assignment you use. The
# second argument is the name of the project, and the third argument is the
# name of the revision.

# Because of the way the Quartus II software automatically runs the scripts,
# you need to use the post_message command to display messages, instead of the
# puts command.

################################################################################
# Configure
################################################################################
set module_name [lindex $quartus(args) 1]
set project_name [lindex $quartus(args) 2]
set revision_name [lindex $quartus(args) 3]

# Choose your TOP entity
# set top_entity_dir "top_spi2jtag"
# set top_entity_dir "top_dev_board_nios2"
# set top_entity_dir "top_spi_master"
# set top_entity_dir "top_spi_slave"
set top_entity_dir "top_hipe_spi"

################################################################################
# Open
################################################################################
if {[project_exists $project_name]} {
    project_open -current_revision $project_name
} else {
    throw {NONE} {No project with that name}
}

################################################################################
# Add to QSF
################################################################################

# Remove all assignments # -remove to remove unwanted files from qsf
remove_all_global_assignments -name SYSTEMVERILOG_FILE
remove_all_global_assignments -name VERILOG_FILE
remove_all_global_assignments -name QSYS_FILE

# Tell Quartus where to find hipe_* modules.
if {[file isdirectory ../../../../hipe/hipe_sv/hdl]} {
} else {
    post_message "HIPE DEPENDENCY : This project assumes hipe hdl is located at ../../../../hipe/hipe_sv/hdl relative to the pre_flow.qsf.tcl script"
    throw {NONE} {HIPE DEPENDENCY : Not Found}
}

set_global_assignment -name SEARCH_PATH ../../../../hipe/hipe_sv/hdl

# Tell Quartus where to find common util modules.
if {[file isdirectory ../../../firmware/common]} {
} else {
    post_message "COMMON DEPENDENCY : This project assumes common hdl is located at ../../../firmware/common relative to the pre_flow.qsf.tcl script"
    throw {NONE} {COMMON DEPENDENCY : Not Found}
}
set_global_assignment -name SEARCH_PATH ../../../firmware/common

# Project-Wide Assignments
set_global_assignment -name SYSTEMVERILOG_FILE ../hdl/$top_entity_dir/$top_entity_dir.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../hdl/jtag_tap_pkg.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../hdl/spi2jtag.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../hdl/hipe_spi.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../hdl/dev_board_hiper.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../hdl/nios2_spi_master.sv

# Project-Wide Assignments - Family Specific
set qsf_family [get_global_assignment -name FAMILY]
if {[string match "Cyclone II" $qsf_family]} {
    set_global_assignment -name SYSTEMVERILOG_FILE ../hdl/$top_entity_dir/DE2_TOP.v
    set_global_assignment -name QSYS_FILE ./nios2_spi_pio_q13.qsys

} elseif {[string match "MAX 10" $qsf_family]} {
    set_global_assignment -name SYSTEMVERILOG_FILE ../hdl/$top_entity_dir/DE10_LITE_TOP.v
    set_global_assignment -name QSYS_FILE ./nios2_spi_pio_q16.qsys

} elseif {[string match "Cyclone V" $qsf_family]} {
    set_global_assignment -name QSYS_FILE ./nios2_spi_pio_q16.qsys

    set top_level_entity [get_global_assignment -name TOP_LEVEL_ENTITY ]
    if {[string match "C5GT" $top_level_entity]} {
        set_global_assignment -name SYSTEMVERILOG_FILE ../hdl/$top_entity_dir/C5GT_PRO_TOP.v
    } else {
        set_global_assignment -name SYSTEMVERILOG_FILE ../hdl/$top_entity_dir/DE1_SOC_TOP.v
    }

} else {
    throw {NONE} {Family ($qsf_family) is not supported.}
}

# Reorganize QSF
export_assignments -reorganize

# User Message
post_message "Please see preflow tcl script to add or remove files to this project..."
