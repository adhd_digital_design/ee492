
`ifndef hipe_spi_
`define hipe_spi_
`include "jtag_tap/jtag_types.sv"

module hipe_spi #(
    parameter NUM_RW = 1,
    parameter NUM_RO = 0,
    parameter IR_LEN = 12,
    parameter DR_LEN = 8,
    parameter LSB_FIRST = 1
    // ,parameter NUMCS = 2 // TODO: Enable
) (
    input   wire  [NUM_RW*DR_LEN-1:0]  rw_port_i,
    output  wire  [NUM_RW*DR_LEN-1:0]  rw_latch_o,
    input   wire  [NUM_RO*DR_LEN-1:0]  ro_port_i,

    spi_inf spi // TODO: #(.NUMCS(NUMCS))

    , output wire [IR_LEN-1:0] jtag_ir // DEBUG_IR
    , output jtag_types::jtag_tap_fsm_state_t encoded_state // DEBUG_STATE
);

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
reg jtag_tdo;

jtag_inf jtag();
jtag_inf jtag_tdrs();
jtag_inf jtag_tap();

jtag_tap_fsm_one_hot_state_inf onehot_state();

wire [IR_LEN-1:0] feedback_ir;
// wire [IR_LEN-1:0] jtag_ir; // DEBUG_IR
// jtag_types::jtag_tap_fsm_state_t encoded_state; // DEBUG_STATE

//****************************************************************************
//                            Combinational logic
//****************************************************************************

//TODO: Array of jtag_inf , for 
// Assign Drivers
assign jtag_tap.tdi = jtag.tdi;
assign jtag_tap.tck = jtag.tck;
assign jtag_tap.tms = jtag.tms;
assign jtag_tap.trst_L = jtag.trst_L;

// Assign Drivers
assign jtag_tdrs.tdi = jtag.tdi;
assign jtag_tdrs.tck = jtag.tck;
assign jtag_tdrs.tms = jtag.tms;
assign jtag_tdrs.trst_L = jtag.trst_L;

// Output TDO
assign jtag.tdo  = jtag_tdo;

always @(*) begin : comb_jtag_tdo
    case(encoded_state)
        jtag_types::CAPTURE_DR,
        jtag_types::SHIFT_DR,
        jtag_types::EXIT1_DR,
        jtag_types::PAUSE_DR,
        jtag_types::EXIT2_DR,
        jtag_types::UPDATE_DR:          jtag_tdo = jtag_tdrs.tdo;
        jtag_types::SHIFT_IR:           jtag_tdo = jtag_tap.tdo;
        default:                        jtag_tdo = jtag_tap.tdo;
    endcase
end

//****************************************************************************
//                              Internal Modules
//****************************************************************************

spi2jtag s2j(
     .spi(spi)
    ,.jtag(jtag)
    ,.jtag_encoded_state(encoded_state)
);

jtag_tap_ctrl #(
    .JTAG_IR_WIDTH(IR_LEN)
) tap (
     .jtag(jtag_tap)
    ,.one_hot_state(onehot_state)
    ,.encoded_state(encoded_state)
    ,.ir_in(feedback_ir)
    ,.ir_out(jtag_ir)
);

hipe_jtag #(
    .NUM_RW(NUM_RW),
    .NUM_RO(NUM_RO),
    .IR_LEN(IR_LEN),
    .DR_LEN(DR_LEN)
) tdrs (
    .rw_port_i( rw_port_i ),
    .rw_latch_o( rw_latch_o ),
    .ro_port_i( ro_port_i ),

    .sdr({onehot_state.sdr & (~jtag.tms)}),
    .cdr(onehot_state.cdr),
    .udr(onehot_state.udr),
    .uir(onehot_state.uir),

    .ir_in(jtag_ir),
    .ir_out(feedback_ir),

    .tck(jtag_tdrs.tck),
    .tdi(jtag_tdrs.tdi),
    .tdo(jtag_tdrs.tdo)
    // TODO: pass onehot_state
);

endmodule

// hipe_spi #(
//     .NUM_RW(1),
//     .NUM_RO(0),
//     .IR_LEN(12),
//     .DR_LEN(8)
// ) hipe0 (
//     .rw_port_i(rw_port_i),
//     .rw_latch_o(rw_latch_o),
//     .ro_port_i(ro_port_i),

//     .spi(spi)

//     .jtag_ir() // DEBUG_IR
//     .encoded_state() // DEBUG_STATE
// );

`endif
