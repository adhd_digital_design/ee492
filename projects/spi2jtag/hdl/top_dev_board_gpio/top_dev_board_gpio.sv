
`ifndef TOP_DEV_BOARD_HIPER_NIOS2_
`define TOP_DEV_BOARD_HIPER_NIOS2_

module top_dev_board_hiper_nios2 #(
      parameter NGPIO=36
    , parameter NCLK=2
    , parameter NKEY=4
    , parameter NSW=16
    , parameter NLED=16
    , parameter NSEGS=6
) (
      inout  wire [NGPIO-1:0]         gpio
    , input  wire [NCLK-1:0]          clk
    , input  wire [NKEY-1:0]          key
    , input  wire [NSW-1:0]           sw
    , output wire [NLED-1:0]          led
    , output wire [(NSEGS*7)-1:0]     hex_L
    , spi_inf spi_slave // {tap, jtag, loopback}
);

dev_board_hiper #(
      .NGPIO(NGPIO)
    , .NCLK(NCLK)
    , .NKEY(NKEY)
    , .NSW(NSW)
    , .NLED(NLED)
    , .NSEGS(NSEGS)
) devboard (
      .gpio(gpio) // inout  wire [NGPIO-1:0]         gpio,
    , .clk(clk) // inout  wire [NCLK-1:0]          clk,
    , .key(key) // input  wire [NKEY-1:0]          key,
    , .sw(sw) // input  wire [NSW-1:0]           sw,
    , .led(led) // output wire [NLED-1:0]          led,
    , .hex_L(hex_L) // output wire [(NSEGS*HEX_W)-1:0] hex_L
    , .spi_slave(spi_slave) 
);

endmodule

`endif