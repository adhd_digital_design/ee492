
`ifndef spi_inf_
`define spi_inf_

// https://en.wikipedia.org/wiki/Serial_Peripheral_Interface_Bus

// At CPOL=0 the base value of the clock is zero, i.e. the idle state is 0 and
// active state is 1.
    // For CPHA=0, data are captured on the clock's rising edge (low→high
    // transition) and data is output on a falling edge (high→low clock
    // transition).
    // For CPHA=1, data are captured on the clock's falling edge and data is
    // output on a rising edge.
// At CPOL=1 the base value of the clock is one (inversion of CPOL=0), i.e.
// the idle state is 1 and active state is 0.
    // For CPHA=0, data are captured on clock's falling edge and data is
    // output on a rising edge.
    // For CPHA=1, data are captured on clock's rising edge and data is output
    // on a falling edge.

interface spi_inf ;
    parameter NUMCS = 2;

    // SCLK: Serial Clock (output from master).
    // Serial Clock: SCLK: SCK.
    wire sclk;

    // The MOSI/MISO convention requires that, on devices using the alternate
    // names, SDI on the master be connected to SDO on the slave, and vice
    // versa.
    // MOSI: Master Output Slave Input, or Master Out Slave In (data output from master).
    // Master Output Slave Input: MOSI: SIMO, SDI, DI, DIN, SI, MTSR.
    wire mosi;
    // MISO: Master Input Slave Output, or Master In Slave Out (data output from slave).
    // Master Input Slave Output: MISO: SOMI, SDO, DO, DOUT, SO, MRST.
    wire miso;

    // SDIO: Serial Data I/O (bidirectional I/O)
    // Serial Data I/O (bidirectional): SDIO: SIO
    // wire sdio; // TODO

    // SS: Slave Select (often active low, output from master).
    // Slave Select is the same functionality as chip select and is used
    // instead of an addressing concept.
    // Slave Select: SS: S̅S̅, SSEL, CS, C̅S̅, CE, nSS, /SS, SS#.
    // wire [NUMCS-1:0] cs;
    wire [NUMCS-1:0] cs_n; // TODO: Make active low input

endinterface

`endif
