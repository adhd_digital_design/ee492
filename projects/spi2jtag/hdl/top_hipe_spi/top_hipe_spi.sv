
`ifndef top_hipe_spi_
`define top_hipe_spi_

module top_hipe_spi #(
    parameter NGPIO=36,
    parameter NCLK=2,
    parameter NKEY=4,
    parameter NSW=10,
    parameter NLED=10,
    parameter NSEGS=6,
    parameter USE_QUARTUS_13=0 // (1) Quartus 13 (default) Quartus 16
) (
    inout  wire [NGPIO-1:0]         gpio,
    input  wire [NCLK-1:0]          clk,
    input  wire [NKEY-1:0]          key,
    input  wire [NSW-1:0]           sw,
    output wire [NLED-1:0]          led,
    output wire [(NSEGS*7)-1:0]     hex_L
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

// HIPE - Required
localparam NUM_RW = 4;  // Number of Read Write Registers in hipe_spi
localparam NUM_RO = 0;  // Number of Read Only Registers in hipe_spi
localparam DR_LEN = 8;  // Data Register lengths of Registers in hipe_spi
localparam IR_LEN = 12; // Match 8 bit ADDR_LEN and 4 bit OPCODE_LEN used in HIPE by default
localparam HIPE_LEN = DR_LEN*NUM_RW;
localparam HEX_LEN = NSEGS*4;

// SPI - Required
localparam NUMCS=10; // Number of Chip Selects in SPI

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
// hipe_spi - Required SPI Interface
spi_inf #(.NUMCS(NUMCS)) spi_slaves ();

// PioTdr feedback (pio_out->hipe->pio_in)
wire [HIPE_LEN-1:0] hino; // HIPE In  Nios Out
wire [HIPE_LEN-1:0] honi; // HIPE Out Nios In

// Jtag status
wire [IR_LEN-1:0] jtag_ir_out;
jtag_types::jtag_tap_fsm_state_t tap_state;
wire [31:0] jtag_status;

reg [3:0] led_select;
reg [3:0] hex_select;
reg [HEX_LEN-1:0] hex_reg;
reg [NLED-1:0] led_reg;

//****************************************************************************
//                             Sequential logic
//****************************************************************************

always @(posedge key[0]) begin : key0_hex_select
    hex_select = sw[3:0];
end

always @(posedge key[1]) begin : key1_led_select
    led_select = sw[3:0];
end


//****************************************************************************
//                            Combinational logic
//****************************************************************************

assign jtag_status = {jtag_ir_out[IR_LEN-1:0], tap_state};

always @(*) begin
    casex(hex_select) 
        4'b0100: hex_reg = { 4'h0, hino[0 +:DR_LEN], honi[0 +:DR_LEN] };
        4'b0101: hex_reg = { 4'h1, hino[1 +:DR_LEN], honi[1 +:DR_LEN] };
        4'b0110: hex_reg = { 4'h2, hino[2 +:DR_LEN], honi[2 +:DR_LEN] };
        4'b0111: hex_reg = { 4'h3, hino[3 +:DR_LEN], honi[3 +:DR_LEN] };
        default:
        /*4'b00xx:*/ hex_reg = jtag_status[0 +: HEX_LEN];    
    endcase
end

always @(*) begin
    casex(led_select) 
        default: led_reg = jtag_ir_out;    
    endcase
end


// View SPI on GPIO - ASSUME NUM_SPI_TO_READ = 5
assign gpio[10] = spi_slaves.cs_n[2];
assign gpio[8] = spi_slaves.cs_n[1];
assign gpio[6] = spi_slaves.cs_n[0];
assign gpio[4] = spi_slaves.sclk;
assign gpio[2] = spi_slaves.miso;
assign gpio[0] = spi_slaves.mosi;

//****************************************************************************
//                              Internal Modules
//****************************************************************************

// SPI Slave - Required
hipe_spi #(
    .NUM_RW(NUM_RW)
    ,.NUM_RO(NUM_RO)
    ,.IR_LEN(IR_LEN)
    ,.DR_LEN(DR_LEN)
) hipe0 (
     .rw_port_i(hino)
    ,.rw_latch_o(honi)

    ,.spi(spi_slaves) // HIPE contains multiple spi slaves

    ,.jtag_ir(jtag_ir_out)     // Used to Verify JTAG TAP IR on GPIO 
    ,.encoded_state(tap_state) // Used to Verify JTAG TAP STATE on GPIO
);

// NIOS2 and SPI Master, - Required
    // PIO used for SW based verification - Required for pio_test
generate
    if(USE_QUARTUS_13 == 1) begin
        nios2_spi_pio_q13 nios2_q13 (
            .clk_clk   (clk[0])   // clk.clk
            
            // PIO To Drive Parellel Load of a shift register - to PROVE SPI is working
            ,.po_in_port (honi)  // po_in_port  [31:0]
            ,.po_out_port(hino)  // po_out_port [31:0]
            
            // SPI Interface  
            ,.spi_MISO  (spi_slaves.miso)   // spi.MISO
            ,.spi_MOSI  (spi_slaves.mosi)   //    .MOSI
            ,.spi_SCLK  (spi_slaves.sclk)   //    .SCLK
            ,.spi_SS_n  (spi_slaves.cs_n)   //    .SS_n
        );
    end
    else begin
        nios2_spi_pio_q16 nios2_q16 (
            .clk_clk   (clk[0])  // clk.clk
            
            // PIO To Drive Parellel Load of a shift register - to PROVE SPI is working
            ,.po_in_port (honi)  // po_in_port  [31:0]
            ,.po_out_port(hino)  // po_out_port [31:0]
            
            // SPI Interface  
            ,.spi_MISO  (spi_slaves.miso)   // spi.MISO
            ,.spi_MOSI  (spi_slaves.mosi)   //    .MOSI
            ,.spi_SCLK  (spi_slaves.sclk)   //    .SCLK
            ,.spi_SS_n  (spi_slaves.cs_n)   //    .SS_n
        );
    end
endgenerate

// HEX Based Verification
display_on_hex #(
    .NSEGS(NSEGS)
) hex_display (
      .hex_L( hex_L )
    , .value( hex_reg )
);

endmodule

`endif
