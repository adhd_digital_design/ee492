
`ifndef top_spi_master_
`define top_spi_master_

module top_spi_master #(
    parameter NGPIO=36,
    parameter NCLK=2,
    parameter NKEY=4,
    parameter NSW=10,
    parameter NLED=10,
    parameter NSEGS=6,
    parameter USE_QUARTUS_13=0 // (1) Quartus 13 (default) Quartus 16
) (
    inout  wire [NGPIO-1:0]         gpio,
    input  wire [NCLK-1:0]          clk,
    input  wire [NKEY-1:0]          key,
    input  wire [NSW-1:0]           sw,
    output wire [NLED-1:0]          led,
    output wire [(NSEGS*7)-1:0]     hex_L
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

// HIPE - Required
localparam NUM_RW = 4;  // Number of Read Write Registers in hipe_spi
localparam NUM_RO = 0;  // Number of Read Only Registers in hipe_spi
localparam DR_LEN = 8;  // Data Register lengths of Registers in hipe_spi
localparam IR_LEN = 12; // Match 8 bit ADDR_LEN and 4 bit OPCODE_LEN used in HIPE by default

// SPI - Required
localparam NUMCS=10; // Number of Chip Selects in SPI Master

// View SPI on GPIO  
localparam NUM_SPI_TO_READ = 5+8; // cs[1:0] , sclk , miso, mosi // Used for gpio spacing
localparam SPI_SLAVES_OFFSET = 0*(NUM_SPI_TO_READ*2)+1 ; // ODD  // Top Left

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
// hipe_spi - Required SPI Interface
spi_inf #(.NUMCS(NUMCS)) spi_slaves ();

// GPIO Driven Slave - Required
wire [NUM_SPI_TO_READ-1:0] spi_slaves_wires; // Driven by spi_slaves

// HEX Based Verification
wire [NUM_SPI_TO_READ-1:0] status;

//****************************************************************************
//                            Combinational logic
//****************************************************************************

// HEX Verification
assign status = { spi_slaves_wires };
assign led = status;

// SPI Packet to view on GPIO    
assign spi_slaves_wires = {
     spi_slaves.cs_n[9],spi_slaves.cs_n[8]
    ,spi_slaves.cs_n[7],spi_slaves.cs_n[6]
    ,spi_slaves.cs_n[5],spi_slaves.cs_n[4]
    ,spi_slaves.cs_n[3],spi_slaves.cs_n[2]
    ,spi_slaves.cs_n[1]       // 4
    ,spi_slaves.cs_n[0]       // 3
    ,spi_slaves.sclk          // 2
    ,spi_slaves.miso          // 1
    ,spi_slaves.mosi          // 0
    } ;

// Drive SPI on GPIO - ASSUME NUM_SPI_TO_READ = 5+8 - Required
assign { 
         gpio[SPI_SLAVES_OFFSET+24],gpio[SPI_SLAVES_OFFSET+22] // 8 extra chip selects {cs[9:2]}
        ,gpio[SPI_SLAVES_OFFSET+20],gpio[SPI_SLAVES_OFFSET+18] 
        ,gpio[SPI_SLAVES_OFFSET+16],gpio[SPI_SLAVES_OFFSET+14] 
        ,gpio[SPI_SLAVES_OFFSET+12],gpio[SPI_SLAVES_OFFSET+10] 

                                  ,gpio[SPI_SLAVES_OFFSET+8] // Standard 5 signals in all other top modules
        ,gpio[SPI_SLAVES_OFFSET+6],gpio[SPI_SLAVES_OFFSET+4]
        /*,gpio[SPI_SLAVES_OFFSET+2] Do NOT DRIVE miso from MASTER*/  
                                    ,gpio[SPI_SLAVES_OFFSET+0]} = {
            spi_slaves_wires[NUM_SPI_TO_READ-1:2], spi_slaves.mosi } ; // ODD

// Assign miso from GPIO as an input of this->top_spi_master
assign spi_slaves.miso = gpio[SPI_SLAVES_OFFSET+2];

//****************************************************************************
//                              Internal Modules
//****************************************************************************

// SPI Slave - On a Seperate Development Board

// NIOS2 and SPI Master, - Required
    // PIO not used
generate
    if(USE_QUARTUS_13 == 1) begin
        nios2_spi_pio_q13 nios2_q13 (
            .clk_clk   (clk[0])   // clk.clk
            
            // PIO not used
            ,.po_in_port ()  // po_in_port  [31:0]
            ,.po_out_port()  // po_out_port [31:0]
            
            // SPI Interface  
            ,.spi_MISO  (spi_slaves.miso)   // spi.MISO
            ,.spi_MOSI  (spi_slaves.mosi)   //    .MOSI
            ,.spi_SCLK  (spi_slaves.sclk)   //    .SCLK
            ,.spi_SS_n  (spi_slaves.cs_n)   //    .SS_n
        );
    end
    else begin
        nios2_spi_pio_q16 nios2_q16 (
            .clk_clk   (clk[0])  // clk.clk
            
            // PIO not used
            ,.po_in_port ()  // po_in_port  [31:0]
            ,.po_out_port()  // po_out_port [31:0]
            
            // SPI Interface  
            ,.spi_MISO  (spi_slaves.miso)   // spi.MISO
            ,.spi_MOSI  (spi_slaves.mosi)   //    .MOSI
            ,.spi_SCLK  (spi_slaves.sclk)   //    .SCLK
            ,.spi_SS_n  (spi_slaves.cs_n)   //    .SS_n
        );
    end
endgenerate

// HEX Based Verification
display_on_hex #(
    .NSEGS(NSEGS)
) hex_display (
     .hex_L( hex_L )
    ,.value( status )
);

endmodule

`endif
