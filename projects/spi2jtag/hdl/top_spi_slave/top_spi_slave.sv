
`ifndef top_spi_slave_
`define top_spi_slave_

// Connects hipe_spi between switches and leds. Expects SPI on GPIO.
module top_spi_slave #(
    parameter NGPIO=6,
    parameter NCLK=2,
    parameter NKEY=4,
    parameter NSW=10,
    parameter NLED=10,
    parameter NSEGS=6
) (
    inout  wire [NGPIO-1:0]         gpio,
    input  wire [NCLK-1:0]          clk,
    input  wire [NKEY-1:0]          key,
    input  wire [NSW-1:0]           sw,
    output wire [NLED-1:0]          led,
    output wire [(NSEGS*7)-1:0]     hex_L
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

// HIPE
localparam NUM_RW = 4;  // Number of Read Write Registers in hipe_spi
localparam NUM_RO = 0;  // Number of Read Only Registers in hipe_spi
localparam DR_LEN = 8;  // Data Register lengths of Registers in hipe_spi
localparam IR_LEN = 12; // Match 8 bit ADDR_LEN and 4 bit OPCODE_LEN used in HIPE by default

// SPI slave on GPIO  
localparam NUMCS=3; // Number of Chip Selects in SPI
localparam GPIO_SPI_LEN = 3+NUMCS; // {cs , sclk , miso, mosi}

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

// hipe_spi - Required SPI Interface
spi_inf #(.NUMCS(NUMCS)) spi_slaves ();

// HW Based Verification
wire [DR_LEN*NUM_RW-1:0] hiso; // HIPE In  Something Out
wire [DR_LEN*NUM_RW-1:0] hosi; // HIPE Out Something In

wire [IR_LEN-1:0] jtag_ir_out;
jtag_types::jtag_tap_fsm_state_t tap_state;
wire [32-1:0] status;

//****************************************************************************
//                            Combinational logic
//****************************************************************************

// HEX Verification
assign status = {jtag_ir_out[IR_LEN-1:0], tap_state, hosi[4-1:0], hiso[4-1:0]};
assign led = status;

// Drive HIPE Parellel Input
assign hiso = {
      {DR_LEN*NUM_RW-NSW{1'b0}}
    , sw
};

// SPI from GPIO
assign gpio[0] = spi_slaves.miso;  // MISO is the one output wire
assign spi_slaves.mosi = gpio[1];
assign spi_slaves.sclk = gpio[2];
assign spi_slaves.cs_n[0+:NUMCS] = gpio[3+:NUMCS];

//****************************************************************************
//                              Internal Modules
//****************************************************************************

// SPI Slave - Required
hipe_spi #(
    .NUM_RW(NUM_RW)
    ,.NUM_RO(NUM_RO)
    ,.IR_LEN(IR_LEN)
    ,.DR_LEN(DR_LEN)
) hipe0 (
     .rw_port_i(hiso)
    ,.rw_latch_o(hosi)

    ,.spi(spi_slaves) // HIPE contains multiple spi slaves

    ,.jtag_ir(jtag_ir_out)     // Used to Verify JTAG TAP IR on GPIO 
    ,.encoded_state(tap_state) // Used to Verify JTAG TAP STATE on GPIO
);

// HEX Based Verification
display_on_hex #(
    .NSEGS(NSEGS)
) hex_display (
     .hex_L( hex_L )
    ,.value( status )
);

endmodule

`endif
