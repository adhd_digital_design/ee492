
`ifndef jtag_tap_pkg_
`define jtag_tap_pkg_

`include "jtag_tap/jtag_types.sv"
`include "jtag_tap/jtag_inf.sv"
`include "jtag_tap/jtag_tap_fsm_one_hot_state_inf.sv" 
`include "jtag_tap/jtag_tap_fsm.sv" 
`include "jtag_tap/jtag_tap_ctrl.sv"

`endif
