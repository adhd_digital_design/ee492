
`ifndef top_top_spi2jtag_
`define top_top_spi2jtag_

module top_spi2jtag #(
    parameter NGPIO=36,
    parameter NCLK=2,
    parameter NKEY=4,
    parameter NSW=10,
    parameter NLED=10,
    parameter NSEGS=6,
    parameter USE_QUARTUS_13=0 // (1) Quartus 13 (default) Quartus 16
) (
    inout  wire [NGPIO-1:0]         gpio,
    input  wire [NCLK-1:0]          clk,
    input  wire [NKEY-1:0]          key,
    input  wire [NSW-1:0]           sw,
    output wire [NLED-1:0]          led,
    output wire [(NSEGS*7)-1:0]     hex_L
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

// HIPE - Required
localparam NUM_RW = 4;  // Number of Read Write Registers in hipe_spi
localparam NUM_RO = 0;  // Number of Read Only Registers in hipe_spi
localparam DR_LEN = 8;  // Data Register lengths of Registers in hipe_spi
localparam IR_LEN = 12; // Match 8 bit ADDR_LEN and 4 bit OPCODE_LEN used in HIPE by default

// SPI - Required
localparam NUMCS=10; // Number of Chip Selects in SPI Master

// View SPI on GPIO  
localparam NUM_SPI_TO_READ = 5; // cs[1:0] , sclk , miso, mosi // Used for gpio spacing
localparam SPI_SLAVES_OFFSET = 0*(NUM_SPI_TO_READ*2)+1 ; // ODD  // Top Left
localparam JTAG_TAP_OFFSET   = 1*(NUM_SPI_TO_READ*2)+1 ; // ODD  // Top Right
localparam IR_OUT_OFFSET     = 0 ;                       // Even // Bottom Left


//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
// hipe_spi - Required SPI Interface
spi_inf #(.NUMCS(NUMCS)) spi_slaves ();

// PIO SW Based Verification
wire [DR_LEN*NUM_RW-1:0] hino; // HIPE In  Nios Out
wire [DR_LEN*NUM_RW-1:0] honi; // HIPE Out Nios In

// GPIO Based Verification
wire [IR_LEN-1:0] jtag_ir_out;               // Used for GPIO verification
jtag_types::jtag_tap_fsm_state_t tap_state;  // Used for GPIO verification

// GPIO Based Verification - Duplicate of spi_slaves
wire [NUM_SPI_TO_READ-1:0] spi_slaves_wires; // Driven by spi_slaves // Used for GPIO verification

// HEX Based Verification
wire [NSEGS*4-1:0] status;

//****************************************************************************
//                            Combinational logic
//****************************************************************************

// HEX Verification
assign status = {honi, jtag_ir_out[IR_LEN-1:0], tap_state};
assign led = status;

// SPI Packet to view on GPIO    
assign spi_slaves_wires = {
     spi_slaves.cs_n[1]       // 4
    ,spi_slaves.cs_n[0]       // 3
    ,spi_slaves.sclk          // 2
    ,spi_slaves.miso          // 1
    ,spi_slaves.mosi          // 0
    } ;

// View SPI on GPIO - ASSUME NUM_SPI_TO_READ = 5
assign {                           gpio[SPI_SLAVES_OFFSET+8]
        ,gpio[SPI_SLAVES_OFFSET+6],gpio[SPI_SLAVES_OFFSET+4]
        ,gpio[SPI_SLAVES_OFFSET+2],gpio[SPI_SLAVES_OFFSET+0]} = {
            spi_slaves_wires } ; // ODD

// View JTAG IR on GPIO - ASSUME IR_LEN = 12
assign {gpio[IR_OUT_OFFSET+22],gpio[IR_OUT_OFFSET+20],
        gpio[IR_OUT_OFFSET+18],gpio[IR_OUT_OFFSET+16],
        gpio[IR_OUT_OFFSET+14],gpio[IR_OUT_OFFSET+12],
        gpio[IR_OUT_OFFSET+10],gpio[IR_OUT_OFFSET+8],
        gpio[IR_OUT_OFFSET+6] ,gpio[IR_OUT_OFFSET+4],
        gpio[IR_OUT_OFFSET+2] ,gpio[IR_OUT_OFFSET+0]} = {
            jtag_ir_out } ; // Even

// View JTAG TAP on GPIO - ALWAYS 4-Bits
assign {gpio[JTAG_TAP_OFFSET+6],gpio[JTAG_TAP_OFFSET+4],
        gpio[JTAG_TAP_OFFSET+2],gpio[JTAG_TAP_OFFSET+0]} = {
            tap_state } ; // ODD

//****************************************************************************
//                              Internal Modules
//****************************************************************************

// SPI Slave - Required
hipe_spi #(
    .NUM_RW(NUM_RW)
    ,.NUM_RO(NUM_RO)
    ,.IR_LEN(IR_LEN)
    ,.DR_LEN(DR_LEN)
) hipe0 (
     .rw_port_i(hino)
    ,.rw_latch_o(honi)

    ,.spi(spi_slaves) // HIPE contains multiple spi slaves

    ,.jtag_ir(jtag_ir_out)     // Used to Verify JTAG TAP IR on GPIO 
    ,.encoded_state(tap_state) // Used to Verify JTAG TAP STATE on GPIO
);

// NIOS2 and SPI Master, - Required
    // PIO used for SW based verification - Required for pio_test
generate
    if(USE_QUARTUS_13 == 1) begin
        nios2_spi_pio_q13 nios2_q13 (
            .clk_clk   (clk[0])   // clk.clk
            
            // PIO To Drive Parellel Load of a shift register - to PROVE SPI is working
            ,.po_in_port (honi)  // po_in_port  [31:0]
            ,.po_out_port(hino)  // po_out_port [31:0]
            
            // SPI Interface  
            ,.spi_MISO  (spi_slaves.miso)   // spi.MISO
            ,.spi_MOSI  (spi_slaves.mosi)   //    .MOSI
            ,.spi_SCLK  (spi_slaves.sclk)   //    .SCLK
            ,.spi_SS_n  (spi_slaves.cs_n)   //    .SS_n
        );
    end
    else begin
        nios2_spi_pio_q16 nios2_q16 (
            .clk_clk   (clk[0])  // clk.clk
            
            // PIO To Drive Parellel Load of a shift register - to PROVE SPI is working
            ,.po_in_port (honi)  // po_in_port  [31:0]
            ,.po_out_port(hino)  // po_out_port [31:0]
            
            // SPI Interface  
            ,.spi_MISO  (spi_slaves.miso)   // spi.MISO
            ,.spi_MOSI  (spi_slaves.mosi)   //    .MOSI
            ,.spi_SCLK  (spi_slaves.sclk)   //    .SCLK
            ,.spi_SS_n  (spi_slaves.cs_n)   //    .SS_n
        );
    end
endgenerate

// HEX Based Verification
display_on_hex #(
    .NSEGS(NSEGS)
) hex_display (
     .hex_L( hex_L )
    ,.value( status )
);

endmodule

`endif
