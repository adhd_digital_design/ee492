
`ifndef DEV_BOARD_HIPER_
`define DEV_BOARD_HIPER_

`define TDR_BITS_TO_BYTES(bits) (((bits-1)/8)+1)

// Primary intention is to connect as much of a dev board to hipe as possible
// and as configurable as possible.
// I am starting with a internal nios, but will refactor later to bring jtag or
// SPI signals out.
module dev_board_hiper #(
      parameter NGPIO=36
    , parameter NCLK=2
    , parameter NKEY=4
    , parameter NSW=16
    , parameter NLED=16
    , parameter NSEGS=6
) (
      inout  wire [NGPIO-1:0]         gpio
    , input  wire [NCLK-1:0]          clk
    , input  wire [NKEY-1:0]          key
    , input  wire [NSW-1:0]           sw
    , output wire [NLED-1:0]          led
    , output wire [(NSEGS*7)-1:0]     hex_L
    , spi_inf spi_slave // {tap, jtag, loopback}
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************
localparam NUM_BITS = 8;
localparam NUM_RW = 8;
localparam DR_LEN = 8;

localparam NHEX = (NSEGS*4);

localparam IHEX = 0; // HEX<-hex_latch_w<-HIPE<-hex_value_reg<- XXX
localparam ILED = (IHEX+NHEX); // led<-HIPE<-led_reg<- XXX
localparam ISW = (ILED+NLED); // sw->HIPE->sw_w-> XXX
localparam ISW2 = (ISW+NSW);
localparam IGPIO0 = (ISW2+NSW);

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
wire [(DR_LEN*NUM_RW)-1:0] hipe_port_w;
wire [(DR_LEN*NUM_RW)-1:0] hipe_latch_w;

wire [NHEX-1:0] hex_latch_w;
reg [NHEX-1:0] hex_value_reg;

wire [NHEX-1:0] led_w;
reg [NHEX-1:0] led_reg;

wire [NSW-1:0] sw_w;
wire [NSW-1:0] sw_clk0_w;
reg [NSW-1:0] sw_clk0_reg;
//****************************************************************************
//                             Sequential logic
//****************************************************************************

always @(posedge clk[0]) begin
    hex_value_reg  = sw_w;
end

always @(posedge clk[0]) begin
    led_reg  = sw_w;
end

always @(posedge clk[0]) begin
    sw_clk0_reg  = sw;
end

//****************************************************************************
//                            Colsbinational logic
//****************************************************************************

// hipe.out
assign hex_latch_w = hipe_latch_w[IHEX+:NHEX];
assign led = hipe_latch_w[ILED+:NLED];
 assign sw_w = hipe_port_w[ISW+:NSW];
// assign sw_clk0_w = hipe_port_w[ISW1+:NSW];


// hipe.in
// assign hipe_port_w[IHEX+:NHEX] = hex_value_reg;
// assign hipe_port_w[ILED+:NLED] = led_reg;
assign hipe_port_w[ISW+:NSW] = sw;
assign hipe_port_w[ISW1+:NSW] = sw_clk0_reg;

//****************************************************************************
//                              Internal Modules                    dev_board
//****************************************************************************
display_on_hex #(
    .NSEGS(NSEGS)
) hex_display (
      .hex_L( hex_L )
    , .value( hex_latch_w )
);

//****************************************************************************
//                              Internal Modules                    hipe_nios2
//****************************************************************************
hipe_spi #(
      .NUM_RW(NUM_RW)
    , .NUM_RO(0)
    , .DR_LEN(DR_LEN)
) hipe0 (
     .rw_port_i(hipe_port_w)
    , .rw_latch_o(hipe_latch_w)

    , .spi(spi_slave)
);

endmodule

`endif