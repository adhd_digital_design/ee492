// jtag_tap_ctrl is modeled after the standard IEEE 1149.1-2003 JTAG Test Access Point Controller
// JTAG Tap Reference: https://github.com/lgeek/adv_debug_sys/blob/master/Hardware/jtag/tap/rtl/verilog/tap_top.v


`ifndef jtag_tap_ctrl_
`define jtag_tap_ctrl_

`include "jtag_types.sv"
`include "hipe_tdr_inf.sv"
`include "hipe_rw.sv"

module jtag_tap_ctrl #(
    parameter JTAG_IR_WIDTH = 12 // Match 8 bit ADDR_LEN and 4 bit OPCODE_LEN used in HIPE by default
) (
    jtag_inf                                jtag,
    // jtag_tap_ctrl takes state from jtag_tap_fsm and controls instruction register
    jtag_tap_fsm_one_hot_state_inf          one_hot_state,
    output jtag_types::jtag_tap_fsm_state_t encoded_state,
    input  wire [JTAG_IR_WIDTH-1:0]         ir_in,
    output wire [JTAG_IR_WIDTH-1:0]         ir_out
);

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
hipe_tdr_inf ir_inf (); // udr,cdr,sdr,so,si,tck... ir is just like a tdr
reg ir_so_delayed;

//****************************************************************************
//                            Combinational logic
//****************************************************************************

// TDR Control:
assign ir_inf.udr  =   one_hot_state.uir; // Uptate (latch) Instruction Register
assign ir_inf.cdr  =   one_hot_state.cir; // Capture Instruction Register
// assign ir_inf.sdr  =   one_hot_state.sir; // Shift Instruction Register
assign ir_inf.sdr  =   (one_hot_state.sir & (~jtag.tms)); // Shift Instruction Register


// TDR Data:
assign jtag.tdo       = ir_so_delayed; // Test Data (Shift) out
assign ir_inf.si      = jtag.tdi; // Test Data (Shift) in
assign ir_inf.tck     = jtag.tck; // Test Clock



//****************************************************************************
//                              Sequential Logic
//****************************************************************************

always @(negedge ir_inf.tck) begin : update_so_after_check
    ir_so_delayed = ir_inf.so;
end


//****************************************************************************
//                              Internal Modules
//****************************************************************************

// Tap Controller FSM
jtag_tap_fsm jtfsm (
     .jtag(jtag)
    ,.one_hot_state(one_hot_state)
    ,.encoded_state(encoded_state)
);

// jtag_ir:  JTAG Instruction Register
hipe_rw #(
    .DR_LEN(JTAG_IR_WIDTH)
) jtag_ir (
    .in(ir_in),
    .out(ir_out),
    .write_enable_i({JTAG_IR_WIDTH{1'b1}}), // use the latched IR value, if write_enable_i[0]=0 then ir_out[x]==ir_in[x]
    .tdr_inf(ir_inf) // udr,cdr,sdr,so,si,tck... ir is just like a tdr
);

endmodule

// jtag_tap_ctrl #(
//     .JTAG_IR_WIDTH(IR_LEN)
// ) jtc (
//     .jtag(jtag_inf_0)
//     ,.one_hot_state(jtag_tap_fsm_one_hot_state_inf_0)
//     ,.encoded_state(encoded_state)
//     ,.ir_in(jtag_ir_in)
//     ,.ir_out(jtag_ir_out)
// );
`endif
