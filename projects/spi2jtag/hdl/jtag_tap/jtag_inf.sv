// jtag_inf contains standard IEEE 1149.1 signals used for controling a JTAG Scan Chain Tap Controller

`ifndef jtag_inf_
`define jtag_inf_

interface jtag_inf ;

    // Test data in (TDI), used to shift data into the IR and DR shift
    // register chains
    wire tdi; // Test Data (Shift) in

    // Test data out (TDO), used to shift data out of the IR and DR shift
    // register chains
    wire tdo; // Test Data (Shift) out

    // TODO: Implement tdo_oe
    // Shift control
    // wire tdo_oe; // Test Data (Shift) out output enable

    // TCK, used as the clock source for the JTAG circuitry
    wire tck; // Test Clock

    // Clocking changes on TMS steps through a standardized JTAG state
    // machine. The JTAG state machine can reset, access an instruction
    // register, or access data selected by the instruction register.
    wire tms; // Test Mode Select

    // TODO: Implement trst_L for the instruction register
    // The TRST pin is an optional active-low reset to the test logic
    // - usually asynchronous, but sometimes synchronous, depending on the
    // chip. If the pin is not available, the test logic can be reset by
    // switching to the reset state synchronously, using TCK and TMS. Note
    // that resetting test logic doesn't necessarily imply resetting
    // anything else. There are generally some processor-specific JTAG
    // operations which can reset all or part of the chip being debugged.
    wire trst_L; // Test Reset _L Active Low

endinterface

`endif
