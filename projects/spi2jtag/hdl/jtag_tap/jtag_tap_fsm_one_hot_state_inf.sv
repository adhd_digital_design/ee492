// jtag_tap_fsm_one_hot_state_inf contains standard IEEE 1149.1 one hot state signals for a JTAG Scan Chain tap controller
// One-hot states from "jtag_tap_ctrl_state_t"
// State names from pg39 Altera's virtual jtag module document.
// https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/ug/ug_virtualjtag.pdf

`ifndef jtag_tap_fsm_one_hot_state_inf_
`define jtag_tap_fsm_one_hot_state_inf_
interface jtag_tap_fsm_one_hot_state_inf ;
    wire tlr;    //TEST_LOGIC_RESET = 4'h0
    wire rti;    //RUN_TEST_IDLE    = 4'h1
    wire sdrs;   //SELECT_DR   = 4'h2
    wire sirs;   //SELECT_IR   = 4'h3
    wire cir;    //CAPTURE_IR  = 4'h4
    wire sir;    //SHIFT_IR    = 4'h5
    wire e1ir;   //EXIT1_IR    = 4'h6
    wire pir;    //PAUSE_IR    = 4'h7
    wire e2ir;   //EXIT2_IR    = 4'h8
    wire uir;    //UPDATE_IR   = 4'h9
    wire cdr;    //CAPTURE_DR  = 4'hA
    wire sdr;    //SHIFT_DR    = 4'hB
    wire e1dr;   //EXIT1_DR    = 4'hC
    wire pdr;    //PAUSE_DR    = 4'hD
    wire e2dr;   //EXIT2_DR    = 4'hE
    wire udr;    //UPDATE_DR   = 4'hF
endinterface

`endif
