// jtag_tap_fsm is modeled after the standard IEEE 1149.1-2003 JTAG Test Access Point Controller
// JTAG Tap Reference: https://github.com/lgeek/adv_debug_sys/blob/master/Hardware/jtag/tap/rtl/verilog/tap_top.v

`ifndef jtag_tap_fsm_
`define jtag_tap_fsm_

`include "jtag_types.sv"

module jtag_tap_fsm (
    jtag_inf                                jtag,
    jtag_tap_fsm_one_hot_state_inf          one_hot_state,
    output jtag_types::jtag_tap_fsm_state_t encoded_state
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

// FSM State Variables
jtag_types::jtag_tap_fsm_state_t next_state;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************

always @(posedge jtag.tck or negedge jtag.trst_L) begin : fsm_logic
    if(jtag.trst_L == 0) encoded_state <= jtag_types::TEST_LOGIC_RESET; // Async Reset
    else                 encoded_state <= next_state; 
end

//****************************************************************************
//                             Sequential logic
//****************************************************************************

//****************************************************************************
//                            Combinational logic
//****************************************************************************

// Assign State Signals
always @ (*) begin : next_state_logic

    case(encoded_state)
        jtag_types::TEST_LOGIC_RESET:  begin
                            if(jtag.tms) next_state = jtag_types::TEST_LOGIC_RESET ;
                            else               next_state = jtag_types::RUN_TEST_IDLE ;
        end
        jtag_types::RUN_TEST_IDLE:     begin
                            if(jtag.tms) next_state = jtag_types::SELECT_DR ;
                            else               next_state = jtag_types::RUN_TEST_IDLE ;
        end
        jtag_types::SELECT_DR:         begin
                            if(jtag.tms) next_state = jtag_types::SELECT_IR ;
                            else               next_state = jtag_types::CAPTURE_DR ;
        end
        jtag_types::SELECT_IR:         begin
                            if(jtag.tms) next_state = jtag_types::TEST_LOGIC_RESET ;
                            else               next_state = jtag_types::CAPTURE_IR ;
        end
        jtag_types::CAPTURE_IR:        begin
                            if(jtag.tms) next_state = jtag_types::EXIT1_IR ;
                            else               next_state = jtag_types::SHIFT_IR ;
        end
        jtag_types::SHIFT_IR:          begin
                            if(jtag.tms) next_state = jtag_types::EXIT1_IR ;
                            else               next_state = jtag_types::SHIFT_IR ;
        end
        jtag_types::EXIT1_IR:          begin
                            if(jtag.tms) next_state = jtag_types::UPDATE_IR ;
                            else               next_state = jtag_types::PAUSE_IR ;
        end
        jtag_types::PAUSE_IR:          begin
                            if(jtag.tms) next_state = jtag_types::EXIT2_IR ;
                            else               next_state = jtag_types::PAUSE_IR ;
        end
        jtag_types::EXIT2_IR:          begin
                            if(jtag.tms) next_state = jtag_types::UPDATE_IR ;
                            else               next_state = jtag_types::SHIFT_IR ;
        end
        jtag_types::UPDATE_IR:         begin
                            if(jtag.tms) next_state = jtag_types::SELECT_DR ;
                            else               next_state = jtag_types::RUN_TEST_IDLE ;
        end
        jtag_types::CAPTURE_DR:        begin
                            if(jtag.tms) next_state = jtag_types::EXIT1_DR ;
                            else               next_state = jtag_types::SHIFT_DR ;
        end
        jtag_types::SHIFT_DR:          begin
                            if(jtag.tms) next_state = jtag_types::EXIT1_DR ;
                            else               next_state = jtag_types::SHIFT_DR ;
        end
        jtag_types::EXIT1_DR:          begin
                            if(jtag.tms) next_state = jtag_types::UPDATE_DR ;
                            else               next_state = jtag_types::PAUSE_DR ;
        end
        jtag_types::PAUSE_DR:          begin
                            if(jtag.tms) next_state = jtag_types::EXIT2_DR ;
                            else               next_state = jtag_types::PAUSE_DR ;
        end
        jtag_types::EXIT2_DR:          begin
                            if(jtag.tms) next_state = jtag_types::UPDATE_DR ;
                            else               next_state = jtag_types::SHIFT_DR ;
        end
        jtag_types::UPDATE_DR:         begin
                            if(jtag.tms) next_state = jtag_types::SELECT_DR ;
                            else               next_state = jtag_types::RUN_TEST_IDLE ;
        end
        default: next_state = jtag_types::TEST_LOGIC_RESET ; // RESET @ INVALID STATEs
    endcase
end

// Assign One Hot State Signals
assign one_hot_state.tlr  = (encoded_state == jtag_types::TEST_LOGIC_RESET ) ;
assign one_hot_state.rti  = (encoded_state == jtag_types::RUN_TEST_IDLE    ) ;
assign one_hot_state.sdrs = (encoded_state == jtag_types::SELECT_DR        ) ;
assign one_hot_state.sirs = (encoded_state == jtag_types::SELECT_IR        ) ;
assign one_hot_state.cir  = (encoded_state == jtag_types::CAPTURE_IR       ) ;
assign one_hot_state.sir  = (encoded_state == jtag_types::SHIFT_IR         ) ;
assign one_hot_state.e1ir = (encoded_state == jtag_types::EXIT1_IR         ) ;
assign one_hot_state.pir  = (encoded_state == jtag_types::PAUSE_IR         ) ;
assign one_hot_state.e2ir = (encoded_state == jtag_types::EXIT2_IR         ) ;
assign one_hot_state.uir  = (encoded_state == jtag_types::UPDATE_IR        ) ;
assign one_hot_state.cdr  = (encoded_state == jtag_types::CAPTURE_DR       ) ;
assign one_hot_state.sdr  = (encoded_state == jtag_types::SHIFT_DR         ) ;
assign one_hot_state.e1dr = (encoded_state == jtag_types::EXIT1_DR         ) ;
assign one_hot_state.pdr  = (encoded_state == jtag_types::PAUSE_DR         ) ;
assign one_hot_state.e2dr = (encoded_state == jtag_types::EXIT2_DR         ) ;
assign one_hot_state.udr  = (encoded_state == jtag_types::UPDATE_DR        ) ;

//****************************************************************************
//                             Procedural Statements
//****************************************************************************

//****************************************************************************
//                              Internal Modules
//****************************************************************************

endmodule

`endif
