
`ifndef spi2jtag_
`define spi2jtag_

`include "spi_inf.sv"
`include "shift_reg_async_ld.sv"
`include "jtag_tap/jtag_inf.sv"
`include "jtag_tap/jtag_types.sv"

module spi2jtag ( // ,parameter NUMCS = 2 // TODO: Enable
    spi_inf spi, // TODO: #(.NUMCS(NUMCS))
    jtag_inf jtag,
    input jtag_types::jtag_tap_fsm_state_t jtag_encoded_state 
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

localparam TSR_DR_LEN = 8;

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
wire cs_n_tap;
wire cs_n_jtag;
wire cs_n_loopback;

wire tapp_state_so;
// hipe_tdr_inf tsr_tdr (); // udr,cdr,sdr,so,si,tck

// reg now_reg;
// reg previous_reg;

reg tapp_state_so_delayed;

//****************************************************************************
//                             Sequential logic
//****************************************************************************
//(sudo) always @( posedge cs_n_tap, posedge spi.sclk ) begin : set_cdr_logic
//(sudo)         previous_reg    <= now_reg; // Shift now to previous_reg;
//(sudo)         now_reg         <= cs_n_tap; // Update current.
//(sudo) end

always @(negedge spi.sclk) begin : update_so_after_check
    tapp_state_so_delayed = tapp_state_so;
end

//****************************************************************************
//                            Combinational logic
//****************************************************************************
assign cs_n_tap      = spi.cs_n[0]; // Shift out jtag_encoded_state
assign cs_n_jtag     = spi.cs_n[1]; // Shift out Instruction Register or Data Register
assign cs_n_loopback = spi.cs_n[2]; // Loopback

assign jtag.trst_L = 1'b1; // Unused Active low reset
assign jtag.tck = spi.sclk; // Test Data Clock
assign jtag.tdi = spi.mosi; // Test Data (Shift) in

// Allow tdi to control Tap FSM when tap is enabled.
// When disabled the Tap will move to and stay at nearest stable state. (on tck)
// Will steady out to one of these states: RTI, SDR, PDR, SIR, PIR.
assign jtag.tms = ( (1'b0==cs_n_tap     )  ? spi.mosi   : // Test Mode Select
                                             1'b0      ); // Default 

assign spi.miso = ( (1'b0==cs_n_jtag    )  ? jtag.tdo   : // Test Data (Shift) out
                    (1'b0==cs_n_tap     )  ? tapp_state_so_delayed :
                    (1'b0==cs_n_loopback)  ? spi.mosi   : 
                                             1'b1      ); // Default

// // tap_shift_reg
// // TDR Control:
// assign tsr_tdr.udr  =   1'b0; // Uptate (latch) Instruction Register
// // TODO: Debug cdr, we cannot get the previous state by running spi transactions with cs_n_tap twice @NG
// // Capture has priority over shift.
// assign tsr_tdr.cdr  =   cs_n_tap; // Capture on sck if not selecting tap
// // (sudo) WARNING: First sck taken to capture
// // (sudo) WARNING: Second sck taken to reset previous_reg
// // (sudo) assign tsr_tdr.cdr  =   (previous_reg==now_reg); // Capture on sck if last shift was cs_n_tap 
// assign tsr_tdr.sdr  =   1'b1; // Shift 

// // TDR Data:
// // assign spi.miso        = tsr_tdr.so; // Test Data (Shift) out
// assign tsr_tdr.si      = 1'b1; // Test Data (Shift) in
// assign tsr_tdr.tck     = jtag.tck; // Test Clock

//****************************************************************************
//                              Internal Modules
//****************************************************************************
// // TODO: use a shift register which captures on negedge cs_n_tap (no sclk required), and shifts on sclk...
// hipe_rw #(
//     .DR_LEN(TSR_DR_LEN)
// ) tap_shift_reg ( // tsr
//     .in({{TSR_DR_LEN-4{1'b0}},jtag_encoded_state}), // TAP State is ALWAYS 4-bits
//     .out(), // Unused
//     .write_enable_i({TSR_DR_LEN{1'b1}}), // use the latched value, if write_enable_i[0]=0 then out[x]==in[x]
//     .tdr_inf(tsr_tdr) // udr,cdr,sdr,so,si,tck
// );

shift_reg_async_ld #(
    .n(TSR_DR_LEN)
) tap_shift_reg (
     .clk(spi.sclk)
    ,.load(cs_n_tap) // TODO: use counter reset at cs_n_tap to continuously shift out state
    ,.reset(0)
    ,.value_i({{TSR_DR_LEN-4{1'b0}},jtag_encoded_state}) // TAP State is ALWAYS 4-bits
    ,.shift(1'b1)// Always Shift on sck 
    ,.s_i(spi.mosi)
    ,.s_o(tapp_state_so)
    ,.value_o()// Unused
);

endmodule

`endif
