
// Use a Qsys system with a Nios2 system to act as SPI master.
module nios2_spi_master #(
      parameter NCLK=1
    , parameter NGPIO=6
    , parameter USE_QUARTUS_13=0
) (
      input wire [NCLK-1:0]          clk
    , inout wire [NGPIO-1:0]         gpio
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************
// SPI slave on GPIO  
localparam NUMCS=3; // Number of Chip Selects in SPI
localparam GPIO_SPI_LEN = 3+NUMCS; // {cs , sclk , miso, mosi}

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
spi_inf #(.NUMCS(NUMCS)) spi_slaves ();

//****************************************************************************
//                            Combinational logic
//****************************************************************************
assign spi_slaves.miso = gpio[0];
assign gpio[1] = spi_slaves.mosi; // MISO is the one output wire
assign spi_slaves.sclk = gpio[2];
assign spi_slaves.cs_n[0 +: NUMCS] = gpio[3 +: NUMCS];

//****************************************************************************
//                              Internal Modules
//****************************************************************************
generate
    if(USE_QUARTUS_13 == 1) begin
        nios2_spi_pio_q13 nios2_q13 (
            .clk_clk   (clk[0])   // clk.clk
            
            // PIO not used
            ,.po_in_port ()  // po_in_port  [31:0]
            ,.po_out_port()  // po_out_port [31:0]
            
            // SPI Interface  
            ,.spi_MISO  (spi_slaves.miso)   // spi.MISO
            ,.spi_MOSI  (spi_slaves.mosi)   //    .MOSI
            ,.spi_SCLK  (spi_slaves.sclk)   //    .SCLK
            ,.spi_SS_n  (spi_slaves.cs_n)   //    .SS_n
        );
    end
    else begin
        nios2_spi_pio_q16 nios2_q16 (
            .clk_clk   (clk[0])  // clk.clk
            
            // PIO not used
            ,.po_in_port ()  // po_in_port  [31:0]
            ,.po_out_port()  // po_out_port [31:0]
            
            // SPI Interface  
            ,.spi_MISO  (spi_slaves.miso)   // spi.MISO
            ,.spi_MOSI  (spi_slaves.mosi)   //    .MOSI
            ,.spi_SCLK  (spi_slaves.sclk)   //    .SCLK
            ,.spi_SS_n  (spi_slaves.cs_n)   //    .SS_n
        );
    end
endgenerate

endmodule
