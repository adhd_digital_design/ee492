

// module jtag_slave (
//     input wire TCK; // PROG_TCK Test Clock Pin Output TCK Input
//     input wire TDI; // PROG_TDI Test Data Input Output TDI Input
//     output wire TDO; // PROG_TDO Test Data Output Input TDO Output
//     input wire TMS; // PROG_TMS Test Mode Select Input TMS Output
// ):

// endmodule

// TODO: spi_inf
module spi_slave (
    input wire SCK, // SCK Serial Clock Output
    input wire MOSI, // MOSI Master OUT, Slave In Output
    input wire CS, // Chip Select (CS) Chip Select Output 
    output wire MISO, // MISO Master IN, Slave OUT Input

    input wire [1:0] addr, // {0:tap, 1:dr, 2:ir, 3:bypass}
    input wire tap_tck,
    input wire tap_tms,
    output wire [12:0] jtag_ir,
    output wire [15:0] jtag_tap_state
);

//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************
// HIPE
localparam NUM_RW = 1;
localparam NUM_RO = 1;
localparam DR_LEN = 8;
localparam IR_LEN = 12; // Match 8 bit ADDR_LEN and 4 bit OPCODE_LEN used in HIPE by default

// At CPOL=0 the base value of the clock is zero, i.e. the idle state is 0 and
// active state is 1.
    // For CPHA=0, data are captured on the clock's rising edge (low→high
    // transition) and data is output on a falling edge (high→low clock
    // transition).
    // For CPHA=1, data are captured on the clock's falling edge and data is
    // output on a rising edge.
// At CPOL=1 the base value of the clock is one (inversion of CPOL=0), i.e.
// the idle state is 1 and active state is 0.
    // For CPHA=0, data are captured on clock's falling edge and data is
    // output on a rising edge.
    // For CPHA=1, data are captured on clock's rising edge and data is output
    // on a falling edge.
// https://en.wikipedia.org/wiki/Serial_Peripheral_Interface_Bus
localparam CPOL=0;
localparam CPHA=1;

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
wire tdi;
wire tck;
reg miso_reg;
reg bypass_reg;
reg tap_tms_reg;
reg tap_tck_reg;

jtag_inf  jtag_inf_0();

jtag_inf  tap_jtag_inf();
jtag_tap_fsm_one_hot_state_inf tap_state_inf();

//****************************************************************************
//                            Combinational logic
//****************************************************************************
assign tdi = MOSI;
assign tck = SCK;

assign jtag_ir = jtag_ir_in;
assign MISO = miso_reg;

assign jtag_inf_0.tck = tck;
assign jtag_inf_0.tdi = tdi;

assign tap_jtag_inf.tck = tap_tck_reg;
assign tap_jtag_inf.tms = tap_tms_reg;
assign tap_jtag_inf.tdi = tdi;
assign tap_jtag_inf.trst_L = 1'b1; // never reset.

assign jtag_tap_state = {
      tap_state_inf.tlr
    , tap_state_inf.rti
    , tap_state_inf.sdrs
    , tap_state_inf.sirs
    , tap_state_inf.cir
    , tap_state_inf.sir
    , tap_state_inf.e1ir
    , tap_state_inf.pir
    , tap_state_inf.e2ir
    , tap_state_inf.uir
    , tap_state_inf.cdr
    , tap_state_inf.sdr
    , tap_state_inf.e1dr
    , tap_state_inf.pdr
    , tap_state_inf.e2dr
    , tap_state_inf.udr
    };

always @(*) begin : comb_miso
    case(addr)
        0: miso_reg = 1'b0;
        1: miso_reg = jtag_inf_0.tdo;
        2: miso_reg = tap_jtag_inf.tdo;
        3: miso_reg = bypass_reg;
    endcase
end

always @(*) begin : comb_tap_tck
    case(addr)
        0:          tap_tck_reg = tck;
        2:          tap_tck_reg = tck;
        default:    tap_tck_reg = tap_tck;
    endcase
end

always @(*) begin : comb_tap_tms
    case(addr)
        0:          tap_tms_reg = tdi;
        default:    tap_tms_reg = tap_tms;
    endcase
end

//****************************************************************************
//                             Sequential logic
//****************************************************************************
always @(posedge SCK) begin : sck_bypass // See CPHA
    bypass_reg = tdi;
end

//****************************************************************************
//                              Internal Modules
//****************************************************************************
hipe_jtag #(
    .NUM_RW(NUM_RW),
    .NUM_RO(NUM_RO),
    .IR_LEN(IR_LEN),
    .DR_LEN(DR_LEN)
) jtag_use_hipe (
    .rw_port_i ( {NUM_RW*8 {1'b1} } ),
    .rw_latch_o(),
    .ro_port_i ( {NUM_RO*8 {1'b1} } ),

    .sdr(tap_state_inf.sdr),
    .cdr(tap_state_inf.cdr),
    .udr(tap_state_inf.udr),
    .uir(tap_state_inf.uir),

    .ir_in(jtag_ir_in),
    .ir_out(),

    .tck(jtag_inf_0.tck),
    .tdi(jtag_inf_0.tdi),
    .tdo(jtag_inf_0.tdo)
);

jtag_tap_ctrl #(
    .JTAG_IR_WIDTH(IR_LEN)
) jtc (
      .jtag(tap_jtag_inf)
    , .one_hot_state(tap_state_inf)
    , .ir_in(jtag_ir_in)
    , .ir_out()
);

endmodule

// reg [2:0] i_bit = 0;
// reg [7:0] s_reg;
// wire [1:0] opcode_w;
// wire [2:0] n_bytes_w;
// wire [2:0] n_bits_in_lsb_w;

// always @(SCK) begin : clk_i_bit
//     i_bit=i_bit+1;
// end

// always @(SCK) begin : clk_sreg
//     if( !CS ) begin
//         sreg=00;
//     end
//     else begin
//         sreg=sreg;
//         case(i_bit) begin
//             0: sreg[7] = MOSI;
//             1: sreg[6] = MOSI;
//             2: sreg[5] = MOSI;
//             3: sreg[4] = MOSI;
//             4: sreg[3] = MOSI;
//             5: sreg[2] = MOSI;
//             6: sreg[1] = MOSI;
//             7: sreg[0] = MOSI;
//         end
//     else
// end


// jtag_slave js (
//      .TCK(SCK)
//     ,.TDI(MOSI)
//     ,.TDO(MISO)
//     // These are not equivalent signals.
//     ,.CS(TMS)
// );


