// jtag_tap_ctrl_inf contains standard IEEE 1149.1 signals used for controling a JTAG Scan Chain

`ifndef jtag_tap_ctrl_inf_
`define jtag_tap_ctrl_inf_

// interface jtag_inf ;
//     // TCK, used as the clock source for the JTAG circuitry
//     wire tck; // Test Clock
//     // Test data in (TDI), used to shift data into the IR and DR shift
//     // register chains
//     wire tdi; // Test Data (Shift) in
//     // Test data out (TDO), used to shift data out of the IR and DR shift
//     // register chains
//     wire tdo; // Test Data (Shift) out

//     // Clocking changes on TMS steps through a standardized JTAG state
//     // machine. The JTAG state machine can reset, access an instruction
//     // register, or access data selected by the instruction register.
//     wire tms; // Test Mode Select

//     // The TRST pin is an optional active-low reset to the test logic
//     // - usually asynchronous, but sometimes synchronous, depending on the
//     // chip. If the pin is not available, the test logic can be reset by
//     // switching to the reset state synchronously, using TCK and TMS. Note
//     // that resetting test logic doesn't necessarily imply resetting
//     // anything else. There are generally some processor-specific JTAG
//     // operations which can reset all or part of the chip being debugged.
//     wire trst_L; // Test Reset _L Active Low
// endinterface

// // One-hot states from "jtag_tap_ctrl_state_t"
// // State names from pg39 Altera's virtual jtag module document.
// // https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/ug/ug_virtualjtag.pdf
// interface jtag_tap_state_inf ;
//     cdr,
//     cir,
//     e1dr,
//     e1ir,
//     e2dr,
//     e2ir,
//     pdr,
//     pir,
//     rti,
//     sdr,
//     sdrs,
//     sir,
//     sirs,
//     tlr,
//     udr,
//     uir,
// endinterface

interface jtag_tap_ctrl_inf ;

    // TAP Control:
    wire udr; // Uptate (latch) Data Register
    wire cdr; // Capture Data Register
    wire sdr; // Shift Data Register
    wire tdo_oe; // Test Data (Shift) out output enable
    wire tms; // Test Mode Select
    wire trst_L; // Test Reset _L Active Low

    // TAP Data:
    wire tdi; // Test Data (Shift) in
    wire tdo; // Test Data (Shift) out
    wire tck; // Test Clock

endinterface

`endif
