// hipe_ir_opcode_state_t contains opcodes for HIPE - Hardware Integrated Prototyping Environment 
// Standard OPCODES Reference: https://github.com/lgeek/adv_debug_sys/blob/master/Hardware/jtag/tap/rtl/verilog/tap_defines.v

`ifndef hipe_ir_opcode_state_t_
`define hipe_ir_opcode_state_t_

localparam HIPE_IR_OPCODE_STATE_WIDTH = 4 ; // Match 4 bit OPCODE_LEN used in HIPE

typedef enum reg [HIPE_IR_OPCODE_STATE_WIDTH-1:0] {
// Standard IEEE JTAG OPCODEs
// EXTEST                  = 4'b0000 // TODO: Enable
// ,SAMPLE_PRELOAD         = 4'b0001 // TODO: Enable
// ,IDCODE                 = 4'b0010 // TODO: Enable
// ,DEBUG                  = 4'b1000 // TODO: Enable
// ,MBIST                  = 4'b1001 // TODO: Enable
// ,BYPASS                 = 4'b1111 // TODO: Enable
FAULT_DECTECTION          = 4'b0101 // This value is fixed for easier fault detection

// HIPE Specific OPCODEs
// ,OP_SHIFT               = 4'b0101 // TODO: Enable
// ,OP_ONLY_READ           = 4'b0111 // TODO: Enable
,OP_WRITE_LATCH         = 4'b0000 // TODO: Depreciate and remove
,OP_READ_PORT           = 4'b0001 // TODO: Depreciate and remove
,OP_WRITE_WRITE_ENABLE  = 4'b0010 // TODO: Depreciate and remove
,OP_READ_WRITE_ENABLE   = 4'b0011 // TODO: Depreciate and remove
} hipe_ir_opcode_state_t;

`endif
