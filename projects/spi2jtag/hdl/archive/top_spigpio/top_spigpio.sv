
`ifndef top_spigpio_
`define top_spigpio_

`include "../jtag_tap/jtag_types.sv"
`include "hipe_tdr_inf.sv"
`include "hipe_rw.sv"

module top_spigpio #(
    parameter NGPIO=36,
    parameter NCLK=2,
    parameter NKEY=4,
    parameter NSW=10,
    parameter NLED=10,
    parameter NSEGS=6,
    parameter USE_QUARTUS_13=0 // (1) Quartus 13 (default) Quartus 16
) (
    inout  wire [NGPIO-1:0]         gpio,
    input  wire [NCLK-1:0]          clk,
    input  wire [NKEY-1:0]          key,
    input  wire [NSW-1:0]           sw,
    output wire [NLED-1:0]          led,
    output wire [(NSEGS*HEX_W)-1:0] hex_L
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************
localparam HEX_W = 7; // Constants for using hex_L.
localparam WREG_LEN = 32; // Constants for using wreg.

// HIPE
localparam NUM_RW = 2; // 16 signals
localparam NUM_RO = 0;
localparam DR_LEN = 8;
localparam IR_LEN = 12; // Match 8 bit ADDR_LEN and 4 bit OPCODE_LEN used in HIPE by default

// NIOS2 - QSYS
localparam USE_NIOS = 1; // (1) Use NIOS (default) Do NOT use NIOS

// hipe_rw
localparam RW0_DR_LEN = 32;
localparam RW1_DR_LEN = 32;

// SPI
localparam NUM_SPI_TO_READ = 5; // cs[1:0] , sclk , miso, mosi
localparam NUMCS=2;

// View SPI on GPIO
// Staggared as every other (ODD, or EVEN) on GPIO from OFFSET to OFFSET + NUM_SPI_TO_READ*2 -1
localparam SPI_GPIO_OFFSET  = 0*(NUM_SPI_TO_READ*2)+1 ; // ODD //Top Left
localparam SPI_SLAVE_OFFSET = 1*(NUM_SPI_TO_READ*2)+1 ; // ODD //Top Right
localparam NIOS2_SPI_OFFSET = 0*(NUM_SPI_TO_READ*2) ; // Even //Bottom left
localparam NIOS2_PIO_OFFSET = 1*(NUM_SPI_TO_READ*2) ; // Even //Bottom Right

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

// Working Register
wire key0_debounced;
reg  [WREG_LEN-1:0] wreg;
reg  [WREG_LEN-1:0] hex_mux;
reg  [NSEGS*HEX_W-1:0] hex_reg; // 4-bits per seg
reg  [WREG_LEN-1:0] led_mux;
reg  [NLED-1:0] led_reg;

// hipe_rw // Arbitrary Shift Registers
hipe_tdr_inf rw0_tdr (); // udr,cdr,sdr,so,si,tck
reg  [RW0_DR_LEN-1:0] rw0_in;
wire [RW0_DR_LEN-1:0] rw0_out;

hipe_tdr_inf rw1_tdr (); // udr,cdr,sdr,so,si,tck
reg  [RW1_DR_LEN-1:0] rw1_in;
wire [RW1_DR_LEN-1:0] rw1_out;

// hipe_spi
spi_inf #(.NUMCS(NUMCS)) spi_slave ();
jtag_types::jtag_tap_fsm_state_t tap_state;
wire [IR_LEN-1:0] jtag_ir_out;

// NIOS2
spi_inf #(.NUMCS(NUMCS)) spi_nios2_spi ();
spi_inf #(.NUMCS(NUMCS)) spi_nios2_pio ();
wire [31:0] pio_in;
wire [31:0] pio_out;

// SPI
wire [NUM_SPI_TO_READ-1:0] spi_slave_wires;
wire [NUM_SPI_TO_READ-1:0] spi_nios2_pio_wires;
wire [NUM_SPI_TO_READ-1:0] spi_nios2_spi_wires;
wire [NUM_SPI_TO_READ-1:0] spi_gpio_wires;
wire [NUM_SPI_TO_READ-1:0] spi_sw_wires;
wire floating;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************


//****************************************************************************
//                             Sequential logic
//****************************************************************************

always @(negedge key0_debounced) begin : set_wreg
    wreg <= wreg_in;
end

always @(posedge key0_debounced) begin : set_from_wreg
    case(wreg_select)
        // 0:
        1: hex_mux  <= wreg; // TODO
        2: led_mux  <= wreg; // TODO
        8: rw0_in   <= {{RW0_DR_LEN - WREG_LEN{1'b0}},wreg};
        9: rw1_in   <= {{RW1_DR_LEN - WREG_LEN{1'b0}},wreg};
        default: wreg <= wreg;
    endcase
end

//****************************************************************************
//                            Combinational logic
//****************************************************************************
// input:
wire [WREG_LEN-1:0] wreg_in = sw[NSW-1:0];
wire [7:0] wreg_select = sw[NSW-1:0];
wire [1:0] spi_select  = sw[9:8];
// output:
assign led[NLED-1:0]  = led_reg[NLED-1:0];

// internal:
// Clock Domain: spi_nios2_spi
assign rw0_tdr.tck  = spi_slave.sclk;
assign rw1_tdr.tck  = spi_slave.sclk;

// TDR Control
// RW1: Always update and shift, never capture.
assign rw0_tdr.udr  = 1'b1;
assign rw0_tdr.cdr  = 1'b0;
assign rw0_tdr.sdr  = 1'b1;
// RW1: Always update and shift, never capture.
assign rw1_tdr.udr  = 1'b1;
assign rw1_tdr.cdr  = 1'b0;
assign rw1_tdr.sdr  = 1'b1;

// TDR Serial connections.
assign rw0_tdr.si   = spi_slave.mosi;
assign rw1_tdr.si   = spi_slave.miso;

//****************************************************************************
// Multipliexing SPI signals
// 2'b10:    //SPI DRIVER = GPIO
// 2'b01:    //SPI DRIVER = SWs
// 2'b11:    //SPI DRIVER = NIOS2_PIO
// default:  //SPI DRIVER = NIOS2_SPI

assign  {
    // {8-5 {1'b0}} // Unused
     spi_slave.cs_n[1]         // 4
    ,spi_slave.cs_n[0]         // 3
    ,spi_slave.sclk          // 2
    ,floating // Don't drive miso
    ,spi_slave.mosi          // 0
    } =
      (spi_select == 2'b01 ) ? spi_gpio_wires
    : (spi_select == 2'b10 ) ? spi_sw_wires
    : (spi_select == 2'b11 ) ? spi_nios2_pio_wires
                             : spi_nios2_spi_wires ;

// Always drive MISO
assign  gpio[3]              =  spi_slave.miso;
assign  spi_nios2_spi.miso   =  spi_slave.miso;
assign  spi_nios2_pio.miso   =  spi_slave.miso;

// SPI packets    
assign spi_slave_wires = {
     spi_slave.cs_n[1]       // 4
    ,spi_slave.cs_n[0]       // 3
    ,spi_slave.sclk          // 2
    ,spi_slave.miso          // 1
    ,spi_slave.mosi          // 0
    } ;
assign spi_nios2_pio_wires = {
     spi_nios2_pio.cs_n[1]       // 4
    ,spi_nios2_pio.cs_n[0]       // 3
    ,spi_nios2_pio.sclk          // 2
    ,spi_nios2_pio.miso          // 1
    ,spi_nios2_pio.mosi          // 0
    } ;

assign spi_nios2_spi_wires = {
     spi_nios2_spi.cs_n[1]       // 4
    ,spi_nios2_spi.cs_n[0]       // 3
    ,spi_nios2_spi.sclk          // 2
    ,spi_nios2_spi.miso          // 1
    ,spi_nios2_spi.mosi          // 0
    } ;

assign spi_gpio_wires = { 
     gpio[SPI_GPIO_OFFSET+8]
    ,gpio[SPI_GPIO_OFFSET+6]
    ,gpio[SPI_GPIO_OFFSET+4]
    ,gpio[SPI_GPIO_OFFSET+2]
    ,gpio[SPI_GPIO_OFFSET+0]
    } ; // ODD

assign spi_sw_wires = {
     sw[6]        // 3
    ,sw[5]        // 2
    ,key0_debounced        // 1
    ,1'b0 // arbitrary constant for miso
    ,sw[4]          // 0
    } ;

//****************************************************************************
// Output Multiplexers
always @(*) begin : drive_hex_mux
    case(hex_mux)
        1: hex_reg <= {rw0_out};
        2: hex_reg <= {rw1_out};
        4: hex_reg <= {jtag_ir_out[11:0], tap_state, rw1_out[7:0], rw0_out[7:0]};
        default: hex_reg <= {wreg[7:0], jtag_ir_out[11:0], tap_state};
    endcase
end

always @(*) begin : drive_led_mux
    case(led_mux)
        1: led_reg <= {rw0_out};
        2: led_reg <= {rw1_out};
        4: led_reg <= {jtag_ir_out[11:0], tap_state, rw1_out[7:0], rw0_out[7:0]};
        default: led_reg <= {wreg[7:0], jtag_ir_out[11:0], tap_state};
    endcase
end
//
//****************************************************************************

//****************************************************************************
// NIOS PIO:
assign pio_in = {
    {31-19 - 1 {1'b0}},
     led_reg                 // 17:26  // 10-bit data register
    ,jtag_ir_out             // 6:17  // 12-bit data register
    ,tap_state               // 5     // 4-bit data register
    ,spi_slave_wires
    };

assign pio_out = {
     spi_nios2_pio.cs_n[1]       // 3
    ,spi_nios2_pio.cs_n[0]       // 2
    ,spi_nios2_pio.sclk          // 1
    ,spi_nios2_pio.mosi          // 0
    };
//
//****************************************************************************

// View SPI on GPIO
assign {gpio[SPI_SLAVE_OFFSET+8],gpio[SPI_SLAVE_OFFSET+6],gpio[SPI_SLAVE_OFFSET+4],gpio[SPI_SLAVE_OFFSET+2],gpio[SPI_SLAVE_OFFSET+0]} = {
    spi_slave_wires } ; // ODD

assign {gpio[NIOS2_SPI_OFFSET+8],gpio[NIOS2_SPI_OFFSET+6],gpio[NIOS2_SPI_OFFSET+4],gpio[NIOS2_SPI_OFFSET+2],gpio[NIOS2_SPI_OFFSET+0]} = {
    spi_nios2_spi_wires } ; // EVEN
assign {gpio[NIOS2_PIO_OFFSET+8],gpio[NIOS2_PIO_OFFSET+6],gpio[NIOS2_PIO_OFFSET+4],gpio[NIOS2_PIO_OFFSET+2],gpio[NIOS2_PIO_OFFSET+0]} = {
    spi_nios2_pio_wires } ; // EVEN

// View led_reg on leds
assign leds = led_reg;

//****************************************************************************
//                              Internal Modules
//****************************************************************************

// NIOS2
generate
    if(USE_NIOS == 1) begin

        if(USE_QUARTUS_13 == 1) begin
                nios2_spi_pio_q13 nios2_q13 (
                    .clk_clk   (clk[0])   // clk.clk
                    
                    // PIO Debugging
                    ,.po_in_port ( pio_in ) // pi.export [31:0]
                    ,.po_out_port( pio_out )  // po.export [31:0] // float unused output
                    
                    // SPI Interface  
                    ,.spi_MISO  (spi_nios2_spi.miso)  // spi.MISO
                    ,.spi_MOSI  (spi_nios2_spi.mosi)  //    .MOSI
                    ,.spi_SCLK  (spi_nios2_spi.sclk)   //    .SCLK
                    ,.spi_SS_n  ({spi_nios2_spi.cs_n[1],spi_nios2_spi.cs_n[0]})   //    .SS_n // DRIVE AS ACTIVE LOW FROM SOFTWARE
                );
        end else begin
                nios2_spi_pio_q16 nios2_q16 (
                    .clk_clk   (clk[0])   // clk.clk
                    
                    // PIO Debugging
                    ,.po_in_port ( pio_in ) // pi.export [31:0]
                    ,.po_out_port( pio_out )  // po.export [31:0] // float unused output

                    // SPI Interface  
                    ,.spi_MISO  (spi_nios2_spi.miso)  // spi.MISO
                    ,.spi_MOSI  (spi_nios2_spi.mosi)  //    .MOSI
                    ,.spi_SCLK  (spi_nios2_spi.sclk)  //    .SCLK
                    ,.spi_SS_n  ({spi_nios2_spi.cs_n[1],spi_nios2_spi.cs_n[0]})   //    .SS_n
                );
        end

    end
endgenerate

// HIPE
hipe_spi #(
    .NUM_RW(NUM_RW)
    ,.NUM_RO(NUM_RO)
    ,.IR_LEN(IR_LEN)
    ,.DR_LEN(DR_LEN)
) hipe0 (
     .rw_port_i(led_reg)
    ,.rw_latch_o(led_w)
    // ,.ro_port_i(ro_port_i),

    ,.spi(spi_slave)

    ,.jtag_ir(jtag_ir_out) // DEBUG_IR
    ,.encoded_state(tap_state) // DEBUG_STATE
);

// Debounce the button to clock in wreg
debounce #(
    .clk_freq_hz    (  (50) * (10** 6)), // Mhz  = 10^6 Hz
    .wait_time_sec  ((   4) * (10**-3))  // ms  = 10^-3 sec
) debounce_key0 (
    .in_sig(key[0]),
    .clk(clk[0]),           // Input Clock
    .out_sig( key0_debounced )
);

display_on_hex #(
    .NSEGS(NSEGS)
) hex_display (
     .hex_L( hex_L )
    ,.value( hex_reg )
);

// Arbitrary Shift Registers
hipe_rw #(
    .DR_LEN(RW0_DR_LEN)
) rw0 (
    .in(rw0_in),
    .out(rw0_out),
    .write_enable_i({RW0_DR_LEN{1'b1}}), // use the latched value, if write_enable_i[0]=0 then out[x]==in[x]
    .tdr_inf(rw0_tdr) // udr,cdr,sdr,so,si,tck
);

hipe_rw #(
    .DR_LEN(RW1_DR_LEN)
) rw1 (
    .in(rw1_in),
    .out(rw1_out),
    .write_enable_i({RW1_DR_LEN{1'b1}}), // use the latched value, if write_enable_i[0]=0 then out[x]==in[x]
    .tdr_inf(rw1_tdr) // udr,cdr,sdr,so,si,tck
);


endmodule

`endif
