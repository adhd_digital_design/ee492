onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /simple_top/uart/rx_in/sampler/rxd_i
add wave -noupdate /simple_top/uart/rx_in/sampler/rxd_o
add wave -noupdate /simple_top/uart/rx_in/sampler/state
add wave -noupdate /simple_top/uart/rx_in/state
add wave -noupdate -radix unsigned /simple_top/uart/rx_in/sampler/counter_value_w
add wave -noupdate /simple_top/uart/rx_in/sampler/reset_counter
add wave -noupdate /simple_top/uart/rx_in/sampler/previous_bits_w
add wave -noupdate /simple_top/uart/rx_in/sampler/sample_enable
add wave -noupdate -radix hexadecimal /simple_top/uart/rx_in/sampler/sample_value_reg
add wave -noupdate /simple_top/uart/rx_in/sampler/rxd_potential_start_bit_detected
add wave -noupdate /simple_top/uart/rx_in/sampler/rxd_early_start_bit_rejection
add wave -noupdate /simple_top/uart/rx_in/sampler/rxd_actual_start_bit_detected
add wave -noupdate /simple_top/uart/rx_in/sampler/noise_flag_o
add wave -noupdate /simple_top/uart/rx_in/noise_flag_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {266941 ns} 0} {{Cursor 2} {162781 ns} 0} {{Cursor 3} {279610 ns} 0}
quietly wave cursor active 3
configure wave -namecolwidth 221
configure wave -valuecolwidth 52
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 1
configure wave -timelineunits ns
update
WaveRestoreZoom {136162 ns} {285288 ns}
