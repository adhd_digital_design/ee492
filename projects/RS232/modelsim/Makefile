################################################################################################################
# Begin Project Settings
################################################################################################################
# The name of the library folder to be created
LIB_PREFIX = ./
LIB_NAME = work
LIB_PATH = ./$(LIB_PREFIX)/$(LIB_NAME)
# Error To Report if Makefile Fails
REPORT_ERROR = ( echo "Makefile Exit Code = $$?"; exit 1 )
# Path to the cpp include directory, relative to the makefile
# HIPE Variables exported from profile.hipe
# HipeC requires c++-11 , (ie. g++-5)
# UVM Variables exported from profile.hipe 
PROJECT=../../../firmware/rs232/
# CITED_UART2_AND_SIM=../../../cited/uart2_and_sim/
UVM_1_1c_DIR=$(HOME)/altera/13.0sp1/modelsim_ase/verilog_src/uvm-1.1c/src
INCLUDES = +incdir+$(PROJECT) +incdir+$(PROJECT)/src_tb +incdir+$(PROJECT)/src_tb_uart +incdir+$(UVM_1_1c_DIR)
# General Compiler Flags
SV_COMPILE_FLAGS  = -sv12compat -sv
V_COMPILE_FLAGS   = 
VHD_COMPILE_FLAGS = -mixedsvvh -93 -showsubprograms
# Modelsim Simulation Flags
SIM_FLAGS = 
DEBUG_FLAGS = 
# TOP_MODULE = ng_top
# TOP_MODULE = uart_tb_top
# TOP_MODULE = top
TOP_MODULE = simple_top
################################################################################################################
# End Project Settings
# Generally should not need to edit below this line
################################################################################################################

################################################################################################################
# Makefile Tools
################################################################################################################

# Function used to check variables. Use on the command line:
# make print-VARNAME
# Useful for debugging and adding features
print-%: ; @echo $*=$($*)

# Shell used in this makefile
# bash is used for 'echo -en'
SHELL = /usr/bin/env bash

# Verbose option, to output compile and link commands
export V := false
export CMD_PREFIX := @
ifeq ($(V),true)
	CMD_PREFIX :=
endif

################################################################################################################
# Begin Targets
################################################################################################################

all:				clean build
	
build:              work sv

################################################################################################################
# Compilation
################################################################################################################

work:
	# Make the physical "$(LIB_PATH)" directory/library.
	@if  [ -e $(LIB_PATH) ] ; then : "Warning: -e $(LIB_PATH) ALREADY EXISTS" ; \
	 else mkdir -p $(LIB_PREFIX) ; vlib $(LIB_PATH) ; fi

clean:
	#Remove previous "$(LIB_PATH)"
	@if [ -e $(LIB_PATH) ] ; then rm $(LIB_PATH) -r ; \
	 else :  ; fi

sv:	
	# Compile the source files (SystemVerilog) into the specified
	# working directory.
	# -work $(LIB_PATH) # compile into the "$(LIB_PATH)" directory.
	@# -sv # compile SystemVerilog files.
	@vlog  $(SV_COMPILE_FLAGS) -sv -work $(LIB_PATH) \
            ./src/*.sv $(INCLUDES) || $(REPORT_ERROR)
	@echo "" ;
	@echo "    *** $(LIB_NAME) SystemVerlog firmware compiled ok. ***" ;
	@echo "" ; 

v:	
	# Compile the source files (Verilog) into the specified
	# working directory.
	# -work $(LIB_PATH) # compile into the "$(LIB_PATH)" directory.
	@vlog  $(V_COMPILE_FLAGS) -work $(LIB_PATH) \
            ./src/*.v $(INCLUDES) || $(REPORT_ERROR)
	@echo "" ;
	@echo "    *** $(LIB_NAME) Verilog firmware compiled ok. ***" ;
	@echo "" ; 

vhd:	
	# Compile the source files (VHDL) into the specified
	# working directory.
	# -work $(LIB_PATH) # compile into the "$(LIB_PATH)" directory.
	@vcom  $(VHD_COMPILE_FLAGS) -work $(LIB_PATH) \
            ./src/*.vhd $(INCLUDES) || $(REPORT_ERROR)
	@echo "" ;
	@echo "    *** $(LIB_NAME) VHDL firmware compiled ok. ***" ;
	@echo "" ; 

################################################################################################################
# Simulation
################################################################################################################

run:
	@vsim -c $(SIM_FLAGS) $(LIB_PATH).$(TOP_MODULE) -do "do ./default.do.tcl" || $(REPORT_ERROR)

sim:
	@vsim -c $(SIM_FLAGS) $(LIB_PATH).$(TOP_MODULE) || $(REPORT_ERROR)

gui:
	@vsim $(SIM_FLAGS) $(LIB_PATH).$(TOP_MODULE)

debug:
	@vsim -c $(DEBUG_FLAGS) $(LIB_PATH).$(TOP_MODULE) -do "run -all" || $(REPORT_ERROR)

################################################################################################################
# Help Section
################################################################################################################
help:
	@echo ""
	@echo "The following targets are available:"
	@echo "    make all  	- cleans and compiles $(LIB_PATH)"
	@echo "    make build 	- compiles changes in $(LIB_PATH)"
	@echo "    make work	- creates $(LIB_PATH)"
	@echo "    make clean	- cleans  $(LIB_PATH)"
	@echo "    make sv   	- compiles SystemVerilog from ./src into $(LIB_PATH)"
	@echo "    make v   	- compiles Verilog from ./src into $(LIB_PATH)"
	@echo "    make vhd   	- compiles VHDL from ./src into $(LIB_PATH)"
	@echo "    make sim 	- command line simulate $(LIB_PATH).$(TOP_MODULE)"
	@echo "    make gui 	- gui simulate $(LIB_PATH).$(TOP_MODULE)"
	@echo "    make debug 	- command line simulate $(LIB_PATH).$(TOP_MODULE) with DEBUG_FLAGS"
	@echo ""
