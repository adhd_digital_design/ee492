#! /usr/bin/env bash

repo=$(git rev-parse --show-toplevel)
project=$repo/projects/RS232

# Windows Users and symlinks: http://stackoverflow.com/a/11664406

cd $project || exit

################################################################################
# Testbench Firmware
################################################################################
mkdir -p $project/hdl/sim_rs232
cd $project/hdl/sim_rs232  && (
    ln  -s -f $repo/firmware/rs232/src_tb/simple_top.sv  ./simple_top.sv
)

################################################################################
# TOP Firmware
###############################################################################
mkdir -p $project/hdl/top_rs232
cd $project/hdl/top_rs232  && (
    ln  -s -f  $repo/firmware/rs232/top_rs232_pkg.sv              ./top_rs232_pkg.sv
    ln  -s -f  $repo/firmware/rs232/rs232_top.v                   ./rs232_top.v
    ln  -s -f  $repo/firmware/rs232/DE2_TOP.v                     ./DE2_TOP.v
    ln  -s -f  $repo/firmware/rs232/C5GT_PRO_TOP.v                ./C5GT_PRO_TOP.v
    ln  -s -f  $repo/firmware/rs232/DE1_SOC_TOP.v                 ./DE1_SOC_TOP.v
    ln  -s -f  $repo/firmware/rs232/DE10_LITE_TOP.v               ./DE10_LITE_TOP.v
)

################################################################################
# DUT Firmware
################################################################################
mkdir -p $project/hdl/rs232
cd $project/hdl/rs232  && (
    ln  -s -f  $repo/firmware/rs232/rs232_pkg.sv                  ./rs232_pkg.sv
    ln  -s -f  $repo/firmware/rs232/src/rs232.sv                  ./rs232.sv
    ln  -s -f  $repo/firmware/rs232/src/rx_sampler.sv             ./rx_sampler.sv
    ln  -s -f  $repo/firmware/rs232/src/power_on_reset.v          ./power_on_reset.v
    ln  -s -f  $repo/firmware/rs232/src/rs232_inf.sv              ./rs232_inf.sv
    ln  -s -f  $repo/firmware/rs232/src/pulse_generator.sv        ./pulse_generator.sv
    ln  -s -f  $repo/firmware/rs232/src/rs232_out_serializer.sv   ./rs232_out_serializer.sv
    ln  -s -f  $repo/firmware/rs232/src/fifo_n.v                  ./fifo_n.v
    ln  -s -f  $repo/firmware/rs232/src/rs232_ports.sv            ./rs232_ports.sv
    ln  -s -f  $repo/firmware/rs232/src/rs232_rx_buffered.sv      ./rs232_rx_buffered.sv
    ln  -s -f  $repo/firmware/rs232/src/dual_port_ram.sv          ./dual_port_ram.sv
    ln  -s -f  $repo/firmware/rs232/src/data_recovery.sv          ./data_recovery.sv
    ln  -s -f  $repo/firmware/rs232/src/rs232_tx_buffered.sv      ./rs232_tx_buffered.sv
    ln  -s -f  $repo/firmware/rs232/src/fifo_ctrl.sv              ./fifo_ctrl.sv
    ln  -s -f  $repo/firmware/rs232/src/parity.v                  ./parity.v
    ln  -s -f  $repo/firmware/rs232/src/rs232_in_deserializer.sv  ./rs232_in_deserializer.sv
)

################################################################################
# Common Firmware
################################################################################

