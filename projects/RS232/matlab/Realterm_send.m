%https://www.mathworks.com/matlabcentral/answers/132341-how-can-i-send-a-hex-code-over-realterm-without-converting-it?requestedDomain=www.mathworks.com
function [  ] = Realterm_send(send)  
hrealterm=actxserver('realterm.realtermintf'); % start Realterm as a server
hrealterm.baud=115200;
hrealterm.caption='Matlab Realterm Server';
hrealterm.port='1';
hrealterm.windowstate=1; %minimized
hrealterm.PortOpen=1;
send = ('7e55443322117f');
invoke(hrealterm, 'PutString',send);
end