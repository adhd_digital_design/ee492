
module lcd_program #(
    parameter SIZE=5+32+1
) (
    input wire [INDEX_SIZE-1:0] index,
    output reg [1+7:0] data // { 1'RS, 8'DATA }
);
localparam  INDEX_SIZE = $clog2(SIZE);

localparam  RS_CMD   =  1'b0;
localparam  RS_DATA  =  1'b1;

// Special Locations in program
localparam  INIT     =  0;
localparam  LINE1    =  5;
localparam  WIDTH    =  16;
localparam  CH_LINE  =  LINE1  +  WIDTH;
localparam  LINE2    =  LINE1  +  WIDTH   +1;

//===============================================================================================
// HD44780 Instruction Set
// Source: https://dawes.wordpress.com/2010/01/05/hd44780-instruction-set/
//===============================================================================================

localparam CMD_FS_8B_2 = 8'h38; // Function Set (8-bit Interface, 2 lines,5*7 Pixels)
localparam CMD_FS_8B_1 = 8'h30; // Function Set (8-bit Interface, 1 line,5*7 Pixels)
localparam CMD_FS_4B_2 = 8'h28; // Function Set (4-bit Interface, 2 lines,5*7 Pixels)
localparam CMD_FS_4B_1 = 8'h20; // Function Set (4-bit Interface, 1 line,5*7 Pixels)

// Instruction	Byte
localparam CMD_CLEAR = 8'h01; // Clear display
localparam CMD_HOME = 8'h02; // Return cursor to home, and un-shift display

// Entry mode: The following control how the cursor behaves after each
// character is entered. Sets cursor move direction and specified display
// shift. These operations are performed during data write and read.
localparam CMD_ENTRY_CR = 8'h06; // cursor++ // move cursor right, don’t shift display (this is the most common)
localparam CMD_ENTRY_CL = 8'h04; // cursor-- // move cursor right, don’t shift display.
localparam CMD_ENTRY_CR_DR = 8'h07; // cursor++, disp++ // move cursor right, shift display right
localparam CMD_ENTRY_CL_DL = 8'h05; // cursor--, disp-- // move cursor right, shift display left

// Display control: The following control display properties	
localparam CMD_DISP_OFF = 8'h0B; // turn display off	0x08 .. 0x0B // TODO: What does the range mean?
localparam CMD_DISP_ON_COFF = 8'h0C; // display on, cursor off,
localparam CMD_DISP_ON_CSTEADY = 8'h0E; // display on, cursor on, steady cursor
localparam CMD_DISP_ON_CBLINK = 8'h0F; // display on, cursor on, blinking cursor

// The following commands move the cursor and shift the display	
localparam CMD_SHIFT_CL = 8'h10; // Shift cursor left
localparam CMD_SHIFT_CR = 8'h14; // Shift cursor right
localparam CMD_SHIFT_DL = 8'h18; // Shift display left
localparam CMD_SHIFT_DR = 8'h1C; // Shift display right

// Cursor position: see following discussion for more detail.	0x80 + position
// Display Position: //  1   2   3   4   5   ...  39  40
// Cursor Position:  //  00  01  02  03  04  ...  26  27
// Cursor Position:  //  40  41  42  43  44  ...  66  67
localparam CMD_CURSOR_POS = 8'h80;
localparam CMD_CURSOR_LINE1 = CMD_CURSOR_POS  +8'd0;
localparam CMD_CURSOR_LINE2 = CMD_CURSOR_POS +8'd40;

//===============================================================================================
// Define text.
//===============================================================================================
wire [7:0] char_data [1:2][0:15];

//	Line 1	// " Welcome to the "
assign char_data[1][0]  = " ";
assign char_data[1][1]  = "W";
assign char_data[1][2]  = "e";
assign char_data[1][3]  = "l";
assign char_data[1][4]  = "c";
assign char_data[1][5]  = "o";
assign char_data[1][6]  = "m";
assign char_data[1][7]  = "e";
assign char_data[1][8]  = " ";
assign char_data[1][9]  = "t";
assign char_data[1][10] = "o";
assign char_data[1][11] = " ";
assign char_data[1][12] = "t";
assign char_data[1][13] = "h";
assign char_data[1][14] = "e";
assign char_data[1][15] = " ";

//	Line 2	// " Jungle!! - G&R "
assign char_data[2][0]  = " ";
assign char_data[2][1]  = "J";
assign char_data[2][2]  = "u";
assign char_data[2][3]  = "n";
assign char_data[2][4]  = "g";
assign char_data[2][5]  = "l";
assign char_data[2][6]  = "e";
assign char_data[2][7]  = "!";
assign char_data[2][8]  = "!";
assign char_data[2][9]  = " ";
assign char_data[2][10] = "-";
assign char_data[2][11] = " ";
assign char_data[2][12] = "G";
assign char_data[2][13] = "&";
assign char_data[2][14] = "R";
assign char_data[2][15] = " ";

//===============================================================================================
// Define the LCD Program
//===============================================================================================
wire [8:0] program [0:SIZE-1]; // { 1'RS, 8'DATA }

//	Initial
assign  program[INIT+0]  =  {RS_CMD,  CMD_FS_8B_2};
assign  program[INIT+1]  =  {RS_CMD,  CMD_DISP_ON_COFF};
assign  program[INIT+2]  =  {RS_CMD,  CMD_CLEAR};
assign  program[INIT+3]  =  {RS_CMD,  CMD_ENTRY_CR};
assign  program[INIT+4]  =  {RS_CMD,  CMD_CURSOR_LINE1};

//	Line 1	// " Welcome to the "
assign program[LINE1  +0]  =  {RS_DATA, char_data[1][0]  };
assign program[LINE1  +1]  =  {RS_DATA, char_data[1][1]  };
assign program[LINE1  +2]  =  {RS_DATA, char_data[1][2]  };
assign program[LINE1  +3]  =  {RS_DATA, char_data[1][3]  };
assign program[LINE1  +4]  =  {RS_DATA, char_data[1][4]  };
assign program[LINE1  +5]  =  {RS_DATA, char_data[1][5]  };
assign program[LINE1  +6]  =  {RS_DATA, char_data[1][6]  };
assign program[LINE1  +7]  =  {RS_DATA, char_data[1][7]  };
assign program[LINE1  +8]  =  {RS_DATA, char_data[1][8]  };
assign program[LINE1  +9]  =  {RS_DATA, char_data[1][9]  };
assign program[LINE1  +10] =  {RS_DATA, char_data[1][10] };
assign program[LINE1  +11] =  {RS_DATA, char_data[1][11] };
assign program[LINE1  +12] =  {RS_DATA, char_data[1][12] };
assign program[LINE1  +13] =  {RS_DATA, char_data[1][13] };
assign program[LINE1  +14] =  {RS_DATA, char_data[1][14] };
assign program[LINE1  +15] =  {RS_DATA, char_data[1][15] };

//	Change Line
assign program[CH_LINE] =  {RS_CMD,  CMD_CURSOR_LINE2+0}; //0xC0

//	Line 2	// " Jungle!! - G&R "
assign program[LINE2 +0]  =  {RS_DATA, char_data[2][0]  };
assign program[LINE2 +1]  =  {RS_DATA, char_data[2][1]  };
assign program[LINE2 +2]  =  {RS_DATA, char_data[2][2]  };
assign program[LINE2 +3]  =  {RS_DATA, char_data[2][3]  };
assign program[LINE2 +4]  =  {RS_DATA, char_data[2][4]  };
assign program[LINE2 +5]  =  {RS_DATA, char_data[2][5]  };
assign program[LINE2 +6]  =  {RS_DATA, char_data[2][6]  };
assign program[LINE2 +7]  =  {RS_DATA, char_data[2][7]  };
assign program[LINE2 +8]  =  {RS_DATA, char_data[2][8]  };
assign program[LINE2 +9]  =  {RS_DATA, char_data[2][9]  };
assign program[LINE2 +10] =  {RS_DATA, char_data[2][10] };
assign program[LINE2 +11] =  {RS_DATA, char_data[2][11] };
assign program[LINE2 +12] =  {RS_DATA, char_data[2][12] };
assign program[LINE2 +13] =  {RS_DATA, char_data[2][13] };
assign program[LINE2 +14] =  {RS_DATA, char_data[2][14] };
assign program[LINE2 +15] =  {RS_DATA, char_data[2][15] };

//===============================================================================================
// Provide the Command/Data at the requested index.
//===============================================================================================
always begin
    if(index < SIZE) begin : index_exists
        data <= program[index];
    end
    else begin : index_invalid
        data  <=  {RS_CMD , 8'h00};
    end
end

endmodule

// lcd_program #(
//     .SIZE(5+32+1)
// ) prog0 (
//     .index(),
//     .data() // { 1'RS, 8'DATA }
// );
