-- 2017-03-24 - Jordan Ulmer
-- QSYS Procedure for nios2 processor

[0] Instantiate the qsys and firmware/nios2/ top file 
[1] Include the qsys file in the projects/nios2/quartus *.qpf
[2] Open the quartus revision (*.qsf) for the specific dev board
[3] Double click on QSYS - Click Generate HDL
[4] Compile quartus
[5] Program the dev board (ie. DE10_LITE, DE1-SOC, DE2)...
[6] Open eclipse from quartus (> tools > NIOS II...)
[7] Import the eclipse project (Import > General > Existing Projects into Workspace) - Select root folder (ie. ee492\projects\nios2_pio_onchip\)
[8] Regenerate the Board Support Package (bsp) by pointing to the *.sopcinfo (ie. right click on /projects/.../software/*_bsp)
[9] Build the eclipse project (CTRL+B)
[10] Run from NIOS II HW... (Ensure Target is found in the run configuration)
[11] Magic happens :)
