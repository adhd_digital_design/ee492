#! /usr/bin/env bash
# Expects to be in root of Modelsim Project.

# Link firmware in ./src
mkdir -p ./src
cd ./src

################################################################################
# Cheatsheet on ln
################################################################################
# ln {target_file} {symbolic_name} # symbolic_name should be "./linkname"
# ln -s {source} {destination} # Symbolic link
# symbolic links: Refer to a symbolic path indicating the abstract location of another file
# hard links : Refer to the specific location of physical data.

################################################################################
# Testbench Firmware
################################################################################

ln  -s  ../../../../firmware/nios2/nios2_tb_pkg.sv  ./nios2_tb_pkg.sv -f

################################################################################
# DUT Firmware
################################################################################

# DUT Firmware Package
ln  -s  ../../../../firmware/nios2/nios2_pkg.sv  ./nios2_pkg.sv -f

# Common Firmware required for DUT.
# ln  -s  ../../../../hipe_firmware/util/xxxxxxx.v     ./xxxxxxx.v -f

# HIPE Firmware required for DUT
# ln  -s  ../../../../hipe_firmware/examples/default/default_hiper_pkg.sv  ./default_hiper_pkg.sv -f
# Other HIPE Firmware is linked

################################################################################
# Helpful commands/information
################################################################################

# Format:
#+echo "this is some bash command"
# the output.
# until the next empty line.

#+cd src/ && find ../../../../hipe_firmware/examples/counter/src/
# ../../../../hipe_firmware/examples/counter/src/
# ../../../../hipe_firmware/examples/counter/src/first_counter_hiper_0.v
# ../../../../hipe_firmware/examples/counter/src/first_counter_hiper_1.v
# ../../../../hipe_firmware/examples/counter/src/first_counter_hiper.sv
# ../../../../hipe_firmware/examples/counter/src/first_counter.v


#+find .
# ./first_counter_tb.sv
# ./top.sv
# ./first_counter.v
# ./LoggerPkg.sv
