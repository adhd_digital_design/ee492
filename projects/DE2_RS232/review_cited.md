
# RS232

## Baud Rate

JL

``` verilog
module Baud_Counter (
	// Inputs
input	clk,
input	reset,
	
input	reset_counters,

	// Outputs
output reg baud_clock_rising_edge,
output reg baud_clock_falling_edge,
output reg all_bits_transmitted
);

parameter BAUD_COUNTER_WIDTH = 9;
parameter BAUD_COUNT =  5;
parameter BAUD_TICK_COUNT =  BAUD_COUNT - 1; //9'd433;
parameter HALF_BAUD_TICK_COUNT	= BAUD_COUNT / 2; //9'd216;

parameter DATA_WIDTH = 9;
parameter TOTAL_DATA_WIDTH = DATA_WIDTH + 2;
```

Altera

``` verilog
module Altera_UP_RS232_Counters (
	// Inputs
	clk,
	reset,
	
	reset_counters,

	// Bidirectionals

	// Outputs
	baud_clock_rising_edge,
	baud_clock_falling_edge,
	all_bits_transmitted
);

parameter BAUD_COUNTER_WIDTH	= 9;
parameter BAUD_TICK_INCREMENT	= 9'd1;
parameter BAUD_TICK_COUNT		= 9'd433;
parameter HALF_BAUD_TICK_COUNT	= 9'd216;

parameter TOTAL_DATA_WIDTH		= 11;
```

## Store Temporary/Permanent Data

Altera

``` verilog
Altera_UP_SYNC_FIFO RS232_Out_FIFO (
	// Inputs
	.clk			(clk),
	.reset			(reset),

	.write_en		(transmit_data_en & ~fifo_is_full),
	.write_data		(transmit_data),

	.read_en		(read_fifo_en),
	
	// Bidirectionals

	// Outputs
	.fifo_is_empty	(fifo_is_empty),
	.fifo_is_full	(fifo_is_full),
	.words_used		(fifo_used),

	.read_data		(data_from_fifo)
);
defparam 
	RS232_Out_FIFO.DATA_WIDTH	= DATA_WIDTH,
	RS232_Out_FIFO.DATA_DEPTH	= 128,
	RS232_Out_FIFO.ADDR_WIDTH	= 7;
```

## Display data on LCD.

I really like the module from the project by John Loomis. The parameters are
well defined for different commands.

``` verilog
module LCD_Display(
        input iCLK_50MHZ, iRST_N,

        // LCD control
        output reg LCD_RS, LCD_E, 
        output LCD_RW,
        inout [7:0] DATA_BUS,

        // character memory
        output reg [5:0] char_index,
        input  [7:0] char_value

        );
```

## Interface to use RS232.

JL

``` verilog
module RS232_In (
// Inputs
input clk,
input reset,
input serial_data_in,
input receive_data_en,
// Outputs
output reg [(DATA_WIDTH-1):0] received_data,
output reg receiving_data,
output reg data_received,
output baud_clock
);

parameter BAUD_COUNT = 9'd434;
parameter DATA_WIDTH = 8;
parameter TOTAL_DATA_WIDTH = DATA_WIDTH + 2;
```


``` verilog
module RS232_Out(
        // Inputs
        input clk,
        input reset,

        input [DATA_WIDTH:1] transmit_data,
        input transmit_data_en,

        // Outputs
        output reg serial_data_out,
        output reg transmitting_data
        );
```

Altera

``` verilog
module Altera_UP_RS232_In_Deserializer (
	// Inputs
	clk,
	reset,
	
	serial_data_in,

	receive_data_en,

	// Bidirectionals

	// Outputs
	fifo_read_available,

	received_data
);
parameter BAUD_COUNTER_WIDTH	= 9;
parameter BAUD_TICK_INCREMENT	= 9'd1;
parameter BAUD_TICK_COUNT		= 9'd433;
parameter HALF_BAUD_TICK_COUNT	= 9'd216;

parameter TOTAL_DATA_WIDTH		= 11;
parameter DATA_WIDTH			= 9;
```

``` verilog
module Altera_UP_RS232_Out_Serializer (
	// Inputs
	clk,
	reset,
	
	transmit_data,
	transmit_data_en,

	// Bidirectionals

	// Outputs
	fifo_write_space,

	serial_data_out
);
parameter BAUD_COUNTER_WIDTH	= 9;
parameter BAUD_TICK_INCREMENT	= 9'd1;
parameter BAUD_TICK_COUNT		= 9'd433;
parameter HALF_BAUD_TICK_COUNT	= 9'd216;
parameter TOTAL_DATA_WIDTH		= 11;
parameter DATA_WIDTH			= 9;
```

## Coding Style

### Code Section
Altera

I like how they have standard blocks. I think we should try to separate into
similar sections. I think we move the parameters and port declaration into to
the module interface as we have done prior. I do think we should have a
localparam section.

I dislike how they "Box" the comment. I think we should not have any markers
at the end of lines with text that we write, because it is more time consuming
to modify.

For example:

``` verilog
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************

//****************************************************************************
//                             Sequential logic
//****************************************************************************

//****************************************************************************
//                            Combinational logic
//****************************************************************************

//****************************************************************************
//                              Internal Modules
//****************************************************************************
```

## Initalizing Memory

JL

I really like how his project has a txt file with asci text that will be
loaded into memory (After being converted to mif).


