#!/usr/bin/env bash

set -e
cd firmware/

# Firmware for Hipe
# Expects that the "hipe" repo is right next to the EE492 repo.
ln  -s  ../../../../hipe/hipe_firmware/hipe/hipe_jtag.v       ./hipe_jtag.v       -f
ln  -s  ../../../../hipe/hipe_firmware/hipe/jtag_interface.v  ./jtag_interface.v  -f
ln  -s  ../../../../hipe/hipe_firmware/hipe/hipe.v            ./hipe.v            -f
ln  -s  ../../../../hipe/hipe_firmware/hipe/hipe_rw.v         ./hipe_rw.v         -f
# ln  -s  ../../../../../hipe/hipe_firmware/hipe/hipe_ro.v  ./hipe_ro.v  -f
# ln  -s  ../../../../../hipe/hipe_firmware/hipe/hipe_wo.v  ./hipe_wo.v  -f
