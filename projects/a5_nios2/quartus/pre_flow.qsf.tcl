
################################################################################
# Developer notes
################################################################################

# Use this with a quartus project. Can add in qsf as:
# set_global_assignment -name PRE_FLOW_SCRIPT_FILE "quartus_sh:pre_flow.qsf.tcl"

# See Quartus II Tcl Example: Automatic Script Execution
# https://www.altera.com/support/support-resources/design-examples/design-software/tcl/auto_processing.html

# The Quartus II software executes the scripts as shown here:

# <executable> -t <script name> <flow or module name> <project name> <revision name>
# The first argument passed in the quartus(args) variable is the name of the
# flow or module being executed, depending on the assignment you use. The
# second argument is the name of the project, and the third argument is the
# name of the revision.

# Because of the way the Quartus II software automatically runs the scripts,
# you need to use the post_message command to display messages, instead of the
# puts command.

################################################################################
# Configure
################################################################################
set module_name [lindex $quartus(args) 1]
set project_name [lindex $quartus(args) 2]
set revision_name [lindex $quartus(args) 3]

################################################################################
# Open
################################################################################
if {[project_exists $project_name]} {
    project_open -current_revision $project_name
} else {
    throw {NONE} {No project with that name}
}

################################################################################
# Add to QSF
################################################################################

# Project-Wide Assignments
# set_global_assignment -name SYSTEMVERILOG_FILE ../hdl/nios2.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../hdl/top_nios2/top_nios2.sv
set_global_assignment -name QSYS_FILE ./nios2_pio_q13.qsys
# set_global_assignment -name QSYS_FILE ../../nios2_onchip_jtaguart/nios2_onchip_jtaguart.qsys
# set_global_assignment -name QSYS_FILE ../../nios2_pio_onchip/nios2_pio_onchip.qsys

# Project-Wide Assignments - Family Specific
set qsf_family [get_global_assignment -name FAMILY]
if {[string match "Cyclone II" $qsf_family]} {
    set_global_assignment -name VERILOG_FILE ../hdl/top_nios2/DE2_TOP.v
} elseif {[string match "MAX 10" $qsf_family]} {
    set_global_assignment -name VERILOG_FILE ../hdl/top_nios2/DE10_LITE_TOP.v
} elseif {[string match "Cyclone V" $qsf_family]} {
    set_global_assignment -name VERILOG_FILE ../hdl/top_nios2/DE1_SOC_TOP.v
    set_global_assignment -name VERILOG_FILE ../hdl/top_nios2/C5GT_PRO_TOP.v
} else {
    throw {NONE} {Family ($qsf_family) is not supported.}
}
