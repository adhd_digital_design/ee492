#include "a5.h"

int setup();
void loop();

int main()
{
    setup();
    while (1) { // Never exit.
        loop();
    }
    return 0; // Never reached.
}

int setup(){
    print_hello_world();
    print_square_and_cube_1toN(25);
    print_prime_2toN(19);
    // delay_s(1);
    usleep(1*10^6);
    print_hello_world();
    // delay_s(5);
    usleep(5*10^6);
    print_hello_world();
    return 0;
}

void loop(){
    blink_twos_comp_of_switches();
}

