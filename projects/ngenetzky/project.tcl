#!/home/caeuser/altera/13.0sp1/quartus/bin/quartus_stp -t
#!/home/hipe/altera_lite/16.0/quartus/bin/quartus_stp -t

source ./project.qsf.tcl

set project_name "DE2"
set revision_name "DE2"

file delete -force $project_name
file mkdir ./$project_name
cd $project_name

################################################################################
# Main
################################################################################
if {[is_project_open]} {
    if {[string compare $quartus(project) $project_name]} {
        puts "Different project is open. Do nothing."
        exit 0
 
 } else {
        # TODO: Make/build other subcommands.
        puts "Project is open. Do nothing."
    }
} else {
    # Create if not exist.
    if {[project_exists "$project_name"]} {
        puts "Project exists but is not open. Do nothing."
        exit 0
    } else {
        project_new -revision $revision_name $project_name
        qsf_project
    }
}
