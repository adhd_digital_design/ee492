
source ./device.qsf.tcl
source ./top.qsf.tcl

################################################################################
# Project Settings
################################################################################

# Expected to be called when a project is already open.
proc qsf_project {} {
    qsf_device
    qsf_top
    set_global_assignment -name PROJECT_OUTPUT_DIRECTORY output_files

    file link -symbolic ./nios2_a.qsys ../ip/q13_nios2_a/nios2_a.qsys
    set_global_assignment -name QSYS_FILE "./nios2_a.qsys"

    file mkdir ./hdl
    file link -symbolic ./hdl/DE2_TOP.v ../../hdl/top_nios_a/DE2_TOP.v
    set_global_assignment -name VERILOG_FILE "./hdl/DE2_TOP.v"

    export_assignments -reorganize
}

