
module nios2_sdram_ng #(
    parameter EN_DRAM = 1
) (
    ////////////////////	Clock Input	 	////////////////////
    CLK,
    ////////////////////////	LED		////////////////////////
    LEDG,							//	LED Green[8:0]
    LEDR,							//	LED Red[17:0]
    ////////////////////	Push Button		////////////////////
    KEY,							//	Pushbutton[3:0]
    ////////////////////	DPDT Switch		////////////////////
    SW,								//	Toggle Switch[17:0]
    /////////////////////	SDRAM Interface		////////////////
    DRAM_DQ,						//	SDRAM Data bus 16 Bits
    DRAM_ADDR,						//	SDRAM Address bus 12 Bits
    DRAM_LDQM,						//	SDRAM Low-byte Data Mask 
    DRAM_UDQM,						//	SDRAM High-byte Data Mask
    DRAM_WE_N,						//	SDRAM Write Enable
    DRAM_CAS_N,						//	SDRAM Column Address Strobe
    DRAM_RAS_N,						//	SDRAM Row Address Strobe
    DRAM_CS_N,						//	SDRAM Chip Select
    DRAM_BA_0,						//	SDRAM Bank Address 0
    DRAM_BA_1,						//	SDRAM Bank Address 0
    DRAM_CLK,						//	SDRAM Clock
    DRAM_CKE						//	SDRAM Clock Enable
);

//****************************************************************************
//          Module Interface wires and registers Declarations
//****************************************************************************
input wire CLK;
////////////////////////	Push Button		////////////////////////
input	   [3:0]	KEY;					//	Pushbutton[3:0]
////////////////////////	DPDT Switch		////////////////////////
input	  [17:0]	SW;						//	Toggle Switch[17:0]
////////////////////////////	LED		////////////////////////////
output	[8:0]	LEDG;					//	LED Green[8:0]
output  [17:0]	LEDR;					//	LED Red[17:0]
///////////////////////		SDRAM Interface	////////////////////////
inout	  [15:0]	DRAM_DQ;				//	SDRAM Data bus 16 Bits
output  [11:0]	DRAM_ADDR;				//	SDRAM Address bus 12 Bits
output			DRAM_LDQM;				//	SDRAM Low-byte Data Mask 
output			DRAM_UDQM;				//	SDRAM High-byte Data Mask
output			DRAM_WE_N;				//	SDRAM Write Enable
output			DRAM_CAS_N;				//	SDRAM Column Address Strobe
output			DRAM_RAS_N;				//	SDRAM Row Address Strobe
output			DRAM_CS_N;				//	SDRAM Chip Select
output			DRAM_BA_0;				//	SDRAM Bank Address 0
output			DRAM_BA_1;				//	SDRAM Bank Address 0
output			DRAM_CLK;				//	SDRAM Clock
output			DRAM_CKE;				//	SDRAM Clock Enable

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
wire [32:0] in_w;
wire [32:0] out_w;

//****************************************************************************
//                            Combinational logic
//****************************************************************************
assign in_w = {
    9'h00
    ,SW[17:0]
    ,KEY[3:0]
    };
assign LEDG=out_w[8:0];
assign LEDR=out_w[17+9:9];

//****************************************************************************
//                              Internal Modules
//****************************************************************************

// q13_n2_ju_ram40k u0 (
//     .clk_clk ( CLK )  // clk.clk
// );

q13_n2_ju_ram40k_pio u0 (
    .clk_clk      ( CLK ), // clk.clk
    .pio_in_port  ( in_w ), // pio.in_port
    .pio_out_port ( out_w ) //    .out_port
);

endmodule


