#!/usr/bin/env bash

# Exit if any command returns with an error.
set -e

################################################################################
# Setup.Environment
################################################################################
if [ -d "/c/" ]; then
    # Windows environment (Lab PC).
    # Overwrite the windows environment variables.
    QUARTUS_ROOTDIR=/c/altera/13.0sp1/quartus
    SOPC_KIT_NIOS2=/c/altera/13.0sp1/nios2eds
    QSYS_ROOTDIR=/c/intelFPGA_lite/16.1/quartus/sopc_builder/bin

    # Add cywgin tools to PATH
    PATH=/c/altera/13.0sp1/quartus/bin/cygwin/bin:$PATH
    # Add gnu tools to PATH
    PATH=${SOPC_KIT_NIOS2}/bin/gnu/H-i686-mingw32/bin:$PATH
elif [ -d "/home/caeuser/altera/13.0sp1/" ]; then
    # Linux environment (caeuser).
    export QUARTUS_ROOTDIR=~/altera/13.0sp1/quartus
    export SOPC_KIT_NIOS2=~/altera/13.0sp1/nios2eds
    export QSYS_ROOTDIR=~/altera/13.0sp1/quartus/sopc_builder/bin
fi

# Add quartus/bin to PATH
PATH=${QUARTUS_ROOTDIR}/bin:$PATH
# Add nios2eds/bin to PATH
PATH=${SOPC_KIT_NIOS2}/bin:$PATH
# Add nios2eds//sdk/bin to PATH
PATH=${SOPC_KIT_NIOS2}/sdk2/bin:$PATH

# printf "$QUARTUS_ROOTDIR:$SOPC_KIT_NIOS2:$QSYS_ROOTDIR"
################################################################################
# Setup.Configure
################################################################################
project_dir=$(pwd)

project_name=DE2_TOP
revision_name=DE2_TOP
cable_hint=USB-Blaster

qsys_name=q13_n2_ju_ram40k_pio
cpu_name=nios2_qsys_0
sopc_file=${project_dir}/${qsys_name}.sopcinfo

app_name=${qsys_name}
app_dir=${project_dir}/software/${app_name}
bsp_name=${app_name}_bsp
bsp_dir=${project_dir}/software/${bsp_name}
lib_name=EE492
lib_dir=${project_dir}/software/${lib_name}

################################################################################
# Main
################################################################################
command=$1
case $command in
0|new) # Create quartus project from template "DE2_Top".
    # Start from DE2_Top project from altera.
    cp -f ../../altera/DE2/DE2_Top/* ./
    rm ./README.txt
    rm ./*.qdf
    ;;

1|flow) # Compile quartus project.
    # "Compile Design" Flow
    quartus_map --read_settings_files=on --write_settings_files=off \
        ${project_name} -c ${revision_name}
    quartus_fit --read_settings_files=off --write_settings_files=off \
        ${project_name} -c ${revision_name}
    quartus_asm --read_settings_files=off --write_settings_files=off \
        ${project_name} -c ${revision_name}
    quartus_sta \
        ${project_name} -c ${revision_name}
    ;;

2|pgm) # Program (sof) to FPGA.
    # Can't omit "-c ${cable_hint} unless there is only 1 cable.
    # quartus_pgm --no_banner --mode=jtag -o "P;${revision_name}.sof"
    quartus_pgm -c ${cable_hint} --no_banner --mode=jtag -o "P;${revision_name}.sof"
    ;;

3|new_hello) # Create nios2 projects from template, "Hello World".
    cd $project_dir
    nios2-swexample-create \
        --sopc-file=${sopc_file} \
        --type=hello_world \
        --elf-name=${app_name}.elf \
        --app-dir=${app_dir} \
        --bsp-dir=${bsp_dir}

    cd ${bsp_dir}
    ./create-this-bsp --cpu-name $cpu_name --no-make

    cd ${app_dir}
    ./create-this-app --no-make
    ;;

3.1|new_bsp)
    cd ${bsp_dir}
    nios2-bsp hal ${bsp_dir} $sopc_file --cpu-name $cpu_name
    ;;

3.1|bsp)
    cd ${bsp_dir}
    nios2-bsp-update-settings --settings ${bsp_dir}/settings.bsp \
        --bsp-dir ${bsp_dir} --sopc ${sopc_file} --cpu-name ${cpu_name} \
        --script ${project_dir}/software/settings.bsp.tcl

    nios2-bsp-generate-files --bsp-dir ${bsp_dir} \
        --settings ${bsp_dir}/settings.bsp \
        --sopc ${sopc_file} --cpu-name ${cpu_name} \
    ;;

3.2|new_app)
    cd ${app_dir}
    cp ${project_dir}/software/app/* ${app_dir}/
    nios2-app-generate-makefile --app-dir ${app_dir} --bsp-dir ${bsp_dir} \
        --elf-name ${app_name}.elf \
        --use-lib-dir ${lib_dir} \
        --set QUARTUS_PROJECT_DIR=${project_dir} \
        --set OBJDUMP_INCLUDE_SOURCE 1 \
        --src-rdir ${app_dir}
    ;;

4|app) # "make" app.
    cd ${app_dir}
    # nios2-app-update-makefile --app-dir ${app_dir} \
    #     --add-lib-dir ${lib_dir}
    make
    ;;

5|run) # Download (elf) to a Nios2 instance. Then connect stdin/stdout/stderr
    cd ${app_dir}
    nios2-download -g ${app_name}.elf
    nios2-terminal
    ;;

6|new_lib)
    cd ${lib_dir}
    nios2-lib-generate-makefile --lib-name $lib_name  --lib-dir ${lib_dir} \
        --public-inc-dir ${lib_dir}/drivers/inc \
        --src-rdir ${lib_dir}/drivers
    ;;

7|lib)
    cd ${lib_dir}
    nios2-lib-generate-makefile --lib-dir ${lib_dir}
    make
    ;;

*)
    ;;
esac


