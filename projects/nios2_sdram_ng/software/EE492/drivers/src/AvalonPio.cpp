#include "AvalonPio.h"

#ifdef IOWR_ALTERA_AVALON_PIO_DATA
AvalonPio::AvalonPio(unsigned int base):base(base){};
void AvalonPio::set(uint32_t v){ IOWR_ALTERA_AVALON_PIO_DATA(this->base, v); }
uint32_t AvalonPio::get(){ return IORD_ALTERA_AVALON_PIO_DATA(this->base); }
#else
AvalonPio::AvalonPio(unsigned int base):base(base){};
void AvalonPio::set(uint32_t v){ 0; }
uint32_t AvalonPio::get(){ return 0; }
#endif
