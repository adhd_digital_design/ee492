
#pragma once
#include "stdint.h"

// #include "altera_avalon_pio_regs.h"
// #define IOADDR_ALTERA_AVALON_PIO_DATA(base)           __IO_CALC_ADDRESS_NATIVE(base, 0)
// #define IORD_ALTERA_AVALON_PIO_DATA(base)             IORD(base, 0) 
// #define IOWR_ALTERA_AVALON_PIO_DATA(base, data)       IOWR(base, 0, data)

// #define IOADDR_ALTERA_AVALON_PIO_DIRECTION(base)      __IO_CALC_ADDRESS_NATIVE(base, 1)
// #define IORD_ALTERA_AVALON_PIO_DIRECTION(base)        IORD(base, 1) 
// #define IOWR_ALTERA_AVALON_PIO_DIRECTION(base, data)  IOWR(base, 1, data)

// #define IOADDR_ALTERA_AVALON_PIO_IRQ_MASK(base)       __IO_CALC_ADDRESS_NATIVE(base, 2)
// #define IORD_ALTERA_AVALON_PIO_IRQ_MASK(base)         IORD(base, 2) 
// #define IOWR_ALTERA_AVALON_PIO_IRQ_MASK(base, data)   IOWR(base, 2, data)

// #define IOADDR_ALTERA_AVALON_PIO_EDGE_CAP(base)       __IO_CALC_ADDRESS_NATIVE(base, 3)
// #define IORD_ALTERA_AVALON_PIO_EDGE_CAP(base)         IORD(base, 3) 
// #define IOWR_ALTERA_AVALON_PIO_EDGE_CAP(base, data)   IOWR(base, 3, data)

// #define IOADDR_ALTERA_AVALON_PIO_SET_BIT(base)       __IO_CALC_ADDRESS_NATIVE(base, 4)
// #define IORD_ALTERA_AVALON_PIO_SET_BITS(base)         IORD(base, 4) 
// #define IOWR_ALTERA_AVALON_PIO_SET_BITS(base, data)   IOWR(base, 4, data)

// #define IOADDR_ALTERA_AVALON_PIO_CLEAR_BITS(base)       __IO_CALC_ADDRESS_NATIVE(base, 5)
// #define IORD_ALTERA_AVALON_PIO_CLEAR_BITS(base)         IORD(base, 5) 
// #define IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(base, data)   IOWR(base, 5, data)


struct AvalonPio {
    AvalonPio(unsigned int base);
    void set(uint32_t v);
    uint32_t get();
    const unsigned int base;
};
