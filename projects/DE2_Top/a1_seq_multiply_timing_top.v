//a1_seq_multiply_timing_top.v

module a1_seq_multiply_timing_top(
input                    CLOCK_50,                //    50 MHz
input wire [opperand_width-1:0] m,  // Multiplicand
input wire [opperand_width-1:0] q,  // Multiplier
output wire  [P_WIDTH-1:0] p         // Product
);

parameter opperand_width = 10;
localparam P_WIDTH = 2*opperand_width;

// inside sequential_multiplier block
// reg [opperand_width-1:0] m_reg, q_reg;
// reg [2*opperand_width-1:0] q;  
// always@(start) begin    
//     q_reg = q;
//     m_reg = m;
// end

// sequential multiplier needs multiple clock cycles
sequential_multiplier #(
    .opperand_width(opperand_width)
) sm (
    .clk(CLOCK_50),
    .start(CLOCK_50),
    .q(q), // multiplier
    .m(m), // multiplicand
    .p(p)  // product

    // DEBUG ONLY
    // , .seq_mult_done(seq_mult_done)
);


endmodule