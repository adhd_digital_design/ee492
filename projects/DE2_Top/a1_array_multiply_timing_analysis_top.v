//a1_array_multiply_timing_analysis_top

module a1_array_multiply_timing_analysis_top #(
    parameter opperand_width = 10 // Assume we multiply two 8-Bit #s...
)(
    input                    CLOCK_50,                //    50 MHz
    input wire [opperand_width-1:0] m,  // Multiplicand
    input wire [opperand_width-1:0] q,  // Multiplier
    output wire  [(2*opperand_width)-1:0] p         // Product
);
localparam P_WIDTH = 2*opperand_width;

// inside array_multiplier block
// reg [opperand_width-1:0] m_reg, q_reg;
// reg [2*opperand_width-1:0] q;  
// always@(start) begin    
//     q_reg = q;
//     m_reg = m;
// end

// array multiplier calculates the product in a single clock cycle
array_multiplier_sync #(
    .opperand_width(opperand_width)
) ams (
    .clk(CLOCK_50),           // Latch Product on clk
    .start(CLOCK_50),    // Start the multiplication.
    .q(q[opperand_width-1:0]),  // Multiplier
    .m(m[opperand_width-1:0]),  // Multiplicand
    .p(p[opperand_width-1:0]) // Product
);

endmodule