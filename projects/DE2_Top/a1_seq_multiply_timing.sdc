## Generated SDC file "a1_seq_multiply_timing.sdc"

## Copyright (C) 1991-2013 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

## DATE    "Mon Jan 16 20:22:28 2017"

##
## DEVICE  "EP2C35F672C8"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

#https://www.altera.com/en_US/pdfs/literature/ug/ug_tq_tutorial.pdf
#create the 50 MHz (20 ns) clock
create_clock -name clk_50 -period 20.000 [get_ports {CLOCK_50}]


#http://www.alteraforum.com/alterawiki.com/uploads/3/3f/TimeQuest_User_Guide.pdf
derive_pll_clocks
derive_clock_uncertainty

#**************************************************************
# Create Generated Clock
#**************************************************************



#**************************************************************
# Set Clock Latency (fall time/rise time)
#**************************************************************

# 1ns rise time  # rise to rise delay
#set_max_delay -rise_from [get_clocks clk_50] -rise_to [get_clocks clk_50] 1.0


#**************************************************************
# Set Clock Uncertainty
#**************************************************************



#**************************************************************
# Set Input Delay
#**************************************************************


#
set_input_delay -clock clk_50 -max 0.0 [get_ports m[*]]
set_input_delay -clock clk_50 -max 0.0 [get_ports q[*]]

#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -clock clk_50 -max 0.0 [get_ports p[*]]
set_output_delay -clock clk_50 -min 0.0 [get_ports p[*]]

#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************

# False path from p back to m,q
set_false_path -from [get_ports p[*]] -to [get_ports m[*]]
set_false_path -from [get_ports p[*]] -to [get_ports q[*]]

#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

