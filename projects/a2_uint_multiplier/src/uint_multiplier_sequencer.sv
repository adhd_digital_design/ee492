
//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Generate test vectors for unsigned multiplier.
//Date Created : 2017-01-19

// Timescale
`timescale 1ns/10ps

typedef struct {
  integer unsigned q;
  integer unsigned m;
} MultInputs;

module uint_multiplier_sequencer #(
    parameter SEED_Q = 32'hDE, // Can't be zero.
    parameter SEED_M = 32'hAD  // Can't be zero.
);
localparam nspecial = 10;

rand_generator #(.SEED(SEED_Q)) rand_q ();
rand_generator #(.SEED(SEED_M)) rand_m ();

// Test vectors are designed for 8 bit multiplier.
MultInputs  special[0:nspecial-1]  =
'{
    '{  8'h0     ,  8'h0     },
    '{  8'h0     ,  8'h0     },
    '{  4'b1101  ,  4'b1011  },
    '{  8'h0     ,  8'hAA    },
    '{  8'hAA    ,  8'h0     },
    '{  8'h1     ,  8'hFF    },
    '{  8'hFF    ,  8'h1     },
    '{  8'hCA    ,  8'hFE    },
    '{  8'hBA    ,  8'hBE    },
    '{  8'hFF    ,  8'hFF    }
};

initial begin
    assert( 0 != SEED_Q) else $error("SEED can't be zero.");
    assert( 0 != SEED_M) else $error("SEED can't be zero.");
end

task next();
    rand_q.next();
    rand_m.next();
endtask

function integer unsigned get_q(input integer unsigned i);
    if(i<nspecial) return special[i].q;
    else return rand_q.random;
endfunction
function integer unsigned get_m(input integer unsigned i);
    if(i<nspecial) return special[i].m;
    else return rand_m.random;
endfunction

endmodule
