// a1_top_tb.sv
//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: // Top level of the test.
//Date Created : 2017-01-18

//UVM
`include "uvm_macros.svh"
`include "uvm_pkg.sv"

// Timescale
`timescale 1ns/10ps

module a2_top_tb();
    localparam OPPERAND_WIDTH = 8; // Indicates MAX OPERAND width
    localparam P_WIDTH = 2*OPPERAND_WIDTH;     // Product Width
    localparam MAX_VAL =2**(OPPERAND_WIDTH)-1;

    // Declare signals in the module.
    bit clk;
    bit clk_start;
    integer unsigned counter;
    integer unsigned first_seq_q;

    // Sequencer
    uint_multiplier_sequencer sequencer();

    // Instantiate test modules
    // All multipliers share a common clock. They are started by task.
    uint_multiplier_driver #( .OPPERAND_WIDTH(OPPERAND_WIDTH) ) mult_tb_n ( clk );

    // Instantiate Monitors
    uint_multiplier_monitor #( .NUM_TESTS_TO_DO(255+10+5),.OPPERAND_WIDTH(OPPERAND_WIDTH) ) mult_monitor_n (
        .m(mult_tb_n.m),
        .q(mult_tb_n.q),
        .p(mult_tb_n.p)
    );

    // Clock generator
    always begin // Empty sensitivity list.
        // t = 550ps = 1.818 MHz // TODO: What was this period related to?
        #1 clk = ~clk; // Invert clock every X time periods.
    end

    always begin // Empty sensitivity list.
        #20 clk_start = ~clk_start;
    end

    initial begin
        clk=0;
        clk_start=0;
        counter=0;

        // mult_monitor_n.header();
    end

    always @(posedge clk) begin
    end

    always @(posedge clk_start) begin
        // Increment the counter after.
        counter = counter +1; // Increment the counter once per start.
        sequencer.next();
        // if( 255+10+5 == counter ) begin : finish_condition
        //     $finish;
        // end

        mult_monitor_n.log();


        mult_tb_n.start_mult(
            .m_in(sequencer.get_m(counter)),
            .q_in(sequencer.get_q(counter))
        );
    end

    // final begin
    // end

endmodule
