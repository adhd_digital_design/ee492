//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Drive unsigned multiplier firmware
//Date Created : 2017-01-19

// Timescale
`timescale 1ns/10ps

module uint_multiplier_driver #(
    parameter  OPPERAND_WIDTH  =  8
) (
    input wire clk
);
    localparam P_WIDTH = 2*OPPERAND_WIDTH;

    reg [OPPERAND_WIDTH-1:0] m;  // Multiplicand
    reg [OPPERAND_WIDTH-1:0] q;  // Multiplier
    wire [P_WIDTH-1:0] p;        // Product

    reg start;

initial begin
    m = 0;      // initial value of Multiplicand
    q = 0;      // initial value of Multiplier
    start = 0;
end

task start_mult(
    input integer unsigned  q_in,
    input integer unsigned  m_in
);
    // Truncates unsigned integer to the length accepted by module.
    m[OPPERAND_WIDTH-1:0] = m_in;
    q[OPPERAND_WIDTH-1:0] = q_in;

    // Multiplier should latch the inputs on start and begin the calculation.
    start = 1;
    #1
    start = 0;
endtask

// array_multiplier_sync #(
//     .opperand_width(OPPERAND_WIDTH) // Assume we multiply two 8-Bit #s...
// ) a_mult (
//     .clk(clk),                     // Latch Product on clk
//     .start(start),                   // Start the multiplication.
//     .m(m[OPPERAND_WIDTH-1:0]),  // Multiplicand
//     .q(q[OPPERAND_WIDTH-1:0]),  // Multiplier
//     .p(p[P_WIDTH-1:0])         // Product
// );

sequential_multiplier #(
    .opperand_width(OPPERAND_WIDTH) // Assume we multiply two 8-Bit #s...
) s_mult (
    .clk(clk),                     // Latch Product on clk
    .start(start),                   // Start the multiplication.
    .m(m[OPPERAND_WIDTH-1:0]),  // Multiplicand
    .q(q[OPPERAND_WIDTH-1:0]),  // Multiplier
    .p(p[P_WIDTH-1:0])         // Product

    , .seq_mult_done()
);

endmodule
