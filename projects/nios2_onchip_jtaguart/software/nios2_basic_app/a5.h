
#pragma once

#include <stdio.h>

const size_t MAX_PRIME_NUM = 100;

void print_hello_world(){
  printf("Hello from Nios II!\n");
}

void print_square_and_cube(int x){
    printf("%d, %d, %d\n", x, x*x, x*x*x);
}

void print_square_and_cube_1toN(int N){
    for( int i=1; i<=N; i++){
        print_square_and_cube(i);
    }
}

void print_prime_2toN(int N){
    const int NOT_PRIME = 0;
    int is_prime[MAX_PRIME_NUM];

    if( MAX_PRIME_NUM < N) {
        printf("ERROR: Adjust MAX_PRIME_NUM to be larger than N");
    }

    for( int i=2; i<=N; i++){
        is_prime[i] = ! NOT_PRIME; // First assume everything is prime.
    }

    // Remove those that are multiples numbers.
    for( int i=2; i<=N; i++){
        for( int multiple = i+i; multiple <= N; multiple += i ){
            is_prime[multiple] = NOT_PRIME;
        }
    }

    // Print if it is not NOT_PRIME
    for( int i=2; i<=N; i++){
        if( NOT_PRIME != is_prime[i] ){
            printf("%d\n", i);
        }
    }
}
