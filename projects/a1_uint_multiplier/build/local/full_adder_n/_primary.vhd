library verilog;
use verilog.vl_types.all;
entity full_adder_n is
    generic(
        n               : integer := 1
    );
    port(
        x               : in     vl_logic_vector;
        y               : in     vl_logic_vector;
        cin             : in     vl_logic;
        \A\             : out    vl_logic_vector;
        cout            : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of n : constant is 1;
end full_adder_n;
