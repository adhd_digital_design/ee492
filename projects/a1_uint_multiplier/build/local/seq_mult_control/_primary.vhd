library verilog;
use verilog.vl_types.all;
entity seq_mult_control is
    generic(
        opperand_width  : integer := 8
    );
    port(
        clk             : in     vl_logic;
        start           : in     vl_logic;
        prod_lsb        : in     vl_logic;
        shift_en        : out    vl_logic;
        load_lsb        : out    vl_logic;
        load_msb        : out    vl_logic;
        reset_msb       : out    vl_logic;
        add_en          : out    vl_logic;
        seq_mult_done   : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of opperand_width : constant is 1;
end seq_mult_control;
