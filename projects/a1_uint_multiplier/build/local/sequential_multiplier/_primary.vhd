library verilog;
use verilog.vl_types.all;
entity sequential_multiplier is
    generic(
        opperand_width  : integer := 8
    );
    port(
        clk             : in     vl_logic;
        start           : in     vl_logic;
        q               : in     vl_logic_vector;
        m               : in     vl_logic_vector;
        p               : out    vl_logic_vector;
        seq_mult_done   : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of opperand_width : constant is 1;
end sequential_multiplier;
