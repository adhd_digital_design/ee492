library verilog;
use verilog.vl_types.all;
entity multiply_cell is
    port(
        m_i             : in     vl_logic;
        q_i             : in     vl_logic;
        c_i             : in     vl_logic;
        c_o             : out    vl_logic;
        pp_i            : in     vl_logic;
        pp_o            : out    vl_logic
    );
end multiply_cell;
