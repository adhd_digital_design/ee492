library verilog;
use verilog.vl_types.all;
entity shift_reg_n is
    generic(
        n               : integer := 8
    );
    port(
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        load            : in     vl_logic;
        value_i         : in     vl_logic_vector;
        shift           : in     vl_logic;
        s_i             : in     vl_logic;
        s_o             : out    vl_logic;
        value_o         : out    vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of n : constant is 1;
end shift_reg_n;
