library verilog;
use verilog.vl_types.all;
entity uint_multiplier_driver is
    generic(
        OPPERAND_WIDTH  : integer := 8
    );
    port(
        clk             : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of OPPERAND_WIDTH : constant is 1;
end uint_multiplier_driver;
