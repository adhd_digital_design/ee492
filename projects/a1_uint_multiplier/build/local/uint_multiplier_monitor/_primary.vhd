library verilog;
use verilog.vl_types.all;
entity uint_multiplier_monitor is
    generic(
        OPPERAND_WIDTH  : integer := 8;
        P_WIDTH         : vl_notype
    );
    port(
        m               : in     vl_logic_vector;
        q               : in     vl_logic_vector;
        p               : in     vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of OPPERAND_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of P_WIDTH : constant is 3;
end uint_multiplier_monitor;
