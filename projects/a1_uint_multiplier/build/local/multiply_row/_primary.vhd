library verilog;
use verilog.vl_types.all;
entity multiply_row is
    generic(
        m_row_width     : integer := 8
    );
    port(
        m               : in     vl_logic_vector;
        q_i             : in     vl_logic;
        c_i             : in     vl_logic;
        c_o             : out    vl_logic;
        pp_i            : in     vl_logic_vector;
        pp_o            : out    vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of m_row_width : constant is 1;
end multiply_row;
