#ifndef __AVALON_SLAVE_REGS_H
#define __AVALON_SLAVE_REGS_H

#include <io.h>
#include "avalon_slave_macros.h"

#define IOADDR_AVALON_SLAVE_DIN(base, index)           __IO_CALC_ADDRESS_NATIVE(base, BASE_ADDR(index))
#define IORD_AVALON_SLAVE_DIN(base, index)             IORD(base, BASE_ADDR(index))
#define IOWR_AVALON_SLAVE_DIN(base, data, index)       IOWR(base, BASE_ADDR(index), data)

#define IOADDR_AVALON_SLAVE_DOUT(base, index)      __IO_CALC_ADDRESS_NATIVE(base, BASE_ADDR(index))
#define IORD_AVALON_SLAVE_DOUT(base, index)        IORD(base, BASE_ADDR(index))
#define IOWR_AVALON_SLAVE_DOUT(base, data, index)  IOWR(base, BASE_ADDR(index), data)

#define IOADDR_AVALON_SLAVE_IRQ_MASK(base, index)       __IO_CALC_ADDRESS_NATIVE(base, BASE_ADDR(index))
#define IORD_AVALON_SLAVE_IRQ_MASK(base, index)         IORD(base, BASE_ADDR(index))
#define IOWR_AVALON_SLAVE_IRQ_MASK(base, data, index)   IOWR(base, BASE_ADDR(index), data)

#define IOADDR_AVALON_SLAVE_EDGE_CAP(base, index)       __IO_CALC_ADDRESS_NATIVE(base, BASE_ADDR(index))
#define IORD_AVALON_SLAVE_EDGE_CAP(base, index)         IORD(base, BASE_ADDR(index))
#define IOWR_AVALON_SLAVE_EDGE_CAP(base, data, index)   IOWR(base, BASE_ADDR(index), data)

#endif /* __AVALON_SLAVE_REGS_H */
