
//****************************************************************************
// module register_with_bytelanes
// helper module to simplify having a register of variable width and
// containing independent byte lanes
//****************************************************************************
module register_with_bytelanes (
    clk,
    reset,

    data_in,
    write,
    byte_enables,
    data_out
);
//****************************************************************************
// Parameters
//****************************************************************************
parameter DATA_WIDTH = 32;

//****************************************************************************
// Module Interface
//****************************************************************************
input clk;
input reset;

input [DATA_WIDTH-1:0] data_in;
input write;
input [(DATA_WIDTH/8)-1:0] byte_enables;
output reg [DATA_WIDTH-1:0] data_out;

// generating write logic for each group of 8 bits for 'data_out'
generate
    genvar LANE;
    for(LANE = 0; LANE < (DATA_WIDTH/8); LANE = LANE+1) begin: register_bytelane_generation
        always @ (posedge clk or posedge reset) begin
            if(reset == 1) begin
                data_out[(LANE*8)+7:(LANE*8)] <= 0;
            end
            else if((byte_enables[LANE] == 1) & (write == 1)) begin
                // write to each byte lane with write = 1 and the lane byteenable = 1
                data_out[(LANE*8)+7:(LANE*8)] <= data_in[(LANE*8)+7:(LANE*8)]; 
            end
        end
    end
endgenerate

endmodule
