#
# avalon_slave_driver.tcl
#

# Create a new driver
create_driver avalon_slave_driver

# Associate it with some hardware known as "avalon_slave"
set_sw_property hw_class_name avalon_slave

# The version of this driver
set_sw_property version 0.0

# This driver may be incompatible with versions of hardware less
# than specified below. Updates to hardware and device drivers
# rendering the driver incompatible with older versions of
# hardware are noted with this property assignment.
set_sw_property min_compatible_hw_version 0.0

# Initialize the driver in alt_sys_init()
set_sw_property auto_initialize false

# Location in generated BSP that above sources will be copied into
set_sw_property bsp_subdirectory drivers

#
# Source file listings...
#

# C/C++ source files
# add_sw_property c_source HAL/src/avalon_slave_init.c
# add_sw_property c_source HAL/src/avalon_slave_read.c
# add_sw_property c_source HAL/src/avalon_slave_write.c
# add_sw_property c_source HAL/src/avalon_slave_ioctl.c
# add_sw_property c_source HAL/src/avalon_slave_fd.c

# Include files
# add_sw_property include_source HAL/inc/avalon_slave.h
# add_sw_property include_source HAL/inc/avalon_slave_fd.h
add_sw_property include_source inc/avalon_slave_regs.h
add_sw_property include_source inc/avalon_slave_macros.h

# This driver supports HAL & UCOSII BSP (OS) types
add_sw_property supported_bsp_type HAL
add_sw_property supported_bsp_type UCOSII


# Add the following per_driver configuration option to the BSP:
#  o Type of setting (boolean_define_only translates to "either
#    emit a #define if true, or don't if false"). Useful for
#    source code with "#ifdef" style build-options.
#  o Generated file to write to (public_mk_define -> public.mk)
#  o Name of setting for use with bsp command line settings tools
#    (enable_small_driver). This name will be combined with the
#    driver class to form a settings hierarchy to assure unique
#    settings names
#  o '#define' in driver code (and therefore string in generated
#     makefile): "avalon_slave_SMALL", which means: "emit
#     CPPFLAGS += avalon_slave_SMALL in generated makefile
#  o Default value (if the user doesn't specify at BSP creation): false
#    (which means: 'do not emit above CPPFLAGS string in generated makefile)
#  o Description text
# add_sw_setting boolean_define_only public_mk_define enable_small_driver avalon_slave_SMALL false "Small-footprint (polled mode) driver"


# Add the following per_driver configuration option to the BSP:
#  o Type of setting (boolean_define_only translates to "either
#    emit a #define if true, or don't if false"). Useful for
#    source code with "#ifdef" style build-options.
#  o Generated file to write to (public_mk_define -> public.mk)
#  o Name of setting for use with bsp command line settings tools
#    (enable_jtag_uart_ignore_fifo_full_error). This name will be 
#    combined with the driver class to form a settings hierarchy 
#    to assure unique settings names
#  o '#define' in driver code (and therefore string in generated
#     makefile): "avalon_slave_IGNORE_FIFO_FULL_ERROR", 
#     which means: "emit CPPFLAGS += 
#     avalon_slave_IGNORE_FIFO_FULL_ERROR in generated 
#     makefile
#  o Default value (if the user doesn't specify at BSP creation): false
#    (which means: 'do not emit above CPPFLAGS string in generated makefile)
#  o Description text
# add_sw_setting boolean_define_only public_mk_define enable_jtag_uart_ignore_fifo_full_error avalon_slave_IGNORE_FIFO_FULL_ERROR false "Enable JTAG UART driver to recover when host is inactive causing buffer to full without returning error. Printf will not fail with this recovery."

# End of file
