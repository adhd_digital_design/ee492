
// Figure 3. Verilog code for the Avalon MM Interface.
module avs_reg32 (
    csi_clk,
    rsi_reset_n,
    avs_writedata,
    avs_readdata,
    avs_write,
    avs_read,
    avs_byteenable,
    avs_chipselect,
    q_out
    );
    // signals for connecting to the Avalon fabric
    input csi_clk, rsi_reset_n, avs_read, avs_write, avs_chipselect;
    input [3:0] avs_byteenable;
    input [31:0] avs_writedata;
    output [31:0] avs_readdata;
    // signal for exporting register contents outside of the embedded system
    output [31:0] q_out;

    wire [3:0] local_byteenable;
    wire [31:0] to_reg, from_reg;

    assign to_reg = avs_writedata;
    assign local_byteenable = (avs_chipselect & avs_write) ? avs_byteenable : 4'd0;
    reg32 U1 (
        .clock(csi_clk),
        .resetn(rsi_reset_n),
        .D(to_reg),
        .byteenable(local_byteenable),
        .Q(from_reg)
        );
    assign avs_readdata = from_reg;
    assign q_out = from_reg;
endmodule
