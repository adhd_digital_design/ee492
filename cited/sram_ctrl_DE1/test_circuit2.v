 /*  TEST CIRCUIT v2 for SRAM CONTROLLER
 ****************************************************************
Summary:  This circuit will load the entire SRAM with a binary
incrementation and then read it back to be displayed
by 16 LED's.  The read rate can be adjusted in the m_mod module.

Platform:  Altera DE1 board with (IS61LV25616AL-10 SRAM) and
			  50 MHz oscillator.

DE1 hardware profile:
button 0 = reset_n;
button 1 = read  (Read entire SRAM) 
button 2 = write (load SRAM);

Red LEDs = MSB (15 - 8)
Green LEDs = LSB (7 - 0)


by Shawn Gerber 
Aug 13, 2012
**************************************************/

module test_circuit_2
(
//DE1 board interface
input  wire clk ,
input  wire [2:0] key ,  //buttons = write, read, reset_n
output wire [7:0] ledr,
output wire [7:0] ledg,

// sram signals
output wire [17:0] sram_addr ,
output wire sram_oe_n, sram_we_n,
inout  wire [15:0] sram_dq,
output wire sram_ce_n , sram_lb_n, sram_ub_n
);


//internal signals
reg [2:0] state_reg, state_next;
reg [17:0] count_reg, count_next;
wire  reset_n = key[0];
wire  read  = key[1];
wire  write = key[2];
reg   start_n;
reg   rw;
wire ready ;
wire [15:0] data_read;
wire pulse;
reg rst_pulse;

localparam [2:0] state_idle    = 3'b000, // states
						state_read1  = 3'b001,
						state_read2  = 3'b010,
						state_write1 = 3'b011,
						state_write2 = 3'b100;
//SRAM CONTROLLER
sram_ctrl device0(.clk(clk), .reset_n(reset_n), .start_n(start_n), .rw(rw), .addr_in(count_reg), .data_write(count_reg[15:0]), .ready(ready),
					.data_read(data_read), .sram_addr(sram_addr), .we_n(sram_we_n), .oe_n(sram_oe_n), 
					.data_io(sram_dq), .ce_a_n(sram_ce_n), .ub_a_n(sram_ub_n), .lb_a_n(sram_lb_n)
						);
//M_MOD COUNTER		
mod_m_count device1(.clk(clk), .reset(rst_pulse), .count(), .pulse(pulse));  //m_mod counter set to 50ms delay = 54 minutes

always@(posedge clk, negedge reset_n)
begin
	if (!reset_n)
		begin
			state_reg = state_idle;
			count_reg = 0;
		end
	else
		begin
			state_reg <= state_next;
			count_reg <= count_next;
		end
end


//next state logic and outputs
always@*
begin
	start_n = 1'b1;
	rw = 1'b1;
	state_next = state_reg;
	count_next = count_reg;
	rst_pulse = 1'b1;
	case(state_reg)
	state_idle:
		begin
			if (write == 1'b0)
				begin
					state_next = state_write1;
					count_next = 0;
				end
			else
				if(read == 1'b0)
					begin
						state_next = state_read1;
						count_next = 0;
					end
		end //end state_idle	
	state_write1:
		begin
			rw = 1'b0;
			start_n = 1'b0;
			state_next = state_write2;
		end //end state_write1
	state_write2:
		begin
			rw = 1'b0;
			if (ready == 1)
				begin
				count_next = count_reg + 1'b1;
					if(count_next == 0)
						state_next = state_idle;
					else
						state_next = state_write1;
				end
			else
				state_next = state_write2;
		end //end state_write2
	state_read1:
		begin
			rw = 1'b1;
			start_n = 1'b0;
			state_next = state_read2;
			rst_pulse = 1'b0;
		end //end state_read1
	state_read2:
		begin
			rw = 1'b1;
			if ((ready == 1'b1) && (pulse == 1'b1))
				begin
				count_next = count_reg + 1'b1;
					if(count_next == 0)
						state_next = state_idle;
					else
						state_next = state_read1;
				end
			else
				state_next = state_read2;
		end //end state_read2
	default: state_next = state_reg;
   endcase
end

// data outputs
assign ledg = data_read[7:0];
assign ledr = data_read[15:8];  

endmodule

