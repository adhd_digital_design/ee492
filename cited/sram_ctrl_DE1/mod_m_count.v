module  mod_m_count
#(parameter M=50000) //2500000 = 50 ms | 250000 = 5 ms | 25000 = 500 us | 50000 = 1 ms
(
input  wire clk, reset,
output reg [log2(M)-1:0] count,
output wire pulse
);

localparam N = log2(M);
wire [N-1:0] count_next;

always@(posedge clk, negedge reset)
begin
	if(!reset)
		count <= 0;
	else 
		count <= count_next;
end

//next_state
assign count_next = (count == M-1) ? 0 : count + 1'b1;
//output
assign pulse = (count == M-1) ? 1'b1 : 1'b0;
//function
function integer log2(input integer m);
integer i;
begin
	log2 = 1;
	for (i=0; 2**i < m ;i=i+1)
		log2 = i + 1;
end
endfunction
endmodule
