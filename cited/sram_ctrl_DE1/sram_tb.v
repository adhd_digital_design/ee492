//TESTBENCH FOR SRAM CONTROLLER

`timescale 1 ns / 100 ps

module sram_tb();

localparam T = 20, delay = 1;
reg clk , reset_n;
// to / from main system
reg start_n, rw; //rw = 1  is a read
reg [17:0] addr_in;
reg [15:0] data_write;
wire ready;
wire [15:0] data_read;
// to / from sram chip
wire [17:0] sram_addr;
wire we_n, oe_n;
// sram chip a
wire ce_a_n , ub_a_n, lb_a_n;
wire [15:0] data_io;

//assign dio_a = (rw == 1'b1) ?  data_io  : 16'hzzzz;



sram_ctrl uut0(.clk(clk), .reset_n(reset_n), .start_n(start_n), .rw(rw), .addr_in(count_reg), .data_write(data_write), .ready(ready),
					.data_read(), .sram_addr(sram_addr), .we_n(we_n), .oe_n(oe_n), 
					.data_io(data_io), .ce_a_n(ce_a_n), .ub_a_n(ub_a_n), .lb_a_n(lb_a_n)
						);
						
						
task _reset();
begin
	
	reset_n = 1'b1;
	#(delay*3);
	reset_n = 1'b0;
	#T;
	@(negedge clk);
	reset_n = 1'b1;
end
endtask

task initialize();
begin
	_reset();
	start_n = 1'b1;
	rw = 1'b0;
	addr_in = 0;
	data_write = 0;
	
end
endtask

always
begin
	clk = 1'b1;
	#(T/2);
	clk = 1'b0;
	#(T/2);
end

task write(input [15:0] data, input [17:0] address_w);
begin
	if(ready == 1)
	begin
		data_write = data;
		addr_in = address_w;
		start_n = 1'b0;
		#(T);
		start_n = 1'b1;
		
	end
end
endtask

task read(input [17:0] address_r);
begin
	addr_in = address_r;
	rw = 1'b1;  //read
	start_n = 1'b0;
	wait(oe_n == 1'b0);
	#(delay*4);
	//data_read = 16'hAAAA;
	#delay;
	start_n = 1'b1;
end
endtask

initial
begin
initialize();
#(20*T);
write(16'hAAAA, 18'h30f0f); //data, address
#(4*T);
//read(18'h30f0f);
#(3*T);
$stop;
end
						
						
endmodule
