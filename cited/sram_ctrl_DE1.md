
# SRAM Controller in Verilog for Altera DE1 board

- Title: SRAM Controller in Verilog for Altera DE1 board
- Email: klamathSnow@gmail.com
- Date: 2016-08-12
- Type: Firmware
- Tags: Verilog, 
- Copyright: -
- Description:  This code provides a useful interface to an SRAM device on your board. The test code utilizes the SRAM controller to write a binary incrementation to the board and then read it back to 16 LEDs. It is written in Verilog.
- Source Code URL: https://drive.google.com/file/d/0ByZh57epHwSddk1ldklEaDZMRjg/edit
- Youtube Demo URL: https://www.youtube.com/watch?v=UBSREEqE8hw

