
`ifndef uart_tb_pkg_
`define uart_tb_pkg_

// DUT Source Files
`include "uart.v"

// Testbench Source Files
`include "uart_ports.sv"
`include "uart_sb.sv"
`include "uart_tb.sv"
`include "uart_top.sv"
`include "uart_txgen.sv"

`endif