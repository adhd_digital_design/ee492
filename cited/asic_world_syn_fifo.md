
# Synchronous FIFO

- Title: Implementing a Synchronous FIFO using Verilog
- Author: Deepak Kumar Tala 
- Email: deepak@asic-world.com
- Date: 2014-02-09
- Type: Firmware
- Tags: Verilog, 
- Copyright: ©  1998-2014 http://www.asic-world.com
- Source URL: http://www.asic-world.com/examples/verilog/syn_fifo.html
