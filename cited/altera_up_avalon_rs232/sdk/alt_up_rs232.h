/******************************************************************************
*                                                                             *
* License Agreement                                                           *
*                                                                             *
* Copyright (c) 2003 Altera Corporation, San Jose, California, USA.           *
* All rights reserved.                                                        *
*                                                                             *
* Permission is hereby granted, free of charge, to any person obtaining a     *
* copy of this software and associated documentation files (the "Software"),  *
* to deal in the Software without restriction, including without limitation   *
* the rights to use, copy, modify, merge, publish, distribute, sublicense,    *
* and/or sell copies of the Software, and to permit persons to whom the       *
* Software is furnished to do so, subject to the following conditions:        *
*                                                                             *
* The above copyright notice and this permission notice shall be included in  *
* all copies or substantial portions of the Software.                         *
*                                                                             *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER      *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     *
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         *
* DEALINGS IN THE SOFTWARE.                                                   *
*                                                                             *
* This agreement shall be governed in all respects by the laws of the State   *
* of California and by the laws of the United States of America.              *
*                                                                             *
******************************************************************************/

#ifndef __ALT_UP_RS232_H__
#define __ALT_UP_RS232_H__

#include "alt_up_rs232_regs.h"
#include "system.h"

/**
 * @brief Enable the read interrupts for the RS232 UART core
 *
 * @return 0 for success
 **/
int alt_up_rs232_enable_read_interrupt();

/**
 * @brief Disable the read interrupts for the RS232 UART core
 *
 * @return 0 for success
 **/
int alt_up_rs232_disable_read_interrupt();

/**
 * @brief Check whether the DATA field has a parity error
 *
 * @return 0 for no errors, \f$-1\f$ for parity error occurs
 **/
int alt_up_rs232_check_parity(alt_u32 data_reg);

/**
 * @brief Get the number of characters remaining in the read FIFO 
 *
 * @return the number of characters remaining
 **/
unsigned alt_up_rs232_get_num_chars_in_read_FIFO();

/**
 * @brief Get the number of characters remaining in the write FIFO 
 *
 * @return the number of characters remaining
 **/
unsigned alt_up_rs232_get_num_chars_in_write_FIFO();

/**
 * @brief Write data to the RS232 UART core.
 *
 * @param data  -- the character to be transferred to the RS232 UART Core
 * @note User should check the write FIFO is not full before writing a character there, otherwise the character is lost.
 *
 * @return 0 for success or \f$-1\f$ on error
 **/
int alt_up_rs232_write_data(alt_u8 data);

/**
 * @brief Read data from the RS232 UART core.
 *
 * @param data  -- pointer to the memory where the character read from the RS232 UART core should be stored
 *
 * @return 0 for success or \f$-1\f$ on error
 * @note This function will clear the DATA field of the data register after reading and it
 * uses the \c alt_up_rs232_check_parity function to check the parity for the
 * DATA field.
 **/
int alt_up_rs232_read_data(alt_u8 *data);

#endif /*__ALT_UP_RS232_H__*/
