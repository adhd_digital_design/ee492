
# RS232 UART send and recieve text

- Title: RS232 UART Simple lab to send and recieve text , inspired by  Altera UART IP Core - FIFO - Serializer - Deserializer - Counters
- Author: John Loomis
- Date: 18 November 2009
- Type: Firmware, CPP
- Tags: Verilog, CPP
- Source URL: http://www.johnloomis.org/digitallab/rs232/rs232lab.html
- Source Code: http://www.johnloomis.org/digitallab/rs232/rs232lab.zip
