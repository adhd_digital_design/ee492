
# Altera University Program IP Core, RS232 UART

- Title: RS232 UART IP Core - FIFO - Serializer - Deserializer - Counters
- Date: May 2011
- Type: Firmware, Script, C Library
- Tags: Verilog, Perl, C
- Copyright: Copyright (c) 2007 Altera Corporation, San Jose, California, USA.
- Source URL: http://www.johnloomis.org/digitallab/rs232/rs232lab.html
- Source Code: http://www.johnloomis.org/digitallab/rs232/RS232.zip
