module rs232lab(
  input CLOCK_50,	//	50 MHz clock
  input [3:0] KEY,      //	Pushbutton[3:0]
  input [17:0] SW,	//	Toggle Switch[17:0]
  output [6:0]	HEX0,HEX1,HEX2,HEX3,HEX4,HEX5,HEX6,HEX7,  // Seven Segment Digits
  output [8:0] LEDG,  //	LED Green
  output [17:0] LEDR,  //	LED Red
  inout [35:0] GPIO_0,GPIO_1,	//	GPIO Connections
 //	UART
  output UART_TXD, // UART Transmitter
  input  UART_RXD, // UART Receiver
//	LCD Module 16X2
  output LCD_ON,	// LCD Power ON/OFF
  output LCD_BLON,	// LCD Back Light ON/OFF
  output LCD_RW,	// LCD Read/Write Select, 0 = Write, 1 = Read
  output LCD_EN,	// LCD Enable
  output LCD_RS,	// LCD Command/Data Select, 0 = Command, 1 = Data
  inout [7:0] LCD_DATA	// LCD Data bus 8 bits
);

//	All inout port turn to tri-state
assign	GPIO_0		=	36'hzzzzzzzzz;
assign	GPIO_1		=	36'hzzzzzzzzz;

wire [6:0] myclock;
wire RST;
assign RST = KEY[0];
wire reset = ~RST;

// reset delay gives some time for peripherals to initialize
wire DLY_RST;
Reset_Delay r0(	.iCLK(CLOCK_50),.oRESET(DLY_RST) );

// Send switches to red leds 
assign LEDR = SW;

// turn off green leds
//assign LEDG = 9'h000;

//assign LEDG[1] = swap;
assign LEDG[7] = transmitting_data;
assign LEDG[6] = receiving_data;

// turn LCD ON
assign	LCD_ON		=	1'b1;
assign	LCD_BLON	=	1'b1;

reg [7:0] transmit_addr;

romtext mem1(
.address(transmit_addr),
.clock(CLOCK_50),
.q(transmit_data)
);

parameter XMT_LIMIT = 8'h5f; // number of character in packet

// handle transmission
always @(posedge transmit_data_en)
	if (reset)
		transmit_addr <= 8'h00;
	else if (transmit_addr<XMT_LIMIT)
		transmit_addr <= transmit_addr + 1'b1;
	else
		transmit_addr <= 8'h00;

wire [7:0] q;
wire [4:0] addr;
wire [3:0] state;

assign LEDG[3] = data_received;

/*
wire my_receive = ~KEY[1];
assign LEDG[0] = my_receive;
*/

LCD_display_string d(
.clock(CLOCK_50),
.received(data_received), // data_received
.q(received_data),
.adx(adx),
.index(addr),
.out(q)
);


LCD_Display u1(
// Host Side
   .iCLK_50MHZ(CLOCK_50),
   .iRST_N(DLY_RST),
   .char_index(addr),
   .char_value(q),
// LCD Side
   .DATA_BUS(LCD_DATA),
   .LCD_RW(LCD_RW),
   .LCD_E(LCD_EN),
   .LCD_RS(LCD_RS)
);

// blank unused 7-segment digits
wire [6:0] blank = ~7'h00;
//assign HEX0 = blank;
//assign HEX1 = blank;
assign HEX2 = blank;
assign HEX3 = blank;
//assign HEX4 = blank;
//assign HEX5 = blank;
//assign HEX6 = blank;
//assign HEX7 = blank;

/*
reg [7:0] qx;
hex_7seg seg2(qx[3:0],HEX2);
hex_7seg seg3(qx[7:4],HEX3);
always @(posedge data_received)
	qx <= received_data;
*/

wire [4:0] adx;
hex_7seg seg1({3'h0,adx[4]},HEX1);
hex_7seg seg0(adx[3:0],HEX0);

hex_7seg seg4(transmit_data[3:0],HEX4); 
hex_7seg seg5(transmit_data[7:4],HEX5);

hex_7seg seg6(received_data[3:0],HEX6); 
hex_7seg seg7(received_data[7:4],HEX7);



parameter DATA_WIDTH = 8;
wire [DATA_WIDTH-1:0] received_data;
wire [DATA_WIDTH-1:0] transmit_data;
wire transmitting_data;
wire receiving_data, data_received;
wire transmit_data_en;


wire flag;
oneshot trig(flag,~KEY[3],CLOCK_50);

// Count for the oscilloscope
//oneshot(transmit_data_en,clk,CLOCK_50);
assign transmit_data_en = SW[0]? clk: flag;

reg [31:0] count;
reg clk;

always @(posedge CLOCK_50)
begin
	if (count < 50*1000*1000)
	begin
		count <= count + 1'b1;
		clk <= 1'b0;
	end
	else
	begin
		count <= 0;
		clk <= 1'b1;
	end
end



wire UART;

RS232_Out u2(
// Inputs
.clk(CLOCK_50),
.reset(reset),
	
.transmit_data(transmit_data),
.transmit_data_en(transmit_data_en),

// Outputs
.serial_data_out(UART_TXD), // UART_TXD
.transmitting_data(transmitting_data)
);

RS232_In u3(
// Inputs
.clk(CLOCK_50),
.reset(reset),
.serial_data_in(UART_RXD), // UART_RXD
.receive_data_en(1'b1),
// Outputs
.received_data(received_data),
.data_received(data_received),
.receiving_data(receiving_data)
);

endmodule