
# Altera SPI CORE to ADC example communication

- Title: Interface code for the AD7918 ADC fitted to the Nial Stewart Developments Ltd
- Title...: General Purpose Interface Board (GPIB).
- Author: N Gunton
- Org: Dept. Engineering Design & Mathematics, UWE, Bristol
- Date: 2010-01-04
- Type: Software
- Platform: Altera Cyclone III starter board + FreeRTOS
- URL: http://www.cems.uwe.ac.uk/~a2-lenz/n-gunton/worksheets/adc.c
- Tags: Altera, C, SPI, ADC
