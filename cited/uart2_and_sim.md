
# RS232 UART with Testbench

- Title: RS232 UART - and Transaction based testbench
- Author: Deepak Kumar Tala 
- Email: deepak@asic-world.com
- Date: 2014-02-09
- Type: Firmware
- Tags: Verilog, Testbench, SystemVerilog
- Copyright: ©  1998-2014 http://www.asic-world.com
- Source URL: http://www.asic-world.com/examples/systemverilog/uart2.html
- Source Code: http://www.asic-world.com/code/hdl_models/uart.v
- Source Code: http://www.asic-world.com/code/sv_examples/uart_tb.sv
- Source Code: http://www.asic-world.com/code/sv_examples/uart_top.sv
- Source Code: http://www.asic-world.com/code/sv_examples/uart_ports.sv
- Source Code: http://www.asic-world.com/code/sv_examples/uart_sb.sv
- Source Code: http://www.asic-world.com/code/sv_examples/uart_txgen.sv
