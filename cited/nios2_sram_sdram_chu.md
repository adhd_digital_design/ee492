
# Verilog - Off-chip SRAM and SDRAM DE2

- Title:  Off-chip SRAM and SDRAM in Verilog
- Author: Dr. Pong P. Chu
- Email: p.chu@csuohio.no_spam
- Date: 2012
- Type: Firmware
- Tags: Verilog
- Source URL: http://academic.csuohio.edu/chu_p/rtl/sopc_vlog.html
  
