

# FPGA-peripherals

- Title: FPGA-peripherals
- Description: Collection of open-source peripherals in Verilog
- Date: 2016-09-19
- Type: Firmware
- Tags: Verilog, uart
- Authors:
    - Juan González-Gómez (Obijuan)
    - Jesús Arroyo Torrens
- Source URL: https://github.com/FPGAwars/FPGA-peripherals
- Wiki URL: https://github.com/FPGAwars/FPGA-peripherals/wiki

