#!/bin/sh


nios2eds_dir=/c/intelFPGA_lite/16.1/nios2eds/
# nios2eds_dir=/c/altera/13.0sp1/nios2eds/
if ! [ -d  $nios2eds_dir ]
then
    # nios2eds_dir=/c/altera_lite/13.0sp1/nios2eds/
    echo "Unable to location nios2_command_shell.sh"
    exit 1;
else
    ${nios2eds_dir}/nios2_command_shell.sh
fi

