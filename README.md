
# EE 492 S02 – Advanced Digital Hardware Design -- Spring 2017

Objectives: After successfully completing this class, the student will have:
1. Increased his knowledge on implementing FPGA-based digital designs,
2. Learned how to design and implement testbench and monitor programs to
better test digital systems,
3. Learned how to design, modify, and implement SOpC systems designed around a
softcore processor, and
4. Learned how to implement some of the basic “subsystems” from our
microcontroller course. For example, implementing a state-machine based UART
to be included in the system described in 3.
