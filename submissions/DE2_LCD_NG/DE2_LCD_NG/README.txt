
Based on the "DE2_Default" project of "http://www.terasic.com/downloads/cd-rom/de2/".

Revision "DE2_LCD" was created by Nathan Genetzky for EE492 at SDSTATE.

Two example usages of LCD_Controller are found in "./firmware/lcd/*". One
allows static text to be set at startup from the top module (DE2_LCD).
