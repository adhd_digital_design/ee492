nios2_pio_onchip n2_pio (
    .clk_clk(clk[0]),      // clk.clk
    .pi_export(in[9:0]), // pi.export [31:0]
    .po_export(out[9:0])  // po.export [31:0]
    );
