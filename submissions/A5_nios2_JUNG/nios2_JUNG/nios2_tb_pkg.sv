
`ifndef default_tb_pkg_
`define default_tb_pkg_

// Testbench Source Files
`include "src_tb/top.sv"
`include "src_tb/default_driver.sv"
`include "src_tb/default_monitor.sv"
`include "src_tb/default_sequencer.sv"
`include "src_tb/default_scoreboard.sv"

`endif