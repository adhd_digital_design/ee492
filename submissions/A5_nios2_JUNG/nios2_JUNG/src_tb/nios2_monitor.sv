//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: default
//Date Created : 2017-02-25

`ifndef nios2_monitor_
`define nios2_monitor_

// Timescale
`timescale 1ns/10ps

class nios2_monitor #(
    parameter MY_PARAM = 0
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

localparam UPPERCASE_LOCAL = 0;

//****************************************************************************
//                 Internal Member Declarations
//****************************************************************************

// wire internal_wire;
// wire [N -1:0] internal_wires;
// reg internal_reg;
// reg [N -1:0] internal_regs;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************

// always @(posedge clk) begin : fsm_state_name
//     case(state)
//         PREFIX_LOCALPARAM:
//         default:
//     endcase
// end

//****************************************************************************
//                             Public Functions
//****************************************************************************

//****************************************************************************
//                             Private Functions
//****************************************************************************

endclass

`endif