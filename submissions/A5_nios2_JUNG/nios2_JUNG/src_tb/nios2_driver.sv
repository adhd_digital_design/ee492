// driver.sv
//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Drive the firmware Device Under Test (DUT)
//Date Created : 2017-02-25

`ifndef nios2_driver_
`define nios2_driver_

// Timescale
`timescale 1ns/10ps

module nios2_driver #(
    parameter MY_PARAM = 0
) (
    input wire clk
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

localparam UPPERCASE_LOCAL = 0;

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

// wire internal_wire;
// wire [N -1:0] internal_wires;
// reg internal_reg;
// reg [N -1:0] internal_regs;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************

// always @(posedge clk) begin : fsm_state_name
//     case(state)
//         PREFIX_LOCALPARAM:
//         default:
//     endcase
// end

//****************************************************************************
//                             Procedural Blocks
//****************************************************************************

    initial begin
        $display("Driver Initial");
        $system("date +%s%N");
    end

    // task my_task();
    //     # 1 var <= val;
    // endtask : my_task

    final begin
        $display("Driver Final");
        $system("date +%s%N");
    end


//****************************************************************************
//                              Internal Modules
//****************************************************************************

// my_module #(
//     .PARAM(0)
// ) instance_name (
//     .ports()
// );

endmodule

`endif