`ifndef top_nios2_
`define top_nios2_

module top_nios2 #(
    parameter NCLK=4,
    parameter NIN=17+4,
    parameter NOUT=18+8,
    parameter NSEGS=8
) (
    input [NCLK-1:0] clk,
    input [NIN-1:0] in,
    output wire [NOUT-1:0] out,
    output wire [(NSEGS*7)-1:0] hex_L
);

// assign out = in;
assign out[10] = out[0]; // GPIO to measure timing

// nios2_onchip_jtaguart n2_jtag (
//     .clk_clk(clk[0]) // clk.clk
//     );

wire [31:0] unused_out ;

nios2_pio_onchip n2_pio (
    .clk_clk(clk[0]),      // clk.clk
    .pi_export({{31-9 - 1 {1'b0}},  in[9:0] }), // pi.export [31:0]
    .po_export({unused_out[31-9:0], out[9:0]})  // po.export [31:0] // float unused output
    );

endmodule

// top_nios2 top #(
//     .NCLK(),
//     .NIN(),
//     .NOUT(),
//     .NSEGS()
// ) (
// (
//     .clk(KEY[3:0]),
//     .in(SW[17:0]),
//     .out({LEDR[17:0], LEDG[7:0]}),
//     .hex_L({HEX7, HEX6, HEX5, HEX4, HEX3, HEX2, HEX1, HEX0})
// );

`endif
