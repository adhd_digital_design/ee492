
// TODO: This module is intended to serve as a template for other modules. The
// description is consise and MUST be updated. Is wrapped at ~80 chars.
`ifndef nios2_
`define nios2_

module nios2 #(
    parameter UPPERCASE_PARAM = 0
) (
    input wire clk,
    input wire reset,
    // Data:
    // Status:
    output reg done,
    // Control:
    // Passthrough signals:
    output reg [7:0] HEX0
    // Debug Only:
    , output wire [2-1:0] debug_w
);

//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

localparam UPPERCASE_LOCAL = 0;

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

// wire internal_wire;
// wire [N -1:0] internal_wires;
// reg internal_reg;
// reg [N -1:0] internal_regs;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************

// always @(posedge clk) begin : fsm_state_name
//     case(state)
//         PREFIX_LOCALPARAM:
//         default:
//     endcase
// end

//****************************************************************************
//                             Sequential logic
//****************************************************************************

// always @(posedge clk) begin
//     if(reset) begin // Synchronous Reset
//         internal_reg  = 0;
//         internal_regs = 0;
//     end

//****************************************************************************
//                            Combinational logic
//****************************************************************************

// assign internal_wire = 1'b0;
// always @(*) begin : comb_signal_name
// end

//****************************************************************************
//                              Internal Modules
//****************************************************************************

// my_module #(
//     .PARAM(0)
// ) instance_name (
//     .ports()
// );

endmodule


`endif
