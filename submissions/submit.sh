#!/usr/bin/env bash

set -e # Exit on first error.

postfix="_JUNG"
dest=${1?"First argument is destination directory (required)."}
directory=${2?"Second argument is source directory (required)."}

## Helper functions.
# Define a variable using a heredoc. "define VAR << 'HEREDOC' text... 'HEREDOC'"
define(){ IFS='\n' read -r -d '' ${1} || true; }

function error_exit(){
    echo "$1" >&2   ## Send message to stderr. Exclude >&2 if you don't want it that way.
    exit "${2:-1}"  ## Return a code specified by $2 or 1 by default.
}

## Help
define HELP << 'EOF'
Acts on: Projects[A], source files[B], and output files[C].
Copies to new directory, $dest. Appends $postfix.
$directory is assumed to be [A].
Will find all [B] and [C] inside this directory.
EOF
echo $HELP


## Main

# Check dest exists, otherwise create.
[ -d $dest ] || mkdir $dest

# Check directory exists.
[ -d $directory ] || error_exit "directory($directory) is not a valid directory."

mkdir -p $dest/src
mkdir -p $dest/out
for src in $(find $directory); do
    fbase=$(basename $src)
    fname=${fbase%.*}
    fext=${fbase##*.} # Assumes ext is after the LAST period.
    fnew="$fname$postfix.$fext"

    # handle the files in the directory.
    case "$fbase" in

        ## IGNORED
        _primary_*.vhd)
            ;; # Ignore

        ## ./src/       [B]
        *.v |*.vhd|*.sv)
            # Include individual verilog or VHDL files (as many as needed), any output files generated.
            cp $src "$dest/src/$fnew"
            ;;

        ## ./out/       [C]
        *.transcript)
            # Include any output files generated.
            cp $src "$dest/out/$fnew"
            ;;
        *.wlf)
            # Include any output files generated.
            cp $src "$dest/out/$fnew"
            ;;

        ## IGNORED
        *   )
            echo "INFO: Ignored $src"
            ;;
    esac
done

# Include the complete project.     [A]
cp $directory "$dest/$(basename $directory)$postfix" -r

# Zip up the result (in $dest).
zip -r $(basename $dest) $dest
