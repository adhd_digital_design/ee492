#! /usr/bin/env bash
# Expects to be in root of Modelsim Project.

# Link firmware in ./src
cd ./src

################################################################################
# Cheatsheet on ln
################################################################################
# ln {target_file} {symbolic_name} # symbolic_name should be "./linkname"
# ln -s {source} {destination} # Symbolic link
# symbolic links: Refer to a symbolic path indicating the abstract location of another file
# hard links : Refer to the specific location of physical data.

################################################################################
# Testbench Firmware
################################################################################

# ln -s ./a1_top_tb.sv
# ln -s ./array_multiplier_tb.sv
# ln -s ./uint_multiplier_monitor.sv
# ln -s ./uint_multiplier_tb.sv
ln  -s  ../../../firmware/A2/linear_feedback_sr.v  ./linear_feedback_sr.v

################################################################################
# DUT Firmware
################################################################################

# DUT Firmware
ln  -s  ../../../firmware/A1/array_multiplier_sync.v  ./array_multiplier_sync.v -f
ln  -s  ../../../firmware/A1/multiply_row.v         ./multiply_row.v -f
ln  -s  ../../../firmware/A1/multiply_cell.v        ./multiply_cell.v -f

# DUT Firmware
ln  -s  ../../../firmware/A1/sequential_multiplier.v  ./sequential_multiplier.v -f
ln  -s  ../../../firmware/A1/seq_mult_control.v       ./seq_mult_control.v -f

# Common Firmware required for DUT.
ln  -s  ../../../firmware/common/full_adder_n.v     ./full_adder_n.v -f
ln  -s  ../../../firmware/common/shift_reg_n.v  ./shift_reg_n.v -f

################################################################################
# Helpful commands/information
################################################################################

# Format:
#+echo "this is some bash command"
# the output.
# until the next empty line.

#+cd src/ && find ../../../../firmware/A1/
# ../../../../firmware/A1/
# ../../../../firmware/A1/array_multiplier.v
# ../../../../firmware/A1/seq_mult_control.v
# ../../../../firmware/A1/multiply_cell.v
# ../../../../firmware/A1/sequential_multiplier.v
# ../../../../firmware/A1/ee492_a1.v
# ../../../../firmware/A1/multiply_row.v
# ../../../../firmware/A1/array_multiplier_sync.v

#+cd src/ && find ../../../../firmware/common/
# ../../../../firmware/common/
# ../../../../firmware/common/reg_n.v
# ../../../../firmware/common/full_adder_n.v
# ../../../../firmware/common/shift_reg_n.v
# ../../../../firmware/common/debounce.v
# ../../../../firmware/common/HexOn7Seg.v
# ../../../../firmware/common/clk_divider.v
# ../../../../firmware/common/display_on_hex.v

