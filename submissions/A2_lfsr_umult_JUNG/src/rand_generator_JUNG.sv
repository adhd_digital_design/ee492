
//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Drive unsigned multiplier firmware
//Date Created : 2017-01-23

// Timescale
`timescale 1ns/10ps

module rand_generator #(
    parameter SEED = 8
) ();
    reg load;
    reg clk;
    reg [7:0] seed;
    wire [7:0] random;

initial begin
    load = 0;
    clk = 0;
    seed = 0;

    load_seed(SEED);
end

task load_seed( input integer unsigned  new_seed );
    seed = new_seed;
    load = 1;
    #1 load = 0;
endtask

task next();
    clk = 1;
    #1 clk = 0;
endtask

linear_feedback_sr #(
    .ID(0),
    .STAGES(8)
) sr_q (
    .clk(clk),
    .load(load),
    .value_i(seed),
    .value_o(random)
);

endmodule
