//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Observe output of unsigned multiplier.
//Date Created : 2017-01-20

module uint_multiplier_monitor #(
    parameter OPPERAND_WIDTH = 8,
    parameter P_WIDTH = 2*OPPERAND_WIDTH, // should be localparam.
    parameter NUM_TESTS_TO_DO = 1000
) (
    input wire [OPPERAND_WIDTH-1:0] m,  // Multiplicand
    input wire [OPPERAND_WIDTH-1:0] q,  // Multiplier
    input wire [P_WIDTH-1:0] p          // Product
);
    // const int NUM_INPUT_COMBINATIONS_POSSIBLE = factorial(OPPERAND_WIDTH**2);
    // const int PERCENT_OF_MAX_MULTIPLIES_TO_PERFORM = 1;
    // const int NUM_TESTS_TO_DO = NUM_INPUT_COMBINATIONS_POSSIBLE*(PERCENT_OF_MAX_MULTIPLIES_TO_PERFORM/100);

    reg [P_WIDTH-1:0] expected = 0;         // Product
    real counter = 0;
    int num_pass = 0;// First Itteration is all zeros and x's
    string pass_fail_string = "";


task header();
    //Hex
    // $display ("%10s  %8s  %8s  %8s  %8s %12s",
    //     "time", "m", "q", "p", "expected", "[Pass/Fail]");

    //Binary
        // $display ("[%8s]  %10s  %8s  %8s  %16s  %16s  %16s  %16s",
        // "Pass/Fail","time", "m", "q", "p", "expected", "xor(p,e)", "m[msb]==1");
endtask

task log();
    
    scoreboard();

    //Hex
    $display ("[%10g]  [Assert: 0x%h*0x%h == 0x%h]  [%s] - (%%%d) Complete - [%d] Tests Remaining]",
        $time, m, q, expected, pass_fail_string, (counter/NUM_TESTS_TO_DO)*100 ,NUM_TESTS_TO_DO-counter );

    

    //Binary
    // $display ("[%10b]  %10g  %8b  %8b  %16b  %16b  %16b  %16b",
    //     p==expected, $time, m, q, p, expected, p^expected, m&8'b10000000);
endtask


task scoreboard();

        // Update expected result
        expected[P_WIDTH-1:0] = m * q;

        // Does the actual = the expected
        if(counter>0) begin // First Itteration is all zeros and x's

            // Did the current test pass ?
            if(p==expected) begin
                pass_fail_string = $sformatf("Success [%d/%d] Multiplies Passed",num_pass,counter);
                // Record the number of multiplies performed
                num_pass = num_pass + 1;
            end
            else begin 
                pass_fail_string = $sformatf("***FAIL*** [Actual: 0x%h*0x%h == 0x%h]",m,q,p);
                $error("***FAIL*** [%d/%d] Multiplies Failed",counter-num_pass,counter);
                // $finish;
            end

            // Are we done yet ?
            if(counter>=NUM_TESTS_TO_DO) begin
                $display("Completed [%d/%d] Tests",counter,NUM_TESTS_TO_DO);

                // Final Report
                if(num_pass!=counter-1) begin
                    $display("Success [%d/%d] Multiplies Passed",num_pass,counter);
                end
                else begin 
                    $error("***FAIL*** [%d/%d] Multiplies Failed",counter-num_pass,counter);
                end
                        
                $finish;
            end
        end
        else begin 
            // TODO: Fix initial invalid condition
            pass_fail_string = $sformatf("INVALID INITIAL");
        end

        // Record the number of multiplies performed
        counter = counter + 1;


endtask : scoreboard

// function automatic int factorial(input int n);
//    if (n > 1)
//      factorial = factorial (n - 1) * n;
//    else
//      factorial = 1;
// endfunction


endmodule
