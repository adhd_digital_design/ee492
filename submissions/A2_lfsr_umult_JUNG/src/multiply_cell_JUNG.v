//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Array Multiplier Cell
//Date Created : 2017-01-16

module multiply_cell (
    input  wire m_i,
    input  wire q_i,

    input  wire c_i,
    output wire c_o,

    input  wire pp_i,
    output wire pp_o
);

wire mq_w;
assign {mq_w} = m_i & q_i;

full_adder_n #(
    .n(1)
) fa (
    .x(pp_i),
    .y(mq_w),
    .cin(c_i),
    .cout(c_o),
    .A(pp_o)
);

endmodule

// multiply_cell mc (
//     .m_i(),
//     .q_i(),
//     .c_i(),
//     .c_o(),
//     .pp_i(),
//     .pp_o(),
// );
