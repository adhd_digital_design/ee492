//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Generate test vectors for DUT.
//Date Created : 2017-01-19

// Timescale
`timescale 1ns/10ps

// TODO: Convert to UVM sequencer - http://www.verificationguide.com/p/uvm-sequence.html
// class lfsr_sequencer extends uvm_sequence#(mem_seq_item);
class lfsr_sequencer;
localparam NSPECIAL = 3;

typedef struct {
  integer unsigned seed;
  integer unsigned tapp;
} LfsrInputs;

    LfsrInputs  special[0:NSPECIAL-1]  =
    '{
        // '{  'h0    }, // Do NOT want all zeros for XNOR
        // '{  'h00   ,  0   }, // Do NOT want all zeros for XNOR
        // '{  'hFF   ,  0   }, // Do NOT want all ones for XOR
        '{  'hAA   ,  0   },
        '{  'hCA   ,  0   },
        '{  'hBA   ,  0   }
    };

    // Test vectors are designed for 8 bit Linear Feedback Shift Register
    function integer unsigned get_seed(input integer unsigned i);
        if(i<NSPECIAL) return special[i].seed;
        else return $urandom_range(2**8-1,1); // Random unsigned integer.
    endfunction

    function integer unsigned get_tapp(input integer unsigned i);
        if(i<NSPECIAL) return special[i].tapp;
        else return $urandom_range(2**8-1,1); // Random unsigned integer.
    endfunction

endclass
