//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Drive unsigned multiplier firmware
//Date Created : 2017-01-23

// Timescale
`timescale 1ms/1ns

module lfsr_driver #(
    parameter ID = 0
) (
    output wire clk_o
);
localparam STAGES = 8;
    reg [STAGES-1:0] seed;
    reg load;
    bit clk;
    wire [STAGES-1:0] random;

// Assign
assign clk_o = clk; // Output Clock

initial begin
    // seed = 0; // WARNING: Seed of all zeros will lock up XOR LFSR
    // seed = 'hFF; // WARNING: Seed of All Ones will lock up XNOR LFSR
    set_seed('hAA);
    set_load(1'b0);
    set_clk(1'b0);
end

task load_seed( input integer unsigned  new_seed );
    set_seed(new_seed);
    set_load(1'b1); 
    set_clk(1'b0); // ensure rising edge
 #1 toggle_clk();   // rising edge to perform load
    set_load(1'b0); // unset load 
endtask

task toggle_clk( );
       clk = ~clk; // Invert clock 
endtask

// Set Tasks

task set_clk( input bit clk_i );
       clk = clk_i; // Set 
endtask

task set_load( input reg load_i );
       load = load_i; // Set 
endtask

task set_seed( input reg [STAGES-1:0] seed_i );
       seed = seed_i; // Set 
endtask

// Get Tasks

function bit get_clk( );
       return clk; // Get 
endfunction

function reg get_load( );
       return load; // Get 
endfunction

function reg get_seed( );
       return seed; // Get 
endfunction

linear_feedback_sr #(
    .ID(ID),
    .STAGES(8)
) sr_q (
    .clk(clk),
    .load(load),
    .value_i(seed),
    .value_o(random)
);

endmodule
