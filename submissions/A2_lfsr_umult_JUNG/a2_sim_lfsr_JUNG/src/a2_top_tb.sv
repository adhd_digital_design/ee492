// a1_top_tb.sv
//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: // Top level of the test.
//Date Created : 2017-01-23

//UVM
`include "uvm_macros.svh"
`include "uvm_pkg.sv"

// TB Imports
`include "src/lfsr_sequencer.sv"
`include "src/lfsr_monitor.sv"

// Timescale
`timescale 1ms/1ns

module a2_top_tb();

    // Declare signals in the module.
    wire clk_o;
    integer unsigned counter;

    // driver
    lfsr_driver #( .ID(0) ) driver ( .clk_o(clk_o) );

    // Monitor
    lfsr_monitor monitor = new ;

    // Sequencer
    lfsr_sequencer sequencer = new ;

    // Clock generator
    always begin // Empty sensitivity list.
        #1 driver.toggle_clk(); // Invert clock every X time periods.
    end

    initial begin
        counter=0;
        driver.set_clk(1'b0); //clk_o=0;

        driver.load_seed(sequencer.get_seed(0));
        // $display ("%10s  %8s  %8s  %8s",
        //     "time", "counter", "random", "seed");
    end

    always @(posedge clk_o) begin
        // Log the previous values.
        if(0 != counter) begin
        end

        // $display ("%10g  %8h  %8h  %8h",
        //     $time, counter, driver.random, driver.seed);

        monitor.run_phase(driver.random);

        counter = counter +1; // Increment the counter once per start.
    end

    final begin

    end

endmodule
