//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose:
//  Synchronous Unsigned Multiply - Using an array of combinatinal logic
//  Calculates the product in a single clock cycle.
//Date Created : 2017-01-09

module array_multiplier_sync #(
    parameter opperand_width = 8 // Assume we multiply two 8-Bit #s...
)(
    input wire clk,                             // Latch Product on clk
    input wire start,                           // Start the multiplication. Latch input on start.
    input wire [opperand_width-1:0] q,          // Multiplier
    input wire [opperand_width-1:0] m,          // Multiplicand
    output reg  [(2*opperand_width)-1:0] p      // Product
);
localparam P_WIDTH = 2*opperand_width;     // Product Width
localparam M_ROW_WIDTH = opperand_width;   // Number of cells in a row for the array multiply
localparam Q_COL_WIDTH = opperand_width;   // Number of rows in a column for the array multiply

// Wires and Regs
reg [opperand_width-1:0] q_reg;
reg [opperand_width-1:0] m_reg;

// mr - Wires
wire [Q_COL_WIDTH-1:0] c_o  ; // Carry Out
wire [Q_COL_WIDTH*M_ROW_WIDTH-1:0] pp_o ; // Partial Product out
wire [opperand_width-1:0] pp_o_first_bit_of_each_row;

// Final Output wires
wire [P_WIDTH-1:0] concat_pp_o;

// We will latch the input at start.
always @(posedge start) begin : sync_load_opperands
    q_reg = q;
    m_reg = m;
end


///////////////////////////////////////////////////////////////////////////
// Multiply Row Instantiations for m_row_width-1:0
///////////////////////////////////////////////////////////////////////////

// begin : mr[m_row_width-1:0]
// mr - Module // First Row
multiply_row #(
    .m_row_width(M_ROW_WIDTH)
) mr_first (
    .m(m_reg),       //  Multiplicand
    .q_i(q_reg[0]),  //  Multiplier(bit)
    .c_i(1'b0),      //  Carry In
    .c_o(c_o[0]),    //  Carry Out
    .pp_i(8'b0),     // Partial Product In
    .pp_o(pp_o[(0) + (M_ROW_WIDTH-1): (0)]) // Partial Product Out
);

// mr - Module // Middle Rows

generate
  genvar irow;
    for(irow=1; irow<(Q_COL_WIDTH); irow=irow+1) begin: for_rows_1_to_M_ROW_WIDTH

    multiply_row #(
      .m_row_width(M_ROW_WIDTH)
    ) mr_middle (
      .m(m),              // Multiplicand
      .q_i(q_reg[irow]),  // Multiplier (bit)
      .c_i(1'b0),         // Carry In
      .c_o(c_o[irow]),    // Carry Out
      .pp_i({
        c_o[irow-1], // MSB of PPI is Carry Out of previous row.
        pp_o[ ((irow-1)*M_ROW_WIDTH) + (M_ROW_WIDTH-1):
              ((irow-1)*M_ROW_WIDTH) +1] // +1 because LSB of PPO is for final Partial Product.
        }), // Partial Product In
      .pp_o(pp_o[ (irow*M_ROW_WIDTH) + (M_ROW_WIDTH-1):
                  (irow*M_ROW_WIDTH) ]) // Partial Product Out
    );

    end
endgenerate
// end : mr[m_row_width-1:0] 

///////////////////////////////////////////////////////////////////////////
// Last Row Assginments
///////////////////////////////////////////////////////////////////////////
// Assign first bit of pp_o for each row to a single wire
generate
  //genvar irow; // Already declared in scope
    for(irow=0; irow<(Q_COL_WIDTH); irow=irow+1) begin: for_rows_0_to_M_ROW_WIDTH
        assign pp_o_first_bit_of_each_row[irow] = pp_o[irow*M_ROW_WIDTH];
    end
endgenerate

assign concat_pp_o[P_WIDTH-1:0] = {
    c_o[Q_COL_WIDTH-1], // MSB is from c_o of last row.
    pp_o[ ((Q_COL_WIDTH-1)*M_ROW_WIDTH) +(M_ROW_WIDTH-1): // pp_o[P_WIDTH-1:P_WIDTH/2-1] = pp_o of last row.
          ((Q_COL_WIDTH-1)*M_ROW_WIDTH) +1 ], // +1 because last row is included in pp_o_first_bit_of_each_row
    pp_o_first_bit_of_each_row[Q_COL_WIDTH-1:0]
    }; // Partial Product out

// Latch the answer on clk.
always@(posedge clk) begin : sync_output_product
    p[P_WIDTH-1:0] <= concat_pp_o[P_WIDTH-1:0] ; // Latch Product on clk
end

endmodule

// Example:

// array multiplier sync calculates the product using combinational logic and clocks in the inputs and outputs
// array_multiplier_sync #(
//     .opperand_width(8)
// ) ams (
//     .clk(clk),                  // Latch Product on clk
//     .start(start),              // Start the multiplication.
//     .q(q[opperand_width-1:0]),  // Multiplier
//     .m(m[opperand_width-1:0]),  // Multiplicand
//     .p(p[2*opperand_width-1:0]) // Product
// );


// Timing Analysis
// Performed 2017-01-23 on commit (f399fb2).

//  FMAX          |   opperand_width
//  --------------------------------
//  NA            |   1
//  152.39 MHz    |   2
//  150.4 MHz     |   3
//  119.06 MHz    |   4
//  76.44 MHz     |   5
//  91.7 MHz      |   6
//  80.02 MHz     |   7
//  64.0  MHz     |   8
//  57.54 MHz     |   9
//  57.03 MHz     |   10
