//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Sequentially multiply two opperands of (opperand_width)
//         Sequential multiplier needs multiple clock cycles
//Date Created : 2017-01-16

module sequential_multiplier  #(
    parameter opperand_width = 8 // Assume operands are 8-Bit unsigned values.
)(
    input wire clk,                         // Latch Product on clk
    input wire start,                       // Start the multiplication. Latch input on start.
    input wire [opperand_width-1:0] q,      // Multiplier
    input wire [opperand_width-1:0] m,      // Multiplicand
    output wire [(2*opperand_width)-1:0] p  // Product

    // DEBUG ONLY
    , output wire seq_mult_done
);

// Wires and Regs
// Control Signals
wire shift_en;
wire load_lsb;
wire load_msb;
wire reset_msb;
wire add_en;

// Data Signals
wire fa_cout;
wire carry_bit;// Unused output from sr_a
wire srA_to_srQ;
wire [opperand_width-1:0] m_or_zero;
wire [opperand_width-1:0] fa_ans;
wire [opperand_width-1:0] sr_a_out;
wire [opperand_width-1:0] sr_q_out;

// Input Signals
reg [opperand_width-1:0] q_reg;
reg [opperand_width-1:0] m_reg;

// We will latch the input at start.
always @(posedge start) begin : sync_load_opperands
    q_reg = q;
    m_reg = m;
end

// Mux for fa input from m.
assign m_or_zero[opperand_width-1:0] = (add_en ? m_reg[opperand_width-1:0] : 0);
assign p = {sr_a_out, sr_q_out};

seq_mult_control #(
    .opperand_width(opperand_width)
)  cntrl (
    .clk(clk),
    .start(start),
    .prod_lsb(p[0]),

    .shift_en(shift_en),
    .load_lsb(load_lsb),
    .load_msb(load_msb),
    .reset_msb(reset_msb),
    .add_en(add_en)

    // DEBUG ONLY
    , .seq_mult_done(seq_mult_done)
);

shift_reg_n #(
    .n(opperand_width+1) // need to account for carry bit
) sr_a (
    .clk(clk),
    .load(load_msb),
    .reset(reset_msb),
    .value_i({fa_cout,fa_ans}),
    .shift(shift_en),
    .s_i(1'b0),             // shift in constant zero
    .s_o(srA_to_srQ),
    .value_o({carry_bit,sr_a_out}) // MS half of product is from reg A.
);

shift_reg_n #(
    .n(opperand_width)
) sr_q (
    .clk(clk),
    .load(load_lsb), // Load with Q at Start.
    .reset(1'b0),
    .value_i(q_reg),
    .shift(shift_en),
    .s_i(srA_to_srQ),
    .s_o(), // Throw away shift out of multiplier
    .value_o(sr_q_out) // LS half of product is from reg Q.
);

full_adder_n #(
    .n(opperand_width)
) fa (
    .x(sr_a_out),
    .y(m_or_zero),
    .cin(1'b0),
    .cout(fa_cout),
    .A(fa_ans)
);

endmodule

// sequential multiplier needs multiple clock cycles
// sequential_multiplier #(
//     .opperand_width(opperand_width)
// ) sm (
//     .clk(clk),
//     .start(start),
//     .q(q), // multiplier
//     .m(m), // multiplicand
//     .p(p), // product
// );


// Timing Analysis
// Performed 2017-01-23 on commit (f399fb2).

//  FMAX          |   opperand_width
//  --------------------------------
//  NA            |   1
//  151.84 MHz    |   2
//  143.97 MHz    |   3
//  143.72 MHz    |   4
//  144.26 MHz    |   5
//  143.51 MHz    |   6
//  143.97 MHz    |   7
//  143.51 MHz    |   8
//  143.35 MHz    |   9
//  144.03 MHz    |   10

