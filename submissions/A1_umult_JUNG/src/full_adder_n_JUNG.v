// Authors : Jordan D. Ulmer , Nathan Genetzky
// Purpose: N-Bit adder
// Date Created : 2017-01-16
// Reference: http://www.referencedesigner.com/tutorials/verilog/verilog_14.php

module full_adder_n #(
    parameter n=1 // Assume we add two 1-bit numbers.
)(
    input wire [n-1:0] x,
    input wire [n-1:0] y,
    input wire cin,

    output wire [n-1:0] A,
    output wire cout
);
assign {cout,A[n-1:0]} =  cin + y[n-1:0] + x[n-1:0];
endmodule

// full_adder_n #(
//     .n(1)
// ) fa (
//     .x(pp_i),
//     .y(mq_w),
//     .cin(c_i),
//     .cout(c_o),
//     .A(pp_o)
// );
