#Author: Jordan Ulmer
#Date Created: 2017-01-14
#Purpose: Add signals to waveviewer window ...

# s_mult
add wave -position end  sim:/a1_top_tb/mult_tb_n/s_mult/cntrl/i
add wave -position end  sim:/a1_top_tb/mult_tb_n/clk
add wave -position end  sim:/a1_top_tb/mult_tb_n/start
add wave -position end  sim:/a1_top_tb/mult_tb_n/m
add wave -position end  sim:/a1_top_tb/mult_tb_n/q
add wave -position end  sim:/a1_top_tb/mult_tb_n/p
add wave -position insertpoint sim:/a1_top_tb/mult_tb_n/s_mult/sr_a/*
add wave -position insertpoint sim:/a1_top_tb/mult_tb_n/s_mult/sr_q/*

# a_mult
#add wave -position end  sim:/a1_top_tb/mult_tb_n/clk
#add wave -position end  sim:/a1_top_tb/mult_tb_n/start
#add wave -position end  sim:/a1_top_tb/mult_tb_n/m
#add wave -position end  sim:/a1_top_tb/mult_tb_n/q
#add wave -position end  sim:/a1_top_tb/mult_tb_n/p