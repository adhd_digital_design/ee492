#### PROJECT SETTINGS ####

# The name of the library folder to be created
LIB_PREFIX = build
LIB_NAME = local
LIB_PATH = $(LIB_PREFIX)/$(LIB_NAME)
# Modelsim compiler used
# vlog , vcom
# CPP Compiler used
CXX = g++-5
CXX_PREFIX = /usr/bin/
# Extension of source files used in the project
# SRC_EXT = sv , v , vhd
# Path to the source directory, relative to the makefile
SRC_PATH = ./src
# Add additional include paths
# Specify multiple +incdir options as well as multiple directories separated by "+" in a single +incdir option
# usage +incdir+$(INCLUDES) 
# HipeC requires c++-11 , (ie. g++-5)
HIPE_CPP_LIB = ../../hipe_cpp/HipeC
HIPE_SV_LIB  = ../../hipe_systemverilog/build/hipe
# Need to point modelsim to g++-5.
# https://www.altera.com/support/support-resources/knowledge-base/solutions/rd08262013_741.html
LIBSTDC_PATH = /usr/lib/i386-linux-gnu/libstdc++
BOOST_PATH = /opt/boost/shared
UVM_1_2_DIR=/home/ubuntu/altera/16.1/modelsim_ase/verilog_src/uvm-1.2/src
UVM_1_1c_DIR=/home/caeuser/altera/13.0sp1/modelsim_ase/verilog_src/uvm-1.1c/src
INCLUDES = +incdir+$(UVM_1_2_DIR) +incdir+$(UVM_1_1c_DIR)
# General compiler flags
COMPILE_FLAGS = 
SV_COMPILE_FLAGS  = $(COMPILE_FLAGS) -sv12compat -sv 
V_COMPILE_FLAGS   = $(COMPILE_FLAGS) -93
VHD_COMPILE_FLAGS = $(COMPILE_FLAGS) -93

# Modelsim Simulation flags
# SHELL_SIM_FLAGS = -do "run 1000ms; quit -f"
SHELL_SIM_FLAGS = -do "run -all"
GUI_SIM_FLAGS = 
SIM_FLAGS = 
# SIM_FLAGS = -do "run 1000ms; quit -f"

HIPE_FLAGS = -L $(HIPE_SV_LIB) -sv_lib $(HIPE_CPP_LIB)
DEBUG_FLAGS = -L $(HIPE_SV_LIB) -sv_lib $(HIPE_CPP_LIB) -sv_lib $(LIBSTDC_PATH) -dpicpppath $(CXX_PATH) -dpicppinstall  $(CXX)

# Set top of Simulation.
# V_TOP_MODULE = array_multiplier_tb
V_TOP_MODULE = a1_top_tb

HIPE_TOP_MODULE = top 
# Install path (bin/ is appended automatically)
INSTALL_PREFIX = usr/local

#### END PROJECT SETTINGS ####

# Generally should not need to edit below this line


# Function used to check variables. Use on the command line:
# make print-VARNAME
# Useful for debugging and adding features
print-%: ; @echo $*=$($*)

# Shell used in this makefile
# bash is used for 'echo -en'
SHELL = /usr/bin/env bash

# Verbose option, to output compile and link commands
export V := false
export CMD_PREFIX := @
ifeq ($(V),true)
	CMD_PREFIX :=
endif


#### Common Targets ####

all:				clean build
	
build:              work sv v

clean:
	#Remove previous "$(LIB_PATH)" 
	@if [ -e $(LIB_PATH) ] ; then vdel -lib $(LIB_PATH) -all ; else :  ; fi

#### Modelsim Targets ####

work:
	# Make the physical "$(LIB_PATH)" directory/library.
	@if  [ -e $(LIB_PREFIX) ] ; then : ; else mkdir $(LIB_PREFIX) ; fi
	@if  [ -e $(LIB_PATH) ] ; then : ; else vlib $(LIB_PATH) ; fi

sv:	
	@# Compile the source files (SystemVerilog, Cpp) into the specified
	@# working directory.
	@# -sv # compile SystemVerilog files.
	@# -work $(LIB_PATH) # compile into the "$(LIB_PATH)" directory.
	# Compile the (SystemVerilog) code.
	$(CMD_PREFIX)vlog  $(SV_COMPILE_FLAGS) -sv -work $(LIB_PATH)	\
	           	$(SRC_PATH)/*.sv $(INCLUDES) ; \
	echo "" ; \
	echo "    *** $(LIB_NAME) SystemVerlog firmware compiled ok. ***" ; \
	echo "" ;

v:
	@# Compile the source files (Verilog) into the specified
	@# working directory.
	@# -work $(LIB_PATH) # compile into the "$(LIB_PATH)" directory.
	# Compile the (Verilog) code.
	$(CMD_PREFIX)vlog  $(V_COMPILE_FLAGS) -work $(LIB_PATH)	\
            $(SRC_PATH)/*.v ; \
	echo "" ; \
	echo "    *** $(LIB_NAME) Verlog firmware compiled ok. ***" ; \
	echo "" ; \


vhd:
	@# Compile the source files (VHDL) into the specified
	@# working directory.
	@# -work $(LIB_PATH) # compile into the "$(LIB_PATH)" directory.
	# Compile the (VHDL) code.
	$(CMD_PREFIX)vlog  $(VHD_COMPILE_FLAGS) -work $(LIB_PATH)	\
	         $(SRC_PATH)/*.vhd ;
	echo "" ;
	echo "    *** $(LIB_NAME) VHDL firmware compiled ok. ***" ;
	echo "" ;

gui:              
	vsim $(SIM_FLAGS) $(GUI_SIM_FLAGS) $(LIB_PATH).$(V_TOP_MODULE)

hipe_gui:              
	vsim $(HIPE_FLAGS) $(LIB_PATH).$(HIPE_TOP_MODULE)

# -c # (optional) Specifies that the simulator is to be run in command-line
# mode. Refer to “Modes of Operation” for more information. 
sim:
	vsim -c $(SIM_FLAGS) $(SHELL_SIM_FLAGS) $(LIB_PATH).$(V_TOP_MODULE)

hipe_sim:
	vsim -c $(HIPE_FLAGS) $(SIM_FLAGS) $(SHELL_SIM_FLAGS) $(LIB_PATH).$(HIPE_TOP_MODULE)

debug_sim:
	vsim -c $(DEBUG_FLAGS) $(SIM_FLAGS) $(SHELL_SIM_FLAGS)  $(LIB_PATH).(V_TOP_MODULE)

debug_hipe_sim:
	vsim -c $(DEBUG_FLAGS) $(HIPE_FLAGS) $(SIM_FLAGS) $(SHELL_SIM_FLAGS)  $(LIB_PATH).$(HIPE_TOP_MODULE)

testing_comments:
################################################################################################################
#                            Begin Makefile Testing

# ifneq $("wildcard $(SRC_PATH)/*.sv","")
# SV_EXIST = 1
# else
# SV_EXIST = 0
# endif

test: exist print_all_sv multiple_exist

exist:
	# Objective: deterimine if a file exists
	@if [ -a "src/drive_counter.sv" ]; then echo ".sv" ; fi;
	@echo "test>>exist sucessful"

print_all_sv:
	@$(foreach f, $(SRC_PATH)/*.sv, echo $f )
	@echo "test>>print_all_sv sucessful"

multiple_exist:
	# Objective: deterimine if multiple files exist
	@$(foreach f, $(SRC_PATH)/*.sv, if [ -a "$f" ]; then echo "$f" ; else echo "$f does not exist" ; fi; )
	@echo "test>>multiple_exist sucessful"
	@echo "conclusion: need a different way to glob check if files exist. -- Jordan Ulmer 2017-01-14"

#                            END Makefile Testing
################################################################################################################
help:
	@echo ""
	@echo "The following targets are available:"
	@echo "    make all  	- cleans and compiles $(LIB_PATH)"
	@echo "    make build 	- compiles changes in $(LIB_PATH)"
	@echo "    make sv   	- compiles SystemVerilog from $(SRC_PATH) into $(LIB_PATH)"
	@echo "    make v   	- compiles Verilog from $(SRC_PATH) into $(LIB_PATH)"
	@echo "    make vhd   	- compiles VHDL from $(SRC_PATH) into $(LIB_PATH)"
	@echo "    make clean	- cleans $(LIB_PATH)"
	@echo "    make sim 	- command line simulate $(LIB_PATH).first_counter_tb"
	@echo "    make gui  	- gui simulate simulate $(LIB_PATH).first_counter_tb"
	@echo "    make hipe_sim	- command line simulate $(LIB_PATH).top"
	@echo "    make hipe_gui  	- gui simulate simulate $(LIB_PATH).top"
	@echo ""
