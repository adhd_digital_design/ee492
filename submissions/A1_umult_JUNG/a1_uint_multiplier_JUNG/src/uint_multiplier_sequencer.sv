
//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Generate test vectors for unsigned multiplier.
//Date Created : 2017-01-19

// Timescale
`timescale 1ns/10ps


class uint_multiplier_sequencer;
localparam nspecial = 10;

typedef struct {
  integer unsigned q;
  integer unsigned m;
} MultInputs;

    MultInputs  special[0:nspecial-1]  =
    '{
        '{  8'h0   ,  8'h0   },
        '{  8'h0   ,  8'h0   },
        '{  4'b1101   ,  4'b1011   },
        '{  8'h0   ,  8'hAA  },
        '{  8'hAA  ,  8'h0   },
        '{  8'h1   ,  8'hFF  },
        '{  8'hFF  ,  8'h1   },
        '{  8'hCA  ,  8'hFE  },
        '{  8'hBA  ,  8'hBE  },
        '{  8'hFF  ,  8'hFF  }
    };

    // TODO: Move get_m & get_q to separate module.
    // Test vectors are designed for 8 bit multiplier.
    // TODO: should be use int unsigned or unsigned packed array or maybe unsigned vector?
    // function unsigned [OPPERAND_WIDTH:0] get_m(input integer unsigned i);
    function integer unsigned get_q(input integer unsigned i);
        if(i<nspecial) return special[i].q;
        else return $urandom_range(32**2,0); // Random unsigned integer.
    endfunction
    function integer unsigned get_m(input integer unsigned i);
        if(i<nspecial) return special[i].m;
        else return $urandom_range(32**2,0); // Random unsigned integer.
    endfunction

endclass
