//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: First On First Out
//Date Created : 2017-02-04
//Source: http://academic.csuohio.edu/chu_p/rtl/sopc_vlog.html

module fifo
   #(
    parameter DATA_WIDTH=8, // number of bits in a word
              ADDR_WIDTH=4  // number of address bits
   ) (
    input wire clk,
    input wire reset,
    input wire rd, // Read Data     // Dequeue // Pop
    input wire wr, // Write Request // Queue   // Push
    input wire [DATA_WIDTH-1:0] w_data,
    output wire empty,
    output wire full,
    output wire [DATA_WIDTH-1:0] r_data
   );

   //signal declaration
   wire [ADDR_WIDTH-1:0] w_addr;
   // wire [ADDR_WIDTH-1:0] r_addr; // used with reg_file
   wire [ADDR_WIDTH-1:0] r_addr_next;
   wire wr_en;

   // body
   // write enabled only when FIFO is not full
   assign wr_en = wr & ~full;
   
   // instantiate fifo control unit
   fifo_ctrl #(.ADDR_WIDTH(ADDR_WIDTH)) ctrl_0
      (.clk(clk), .reset(reset), .rd(rd), .wr(wr), .empty(empty), 
       .full(full), .w_addr(w_addr), 
       //.r_addr(r_addr), // used with reg_file
       .r_addr_next(r_addr_next));

   // instantiate reg_file
   // reg_file #(.DATA_WIDTH(DATA_WIDTH), .ADDR_WIDTH(ADDR_WIDTH)) reg_file_0
   //    (.clk(clk), .wr_en(wr_en), .w_addr(w_addr), .r_addr(r_addr), 
   //     .w_data(w_data), .r_data(r_data));

   // instantiate dual_port_ram
   // instantiate synchronous SRAM
   dual_port_ram 
      #(.DATA_WIDTH(DATA_WIDTH), .ADDR_WIDTH(ADDR_WIDTH)) ram_unit
      (.clk(clk), .we(wr_en), .w_addr(w_addr), .r_addr(r_addr_next), 
       .d(w_data), .q(r_data));

endmodule

