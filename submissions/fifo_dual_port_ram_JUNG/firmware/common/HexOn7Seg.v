//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Convert binary input to Hexadecimal to display on a 7-Segment display
//Date Created : 2017-01-16

// Output format: {g, f, e, d, c, b, a}
// --a--
// |   |
// f   b
// |   |
// --g--
// |   |
// e   c
// |   |
// --d--

module HexOn7Seg(
    input wire [4-1:0] nIn,
    output reg [7-1:0] ssOut,
    output reg [7-1:0] ssOut_L
);
    always @(nIn)
    begin
        case (nIn)
            4'h0: ssOut = 7'b0111111;
            4'h1: ssOut = 7'b0000110;
            4'h2: ssOut = 7'b1011011;
            4'h3: ssOut = 7'b1001111;
            4'h4: ssOut = 7'b1100110;
            4'h5: ssOut = 7'b1101101;
            4'h6: ssOut = 7'b1111101;
            4'h7: ssOut = 7'b0000111;
            4'h8: ssOut = 7'b1111111;
            4'h9: ssOut = 7'b1100111;
            4'hA: ssOut = 7'b1110111;
            4'hB: ssOut = 7'b1111100;
            4'hC: ssOut = 7'b0111001;
            4'hD: ssOut = 7'b1011110;
            4'hE: ssOut = 7'b1111001;
            4'hF: ssOut = 7'b1110001;
            default: ssOut = 7'b1001001;
        endcase

        ssOut_L = ~ssOut;
    end
endmodule
