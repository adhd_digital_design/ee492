// DE1 has 10 LEDR. // DE1 has 10 SW. // DE1 has 6 HEX.
// DE2 has 8 HEX.

module top_fifo #(
    parameter NCLK=4,
    parameter NIN=8,
    parameter NOUT=10,
    parameter NSEGS=6
)(
    input  wire [NCLK-1:0] clk,
    input  wire [NIN-1:0] in,
    output wire [NOUT-1:0] out,
    output wire [(NSEGS*HEX_W)-1:0] hex_L
);
localparam HEX_W = 7;
localparam FIFO_DATA_WIDTH = 8;
localparam FIFO_ADDR_WIDTH = 4;

// Fifo Wires
wire [FIFO_DATA_WIDTH-1:0] r_data;
wire [FIFO_DATA_WIDTH-1:0] w_data;

// Display registers on Hex.
display_on_hex #( .NSEGS(2)) hex_displays_5_0 (
    .hex_L( hex_L[((NSEGS)*HEX_W)-1:((0)*HEX_W)] ),
    .value( {
             r_data[FIFO_DATA_WIDTH-1:0]
            } )
);


// Connect modules
fifo
   #(
    .DATA_WIDTH(FIFO_DATA_WIDTH), // number of bits in a word
    .ADDR_WIDTH(FIFO_ADDR_WIDTH)  // number of address bits
   ) my_fifo
   (
    .clk(clk[0]),
    .reset(clk[1]),
    .rd(clk[2]),
    .wr(clk[3]),
    .w_data(w_data[FIFO_DATA_WIDTH-1:0]),
    .empty(out[0]),
    .full(out[1]),
    .r_data(r_data[FIFO_DATA_WIDTH-1:0])
   );


// Assign Drivers
assign w_data[FIFO_DATA_WIDTH-1:0]=in[FIFO_DATA_WIDTH-1:0];

// Assign Indicators
assign out[NOUT-1:2]=w_data[FIFO_DATA_WIDTH-1:0]; // show write value on leds


endmodule
