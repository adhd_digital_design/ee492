
# EE 492 S02 – Advanced Digital Hardware Design -- Spring 2017

Demonstration Notes:
0. HW was demonstrated to Dr. Fourney after class on 2017-03-31
1. The most significant switch on the DE10_LITE was used to choose the clock that drives the nios2 processor the 50MHz clock or a 25MHz clock.
2. The twos complement and delay from A5 was shown and timed (~6s with the 50MHz Clock)
3. The 25MHz clock was muxed in and twos complement and delay from A5 was shown and timed (~12s with the 25MHz Clock)
