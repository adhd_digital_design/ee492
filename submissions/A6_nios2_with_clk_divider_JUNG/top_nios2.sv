`ifndef top_nios2_
`define top_nios2_

module top_nios2 #(
    parameter NCLK=4,
    parameter NIN=17+4,
    parameter NOUT=18+8,
    parameter NSEGS=8
) (
    input [NCLK-1:0] clk,
    input [NIN-1:0] in,
    output wire [NOUT-1:0] out,
    output wire [(NSEGS*7)-1:0] hex_L
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************


//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
wire [31:0] unused_out ;
wire clk_mux;
wire clk_div;

//****************************************************************************
//                            Combinational logic
//****************************************************************************
// assign out = in;
assign out[10] = out[0]; // GPIO to measure timing
assign clk_mux = (in[9]) ? clk_div : clk[0] ; // Use in[9] to mux between 50MHz clk and 25MHz clk, should double delay times...


//****************************************************************************
//                              Internal Modules
//****************************************************************************

// Un-used QSYS
// nios2_onchip_jtaguart n2_jtag (
//     .clk_clk(clk[0]) // clk.clk
//     );

// Parellel I/O QSYS
nios2_pio_onchip n2_pio (
    .clk_clk(clk_mux),      // clk.clk
    .pi_export({{31-9 - 1 {1'b0}},  in[9:0] }), // pi.export [31:0]
    .po_export({unused_out[31-9:0], out[9:0]})  // po.export [31:0] // float unused output
    );


// clk_divider_50Mhz_downto_25MHz
clk_divider #(
    .in_clk_freq_hz(  (50) * (10** 6) ),  // Mhz = 10^6 Hz
    .out_clk_freq_hz( (25) * (10** 6) ), // Hz = 10^0 Hz
    //.total_wait_cycles(),               // total_wait_cycles =  in_clk_freq_hz / out_clk_freq_hz ;
    .duty_cycle( 0.5 ) ,                  // Percent of time ON
    .sig_active_state( 1'b1 )             // Input signal is ACTIVE HIGH
) cd_1(
    .in_clk(clk[0]),    // Input Clock
    .out_clk(clk_div)     // Output Clock
);


endmodule

// top_nios2 top #(
//     .NCLK(),
//     .NIN(),
//     .NOUT(),
//     .NSEGS()
// ) (
// (
//     .clk(KEY[3:0]),
//     .in(SW[17:0]),
//     .out({LEDR[17:0], LEDG[7:0]}),
//     .hex_L({HEX7, HEX6, HEX5, HEX4, HEX3, HEX2, HEX1, HEX0})
// );

`endif
