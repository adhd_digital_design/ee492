onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /a3_top_tb/clk
add wave -noupdate /a3_top_tb/in_16_reg
add wave -noupdate /a3_top_tb/in_17_to_22_reg
add wave -noupdate /a3_top_tb/s16/o
add wave -noupdate -expand /a3_top_tb/s17_b/y
add wave -noupdate -expand /a3_top_tb/s17_nb/y
add wave -noupdate -expand /a3_top_tb/s18_b/y
add wave -noupdate -expand /a3_top_tb/s18_nb/y
add wave -noupdate -expand /a3_top_tb/s19_b/y
add wave -noupdate -expand /a3_top_tb/s19_nb/y
add wave -noupdate -expand /a3_top_tb/s20_b/y
add wave -noupdate -expand /a3_top_tb/s20_nb/y
add wave -noupdate -expand /a3_top_tb/s21_b/y
add wave -noupdate -expand /a3_top_tb/s21_nb/y
add wave -noupdate -expand /a3_top_tb/s22_b/y
add wave -noupdate -expand /a3_top_tb/s22_nb/y
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {60 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 203
configure wave -valuecolwidth 46
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 1
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {63 ps}
