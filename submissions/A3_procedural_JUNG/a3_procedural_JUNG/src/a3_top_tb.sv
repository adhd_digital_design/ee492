
// http://www.sutherland-hdl.com/papers/1996-CUG-presentation_nonblocking_assigns.pdf
// Understanding Verilog Blocking
// and Non-blocking Assignments
// International Cadence
// User Group Conference
// September 11, 1996
// presented by
// Stuart Sutherland
// Sutherland HDL Consulting

// Timescale
`timescale 1ps/1ps

module a3_top_tb();
    localparam N_TEST_SLIDE_16 = 10;
    localparam N_TEST_SLIDE_17_TO_22 = 10;

    // Declare signals in the module.
    reg clk;
    reg in_16_reg;
    reg in_17_to_22_reg;
    int slide16_counter;
    int slide17_to_22_counter;

    // I/O for slide16-slide22

    slide16 s16 ( .in(in_16_reg), .o()); // o_16[6:1]) );

    // "_b"=Blocking, "_nb"=Non Blocking
    slide17_b  s17_b  ( .clk(clk), .in(in_17_to_22_reg), .y()); // in_s17_b ), .y(y_s17_b[2:1] ) ); 
    slide17_nb s17_nb ( .clk(clk), .in(in_17_to_22_reg), .y()); // in_s17_nb), .y(y_s17_nb[2:1]) ); 

    slide18_b  s18_b  ( .clk(clk), .in(in_17_to_22_reg), .y()); // in_s18_b ), .y(y_s18_b[2:1] ) ); 
    slide18_nb s18_nb ( .clk(clk), .in(in_17_to_22_reg), .y()); // in_s18_nb), .y(y_s18_nb[2:1]) ); 

    slide19_b  s19_b  ( .clk(clk), .in(in_17_to_22_reg), .y()); // in_s19_b ), .y(y_s19_b[2:1] ) ); 
    slide19_nb s19_nb ( .clk(clk), .in(in_17_to_22_reg), .y()); // in_s19_nb), .y(y_s19_nb[2:1]) ); 

    slide20_b  s20_b  ( .clk(clk), .in(in_17_to_22_reg), .y()); // in_s20_b ), .y(y_s20_b[2:1] ) ); 
    slide20_nb s20_nb ( .clk(clk), .in(in_17_to_22_reg), .y()); // in_s20_nb), .y(y_s20_nb[2:1]) ); 

    slide21_b  s21_b  ( .clk(clk), .in(in_17_to_22_reg), .y()); // in_s21_b ), .y(y_s21_b[2:1] ) ); 
    slide21_nb s21_nb ( .clk(clk), .in(in_17_to_22_reg), .y()); // in_s21_nb), .y(y_s21_nb[2:1]) ); 

    slide22_b  s22_b  ( .clk(clk), .in(in_17_to_22_reg), .y()); // in_s22_b ), .y(y_s22_b[2:1] ) ); 
    slide22_nb s22_nb ( .clk(clk), .in(in_17_to_22_reg), .y()); // in_s22_nb), .y(y_s22_nb[2:1]) ); 

    // Clock generator
    always begin : clk_gen // Empty Sensativity List
        #10 clk = ~clk; // Invert clock // No time
    end

    always begin // Empty Sensativity List
        drive_slide16_input();
    end

    always begin // Empty Sensativity List
        drive_17_to_22_input();
    end

    // always begin
    //     end_if(); // Polling, may slow down sim - TODO: Don't Poll to finish
    // end


    task drive_slide16_input(); // Empty sensitivity list.
        // if(slide16_counter!=N_TEST_SLIDE_16) begin
            #10  in_16_reg  =  1'b1;
            #10  in_16_reg  =  1'b0;
            #10  in_16_reg  =  1'b1;
            #3   in_16_reg  =  1'b0;
            #3   in_16_reg  =  1'b1;
            #9   in_16_reg  =  1'b0;
            slide16_counter = slide16_counter +1;
        // end
        // else begin
            end_if();
        // end
    endtask

    task drive_17_to_22_input(); // Empty sensitivity list.
        // if(slide17_to_22_counter!=N_TEST_SLIDE_17_TO_22) begin
            #5   in_17_to_22_reg  =  1'b1;
            #15  in_17_to_22_reg  =  1'b0;
            #15  in_17_to_22_reg  =  1'b1;
            #18  in_17_to_22_reg  =  1'b0;
            slide17_to_22_counter = slide17_to_22_counter +1;
        // end
        // else begin
            end_if();
        // end
    endtask

    task end_if();
        if( slide16_counter>=N_TEST_SLIDE_16 && slide17_to_22_counter>=N_TEST_SLIDE_17_TO_22) begin
            $display("Success - Blocking and Non-blocking Procedural Delay comparison complete");
            $display("slide16_counter=(%d)",slide16_counter);
            $display("slide17_to_22_counter=(%d)",N_TEST_SLIDE_17_TO_22);
            $finish;
        end
    endtask : end_if

    initial begin : init
        clk=0;
        in_16_reg=0;
        in_17_to_22_reg=0;
        slide16_counter=0;
        slide17_to_22_counter=0;
        // Quit after running once.
        // end_if();
        #1000
        $error("SIM ERROR- Timeout after #1000");
        $finish; // TIMEOUT if sim runs too long
    end

    // final begin
    // end

endmodule
