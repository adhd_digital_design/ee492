
// http://www.sutherland-hdl.com/papers/1996-CUG-presentation_nonblocking_assigns.pdf
// Understanding Verilog Blocking
// and Non-blocking Assignments
// International Cadence
// User Group Conference
// September 11, 1996
// presented by
// Stuart Sutherland
// Sutherland HDL Consulting

// Timescale
`timescale 1ps/1ps

module slide16(
    input wire in,
    output reg [6:1] o
);
    // How will these procedural assignments behave?
    always @(in) begin : blocking_no_delay
        o[1] = in; // Blocking, No delay
    end
    always @(in) begin : non_blocking_no_delay
        o[2] <= in; // Non-blocking, No delay
    end
    always @(in) begin : blocking_delayed_evaluation
        #5 o[3] = in; // Blocking, Delayed evaluation
    end
    always @(in) begin : non_blocking_delayed_evaluation
        #5 o[4] <= in; // Non-blocking, Delayed evaluation
    end
    always @(in) begin : blocking_delayed_assignment
        o[5] = #5 in; // Blocking, Delayed assignment
    end
    always @(in) begin : non_blocking_delayed_assignment
        o[6] <= #5 in; // Non-blocking, Delayed assignment
    end
endmodule

module slide17_b(
    input wire clk,
    input wire in,
    output reg [2:1] y
);
    // How will these procedural assignments behave?
    //     ➤ Sequential assignments
    //     ➤ No delays
    always @(posedge clk) begin  : blocking_no_delay
        //parallel flip-flops
        y[1] = in;
        y[2] = y[1];
    end
endmodule

module slide17_nb(
    input wire clk,
    input wire in,
    output reg [2:1] y
);
    // How will these procedural assignments behave?
    //     ➤ Sequential assignments
    //     ➤ No delays
    always @(posedge clk) begin : non_blocking_no_delay
        //shift-register with zero delays register with zero delays
        y[1] <= in;
        y[2] <= y[1];
    end
endmodule


module slide18_b(
    input wire clk,
    input wire in,
    output reg [2:1] y
);
    // How will these procedural assignments behave?
    //     ➤ Sequential assignments
    //     ➤ No delays
    always @(posedge clk) begin  : blocking_delayed_evaluation
        // shift register with delayed clocks
        #5 y[1] = in;
        #5 y[2] = y[1];
    end
endmodule

module slide18_nb(
    input wire clk,
    input wire in,
    output reg [2:1] y
);
    // How will these procedural assignments behave?
    //     ➤ Sequential assignments
    //     ➤ No delays
    always @(posedge clk) begin : non_blocking_delayed_evaluation
        // shift register with delayed clocks
        #5 y[1] <= in;
        #5 y[2] <= y[1];
    end
endmodule


module slide19_b(
    input wire clk,
    input wire in,
    output reg [2:1] y
);
    // ➤   How will these procedural assignments behave? 
    //      ➤  Sequential assignments 
    //      ➤  Delayed assignment 
    always @(posedge clk) begin : blocking_delayed_assignment
        // shift register delayed clock on second stage 
        y[1] = #5 in;
        y[2] = #5 y[1];
    end 
endmodule

module slide19_nb(
    input wire clk,
    input wire in,
    output reg [2:1] y
);
    // ➤   How will these procedural assignments behave? 
    //      ➤  Sequential assignments 
    //      ➤  Delayed assignment 
    always @(posedge clk) begin : non_blocking_delayed_assignment
        // shift register with delays 
        y[1] <= #5 in;
        y[2] <= #5 y[1];
    end
endmodule



module slide20_b(
    input wire clk,
    input wire in,
    output reg [2:1] y
);
    // ➤   How will these procedural assignments behave? 
    //      ➤  Concurrent assignments 
    //      ➤  No delays 
    always @(posedge clk) begin : blocking_1
        y[1] = in; // shift register with race condition 
    end
    always @(posedge clk) begin : blocking_2
        y[2] = y[1]; // shift register with race condition 
    end
endmodule

module slide20_nb(
    input wire clk,
    input wire in,
    output reg [2:1] y
);
    // ➤   How will these procedural assignments behave? 
    //      ➤  Concurrent assignments 
    //      ➤  No delays 
    always @(posedge clk) begin : non_blocking_1
        y[1] <= in; // shift-register with zero delays 
    end
    always @(posedge clk) begin : non_blocking_2
        y[2] <= y[1]; // shift-register with zero delays 
    end
endmodule

module slide21_b(
    input wire clk,
    input wire in,
    output reg [2:1] y
);
    // ➤   How will these procedural assignments behave? 
    //      ➤  Concurrent assignments 
    //      ➤  Delayed evaluation 
    always @(posedge clk) begin : blocking_1
        #5 y[1] = in; // shift register with race condition 
    end
    always @(posedge clk) begin : blocking_2
        #5 y[2] = y[1]; // shift register with race condition 
    end
endmodule

module slide21_nb(
    input wire clk,
    input wire in,
    output reg [2:1] y
);
    // ➤   How will these procedural assignments behave? 
    //      ➤  Concurrent assignments 
    //      ➤  Delayed evaluation 
    always @(posedge clk) begin : non_blocking_1
        #5 y[1] <= in; // shift register with race condition 
    end
    always @(posedge clk) begin : non_blocking_2
        #5 y[2] <= y[1]; // shift register with race condition 
    end
endmodule

module slide22_b(
    input wire clk,
    input wire in,
    output reg [2:1] y
);
    // ➤   How will these procedural assignments behave? 
    //      ➤  Concurrent assignments 
    //      ➤  Delayed assignment 
    always @(posedge clk) begin : blocking_1
        y[1] = #5 in; // shift register, delay must be < clock period 
    end
    always @(posedge clk) begin : blocking_2
        y[2] = #5 y[1]; // shift register, delay must be < clock period 
    end
endmodule

module slide22_nb(
    input wire clk,
    input wire in,
    output reg [2:1] y
);
    // ➤   How will these procedural assignments behave? 
    //      ➤  Concurrent assignments 
    //      ➤  Delayed assignment 
    always @(posedge clk) begin : non_blocking_1
        y[1] <= #5 in;// shift register with delays 
    end
    always @(posedge clk) begin : non_blocking_2
        y[2] <= #5 y[1]; // shift register with delays 
    end
endmodule

