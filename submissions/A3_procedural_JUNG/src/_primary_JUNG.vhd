library verilog;
use verilog.vl_types.all;
entity slide17_nb is
    port(
        clk             : in     vl_logic;
        \in\            : in     vl_logic;
        y               : out    vl_logic_vector(2 downto 1)
    );
end slide17_nb;
