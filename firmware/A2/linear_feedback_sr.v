
//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: 8-Bit Linear Feedback Shift Register
//Date Created : 2017-01-23

// src: http://www.newwaveinstruments.com/resources/articles/m_sequence_linear_feedback_shift_register_lfsr.htm
// Unforunately they are "1-index" and so all the numbers have to be shifted. Listed here are the originals:
// 8 stages, 4 taps:  (6 sets)
// [8, 7, 6, 1]
// [8, 7, 5, 3]
// [8, 7, 3, 2]
// [8, 6, 5, 4]
// [8, 6, 5, 3]
// [8, 6, 5, 2]
//
// 8 stages, 6 taps:  (2 sets)
// [8, 7, 6, 5, 4, 2]
// [8, 7, 6, 5, 2, 1]

module linear_feedback_sr  #(
    parameter ID=0,
    parameter STAGES=8 // Only 8 is implemented atm.
) (
    input wire clk,
    input wire load, // Synchronous.
    input wire [STAGES-1:0] value_i,
    output reg [STAGES-1:0] value_o =32'hFF // need to set non-zero initial value.
);
localparam NUM_TAPS=4;
// parameter [7:0] lsfr_taps [0 : 7]   = '{8'd9, 8'd5, 8'd3, 8'h21, 8'd9, 8'd9, 8'd5, 8'd9}; // Other option.

wire [NUM_TAPS-1:0] taps;
wire si_w = ^taps; //Reduction XOR // Locks up if taps are all zeros
// wire si_w = ^~ or ~^taps; //Reduction XNOR // Locks up if taps are all ones

generate
    case(ID)
        0: assign taps = {value_o[ 7 ], value_o[ 6 ], value_o[ 5 ], value_o[ 0 ]};
        1: assign taps = {value_o[ 7 ], value_o[ 6 ], value_o[ 4 ], value_o[ 2 ]};
        2: assign taps = {value_o[ 7 ], value_o[ 6 ], value_o[ 2 ], value_o[ 1 ]};
        3: assign taps = {value_o[ 7 ], value_o[ 5 ], value_o[ 4 ], value_o[ 3 ]};
        4: assign taps = {value_o[ 7 ], value_o[ 5 ], value_o[ 4 ], value_o[ 2 ]};
        default:/*5:*/ assign taps = {value_o[ 7 ], value_o[ 5 ], value_o[ 4 ], value_o[ 1 ]};
    endcase
endgenerate

// si_w -> LSB value[0] -> ... -> MSB value[STAGES] -> so
always @ (posedge clk, posedge load) begin : sync_value_shiftreg
    if(load) begin
        value_o[STAGES-1:0] <= value_i[STAGES-1:0];
    end
    else begin
        // si_w -> LSB.
        value_o[STAGES-1:0] <= { value_o[STAGES-2:0], si_w };
    end
end

endmodule

// linear_feedback_sr #(
//     .ID()
//     .STAGES()
// ) lfsr0 (
//     .clk(clk),
//     .load(load),
//     .value_i(seed),
//     .value_o(random_value)
// );

// linear_feedback_sr #(
//     .ID(0)
// ) lfsr0 (
//     .clk(~KEY[0]),
//     .load(~KEY[1]),
//     .value_i(8'FF),
//     .value_o(rand0)
// );
