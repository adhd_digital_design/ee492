// DE1 has 10 LEDR. // DE1 has 10 SW. // DE1 has 6 HEX.
// DE2 has 8 HEX.

module top_signaltap_switches #(
    parameter NCLK=2,
    parameter NIN=10,
    parameter NOUT=10,
    parameter NSEGS=6
)(
    input [NCLK-1:0] clk,
    input [NIN-1:0] in,
    output wire [NOUT-1:0] out,
    output wire [(NSEGS*HEX_W)-1:0] hex_L
);
localparam HEX_W = 7;
localparam SWICH_WIDTH = NIN;
wire [ (SWICH_WIDTH - 1):0]  sw;
wire [ (SWICH_WIDTH - 1):0]  led;


// Instantiate Module
signaltap_switches #(
    .N(SWICH_WIDTH)
) sw_test (
    .sw_i(sw[SWICH_WIDTH-1:0]),
    .clk(clk[2]), // CLOCK_50
    .ledr_o(led[SWICH_WIDTH-1:0])
);


// Assign Drivers
assign sw[SWICH_WIDTH-1:0] = in[SWICH_WIDTH-1:0];
// Assign Indicators
assign out[SWICH_WIDTH-1:0]  = led[SWICH_WIDTH-1:0];

endmodule
// Using with DE10-Lite
// top_serdiv  #(
//     .NCLK(2),
//     .NIN(10),
//     .NOUT(10),
//     .NSEGS(6)
// ) top (
//     .clk(~KEY[1:0]),  // May need to debounce PBs
//     .in(SW[9:0]),
//     .out(LEDR[9:0]),
//     .hex_L({HEX5[6:0],HEX4[6:0],HEX3[6:0],HEX2[6:0],HEX1[6:0],HEX0[6:0]})
// );


