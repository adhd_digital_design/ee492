module signaltap_switches #(
    parameter N=1
) (
    input wire [N-1:0] sw_i,
    input wire clk,
    output reg [N-1:0] ledr_o
);
    
    always @ (posedge clk)
        ledr_o[N-1:0]<= sw_i[N-1:0];
endmodule 
