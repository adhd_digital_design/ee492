
`ifndef top_default_pkg_
`define top_default_pkg_

// Device generic top wrapper module
`include "top_nios2.sv" 

// Required common firmware
`include "../common/clk_divider.v"
// `include "../common/HexOn7Seg.v"
// `include "../common/display_on_hex.v"

`endif