// top.sv
//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Top level of the testbench
//Date Created : 2017-02-25

`ifndef  top_
`define  top_

//UVM
// `include "uvm_macros.svh"
// `include "uvm_pkg.sv"

// TB Imports
`include "nios2_monitor.sv"
`include "nios2_sequencer.sv"
`include "nios2_scoreboard.sv"

module top();
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

localparam UPPERCASE_LOCAL = 0;

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

// wire internal_wire;
// wire [N -1:0] internal_wires;
// reg internal_reg;
// reg [N -1:0] internal_regs;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************

// always @(posedge clk) begin : fsm_state_name
//     case(state)
//         PREFIX_LOCALPARAM:
//         default:
//     endcase
// end

//****************************************************************************
//                             Procedural Blocks
//****************************************************************************

    initial begin
        $display("Top Initial");
        $system("date +%s%N");
    end

    // task my_task();
    //     # 1 var <= val;
    // endtask : my_task

    final begin
        $display("Top Final");
        $system("date +%s%N");
    end


//****************************************************************************
//                              Internal Modules
//****************************************************************************

// my_module #(
//     .PARAM(0)
// ) instance_name (
//     .ports()
// );

endmodule

`endif