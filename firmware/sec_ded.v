
module sec_ded # (
) (
    input [11:0] data_and_check,

    output reg [7:0] data_o,
    input [2:0] check_bit,
    output reg double_error_flag
);

endmodule
