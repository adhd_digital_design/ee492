//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Assignment #1: Multiply two unsigned 8-bit numbers using two methods.
//Date Created : 2017-01-16

module ee492_a1 #(
    parameter opperand_width = 8 // Assume we multiply two 8-Bit #s...
)
(
    input wire clk,
    input wire start,
    input wire select,

    input  wire [opperand_width-1:0] q, // multiplier
    input  wire [opperand_width-1:0] m, // multiplicand
    output wire [2*opperand_width-1:0] p, // product

    output [7-1:0] HEX0,
    output [7-1:0] HEX1,
    output [7-1:0] HEX2,
    output [7-1:0] HEX3,
    output [7-1:0] HEX4,
    output [7-1:0] HEX5,
    output [7-1:0] HEX6,
    output [7-1:0] HEX7

    // DEBUG ONLY
    , output wire slow_clock
    , output wire seq_mult_done

);
localparam product_width = 2*opperand_width;

// Wires and Regs
wire start_debounced;
wire [product_width-1:0] sequential_p;
wire [product_width-1:0] array_p;

// Mux the two products to a single output.
assign p = (select ? sequential_p : array_p);

// Display values on HEX Display
display_on_hex #( .NSEGS(4)) hex3_0 (
    .hex_L( {HEX3,HEX2,HEX1,HEX0} ),
    .value( p )
);
display_on_hex #( .NSEGS(2)) hex5_4 (
    .hex_L( {HEX5,HEX4} ),
    .value( q )
);
display_on_hex #( .NSEGS(2)) hex7_6 (
    .hex_L( {HEX7,HEX6} ),
    .value( m )
);

// Debounce the start signal.
debounce #(
    .clk_freq_hz    (  (50) * (10** 6)), // Mhz  = 10^6 Hz
    .wait_time_sec  ((   4) * (10**-3))  // ms  = 10^-3 sec
) debounce_start (
    .in_sig(start),
    .clk(clk),           // Input Clock
    .out_sig(start_debounced)
);

// clk_divider_50Mhz_downto_1Hz
clk_divider #(
              .in_clk_freq_hz(  (50) * (10** 6) ),  // Mhz = 10^6 Hz
              .out_clk_freq_hz( (1 ) * (10** 0) ), // Hz = 10^0 Hz
              //.total_wait_cycles(),               // total_wait_cycles =  in_clk_freq_hz / out_clk_freq_hz ;
              .duty_cycle( 0.5 ) ,                  // Percent of time ON
              .sig_active_state( 1'b1 )             // Input signal is ACTIVE HIGH
              )
cd_1(
    .in_clk(clk),    // Input Clock
    .out_clk(slow_clock)     // Output Clock
    );

// array multiplier calculates the product in a single clock cycle
array_multiplier_sync #(
    .opperand_width(opperand_width)
) ams (
    .clk(slow_clock),           // Latch Product on clk
    .start(start_debounced),    // Start the multiplication.
    .q(q[opperand_width-1:0]),  // Multiplier
    .m(m[opperand_width-1:0]),  // Multiplicand
    .p(array_p) // Product
);

// sequential multiplier needs multiple clock cycles
sequential_multiplier #(
    .opperand_width(opperand_width)
) sm (
    .clk(slow_clock),
    .start(start_debounced),
    .q(q), // multiplier
    .m(m), // multiplicand
    .p(sequential_p)  // product

    // DEBUG ONLY
    , .seq_mult_done(seq_mult_done)
);

endmodule

// ee492_a1 #(
//     .opperand_width(8) // Assume we multiply two 8-Bit #s...
// ) a1 (
//     .clk(clk),
//     .start(start),
//     .select(select),

//     .m(m[opperand_width-1:0]),  // Multiplicand
//     .q(m[opperand_width-1:0]),  // Multiplier
//     .p(p[2*opperand_width-1:0]),       // Product

//     .HEX0(HEX0[7-1:0]),
//     .HEX1(HEX1[7-1:0]),
//     .HEX2(HEX2[7-1:0]),
//     .HEX3(HEX3[7-1:0]),
//     .HEX4(HEX4[7-1:0]),
//     .HEX5(HEX5[7-1:0]),
//     .HEX6(HEX6[7-1:0]),
//     .HEX7(HEX7[7-1:0])

//     // DEBUG ONLY
//     , .slow_clock(slow_clock)
//     , .seq_mult_done(seq_mult_done)

// );

