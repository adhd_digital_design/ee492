// Authors : Jordan D. Ulmer , Nathan Genetzky
// Purpose: Sequential Multipler Controller
// Date Created : 2017-01-16

module seq_mult_control #(
    parameter opperand_width = 8 // Assume we multiply two 8-bit numbers.
)(
    input clk,
    input start,
    input prod_lsb,

    output reg shift_en,
    output reg load_lsb,
    output reg load_msb,
    output reg reset_msb,
    output wire add_en

    // DEBUG ONLY
    ,output wire seq_mult_done
);
localparam STATE_INIT=2*opperand_width+1;
localparam STATE_FINAL=0;
// STATE_DOING = ~(STATE_INIT || STATE_FINAL);

// Wires and Regs
reg [$clog2(STATE_INIT):0] i;

wire latch_not_shift = ~i[0];
assign add_en = prod_lsb; // Is only important during DOING.
assign seq_mult_done = (0==i);

// Counter will hold the state of the control module.
always @(posedge clk or posedge start) begin : sync_counter
    if(start) begin
        i = STATE_INIT;
    end
    else begin// DONE
        case(i)
            STATE_FINAL: i = STATE_FINAL;
            default: begin
                // Decrement until 0. STATE_INIT -> STATE_FINAL.
                // TODO: check prod_lsb, skip add/latch cycle..
                // be careful of whats in carry_out of fa.
                i=i-1;
            end
        endcase
    end
end

// INIT - Initlize the shift registers.
always @(*) begin : comb_init_srs_a_and_q
    case(i)
        STATE_INIT: begin
            reset_msb=1;
            load_lsb=1;
        end
        default: begin
            reset_msb=0;
            load_lsb=0;
        end
    endcase
end

// DOING - Latch result of fa.
always @(*) begin : comb_load_msb
    case(i)
        STATE_INIT: load_msb=0;
        STATE_FINAL: load_msb=0;
        default: load_msb=latch_not_shift;
    endcase
end

// DOING - Shift through { fa.cout, sr_a.value_o, sr_q.value_o };
always @(*) begin : comb_shift
    case(i)
        STATE_INIT: shift_en=0;
        STATE_FINAL: shift_en=0;
        default: shift_en= ~latch_not_shift;
    endcase
end

endmodule

// seq_mult_control #(
//     .opperand_width(opperand_width)
// )  cntrl (
//     .clk(clk),
//     .start(start),
//     .prod_lsb(p[0]),

//     .shift_en(shift_en),
//     .load_lsb(load_lsb),
//     .load_msb(load_msb),
//     .reset_msb(reset_msb),
//     .add_en(add_en)

//     // DEBUG ONLY
//     , .seq_mult_done(seq_mult_done)
// );
