//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Unsigned Multiply Row - Creates a partial product , used in an array multiply
//Date Created : 2017-01-16
//Resource: http://web.stanford.edu/class/archive/ee/ee371/ee371.1066/tools/verilog_tips.pdf
module multiply_row #(
    parameter m_row_width = 8 // Assume we multiply two 8-Bit #s...// Partial Product Width Out = PPIW + 1
)(
    input wire [(m_row_width)-1:0] m,    // Multiplicand
    input wire q_i,  // Multiplier

    input  wire c_i,  // Carry In
    output wire c_o,  // Carry Out

    input  wire [(m_row_width)-1:0] pp_i, // Partial Product In
    output wire [(m_row_width)-1:0] pp_o  // Partial Product Out
);

// Creating an array of modules. Each port is connected to single bit or vector of the same length as the array.
wire [m_row_width-1:0] co_v;
multiply_cell mcells[m_row_width-1:0] (
    .m_i(m),
    .q_i(q_i),
    .c_i({co_v[m_row_width-2:0], c_i} ), // MSB gets co of 2nd to last. LSB gets 0.
    .c_o(co_v[m_row_width-1:0]),
    .pp_i(pp_i[(m_row_width)-1:0]),
    .pp_o(pp_o[(m_row_width)-1:0])
);

assign c_o = co_v[m_row_width-1]; // MSB carry out

endmodule
// multiply_row #(
//     parameter m_row_width = 8 // Assume we multiply two 8-Bit #s...// Partial Product Width Out = PPIW + 1
// ) mr (
//     .m(m[(m_row_width)-1:0]),    // Multiplicand
//     .q_i(q_i),  // Multiplier

//     .c_i(c_i),  // Carry In
//     .c_o(c_o),  // Carry Out

//     .pp_i(pp_i[(m_row_width)-1:0]), // Partial Product In
//     .pp_0(pp_0[(m_row_width)-1:0])  // Partial Product Out
// );