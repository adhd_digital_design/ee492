
`ifndef rs232_pkg_
`define rs232_pkg_

// DUT firmware
`include "src/data_recovery.sv"
`include "src/dual_port_ram.sv"
`include "src/fifo_ctrl.sv"
`include "src/fifo_n.v"
`include "src/parity.v"
`include "src/power_on_reset.v"
`include "src/rs232.sv"
`include "src/rs232_in_deserializer.sv"
`include "src/rs232_inf.sv"
`include "src/rs232_out_serializer.sv"
`include "src/pulse_generator.sv"
`include "src/rx_sampler.sv"

// Required util firmware
`include "../../firmware/common/counter_wreset.v"
`include "../../firmware/common/shift_reg_n.v"
`include "../../firmware/common/clk_divider.v"
`include "../../firmware/common/debounce.v"

`endif
