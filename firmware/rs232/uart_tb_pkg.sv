
`ifndef uart_tb_pkg_
`define uart_tb_pkg_

// DUT Source Files
`include "src_tb_uart/uart.v"

// Testbench Source Files
`include "src_tb_uart/uart_ports.sv"
`include "src_tb_uart/uart_sb.sv"
`include "src_tb_uart/uart_tb.sv"
`include "src_tb_uart/uart_top.sv"
`include "src_tb_uart/uart_txgen.sv"

`endif