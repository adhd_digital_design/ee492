
`ifndef simple_top_
`define simple_top_

// SIM: Check T_CLOCK after changing.
`timescale 1ns/1ns


// `define LOG_ALL
// `define LOG_TOP
// `define LOG_RX
// `define LOG_RX_SAMPLER
// `define LOG_TX
`define LOG_LOCALPARAM


`define NOISE_PULSE_WIDTH T_SAMPLE*(15/16)

`define ASSERT_RX_STATE(E)                                          \
    assert( uart.rx_in.E == uart.rx_in.state )                      \
    else $warning("Not in expected state (%s). rx_in.state==%s.",    \
                  uart.rx_in.E.name(), uart.rx_in.state.name());

// NOTE: === used to CHECK don't cares (1'bx)
// NOTE: ==? is used to IGNORE don't cares (1'b?) 
// NOTE: the inside keyword is used to IGNORE don't cares (1'bx) 
// http://stackoverflow.com/questions/17179079/is-there-a-ifx-elsex-statement-in-verilog-sv-like-casex
`define ASSERT_SAMPLER_SAMPLE_VALUE_REG(E)                                 \
    assert( uart.rx_in.sampler.sample_value_reg inside {E} )                      \
    else $warning("Not expected value (%b). rx_in.sampler.sample_value_reg==%b.", \
                  E, uart.rx_in.sampler.sample_value_reg);

`define ASSERT_SAMPLER_STATE(E)                                          \
    assert( uart.rx_in.sampler.E == uart.rx_in.sampler.state )                      \
    else $warning("Not in expected state (%s). rx_in.sampler.state==%s.",    \
                  uart.rx_in.sampler.E.name(), uart.rx_in.sampler.state.name());

`define ASSERT_TX_STATE(E)                                          \
    assert( uart.tx_out.E == uart.tx_out.state )                      \
    else $warning("Not in expected state (%s). tx_out.state==%s.",    \
                  uart.tx_out.E.name(), uart.tx_out.state.name());

module simple_top #( ) ( );
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************
localparam CLK0_FREQ = (50) * (10**6);// 50MHz;

localparam HEX_W = 7; // For 7-Seg Displays
localparam PARITY = 1;// 0=none, 1=odd, 2=even // TODO: Make typedef enum
localparam RS232_DATA_WIDTH = 8; // Termite can communicate with 5,6,7,8 bit-data 
localparam BAUD_RATE = 9600; // Hz // 110 Hz -> 9600 -> 115200 Hz tested
localparam RS232_CLK_FREQ = CLK0_FREQ;
localparam RESET_COUNTER_WIDTH = 20; // up counter @ clk_i, power on resets until at maximum count 

localparam SAMPLES_PER_BIT = 16;

// Slow the clock way down.
// localparam RS232_CLK_FREQ = 16;
// localparam BAUD_RATE = 16;

localparam T_CLOCK = 20 ; // (50 Mhz)^-1 = 20 ns
localparam T_BAUD = T_CLOCK * (RS232_CLK_FREQ / BAUD_RATE);
localparam T_SAMPLE = T_BAUD / 16;

// Constants for clarity
localparam START_BIT = 1'b0;
localparam STOP_BIT = 1'b1;

`ifdef LOG_LOCALPARAM
always_comb $display("[%t] %dns\t=T_CLOCK",$time,T_CLOCK);
always_comb $display("[%t] %dns\t=T_BAUD",$time,T_BAUD);

// Check Baud Rate and Clock Rate
localparam CHECK_BAUD_RATE = T_CLOCK * (RS232_CLK_FREQ / T_BAUD);
always_comb begin
    assert(CHECK_BAUD_RATE==BAUD_RATE)$display("[%t] %dhz\t=BAUD_RATE\n[%t] %dhz\t=CHECK_BAUD_RATE",$time,BAUD_RATE,$time,CHECK_BAUD_RATE);
    else $warning("[%t] %dhz\t=BAUD_RATE\n[%t] %dhz\t=CHECK_BAUD_RATE",$time,BAUD_RATE,$time,CHECK_BAUD_RATE);
end
`endif // LOG_LOCALPARAM

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
// Control these during the test.
reg rs232_clk;
reg reset;
reg tx_enable;
reg rx_enable;

// Data:
wire txd_w;
wire rxd_w;
logic rxd_is_loopback;
logic rxd_reg;

// Test the combined unit.
wire loopback;
assign loopback = txd_w;
assign rxd_w = rxd_is_loopback ? loopback : rxd_reg;

// status
wire tx_is_ready;
wire rx_is_ready;
wire framing_error_o;
wire noise_flag_o;
wire parity_error_o;

wire [RS232_DATA_WIDTH-1:0] rx_data;
reg [RS232_DATA_WIDTH-1:0] tx_reg;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************

//****************************************************************************
//                             Sequential logic
//****************************************************************************

//****************************************************************************
//                            Combinational logic
//****************************************************************************

//****************************************************************************
//                            Monitor
//****************************************************************************

`ifdef LOG_TOP
always_comb $display("[%t] %b\t=reset",$time,reset);
always_comb $display("[%t] %b\t=tx_enable",$time,tx_enable);
always_comb $display("[%t] %b\t=rx_enable",$time,rx_enable);
always_comb $display("[%t] %b\t=loopback",$time,loopback);
`endif


`ifdef LOG_TX
always_comb $display("[%t] 0x%h\t=uart.tx_out.is_ready_o",$time,uart.tx_out.is_ready_o);
always_comb $display("[%t] %b\t=uart.tx_out.baud_clk",$time,uart.tx_out.baud_clk);
always_comb $display("[%t] %b\t=uart.tx_out.txd_is_shifted_w",$time,uart.tx_out.txd_is_shifted_w);
always_comb $display("[%t] 0x%h\t=uart.tx_out.tx_sr_o",$time,uart.tx_out.tx_sr_o);
`endif

`ifdef LOG_RX
always_comb $display("[%t] %b\t=uart.rx_in.is_ready_o",$time,uart.rx_in.is_ready_o);
always_comb $display("[%t] %b\t=uart.rx_in.baud_clk",$time,uart.rx_in.baud_clk);
always_comb $display("[%t] 0x%h\t=uart.rx_in.count_bits",$time,uart.rx_in.count_bits);
always_comb $display("[%t] %b\t=uart.rx_in.rxd_is_shifted",$time,uart.rx_in.rxd_is_shifted);
always_comb $display("[%t] 0x%h\t=uart.rx_in.rx_data_o",$time,uart.rx_in.rx_data_o);
always_comb $display("[%t] 0x%h\t=uart.rx_in.rx_sr_w",$time,uart.rx_in.rx_sr_w);
always_comb if(uart.rx_in.parity_error_o) $warning("[%t] 0x%h\t=uart.rx_in.parity_error_o",$time,uart.rx_in.parity_error_o);
always_comb if(uart.rx_in.framing_error_o) $warning("[%t] 0x%h\t=uart.rx_in.framing_error_o",$time,uart.rx_in.framing_error_o);
always_comb if(uart.rx_in.noise_flag_o) $warning("[%t] 0x%h\t=uart.rx_in.noise_flag_o",$time,uart.rx_in.noise_flag_o);
`endif

`ifdef LOG_RX_SAMPLER
always_comb $display("[%t] %b\t=uart.rx_in.sampler.sample_value_reg",$time,uart.rx_in.sampler.sample_value_reg);
always_comb $display("[%t] %b\t=uart.rx_in.sampler.noise_flag_o",$time,uart.rx_in.sampler.noise_flag_o);
always_comb $display("[%t] %b\t=uart.rx_in.sampler.baud_enable_o",$time,uart.rx_in.sampler.baud_enable_o);
always_comb $display("[%t] %b\t=uart.rx_in.sampler.noise_flag_w",$time,uart.rx_in.sampler.noise_flag_w);
`endif

//****************************************************************************
//                      Public Tasks/Functions:
//****************************************************************************

    task tx_byte( input [7:0] tx_i );
        integer ii;

        tx_reg = tx_i; // Set the data.
        #(T_CLOCK) ; // START
        tx_enable = 1'b1;
        rx_enable = 1'b1;
        #(T_CLOCK) ; // START
        tx_enable = 1'b0;
        rx_enable = 1'b0;

        #(T_BAUD - 2*T_CLOCK) ; // @ (posedge uart.tx_out.baud_clk);
        assert( START_BIT == loopback ) else $warning("tx_byte{Expected start bit.}");

        for (ii=0; ii<8; ii=ii+1) begin
            // @ (posedge uart.tx_out.baud_clk);
            #(T_BAUD) ;
            assert( tx_i[ii] == loopback ) else $warning("tx_byte{Wrong data bit.}");
        end

        if( 0 != PARITY ) begin
            // @ (posedge uart.tx_out.baud_clk);
            #(T_BAUD) ;
            assert( uart.tx_out.parity_w == loopback ) else $warning("tx_byte{Expected parity}");
        end

        #(T_BAUD) ; // @ (posedge uart.tx_out.baud_clk);
        assert( STOP_BIT == loopback ) else $warning("tx_byte{Expected stop bit.}");
        assert( 1'b1 == tx_is_ready ) else $warning("tx_byte{Expected tx to be done.}");

        $display("Waiting for tx.is_ready and then rx.is_ready");
        // wait( tx_is_ready );
        wait( rx_is_ready );
        assert( rx_data == tx_i ) else $warning("assert(rx_data == tx_i -> ");

        // @ (posedge rx_in.baud_clk);
        #(T_BAUD) ;
        // assert( rx_in.state==WAIT_FOR_START_STATE ) else $warning("tx_byte{Expected rx to be done.}");
        // case (PARITY)
        //     // 0: $display("tx_byte{No parity to check}");
        //     1: assert( rx_in.rx_parity_w ==  1+$countones(tx_reg)-$ceil($countones(tx_reg)/2)*2 ) else $warning("tx_byte{Odd parity FAILED}"); // odd
        //     2: assert( rx_in.rx_parity_w ==  $countones(tx_reg)-$ceil($countones(tx_reg)/2)*2 ) else $warning("tx_byte{Even parity FAILED}"); // even
        //     default : $error("0x%h=PARITY",PARITY);
        // endcase
        // @ (uart.rx_in.is_ready_o);

    endtask

    task tx_and_check_rx( input [7:0] tx_i );
        rxd_is_loopback = 1'b1;
        rx_start();
        #(T_BAUD); // Make sure there is one stop bit before transmission.
        fork
            tx_start(tx_i);
            rx_wait_for_start();
        join
        rx_wait_for_data(tx_i);
        wait_for_both_ready(tx_i);
    endtask

    task try_special();
        tx_and_check_rx(8'h01);
        tx_and_check_rx(8'h02);
        tx_and_check_rx(8'h80);
        tx_and_check_rx(8'hC0);
        tx_and_check_rx(8'hAA);
        tx_and_check_rx(8'hAF);
        tx_and_check_rx(8'hA0);
    endtask

    task try_random( input int n );
        localparam SEED = 42;
        void'($urandom(SEED));
        for (int ii=0; ii<n; ii=ii+1) begin
            tx_and_check_rx($urandom_range(0,255));
        end
    endtask

    task test_start_bit();
        test_delayed_start_bit(0);
    endtask

    task rx_mock_delayed_start_bit( input int t=0, input int delay_n=0 );
        if(T_BAUD < delay_n) $fatal("Please specify delay_n , such that (T_BAUD > delay_n)");
        #( delay_n ) ; // timing up to this point makes sample_enable occur right on transitions of data, offset to allow more time for data to change
        rx_mock_start_bit(t);
        #( T_BAUD - delay_n ) ; // Get back on track with T_BAUD
    endtask

    task test_delayed_start_bit( input int delay_n=0 );
        rs232_reset();       
        for (int ii=0; ii<=6; ii=ii+1) begin
            $display("");
            rx_mock_delayed_start_bit(ii,delay_n);
            $display("");
            #(11 * T_BAUD);
            rs232_reset();
        end
    endtask

    task main();
        $display("rs232_reset();",);
        rs232_reset();
        $display("test_start_bit();");
        test_start_bit();
        $display("try_special();");
        try_special();
        $display("try_random(10);");
        try_random(10);

        // rx_mock_delayed_start_bit(1,T_SAMPLE/2);
        // rx_mock_start_bit(0);
        // rx_mock_start_bit(1,T_BAUD/16/16); // Delay 1/16 of the sample rate to allow data time to transition
        // test_delayed_start_bit(T_BAUD/16/16); // Delay 1/16 of the sample rate to allow data time to transition

        // Create Figures for Report // do test_delay_start_bit_noise_cases.wave.do.tcl
        // rx_mock_delayed_start_bit(0);
        // rx_mock_delayed_start_bit(1);
        // rx_mock_delayed_start_bit(2);
        // rx_mock_delayed_start_bit(3);
        // rx_mock_delayed_start_bit(4);
        // rx_mock_delayed_start_bit(5);
        // rx_mock_delayed_start_bit(6);
    endtask

//****************************************************************************
//                      Private Tasks/Functions:
//****************************************************************************
    task delay(int n=1);
        repeat(n) #1; // We care about the state right after the clock.
    endtask

    task rs232_reset();
        reset=1'b1;
        @ (posedge rs232_clk); delay();
        `ASSERT_RX_STATE(RESET_STATE);
        `ASSERT_TX_STATE(RESET_STATE);
        // `ASSERT_SAMPLER_STATE(RESET_STATE); // This assertion is not true, because sampler is a sub module and, is one rs232_clk behind rx

        reset=1'b0;
        @ (posedge rs232_clk); delay();
        `ASSERT_RX_STATE(READY_STATE);
        `ASSERT_TX_STATE(READY_STATE);
        `ASSERT_SAMPLER_STATE(RESET_STATE); // data_recovery is held in reset until wait_for_start_STATE

        // Every module should be ready upon leaving RESET_STATE.
        assert( tx_is_ready ) else $error("Expect to be ready after reset");
        assert( rx_is_ready ) else $error("Expect to be ready after reset");
    endtask

    task rx_mock_start_bit( input int t );
        // Will reset after this test.
        // case(t) 0: REAL_START,
        `ASSERT_TX_STATE(READY_STATE);
        `ASSERT_RX_STATE(READY_STATE);

        // Pre signals
        rxd_is_loopback = 1'b0; // use rxd_reg.
        rxd_reg = STOP_BIT;
        rx_enable = 1'b1;
        #T_BAUD ; // These signals for at least T_BAUD
        `ASSERT_RX_STATE(WAIT_FOR_START_STATE);
        `ASSERT_SAMPLER_STATE( LOOK_FOR_START_STATE )

        case(t)

            default: begin : noise_0_fig_9_10
            // 0: begin
                // Figure 9-10. Start Bit - Ideal Case
                // Figure 9-10 shows the details of the ideal case of
                // start-bit recognition. All samples taken at [1] detect
                // logic 1s on the RxD line and correspond to the idle-line
                // time or a stop-bit time prior to this start bit. At [2]
                // a logic-0 sample is preceded by three logic 1 samples.
                // These four samples are called the start-bit qualifiers. The
                // beginning of the start-bit time is tentatively perceived to
                // occur between the third logic 1 sample and the logic
                // 0 sample of the start qualifiers. Next, the samples at RT3,
                // RT5, and RT7 [3] are taken to verify that this bit time is
                // indeed the start bit. The samples at RT8, RT9, and RT10 are
                // called the data samples [4]. In any bit time other than the
                // start bit, these samples would drive a majority voting
                // circuit to determine the logic sense of the bit time. In
                // the special case of the start bit time, the bit value is
                // forced to be 0 independent of what the data samples at RT8,
                // RT9, and RT10 suggest. In this ideal case, the actual start
                // bit and the perceived start bit match. The resolution of
                // the RT clock leads to an uncertainty about the exact
                // placement of the leading edge of the start bit. The
                // uncertainty in the placement of the edge will be
                // one-sixteenth of a bit time.
                `ASSERT_RX_STATE(WAIT_FOR_START_STATE);

                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({16{STOP_BIT}});// Ensure we start with all STOP_BITS

                rxd_reg = STOP_BIT;
                #( 9 * T_SAMPLE ) ;

                rxd_reg = START_BIT;
                #( T_SAMPLE ) ; // RT1
                `ASSERT_SAMPLER_STATE( POTENTIAL_START_STATE )
                #( (16-1) * T_SAMPLE ) ; // RT2-RT16
                `ASSERT_RX_STATE(WAIT_FOR_START_STATE);
                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({16{START_BIT}}); // Ensure we match the table

                rxd_reg = STOP_BIT;
                #( 4 * T_SAMPLE ) ;
                `ASSERT_SAMPLER_STATE(SAMPLE_STATE)
                `ASSERT_RX_STATE(WAIT_TO_SHIFT_STATE);
                assert ( 0 == noise_flag_o ) $info("PASS: Correctly verified noise flag NOT SET for mock_start(0)"); else $fatal("Expected noise flag NOT SET for mock_start(0)");
                assert ( START_BIT == uart.rx_in.rxd_i_checked ) $info("PASS: Correctly received START_BIT for mock_start(0)"); else $fatal("Expected START_BIT for mock_start(0)");


            end

            1: begin : noise_1_fig_9_11
                // Figure 9-11 Start Bit - Noise Case One
                // Figure 9-11 shows what occurs if noise causes a sample to
                // be erroneously detected as a 0 before the actual beginning
                // of the start bit. Logic 0 sample [1] in conjunction with
                // the three preceding samples of logic 1 meet the conditions
                // for start qualification; thus, logic tentatively perceives
                // the start bit as beginning here. Subsequent
                // start-verification samples at RT3 and RT5 [2] are both 1s;
                // therefore, the tentative placement of the start edge is
                // rejected, and the search is restarted. When the sample at
                // the actual beginning of the start bit is detected, the
                // preceding three samples are 1s; the start bit is now
                // perceived to begin here. In this case, the three samples
                // taken at RT3, RT5, and RT7 now verify that the start bit
                // has been found.
                `ASSERT_RX_STATE(WAIT_FOR_START_STATE);

                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({16{STOP_BIT}});// Ensure we start with all STOP_BITS

                rxd_reg = STOP_BIT;
                #( 3 * T_SAMPLE ) ;

                rxd_reg = START_BIT;
                #( 1 * T_SAMPLE ) ;
                `ASSERT_SAMPLER_STATE( POTENTIAL_START_STATE );
                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({{15{1'bx}}, {1{START_BIT}}});// Ensure we match the table 

                rxd_reg = STOP_BIT;
                #( 3 * T_SAMPLE ) ;
                // #( 1 * T_CLOCK ) ;
                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({{12{1'bx}}, {3{STOP_BIT}}, {1{START_BIT}}});// Ensure we match the table
                // #( 1 * T_SAMPLE - T_CLOCK ) ; // Re-sync
                #( 2 * T_SAMPLE ) ;
                `ASSERT_SAMPLER_STATE(LOOK_FOR_START_STATE);
                
                rxd_reg = START_BIT; // RT1-RT16
                #( 16 * T_SAMPLE ) ;
                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({16{START_BIT}}); // Ensure we match the table

                rxd_reg = STOP_BIT;
                #( 1 * T_SAMPLE ) ;
                `ASSERT_SAMPLER_STATE(SAMPLE_STATE); // We provide a baud to rx one T_SAMPLE after the end of bit
                #( 3 * T_SAMPLE ) ;
                `ASSERT_SAMPLER_STATE(SAMPLE_STATE)
                `ASSERT_RX_STATE(WAIT_TO_SHIFT_STATE);
                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({{12{START_BIT}}, {4{STOP_BIT}}}); // Ensure we match the table
                assert ( 0 == noise_flag_o ) $info("PASS: Correctly verified noise flag NOT SET for mock_start(1)"); else $fatal("Expected noise flag NOT SET for mock_start(1)");
                assert ( START_BIT == uart.rx_in.rxd_i_checked ) $info("PASS: Correctly received START_BIT for mock_start(1)"); else $fatal("Expected START_BIT for mock_start(1)");

            end

            2: begin : noise_2_fig_9_12
                // Figure 9-12 Start Bit - Noise Case Two
                // Figure 9-12 is similar to the previous case except noise
                // [1] is now closer to the actual beginning of the start bit.
                // The noise sample and the preceding three 1s meet the start
                // qualification requirements. The start-verification sample
                // at RT3 [2] is 1, which will cause the working NF to be set.
                // The samples at RT5 and RT7 [3] are 0s. Since two out of
                // three of the start-verification samples are correct, the
                // original perceived position for the beginning of the start
                // bit is accepted. The RT clock will roll over from state 16
                // to state 1 [4], and bit-time misalignment [5] will continue
                // for the remainder of this character. (A 1-to-0 transition
                // in the data character would cause the alignment to be
                // readjusted.) Even though the perceived alignment of the
                // serial data to the RT clock is technically incorrect, the
                // data samples at RT8, RT9, and RT10 fall well within the
                // actual bit time. This character would almost certainly be
                // received correctly; however, the NF will be set to inform
                // the user of the questionable character.
                `ASSERT_RX_STATE(WAIT_FOR_START_STATE);

                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({16{STOP_BIT}});// Ensure we start with all STOP_BITS

                rxd_reg = STOP_BIT;
                #( 5 * T_SAMPLE ) ;
                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({{16{1'bx}}, {5{STOP_BIT}}});// Ensure we match the table 

                rxd_reg = START_BIT;
                #( 1 * T_SAMPLE ) ; // RT1 
                `ASSERT_SAMPLER_STATE( POTENTIAL_START_STATE );
                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({{15{1'bx}}, {1{START_BIT}}});// Ensure we match the table 

                rxd_reg = STOP_BIT;
                #( 3 * T_SAMPLE ) ; // RT2-RT4
                `ASSERT_SAMPLER_STATE( POTENTIAL_START_STATE );
                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({{12{1'bx}}, {3{STOP_BIT}}, {1{START_BIT}}});// Ensure we match the table

                rxd_reg = START_BIT;
                #( 1 * T_SAMPLE ) ; // RT5
                assert(uart.rx_in.sampler.noise_flag_w) else $error("Expected NF from RT3 in noise_2");
                #( (16-5) * T_SAMPLE ) ; // RT6-RT16
                #( 1 * T_SAMPLE ) ; // RT1 // We provide a baud to rx one T_SAMPLE after the end of bit
                `ASSERT_SAMPLER_STATE(SAMPLE_STATE); 
                `ASSERT_RX_STATE(WAIT_TO_SHIFT_STATE);
                #( 3 * T_SAMPLE ) ; // RT2-RT4 
                `ASSERT_SAMPLER_STATE(SAMPLE_STATE); 
                `ASSERT_RX_STATE(WAIT_TO_SHIFT_STATE);

                rxd_reg = STOP_BIT;
                #( 4 * T_SAMPLE ); // RT5-RT8 
                `ASSERT_SAMPLER_STATE(SAMPLE_STATE)
                `ASSERT_RX_STATE(WAIT_TO_SHIFT_STATE);
                assert ( 1 == noise_flag_o ) $info("PASS: Correctly verified noise flag was SET for mock_start(2)"); else $fatal("Expected noise flag SET for mock_start(2)");
                assert ( START_BIT == uart.rx_in.rxd_i_checked ) $info("PASS: Correctly received START_BIT for mock_start(2)"); else $fatal("Expected START_BIT for mock_start(2)");

            end

            3: begin : noise_3_fig_9_13
                // Figure 9-13 Start Bit - Noise Case Three
                // Figure 9-13 shows a burst of noise [1] causing two samples
                // to detect erroneous 0s. The sample at RT5 would cause the
                // working NF to set. Even though this example shows
                // a worst-case alignment of perceived bit-time boundaries to
                // actual bit-time boundaries, the data samples taken at RT8,
                // RT9, and RT10 will fall within the actual bit time, and
                // data recovery should still be successful. Perceived
                // bit-time boundary [2] is almost half a bit time too soon;
                // however, the data samples for LSB [3] still fall within the
                // actual LSB bit time. This example is a theoretical case,
                // and such gross noise should never be seen in an actual
                // application. This case is an indication of how tolerant the
                // SCI receiver is to system noise.
                `ASSERT_RX_STATE(WAIT_FOR_START_STATE);

                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({16{STOP_BIT}});// Ensure we start with all STOP_BITS

                rxd_reg = STOP_BIT;
                #( 3 * T_SAMPLE ) ;

                rxd_reg = START_BIT; // RT1
                #( 1 * T_SAMPLE ) ;
                `ASSERT_SAMPLER_STATE( POTENTIAL_START_STATE )

                rxd_reg = STOP_BIT; // RT2
                #( 1 * T_SAMPLE ) ;

                rxd_reg = START_BIT; // RT3
                #( 1 * T_SAMPLE ) ;

                rxd_reg = STOP_BIT;
                #( 3 * T_SAMPLE ) ; // RT4-RT6

                rxd_reg = START_BIT;
                #( (16-6) * T_SAMPLE ) ; // RT7-RT16
                #( 1 * T_SAMPLE ) ; // RT1 // We provide a baud to rx one T_SAMPLE after the end of bit
                assert(uart.rx_in.sampler.noise_flag_o) else $error("Expected NF from RT5 in noise_3"); 
                `ASSERT_SAMPLER_STATE(SAMPLE_STATE);
                `ASSERT_RX_STATE(WAIT_TO_SHIFT_STATE);
                #( (16-10) * T_SAMPLE ) ; // RT2-RT6
                #( 1 * T_SAMPLE ) ; // RT1 // We provide a baud to rx one T_SAMPLE after the end of bit
                `ASSERT_SAMPLER_STATE(SAMPLE_STATE);
                `ASSERT_RX_STATE(WAIT_TO_SHIFT_STATE);

                rxd_reg = STOP_BIT;
                #( 4 * T_SAMPLE ) ;
                `ASSERT_SAMPLER_STATE(SAMPLE_STATE);
                `ASSERT_RX_STATE(WAIT_TO_SHIFT_STATE);
                assert ( 1 == noise_flag_o )$info("PASS: Correctly verified noise flag was SET for mock_start(3)");  else $fatal("Expected noise flag for mock_start(3)");
                assert ( START_BIT == uart.rx_in.rxd_i_checked ) $info("PASS: Correctly received START_BIT for mock_start(3)"); else $fatal("Expected START_BIT for mock_start(3)");
     
            end

            4: begin : noise_4_fig_9_14
                // Figure 9-14. Start Bit - Noise Case Four
                // Figure 9-14 depicts the case of noise causing an erroneous
                // sample of 1 early in the start bit. In this case, the NF
                // would be set due to the 1 at sample RT3. The alignment of
                // the perceived bit-time boundary matches that of the actual
                // bit-time boundary.
                `ASSERT_RX_STATE(WAIT_FOR_START_STATE);

                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({16{STOP_BIT}});// Ensure we start with all STOP_BITS

                rxd_reg = STOP_BIT;
                #( 9 * T_SAMPLE ) ;

                rxd_reg = START_BIT;
                #( 2*T_SAMPLE ) ; // RT1-RT2
                `ASSERT_SAMPLER_STATE( POTENTIAL_START_STATE )

                rxd_reg = STOP_BIT;
                #( 1 * T_SAMPLE ) ; // RT3

                rxd_reg = START_BIT;
                #( 13 * T_SAMPLE ) ; // RT4-RT16

                rxd_reg = STOP_BIT;
                #( 1 * T_SAMPLE ) ; // RT1 // We provide a baud to rx one T_SAMPLE after the end of bit
                assert(uart.rx_in.sampler.noise_flag_o) else $error("Expected NF from RT3 in noise_4");
                #( 3 * T_SAMPLE ) ;
                `ASSERT_SAMPLER_STATE(SAMPLE_STATE);
                `ASSERT_SAMPLER_STATE(SAMPLE_STATE);
                `ASSERT_RX_STATE(WAIT_TO_SHIFT_STATE);
                assert ( 1 == noise_flag_o )$info("PASS: Correctly verified noise flag was SET for mock_start(4)");  else $fatal("Expected noise flag for mock_start(4)");
                assert ( START_BIT == uart.rx_in.rxd_i_checked ) $info("PASS: Correctly received START_BIT for mock_start(4)"); else $fatal("Expected START_BIT for mock_start(4)");
     

            end

            5: begin : noise_5_fig_9_15
                // Figure 9-15. Start Bit - Noise Case Five
                // Figure 9-15 shows a gross burst of noise [1] during the
                // start-verification samples. The two logic-1 samples at RT5
                // and RT7 cause the 1-to-0 transition at the actual beginning
                // of the start bit to be rejected as the perceived start bit.
                // Since there are no more cases of three logic-1 samples in
                // a row [2], the start bit is never detected. Because the
                // circuit could not locate the start bit, the frame will be
                // received as a framing error, be improperly received, or be
                // missed entirely, depending on the data in the frame and
                // when the start search logic synchronized on what it thought
                // was a start bit. This example shows two independent noise
                // incidents that are specifically positioned within a frame.
                // Of the six cases of noise during the start-bit search, this
                // case is the only one that causes incorrect data reception.
                `ASSERT_RX_STATE(WAIT_FOR_START_STATE);

                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({16{STOP_BIT}});// Ensure we start with all STOP_BITS

                rxd_reg = STOP_BIT;
                #( 9 * T_SAMPLE ) ;

                rxd_reg = START_BIT;
                #( 4*T_SAMPLE ) ; // RT1-RT4
                `ASSERT_SAMPLER_STATE( POTENTIAL_START_STATE )
                
                rxd_reg = STOP_BIT;
                #( 1 * T_SAMPLE ) ; // RT5
                rxd_reg = START_BIT;
                #( 1 * T_SAMPLE ) ; // RT6
                rxd_reg = STOP_BIT;
                #( 1 * T_SAMPLE ) ; // RT7

                rxd_reg = START_BIT;
                #( 1 * T_SAMPLE ) ; // RT1
                `ASSERT_SAMPLER_STATE(LOOK_FOR_START_STATE)
                #( 8 * T_SAMPLE ) ; // RT1

                rxd_reg = STOP_BIT;
                #( 4 * T_SAMPLE ) ;

                `ASSERT_SAMPLER_STATE(LOOK_FOR_START_STATE)
                `ASSERT_RX_STATE(WAIT_FOR_START_STATE);
                assert ( 0 == noise_flag_o )$info("PASS: Correctly verified noise flag was NOT SET for mock_start(5)");  else $fatal("Expected noise flag NOT SET for mock_start(5)");
                assert ( STOP_BIT == uart.rx_in.rxd_i_checked ) $info("PASS: Correctly did NOT receive START_BIT for mock_start(5)"); else $fatal("Expected NOT to receive START_BIT for mock_start(5)");

            end

            6: begin : noise_6_fig_9_16
                // Figure 9-16 Start Bit - Noise Case Six
                // The final start-bit case shown in Figure 9-16 shows a burst
                // of noise in the middle of the start-bit time. The noise [1]
                // causes two out of three data samples to be erroneously
                // detected as 1s. The start-bit logic forces this bit to be
                // detected as a 0 even though the majority of samples RT8,
                // RT9, and RT10 suggest it should be 1. The majority of all
                // seven samples taken during the start bit time agree with
                // the forced 0.
                `ASSERT_RX_STATE(WAIT_FOR_START_STATE);

                `ASSERT_SAMPLER_SAMPLE_VALUE_REG({16{STOP_BIT}});// Ensure we start with all STOP_BITS

                rxd_reg = STOP_BIT; // Before start bit.
                #( 9 * T_SAMPLE ) ;

                rxd_reg = START_BIT; // R1-RT7
                #( 7 * T_SAMPLE ) ;
                `ASSERT_SAMPLER_STATE( POTENTIAL_START_STATE )

                rxd_reg = STOP_BIT; // RT8
                #( 1 * T_SAMPLE ) ;

                rxd_reg = START_BIT; // RT9
                #( 1 * T_SAMPLE ) ;

                rxd_reg = STOP_BIT; // RT10
                #( 1 * T_SAMPLE ) ;

                rxd_reg = START_BIT; //RT11-16
                #( 6 * T_SAMPLE ) ;

                rxd_reg = STOP_BIT; // After start bit.

                @ (uart.rx_in.state); // -> START_STATE
                `ASSERT_SAMPLER_STATE(SAMPLE_STATE)
                `ASSERT_RX_STATE(START_STATE);
                assert ( 1 == noise_flag_o ) $info("PASS: Correctly verified noise flag for mock_start(6)"); else $fatal("Expected noise flag for mock_start(6)");
                assert ( START_BIT == uart.rx_in.rxd_i_checked ) $info("PASS: Correctly received START_BIT for mock_start(6)"); else $fatal("Expected START_BIT for mock_start(6)");
            end


        endcase

    endtask

    task tx_disable_for_n_clocks(int n);
        tx_enable = 1'b0;
        repeat(n) begin
            @ (posedge rs232_clk); delay();
            `ASSERT_TX_STATE(READY_STATE);
        end
    endtask

    task tx_start( input [7:0] tx_i );
        // $display("[%t] task tx_start", $time); // TASK_DEBUG
        // Takes 3 T_CLOCK to start. Will transmit START_BIT on next tx_out.baud_clk.

        assert( tx_is_ready ) else $fatal("FAILED Req: tx_is_ready");

        tx_enable = 1'b0; // Disable the tx module
        tx_reg = tx_i; // Set the data.
        @ (posedge rs232_clk); delay(); // -> READY_STATE
        `ASSERT_TX_STATE(READY_STATE);
        assert( tx_i == tx_reg ) else $warning("FAILED tx_data->tx_reg");

        // #(T_SAMPLE * 4) ;// must be disabled so we send START_BIT after rx is ready.

        tx_enable = 1'b1;
        @ (posedge rs232_clk); delay(); // -> START_STATE
        `ASSERT_TX_STATE(START_STATE);
        // assert( ! tx_is_ready ) else $warning("FAILED !tx_is_ready"); // TODO

        tx_enable = 1'b0;
        @ (posedge rs232_clk); delay(); // -> WAIT_FOR_SHIFT
        assert( tx_i                    == uart.tx_out.tx_sr_i[1 +:RS232_DATA_WIDTH] ) else $warning("FAILED tx_data->tx_sr");
        assert( uart.tx_out.START_BIT   == uart.tx_out.tx_sr_i[0] ) else $warning("MISSING START_BIT");
        assert( uart.tx_out.STOP_BIT    == uart.tx_out.tx_sr_i[uart.tx_out.TOTAL_WIDTH-1] ) else $warning("MISSING STOP_BIT");
        assert( uart.tx_out.START_BIT   == uart.tx_out.txd_o ) else $warning("MISSING START_BIT");

        // $display("[%t] endtask tx_start", $time); // TASK_DEBUG
    endtask

    task rx_start();
        assert( rx_is_ready ) else $fatal("FAILED Req: rx_is_ready");

        rx_enable = 1'b0;
        @ (posedge rs232_clk); delay(); // -> READY_STATE
        `ASSERT_RX_STATE(READY_STATE);
        `ASSERT_SAMPLER_STATE(RESET_STATE); // voting is one clock cycle behind rx

        rx_enable = 1'b1;
        @ (posedge rs232_clk); delay(); // -> WAIT_FOR_START_STATE
        `ASSERT_RX_STATE(WAIT_FOR_START_STATE);
        `ASSERT_SAMPLER_STATE(RESET_STATE); // voting is one clock cycle behind rx

        @ (posedge rs232_clk); delay(); // -> LOOK_FOR_START_STATE
        `ASSERT_SAMPLER_STATE(LOOK_FOR_START_STATE); // voting is one clock cycle behind rx

        rx_enable = 1'b0;
    endtask

    task rx_wait_for_start();
        // $display("[%t] task rx_wait_for_start", $time); // TASK_DEBUG
        // Enables rx. Waits for start. and then shifts that first bit in.
        // Precondition:
        `ASSERT_SAMPLER_STATE(LOOK_FOR_START_STATE); // voting is one clock cycle behind rx


        @ (uart.rx_in.state); delay(); // -> START_STATE
        `ASSERT_RX_STATE(START_STATE);
        assert( START_BIT == uart.rx_in.rxd_i_checked ) else $warning("Expecting start bit on rxd_i_checked");

        // TODO Decide which of these two:
        // START->WAIT
        // @ (posedge rs232_clk); delay(); // -> WAIT_TO_SHIFT_STATE // data_recovery doesn't work with this, because baud is not generated on time for first bit
        // START->SHIFT
        // @ (posedge rs232_clk); delay(); // -> SHIFT_STATE
        @ (uart.rx_in.state); delay(); // -> SHIFT_STATE


        `ASSERT_RX_STATE(SHIFT_STATE);
        assert( 0 == uart.rx_in.count_bits ) else $warning("0 != count_bits");
        assert( START_BIT == uart.rx_in.rxd_i_checked ) else $warning("Expecting start bit on rxd_i_checked");
        // assert( START_BIT == loopback ) else $warning("Expecting start bit on loopback");

        @ (posedge rs232_clk); delay(); // -> WAIT_TO_SHIFT
        `ASSERT_RX_STATE(WAIT_TO_SHIFT_STATE);
        assert( 1 == uart.rx_in.count_bits ) else $warning("1 != count_bits");
        assert( START_BIT  == uart.rx_in.rx_sr_w[ uart.rx_in.TOTAL_WIDTH -1 ] ) else $warning("Didn't shift in START_BIT");
        // assert( START_BIT == loopback ) else $warning("Expecting start bit on loopback");

        // $display("[%t] endtask rx_wait_for_start_state", $time); // TASK_DEBUG
    endtask

    task rx_wait_for_data( input [7:0] expected );
        integer ii;
        for (ii=0; ii<8; ii=ii+1) begin
            automatic bit now = expected[ii];
            // assert( now == loopback ) else $warning("expected[%u]!=loopback", ii); // TODO
            @ (posedge uart.tx_out.baud_clk); delay();
            // assert( now == loopback ) else $warning("expected[%u]!=next_loopback", ii); // TODO
            // assert( now == uart.rx_in.rxd_i_checked ) else $warning("expected[%u]==!=rxd_i_checked", ii);
            // assert( now == uart.rx_in.rx_sr_w[ uart.rx_in.TOTAL_WIDTH -1 ] ) else $warning("expected[%u]!=rx_sr_w[msb]", ii);
        end
    endtask

    task wait_for_both_ready( input [7:0] expected );
        wait(tx_is_ready);
        wait(rx_is_ready); delay();
        #(T_BAUD); // TODO: Bug fix. rx_is_ready is too early.
        assert ( expected == rx_data )
            $info("PASS: Correctly received (%u)", rx_data);
            else $warning("Received (%u) not (%u)", rx_data, expected);
        assert ( 0 == framing_error_o ) else $error("Unexpected framing error");
        assert ( 0 == noise_flag_o ) else $error("Unexpected noise flag");
        assert ( 0 == parity_error_o ) else $error("Unexpected parity error");

        $display("\n"); // Put space between transmissions.
    endtask

//****************************************************************************
//                             Procedural Blocks
//****************************************************************************

    initial begin
        $display("top::initial");
        $system("date +%s%N");
        $dumpfile("transcript.rs232.vcd");
        $dumpvars();

        rs232_clk=0;
        reset=1'b0;
        tx_reg={RS232_DATA_WIDTH{1'b0}};
        rxd_reg = 1'b0;
        rxd_is_loopback = 1'b1;
        rx_enable = 1'b0;
        tx_enable = 1'b0;

        main();
        $finish();
    end

    final begin
        $display("top::final");
        $system("date +%s%N");
    end

    always begin
        #(T_CLOCK/2) rs232_clk = ~rs232_clk; // Toggle at half the desired period.
    end


//****************************************************************************
//                              Internal Modules
//****************************************************************************

rs232_inf #(
    .DATA_WIDTH(RS232_DATA_WIDTH)
) inf (
    .clk(rs232_clk),
    .reset(reset),

    // Data:
    .tx_data(tx_reg),
    .rx_data(rx_data),
    .tx_out(txd_w),
    .rx_in(rxd_w),

    // Control:
    .tx_enable(tx_enable),
    .rx_enable(rx_enable),

    // Status:
    .rx_is_ready(rx_is_ready),
    .tx_is_ready(tx_is_ready),

    .framing_error_o(framing_error_o),
    .noise_flag_o(noise_flag_o),
    .parity_error_o(parity_error_o)
);

rs232 #(
    .CLK_FREQ(RS232_CLK_FREQ),// 50MHz
    .PARITY(PARITY),    // 0=none, 1=odd, 2=even // TODO: Make typedef enum
    .DATA_WIDTH(RS232_DATA_WIDTH),
    .BAUD_RATE(BAUD_RATE),
    .FIFO_ADDR_WIDTH(0)
) uart (
    .ports(inf)
    , .tx_debug_o()
    , .rx_debug_o()
);

endmodule

`endif
