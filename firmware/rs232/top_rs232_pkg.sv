
`ifndef top_rs232_pkg_
`define top_rs232_pkg_

// Device generic top wrapper module
`include "rs232_top.v"

// Required util firmware
`include "../../firmware/common/HexOn7Seg.v"
`include "../../firmware/common/display_on_hex.v"

`endif