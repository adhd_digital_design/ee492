
`ifndef rs232_inf_
`define rs232_inf_

interface rs232_inf #(
    parameter DATA_WIDTH = 8
) (
    input wire clk,
    input wire reset,

    // Data:
    input wire [DATA_WIDTH-1:0] tx_data,
    output reg [DATA_WIDTH-1:0] rx_data,
    output wire tx_out,
    input wire rx_in,

    // Control:
    input wire tx_enable,
    input wire rx_enable,

    // Status:
    output wire rx_is_ready,
    output wire tx_is_ready,

    output wire framing_error_o,
    output wire noise_flag_o,
    output wire parity_error_o

    // Debug:
);
endinterface

// TODO: Use queue
// input wire rx_pop_back, // On clock, dequeue new value into "rx_data".
// input wire tx_push_front, // On clock, queue "tx_data" to be sent.
// output wire tx_is_empty,
// output wire rx_is_empty,


`endif

