`ifndef rs232_ports_
`define rs232_ports_

interface rs232_ports #(
    parameter RX_DATA_WIDTH = 8,
    parameter TX_DATA_WIDTH = 8
) (
 output   logic                     reset          ,
 input    wire                      txclk          ,
 output   logic                     ld_tx_data     , // TODO FIFO ?
 output   logic [TX_DATA_WIDTH-1:0] tx_data        ,
 output   logic                     tx_enable      ,
 output   logic                     tx_out         ,
 // output   logic                     tx_is_ready     ,
 input    wire                      tx_empty       , // TODO: use tx_serializer.is_ready to pop tx_data from fifo, @ posedge tx_serializer.is_ready
 input    wire                      rxclk          ,
 output   logic                     uld_rx_data    , // TODO FIFO ?
 input    wire  [RX_DATA_WIDTH-1:0] rx_data        ,
 output   logic                     rx_enable      ,
 output   logic                     rx_in          ,
 // output   logic                     rx_is_ready     ,
 input    wire                      rx_empty       , // TODO: use rx_deserializer.is_ready to push data to fifo, @ posedge rx_deserializer.is_ready
 output   logic                     loopback       , 
 output   logic                     rx_tb_in       
);
endinterface

`endif