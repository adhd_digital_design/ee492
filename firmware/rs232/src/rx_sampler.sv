`ifndef rx_sampler_
`define rx_sampler_

module rx_sampler (
    input wire clk_i,
    input wire reset_i,
    input wire sample_i,
    input wire sync_baud_now_i,

    input  wire rxd_i,
    output reg  rxd_o,
    output reg  noise_flag_o,
    output wire is_ready_o
);

//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
reg [3:0] count_samples;
reg [2:0] voters;
reg baud_now;
reg baud_previous;
wire baud_clk;

//****************************************************************************
//                             Sequential logic
//****************************************************************************
always @(posedge clk_i) begin : clk_countsamples_0_to_15
    // Count 0 -> 7 and 8->15 // Will automatically rollover to 0 after 15.
    if(reset_i)                 count_samples = 4'h0;
    else if(sync_baud_now_i)    count_samples = 4'h7; // Start halfway (before voting).
    else if(sample_i)           count_samples = count_samples+4'h1;
    else                        count_samples = count_samples;
end

always @(posedge clk_i) begin : detect_edge_of_baud
    if( reset_i || sync_baud_now_i ) begin
        baud_previous <= 1'b1;
        baud_now <= 1'b1;
    end
    else begin
        baud_previous <= baud_now;  // Shift now to previous.
        baud_now <= baud_clk;       // Update current.
    end
end

always @(posedge clk_i) begin : rxd_vote_2_of_3
    if( reset_i ) rxd_o <= 1'b1;
    else begin
        case(count_samples) // 1 if 2 voters (of 3) are 1.
            9:         rxd_o <= ( 2 <= voters[0]+voters[1]+voters[2] );
            default:    rxd_o <= rxd_o;
        endcase
    end
end

always @(posedge clk_i) begin : sample_voters
    if( reset_i ) voters <= 3'b111;
    else begin
        voters <= voters; // Default to last value unless specified. NonBlocking is key.
        case(count_samples)
            7: voters[0] <= rxd_i;
            8: voters[1] <= rxd_i;
            9: voters[2] <= rxd_i;
        endcase
    end
end

//****************************************************************************
//                            Combinational logic
//****************************************************************************
assign baud_clk = ~count_samples[3]; // 1 for 0->7, and 0 for 8->15
assign is_ready_o = (~baud_previous) & (baud_now); // ready on posedge.
assign noise_flag_o = !( &(voters) || !(voters) ); // Not all 1 or all 0

endmodule

`endif
