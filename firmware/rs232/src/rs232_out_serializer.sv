//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: RS232, Shift out a byte of data through txd at a specific BAUD_RATE.
//Date Created : 2017-02-25
// Referenced :
// https://github.com/FPGAwars/FPGA-peripherals/blob/master/uart-tx/uart_tx.v
// https://fpga4fun.com/SerialInterface3.html

`ifndef rs232_out_serializer_
`define rs232_out_serializer_

// SIM: Check T_CLOCK after changing.
`timescale 1ns/1ns


module rs232_out_serializer #(
    parameter DATA_WIDTH = 8,
    parameter CLK_FREQ = (50) * (10**6), // Hz
    parameter BAUD_RATE = 115200,        // Hz
    parameter PARITY = 0                 // 0=none, 1=odd, 2=even
) (
    input wire clk,
    input wire reset,
    // Data:
    output reg txd_o,
    input wire [DATA_WIDTH-1 :0] tx_data_i,
    // Control;
    input wire tx_enable_i,
    // Status:
    output reg is_ready_o
    // Debug:
    , output wire [7:0] debug_o
);

//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

localparam TOTAL_WIDTH = DATA_WIDTH +       // Original data width.
                        (1 -(0==PARITY)) +  // Optional parity bit.
                        2 ;                 // START and STOP bit.

localparam START_BIT = 1'b0; // Constants for clarity
localparam STOP_BIT = 1'b1; // Constants for clarity

// FSM
localparam RS232_OUT_DESERIALIZER_STATE_WIDTH = 3;
typedef enum reg [RS232_OUT_DESERIALIZER_STATE_WIDTH-1:0] {
    RESET_STATE           = 3'h0 // Reset all registers / modules.
    ,START_STATE          = 3'h1 // Initialization. Setup tx_sr with data and framing bits.
    ,READY_STATE          = 3'h2 // Waiting for enable.
    ,WAIT_TO_SHIFT_STATE  = 3'h3 // Waiting for next baud clock.
    ,SHIFT_STATE          = 3'h4 // Prepare new byte for Tx.
    ,ERROR_STATE          = 3'h5 // Provide noticeable output and then reset.
} rs232_out_deserializer_state_t; 

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

wire baud_clk;
reg  shift_en;
reg  load_en;
wire txd_is_shifted_w;
wire parity_w;
reg  reset_data;
reg  reset_baud_clk;
wire tx_sr_lsb;

wire [TOTAL_WIDTH -1:0] tx_sr_o;
wire [TOTAL_WIDTH -1:0] tx_sr_i;

rs232_out_deserializer_state_t state;
rs232_out_deserializer_state_t next_state;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************

always @(*) begin : comb_next_state_logic
    case(state)

    // Idle in READY_STATE until enabled. Then go to START_STATE.
    READY_STATE: next_state <= ( tx_enable_i ? START_STATE : state );

    // Prepare tx_sr where LSB will be START_BIT.
    START_STATE:  next_state <= WAIT_TO_SHIFT_STATE;

    WAIT_TO_SHIFT_STATE: begin
        // Only transition on baud_clk. If we are done then go to READY_STATE.
        if(baud_clk)    next_state <= ( txd_is_shifted_w ? READY_STATE : SHIFT_STATE );
        else            next_state <= state;
    end

    // Only shift for single pulse.
    SHIFT_STATE: next_state <= WAIT_TO_SHIFT_STATE;

    RESET_STATE:  next_state <= READY_STATE;
    ERROR_STATE:  next_state <= RESET_STATE;
    default:      next_state <= ERROR_STATE;

    endcase
end

//****************************************************************************
//                             Sequential logic
//****************************************************************************

always @(posedge clk) begin
    if(reset)                   state <= RESET_STATE;
    else                        state <= next_state;
end

//****************************************************************************
//                            Combinational logic
//****************************************************************************

assign txd_is_shifted_w = tx_sr_o[0] & !tx_sr_o[TOTAL_WIDTH-1:1]; // tx_sr is all zeros with a STOP_BIT on txd_o

generate
    case(PARITY)
    0:          assign  tx_sr_i = {STOP_BIT,           tx_data_i, START_BIT};
    default:    assign  tx_sr_i = {STOP_BIT, parity_w, tx_data_i, START_BIT};
    endcase
endgenerate

always @(*) begin : comb_load_en // Moore
    case(state)
    START_STATE:          load_en <= 1'b1;
    default:              load_en <= 1'b0;
    endcase
end

always @(*) begin : comb_shift_en // Moore
    case(state)
    SHIFT_STATE:          shift_en <= 1'b1;
    default:              shift_en <= 1'b0;
    endcase
end

always @(*) begin : comb_reset_data // Moore
    case(state)
    RESET_STATE:          reset_data <= 1'b1;
    default:              reset_data <= 1'b0;
    endcase
end

always @(*) begin : comb_reset_baud_clk // Moore
    case(state)
    READY_STATE:          reset_baud_clk <= 1'b1;
    START_STATE:          reset_baud_clk <= 1'b1;
    RESET_STATE:          reset_baud_clk <= 1'b1;

    default:              reset_baud_clk <= 1'b0;
    endcase
end

// Output Signals

always @(*) begin : comb_ready_flag // Moore
    case(state)
    READY_STATE:  is_ready_o <= 1'b1;
    default:      is_ready_o <= 1'b0;
    endcase
end

always @(*) begin : comb_txd_o // Mealy
    case(state)
    WAIT_TO_SHIFT_STATE:  txd_o <= tx_sr_lsb;
    SHIFT_STATE:          txd_o <= tx_sr_lsb;

    default:              txd_o <= STOP_BIT; // Prevent false START_BITs.
    endcase
end

assign debug_o = {
    state, // assume 3 bits.
    shift_en,
    load_en,
    baud_clk,
    1'b0
};

// always_comb $display("[%t] %s=tx_out.state",$time,state.name());
// always_comb $display("[%t] %s=tx_out.next_state",$time,next_state.name());

//****************************************************************************
//                              Internal Modules
//****************************************************************************

shift_reg_n #(
    .n(TOTAL_WIDTH)
) tx_sr (
    .clk(clk),
    .load(load_en),
    .reset(reset_data),
    .value_i(tx_sr_i),
    .shift(shift_en),
    .s_i( !STOP_BIT ), // Register will be all 0s when shift is done. Note STOP_BIT=1.
    .s_o(tx_sr_lsb),
    .value_o(tx_sr_o)
);

pulse_generator #(
    .CLK_FREQ_IN(CLK_FREQ)
    ,.PULSE_FREQ_OUT(BAUD_RATE)
) gen_baud_pulse (
    .clk_i(clk)
    ,.reset_i(reset_baud_clk)
    ,.pulse_o(baud_clk) // Pulse immediately after reset
);


generate // Conditionally do parity if requested.
    if( 0 != PARITY ) begin
        parity #(
            .N(DATA_WIDTH),
            .TYPE(PARITY)
        ) rx_parity (
            .data_i(tx_data_i), // {stop [, parity], data, start} // Parity is only done on the data
            .parity_o(parity_w)
        );
    end
endgenerate

endmodule

// // Example Instantiation
// rs232_out_serializer #(
//     .DATA_WIDTH(DATA_WIDTH),
//     .CLK_FREQ(CLK_FREQ),
//     .BAUD_RATE(BAUD_RATE),
//     .PARITY(PARITY) // 0=none, 1=odd, 2=even
// ) rx_out (
//     .clk(clk),
//     .reset(reset),

//     // Data:
//     .txd_o(txd_o),
//     .tx_data_i(tx_data_i),//     input  wire [DATA_WIDTH-1 :0] tx_data_i,

//     // Control;
//     .tx_enable_i(tx_enable_i),

//     // Status:
//     .is_ready_o(is_ready_o)
// );
`endif
