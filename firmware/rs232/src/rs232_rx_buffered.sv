//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: RS232
//Date Created : 2017-02-28

`ifndef rs232_rx_buffered_
`define rs232_rx_buffered_

// SIM: Check T_CLOCK after changing.
`timescale 1ns/1ns

// Have compilier optimize state values  // TODO: Create package
typedef enum reg [3-1:0] {
    RESET_STATE             = 3'h0 // Reset all registers / modules.
    ,READY_STATE            = 3'h1 // I have more data. Waiting for enable to update.
    ,BUSY_STATE             = 3'h2 // Shifting to rx_data from buffer.
    ,EMPTY_STATE            = 3'h3 // No data to shift.    
    ,ERROR_STATE            = 3'h4 // Should never get to this state
} rs232_rx_state_t; 

module rs232_in_buffered #(
    parameter CLK_FREQ              = (50) * (10**6),// 50MHz
    parameter PARITY                = 1,// 0=none, 1=odd, 2=even // TODO: Make typedef enum
    parameter DATA_WIDTH            = 8,// Termite can communicate with 5,6,7,8 bit-data 
    parameter BAUD_RATE             = 115200,
    parameter FIFO_ADDR_WIDTH       = 1
) (
    input wire clk,
    input wire reset,

    // Data:
    input wire rxd_i,
    output wire [DATA_WIDTH-1 :0] rx_data_o,

    // Control:
    input rx_enable_i,

    // Status:
    output reg is_ready_o,
    output reg framing_error_o, // FE
    output wire noise_flag_o,   // NF
    output reg parity_error_o   // PE

    // Debug:
    , output wire [7:0] debug_o
);

//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

rs232_rx_state_t state;
rs232_rx_state_t next_state;

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
wire [DATA_WIDTH-1:0] rx_last;
wire internal_enable_w;
wire queue_empty_w;
wire internal_is_ready_w;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************
always @(*) begin
    case(state)

        READY_STATE: begin
            if(ports.rx_enable) next_state <= BUSY_STATE;
            else            next_state <= state;
        end

        BUSY_STATE: begin
            if(queue_empty_w)               next_state <= EMPTY_STATE;
            else if(internal_is_ready_w)    next_state <= READY_STATE;
            else                            next_state <= state;
        end

        EMPTY_STATE: begin
            if(!queue_empty_w)  next_state <= READY_STATE;
            else                next_state <= state;
        end

        RESET_STATE: next_state <= READY_STATE;
        ERROR_STATE: next_state <= RESET_STATE;
        default:     next_state <= ERROR_STATE;

    endcase
end

//****************************************************************************
//                             Sequential logic
//****************************************************************************

always @(posedge ports.clk) begin
    if(ports.reset)   state <= RESET_STATE;
    else        state <= next_state;
end

// always @(posedge ports.clk) begin : clk_rx_data
//     if(ports.rx_is_ready)    rx_reg = rx_last;
//     else                     rx_reg = rx_reg; // latch
// end

//****************************************************************************
//                            Combinational logic
//****************************************************************************

//****************************************************************************
//                             Procedural Blocks
//****************************************************************************

//****************************************************************************
//                              Internal Modules
//****************************************************************************

rs232_in_deserializer #(
    .DATA_WIDTH(DATA_WIDTH),
    .CLK_FREQ(CLK_FREQ),
    .BAUD_RATE(BAUD_RATE),
    .PARITY(PARITY) // 0=none, 1=odd, 2=even
) rx (
    .clk(ports.clk),
    .reset(ports.reset),
    // Data:
    .rxd_i(rxd_in),
    .rx_data_o(rx_last),
    // Control:
    .rx_enable_i(internal_enable_w),
    // Status:
    .is_ready_o(internal_is_ready_w),
    .framing_error_o(ports.framing_error_o), // FE
    .noise_flag_o(ports.noise_flag_o), // NF
    .parity_error_o(ports.parity_error_o) // PE
    // Debug:
    , .debug_o(debug_o)
);

fifo_n #(
    .DATA_WIDTH(DATA_WIDTH), // number of bits in a word
    .ADDR_WIDTH(FIFO_ADDR_WIDTH)  // number of address bits
) rx_fifo (
    .clk(ports.clk)
    ,.reset(ports.reset)
    // Control
    ,.queue_req(rx_fifo_queue) // Write Request // Queue   // Push
    ,.dequeue_req(ports.rx_pop_back) // Read Data     // Dequeue // Pop
    // Data:
    ,.data_i(rx_fifo_data_i)
    ,.data_o(rx_fifo_data_o)
    // Flags:
    ,.is_empty(rx_fifo_is_empty)
    ,.is_full(rx_fifo_is_full)
);

endmodule

`endif
