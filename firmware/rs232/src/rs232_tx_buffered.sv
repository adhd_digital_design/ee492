//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: RS232
//Date Created : 2017-02-28
// Referenced :
// https://github.com/FPGAwars/FPGA-peripherals/blob/master/uart-tx/uart_tx.v
// https://fpga4fun.com/SerialInterface3.html

`ifndef rs232_tx_buffered_
`define rs232_tx_buffered_

// SIM: Check T_CLOCK after changing.
`timescale 1ns/1ns

// Have compiler optimize state values // TODO: Create package
typedef enum reg [3-1:0] {
    RESET_STATE             = 3'h0 // Reset all registers / modules.
    ,READY_STATE            = 3'h1 // Waiting for enable.
    ,BUSY_STATE             = 3'h2 // Shifting from tx_data.
    ,FULL_STATE             = 3'h3 // Can't accept more data.
    ,ERROR_STATE            = 3'h4 // Should never get to this state
} rs232_tx_state_t; 

module rs232_tx_buffered #(
    parameter CLK_FREQ              = (50) * (10**6),// 50MHz
    parameter PARITY                = 1,// 0=none, 1=odd, 2=even // TODO: Make typedef enum
    parameter DATA_WIDTH            = 8,// Termite can communicate with 5,6,7,8 bit-data 
    parameter BAUD_RATE             = 115200,
    parameter FIFO_ADDR_WIDTH       = 1
) (
    rs232_inf ports
    , output wire [7:0] tx_debug_o
);

//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

rs232_tx_state_t state;
rs232_tx_state_t next_state;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************

always @(*) begin : comb_next_state_logic
    case(state)

        READY_STATE: begin
            if(tx_enable_i)  next_state <= START_STATE;
            else             next_state <= state;
        end

        BUSY_STATE: begin
            if(tx_enable_i)  next_state <= START_STATE;
            else             next_state <= state;
        end

        RESET_STATE:  next_state <= READY_STATE;
        ERROR_STATE:  next_state <= RESET_STATE;
        default:      next_state <= ERROR_STATE;

    endcase
end

//****************************************************************************
//                             Sequential logic
//****************************************************************************

always @(posedge clk) begin
    if(reset)                   state <= RESET_STATE;
    else                        state <= next_state;
end

//****************************************************************************
//                            Combinational logic
//****************************************************************************


// Moore
always @(*) begin : comb_load_en
    case(state)
    START_STATE:          load_en <= 1'b1;
    default:              load_en <= 1'b0;
    endcase
end


//****************************************************************************
//                              Internal Modules
//****************************************************************************

rs232_out_serializer #(
    .DATA_WIDTH(DATA_WIDTH),
    .CLK_FREQ(CLK_FREQ),
    .BAUD_RATE(BAUD_RATE),
    .PARITY(PARITY) // 0=none, 1=odd, 2=even
) tx (
    .clk(ports.clk),
    .reset(ports.reset),
    // Data:
    .txd_o(ports.tx_out),
    .tx_data_i(tx_next),
    // Control;
    .tx_enable_i(ports.tx_enable),
    // Status:
    .is_ready_o(ports.tx_is_ready)
    // Debug:
    , .debug_o(tx_debug_o)
);

// // TODO Use tx_fifo
// fifo_n #(
//     .DATA_WIDTH(DATA_WIDTH), // number of bits in a word
//     .ADDR_WIDTH(FIFO_ADDR_WIDTH)  // number of address bits
// ) tx_fifo (
//     .clk(clk)
//     ,.reset(reset)
//     // Control
//     ,.queue_req(tx_fifo_queue) // Write Request // Queue   // Push
//     ,.dequeue_req(tx_fifo_dequeue) // Read Data     // Dequeue // Pop
//     // Data:
//     ,.data_i(tx_fifo_data_i)
//     ,.data_o(tx_fifo_data_o)
//     // Flags:
//     ,.is_empty(tx_fifo_is_empty)
//     ,.is_full(tx_fifo_is_full)
// );

endmodule

// Example Instantiation // TODO:

`endif






