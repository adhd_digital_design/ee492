
// Uses "clk_divider" to generate a pulse at a frequency.
module pulse_generator # (
    parameter CLK_FREQ_IN = (50) * (10**6), // Hz
    parameter PULSE_FREQ_OUT = 115200/16   // Hz
    // TODO: parameter for POSEDGE/NEGEDGE
) (
    input wire clk_i,
    input wire reset_i,
    output wire pulse_o
);
//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
wire slow_clk_w;
reg now_reg;
reg previous_reg;

//****************************************************************************
//                             Sequential logic
//****************************************************************************
always @(posedge clk_i) begin
    if( ! reset_i) begin
        previous_reg    <= now_reg; // Shift now to previous_reg;
        now_reg         <= slow_clk_w;   // Update current.
    end
    else begin // Ensure we don't detect edge on reset.
        previous_reg    <= slow_clk_w;
        now_reg         <= slow_clk_w;
    end
end

//****************************************************************************
//                            Combinational logic
//****************************************************************************
assign pulse_o = (~previous_reg) & (now_reg); // posedge
// assign clk_negedge_o = (previous_reg) & (~now_reg); // negedge

//****************************************************************************
//                              Internal Modules
//****************************************************************************
clk_divider #(
    .in_clk_freq_hz( CLK_FREQ_IN  ),
    .out_clk_freq_hz( PULSE_FREQ_OUT ),
    .sig_active_state( 1'b0 ) // 1'b0 means to start HIGH and go LOW
) clock_divider (
    .in_clk(clk_i),
    .in_reset(reset_i),
    .out_clk(slow_clk_w)
);

endmodule

