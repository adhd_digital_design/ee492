//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: RS232
//Date Created : 2017-02-25

`ifndef data_recovery_
`define data_recovery_

module data_recovery # ( // TODO: Rename START Bit Detect and Data Recovery
    parameter CLK_FREQ = (50) * (10**6), // Hz
    parameter BAUD_RATE = 115200         // Hz
) (
    input  wire clk,
    input  wire reset_i,
    input  wire wait_for_start_state_i, 
    input  wire rxd_i,
    output reg  rxd_o,
    output reg  noise_flag_o,
    output wire baud_enable_o
);

// generate
// if ( SAMPLES_PER_BIT != 16 ) begin // TODO Generalize and constrain parameter
//     // Error (12006): Node instance "assert_samples_per_bit_not_equal_to_16" instantiates undefined entity "EXCEPTION_DATA_RECOVERY"
//     EXCEPTION_DATA_RECOVERY assert_samples_per_bit_not_equal_to_16();
// end
// endgenerate

//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************
localparam START_BIT = 1'b0; // Constants for clarity
localparam STOP_BIT  = 1'b1; // Constants for clarity

localparam START_DETECT_SIZE = 4; // 3xSTOP_BIT +1xSTART_BIT
localparam LAST_CHECK_BIT_BEFORE_START = 6; // Zero indexed

localparam SAMPLE_WIDTH = $clog2(SAMPLES_PER_BIT);// $clog2 = round_up(log_2(SAMPLES_PER_BIT))

localparam SAMPLES_PER_BIT = 16;

// FSM
typedef enum reg [3-1:0] {
     RESET_STATE          = 3'h0 // Reset all registers / modules.
    ,LOOK_FOR_START_STATE        // Vote before start
    ,POTENTIAL_START_STATE       // Vote before start, transition if vote passes
    ,SAMPLE_STATE                // Sample data and perform voting
    ,ERROR_STATE                
} data_recovery_state_t; 

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

wire [START_DETECT_SIZE -1:0] previous_bits_w;
// Initialize to START_BIT , requires the most change and will be more likely to properly detect a start
// wire [START_DETECT_SIZE -1:0] previous_bits_initial_state = {START_DETECT_SIZE{START_BIT}}; // Todo: remove

data_recovery_state_t state ;
data_recovery_state_t next_state ;

wire reset_w = (RESET_STATE == state);
wire rxd_potential_start_bit_detected;
wire sample_enable;
logic rxd_actual_start_bit_detected;
logic rxd_early_start_bit_rejection;
reg  reset_sample_enable;
reg  reset_counter;
reg  baud_now;
reg  baud_previous;
reg  rxd_o_voting;
wire noise_flag_w;
reg  reset_previous_bits;
// sample_value_reg is zero Indexed
reg  [SAMPLES_PER_BIT-1:0]  sample_value_reg;
wire [SAMPLE_WIDTH-1:0]     counter_value_w;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************

always @(*) begin : comb_next_state_logic
    case(state)

        RESET_STATE:  begin
            if(wait_for_start_state_i)              next_state <= LOOK_FOR_START_STATE;
            else                                    next_state <= RESET_STATE; // Hold in reset until wait_for_start_state
        end

        // TODO: No else case
        LOOK_FOR_START_STATE: if( rxd_potential_start_bit_detected ) next_state <= POTENTIAL_START_STATE; // Done Sampling

        POTENTIAL_START_STATE:  begin
            if(rxd_actual_start_bit_detected) begin
                // Continue to SAMPLE_STATE, start bit confirmed.
                if( baud_enable_o )     next_state <= SAMPLE_STATE;
                else                    next_state <= POTENTIAL_START_STATE; // = state
            end
            // We have sampled enough bits to know its NOT a start bit.
            else if( LAST_CHECK_BIT_BEFORE_START < counter_value_w )    next_state <= LOOK_FOR_START_STATE;
            else if( 4 < counter_value_w ) begin
                if(rxd_early_start_bit_rejection)   next_state <= LOOK_FOR_START_STATE;
                else                                next_state <= POTENTIAL_START_STATE; // = state
            end
            // We have not sampled enough bits to know whether it was start bit.
                else                        next_state = POTENTIAL_START_STATE; // = state
            end

        // Stay in Sample until reset
        SAMPLE_STATE:                               next_state <= SAMPLE_STATE;

        ERROR_STATE:                                next_state <= RESET_STATE;
        default:                                    next_state <= ERROR_STATE;

    endcase
end

//****************************************************************************
//                             Sequential logic
//****************************************************************************

always @(posedge clk) begin
    if(reset_i)                    state <= RESET_STATE;
    else                           state <= next_state;
end

always @(posedge clk) begin : detect_edge_of_baud
    if( reset_counter ) begin
        baud_previous   <= 1'b1;
        baud_now        <= 1'b1;
    end
    else begin
        // "baud_now" is 1 for 0->7, and 0 for 8->15
        baud_previous   <= baud_now;          // Shift now to previous.
        baud_now        <= ~counter_value_w[3];   // Update current. on MSB
    end
end

always @(posedge clk) begin : sample_enable_sample_value_reg // Moore TODO: Not Moore if it depends on inputs.
    case(state)
        // Sampling the previous count value
        SAMPLE_STATE: sample_value_reg[counter_value_w] <= previous_bits_w[3];

        POTENTIAL_START_STATE:sample_value_reg[counter_value_w] <= previous_bits_w[3];
        
        RESET_STATE:           sample_value_reg <= {SAMPLES_PER_BIT{STOP_BIT}};
      default:                 sample_value_reg <= sample_value_reg ; // Keep Previous Value
    endcase
end

// 2/3 voting on rxd_i for samples (8,9,10 if 1 indexed).
// always @(posedge baud_enable_o) rxd_o = ( 2 <= sample_value_reg[7]+sample_value_reg[8]+sample_value_reg[9] );
always @(posedge baud_enable_o or posedge reset_w) begin : comb_rxd_o // Mealy
    case(state) // TODO : rxd_o <= 1'b0 @ Start, ALWAYS
        POTENTIAL_START_STATE: rxd_o <= START_BIT;
        RESET_STATE:           rxd_o <= STOP_BIT;
        SAMPLE_STATE:          rxd_o <= rxd_o_voting;
        default:               rxd_o <= STOP_BIT;
    endcase
end

// Update if samples don't match (8,9,10 if 1 indexed).
always @(posedge baud_enable_o, posedge reset_w) begin : comb_noise_flag_o
    if(reset_w) noise_flag_o = 1'b0;
    else        noise_flag_o = noise_flag_w;
end

//****************************************************************************
//                            Combinational logic
//****************************************************************************


always@(*) begin : comb_rxd_o_voting
        casex({sample_value_reg[7],sample_value_reg[8],sample_value_reg[9]}) // Consider XOR
            3'bx11:  rxd_o_voting <= 1'b1 ;
            3'b1x1:  rxd_o_voting <= 1'b1 ;
            3'b11x:  rxd_o_voting <= 1'b1 ;
            default: rxd_o_voting <= 1'b0 ;
        endcase
end

always @(*) begin : comb_reset_sample_enable // Moore
    case(state)
    RESET_STATE:            reset_sample_enable <= 1'b1;
    default:                reset_sample_enable <= 1'b0;
    endcase
end

always @(*) begin : comb_reset_counter // Moore
    case(state)
    // Hold in reset to and ignore first pulse of sample_enable
    LOOK_FOR_START_STATE:   reset_counter <= 1'b1;
    RESET_STATE:            reset_counter <= 1'b1;
    
    default:                reset_counter <= 1'b0;
    endcase
end

always @(*) begin : reset_comb_previous_bits // Moore
    case(state)
    RESET_STATE:          reset_previous_bits <= 1'b1; // Load default @ Reset // We want 
    default:              reset_previous_bits <= 1'b0;
    endcase
end

assign rxd_potential_start_bit_detected = ( // Need last four values to detect start
       previous_bits_w[0] == STOP_BIT 
    && previous_bits_w[1] == STOP_BIT 
    && previous_bits_w[2] == STOP_BIT 
    && previous_bits_w[3] == START_BIT) ;// START_BIT is the last bit shifted in


assign baud_enable_o = (~baud_previous) & (baud_now); // ready on posedge.
// Async - 2/3 voting on samples 3,5,7 (one indexed)


// Reset once 2/3 samples are logic high
assign rxd_early_start_bit_rejection = &{sample_value_reg[2],sample_value_reg[4]} ;

// Async - 2/3 voting on samples 3,5,7 (one indexed)
// assign  rxd_actual_start_bit_detected = ( 2 <= sample_value_reg[2]+sample_value_reg[4]+sample_value_reg[LAST_CHECK_BIT_BEFORE_START] ); // Todo
// NOTE Start Bit so looking for ZEROS NOT Ones
// assign  rxd_actual_start_bit_detected = ^({sample_value_reg[2],sample_value_reg[4],sample_value_reg[LAST_CHECK_BIT_BEFORE_START]}); // Wrong implementation
always @(*) begin : comb_rxd_actual_start_bit_detected // Mealy
        casex({sample_value_reg[2],sample_value_reg[4],sample_value_reg[LAST_CHECK_BIT_BEFORE_START]}) // Consider XOR
            3'bx00:  rxd_actual_start_bit_detected <= 1 ;
            3'b0x0:  rxd_actual_start_bit_detected <= 1 ;
            3'b00x:  rxd_actual_start_bit_detected <= 1 ;
            default: rxd_actual_start_bit_detected <= 0 ;
        endcase
end

assign noise_flag_w = (!( &(sample_value_reg[9:7]) || // ( Not ( all 1 or
                                  !(sample_value_reg[9:7]) ) || // all 0 ) or
                                // for Noise Case Two noise on start bit
                               !( &({sample_value_reg[2],sample_value_reg[4],sample_value_reg[LAST_CHECK_BIT_BEFORE_START]}) || // Not ( all 1 or
                                  !({sample_value_reg[2],sample_value_reg[4],sample_value_reg[LAST_CHECK_BIT_BEFORE_START]}) ) ); // all 0 ) )
//****************************************************************************
//                              Internal Modules
//****************************************************************************

counter_wreset #(
    .N(SAMPLE_WIDTH) // 4-bits, counter rolls over after 16 samples
) rx_count_samples (
    .clk(clk),
    .enable_i(sample_enable),// TODO: May need to latch sample_enable internally, because if a sample_enable happens on a state transition, we could miss it.
    .reset_i(reset_counter),
    .value_o(counter_value_w)
);

// Detect Start by shifting in start bits
shift_reg_n #(
    .n(START_DETECT_SIZE) // Continuously sample on sample_enable (sample_en)
) previous_bits (
    .clk(clk)
    ,.load() // Not Used
    ,.reset(reset_previous_bits) 
    ,.value_i() // Not Used // Load all zeros
    ,.shift(sample_enable)
    ,.s_i(rxd_i)// Shift right. s_i -> MSB.
    ,.s_o()
    ,.value_o(previous_bits_w)
);

pulse_generator #(
    .CLK_FREQ_IN(CLK_FREQ)
    ,.PULSE_FREQ_OUT(BAUD_RATE*SAMPLES_PER_BIT)
) gen_sample_pulse (
    .clk_i(clk)
    ,.reset_i(reset_sample_enable)
    ,.pulse_o(sample_enable) // Pulse immediately after reset //TODO: pulse at end of clock cycle, to sample previous data
);

endmodule
// data_recovery # (
//     .SAMPLES_PER_BIT(SAMPLES_PER_BIT)
// ) sampler (
//     .clk(clk),
//     .reset_i(reset),
//     .SAMPLE_STATE_i(sample_enable),
//     .rxd_i(shift_in),
//     .rxd_o(shift_in_checked),
//     .noise_flag_o(noise_flag)
// );

`endif
