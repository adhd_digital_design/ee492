
module power_on_reset #(
    parameter COUNTER_WIDTH = 20 // up counter @ clk_i, resets until at maximum count 
) (
    input  wire clk_i,
    output reg reset_o
);

reg [COUNTER_WIDTH-1:0] counter;

always@(posedge clk_i)
begin
    case (counter)
        {2**(COUNTER_WIDTH/2)}:  begin // Pulse Low 1/2 way through count
            reset_o    <=  1'b0;
            counter    <=  counter; // Latch
        end
        {COUNTER_WIDTH{1'b1}}:  begin // Hold Low at end of count
            reset_o    <=  1'b0;
            counter    <=  counter; // Latch
        end
        default : begin // Hold High
            counter     <=  counter+1'b1;
            reset_o     <=  1'b1;
        end
    endcase
end
endmodule
