//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Contain all modules required for UART communication with RS232
//Date Created : 2017-02-25

`ifndef rs232_
`define rs232_

// SIM: Check T_CLOCK after changing.
`timescale 1ns/1ns

module rs232 #(
    parameter CLK_FREQ              = (50) * (10**6),// 50MHz
    parameter PARITY                = 1,// 0=none, 1=odd, 2=even // TODO: Make typedef enum
    parameter DATA_WIDTH            = 8,// Termite can communicate with 5,6,7,8 bit-data 
    parameter BAUD_RATE             = 115200,
    parameter FIFO_ADDR_WIDTH       = 1
) (
    rs232_inf ports
    , output wire [7:0] rx_debug_o
    , output wire [7:0] tx_debug_o
);

//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************
wire [DATA_WIDTH-1:0] tx_next;
wire [DATA_WIDTH-1:0] rx_last;

// TODO: Only declare these if FIFO_ADDR_WIDTH==0.
reg [DATA_WIDTH-1:0] tx_reg;
reg [DATA_WIDTH-1:0] rx_reg;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************

//****************************************************************************
//                             Sequential logic
//****************************************************************************
always @(posedge ports.clk) begin : clk_tx_next
    if(ports.tx_is_ready)    tx_reg = ports.tx_data;
    else                     tx_reg = tx_reg; // latch
end

always @(posedge ports.clk) begin : clk_rx_data
    if(ports.rx_is_ready)    rx_reg = rx_last;
    else                     rx_reg = rx_reg; // latch
end

//****************************************************************************
//                            Combinational logic
//****************************************************************************
assign tx_next = tx_reg; // TODO: Use fifo.
assign ports.rx_data = rx_reg; // TODO: Use fifo.

//****************************************************************************
//                             Procedural Blocks
//****************************************************************************

initial begin
    tx_reg = {DATA_WIDTH {1'b0}};
    rx_reg = {DATA_WIDTH {1'b0}};
end

//****************************************************************************
//                              Internal Modules
//****************************************************************************
rs232_out_serializer #(
    .DATA_WIDTH(DATA_WIDTH),
    .CLK_FREQ(CLK_FREQ),
    .BAUD_RATE(BAUD_RATE),
    .PARITY(PARITY) // 0=none, 1=odd, 2=even
) tx_out (
    .clk(ports.clk),
    .reset(ports.reset),
    // Data:
    .txd_o(ports.tx_out),
    .tx_data_i(tx_next),
    // Control;
    .tx_enable_i(ports.tx_enable),
    // Status:
    .is_ready_o(ports.tx_is_ready)
    // Debug:
    , .debug_o(tx_debug_o)
);

rs232_in_deserializer #(
    .DATA_WIDTH(DATA_WIDTH),
    .CLK_FREQ(CLK_FREQ),
    .BAUD_RATE(BAUD_RATE),
    .PARITY(PARITY) // 0=none, 1=odd, 2=even
) rx_in (
    .clk(ports.clk),
    .reset(ports.reset),
    // Data:
    .rxd_i(ports.rx_in),
    .rx_data_o(rx_last),
    // Control:
    .rx_enable_i(ports.rx_enable),
    // Status:
    .is_ready_o(ports.rx_is_ready),
    .framing_error_o(ports.framing_error_o), // FE
    .noise_flag_o(ports.noise_flag_o), // NF
    .parity_error_o(ports.parity_error_o) // PE
    // Debug:
    , .debug_o(rx_debug_o)
);

endmodule

`endif
