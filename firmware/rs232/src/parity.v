
module parity #(
    parameter N = 8,
    parameter TYPE = 0 // 0=none, 1=odd, 2=even // TODO: Make typedef enum
) (
    input [N-1:0] data_i,
    output parity_o
);
// TODO: Throw error if TYPE is invalid.

generate
    // : gen_parity
    case(TYPE)
    1:       assign parity_o = ~(^data_i);
    2:       assign parity_o = ^data_i;
    
    0:       assign parity_o = 0;
    default: assign parity_o = 0;
    endcase
endgenerate

endmodule
