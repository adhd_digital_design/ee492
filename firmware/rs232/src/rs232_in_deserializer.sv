//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: RS232
//Date Created : 2017-02-25

`ifndef rs232_in_deserializer_
`define rs232_in_deserializer_

// SIM: Check T_CLOCK after changing.
`timescale 1ns/1ns

module rs232_in_deserializer #(
    parameter DATA_WIDTH = 8,
    parameter CLK_FREQ = (50) * (10**6), // Hz
    parameter BAUD_RATE = 115200,        // Hz
    parameter PARITY = 0                 // 0=none, 1=odd, 2=even
) (
    input wire clk,
    input wire reset,

    // Data:
    input wire rxd_i,
    output wire [DATA_WIDTH-1 :0] rx_data_o,

    // Control:
    input rx_enable_i,

    // Status:
    output reg is_ready_o,
    output reg  framing_error_o, // FE
    output reg noise_flag_o,   // NF
    output reg  parity_error_o   // PE

    // Debug:
    , output wire [7:0] debug_o
);

//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

localparam TOTAL_WIDTH = DATA_WIDTH +       // Original data width.
                        (1 -(0==PARITY)) +  // Optional parity bit.
                        2 ;                 // START and STOP bit.

localparam START_BIT = 1'b0; // Constants for clarity
localparam STOP_BIT = 1'b1; // Constants for clarity

localparam START_ADDR = 0;
localparam DATA_ADDR = START_ADDR + 1;
localparam PARITY_ADDR = TOTAL_WIDTH-2; // Obviously only valid if parity exists.
localparam STOP_ADDR = TOTAL_WIDTH-1;

localparam COUNTER_WIDTH = $clog2(TOTAL_WIDTH);// $clog2 = round_up(log_2(TOTAL_WIDTH))

// FSM
localparam RS232_IN_DESERIALIZER_STATE_WIDTH = 3;
// Have compiler optimize state values
typedef enum reg [RS232_IN_DESERIALIZER_STATE_WIDTH-1:0] {
    RESET_STATE             = 3'h0 // Reset all registers / modules.
    ,READY_STATE            = 3'h1 // Waiting for enable.
    ,WAIT_FOR_START_STATE   = 3'h2 // Only procded after start bit is detected.
    ,START_STATE            = 3'h3 // Initalize some things.
    ,WAIT_TO_SHIFT_STATE    = 3'h4 // Waiting for next baud clock.
    ,SHIFT_STATE            = 3'h5 // Read a new bit from Rx.
    ,ERROR_STATE            = 3'h6 // Provide noticeable output and then reset.
} rs232_in_deserializer_state_t; 

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

wire noise_flag_w;
wire rxd_i_checked;
reg  rxd_bit_shift_en;
wire parity_w;
wire rxd_is_shifted;
reg  wait_for_start_state;
wire baud_clk;
reg  reset_bit_count;
reg  reset_sampler;
reg  reset_rx_sr_data;
// wire sample_clk; // Moved into data_recovery.

reg count_bits_en;

wire [COUNTER_WIDTH -1:0] count_bits;

wire [TOTAL_WIDTH -1:0] rx_sr_w;

// FSM
rs232_in_deserializer_state_t state;
rs232_in_deserializer_state_t next_state;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************


always @(*) begin : comb_next_state_logic
    case(state)

        READY_STATE: begin
            if(rx_enable_i)    next_state <= WAIT_FOR_START_STATE;
            else               next_state <= state;
        end

        WAIT_FOR_START_STATE: begin
            // if( baud_clk && (START_BIT == rxd_i_checked) )     next_state <= START_STATE; // TODO: Should check for START_BIT
            if( baud_clk )     next_state <= START_STATE;
            else               next_state <= state;
        end

        // Reset bit_counter and shift register
        START_STATE:  next_state <= SHIFT_STATE;

        WAIT_TO_SHIFT_STATE: begin
            // TODO: Make like tx, rxd_is_shifted && baud_clk
            if(rxd_is_shifted)              next_state <= READY_STATE; // Done receiving.
            else  if(baud_clk)              next_state <= SHIFT_STATE;
            else                            next_state <= state;
        end

        SHIFT_STATE: begin
             next_state <= WAIT_TO_SHIFT_STATE;
        end

        RESET_STATE:  next_state <= READY_STATE;
        ERROR_STATE:  next_state <= RESET_STATE;
        default:      next_state <= ERROR_STATE;

    endcase
end

//****************************************************************************
//                             Sequential logic
//****************************************************************************

always @(posedge clk) begin
    if(reset)                      state <= RESET_STATE;
    else                           state <= next_state;
end

//****************************************************************************
//                            Combinational logic
//****************************************************************************

always @(*) begin : comb_reset_rx_sr_data // Moore
    case(state)
    RESET_STATE:          reset_rx_sr_data <= 1'b1;
    START_STATE:          reset_rx_sr_data <= 1'b1;
    
    default:              reset_rx_sr_data <= 1'b0;
    endcase
end

always @(*) begin : comb_reset_sampler // Moore
    case(state)
    // Reset when going from READY_STATE to wait_for_start_state
    READY_STATE:          reset_sampler <= 1'b1;
    RESET_STATE:          reset_sampler <= 1'b1;
    
    default:              reset_sampler <= 1'b0;
    endcase
end

always @(*) begin : comb_reset_bit_counter // Moore
    case(state)
    // READY_STATE:          reset_bit_count <= 1'b1;
    START_STATE:          reset_bit_count <= 1'b1;
    RESET_STATE:          reset_bit_count <= 1'b1;
    
    default:              reset_bit_count <= 1'b0;
    endcase
end

always @(*) begin : comb_rxd_bit_shift_en // Moore
    case(state)
    SHIFT_STATE:          rxd_bit_shift_en <= 1'b1;
    default:              rxd_bit_shift_en <= 1'b0;
    endcase
end

always @(*) begin : comb_count_bits_en // TODO: Could this be combined with rxd_bit_shift_en? // Moore
    case(state)
    SHIFT_STATE:          count_bits_en <= 1'b1;
    default:              count_bits_en <= 1'b0;
    endcase
end


always @(*) begin : comb_wait_for_start_state // Moore
    case(state)
    WAIT_FOR_START_STATE:       wait_for_start_state <= 1'b1;
    default:                    wait_for_start_state <= 1'b0;
    endcase
end

assign  rxd_is_shifted = ( TOTAL_WIDTH == count_bits ); // Mealy

// Output Signals

assign  rx_data_o = rx_sr_w[DATA_ADDR +: DATA_WIDTH]; // {stop [, parity], data, start} // Mealy

always @(*) begin : comb_is_ready_o // Moore
    case(state)
    READY_STATE:          is_ready_o <= 1'b1;
    default:              is_ready_o <= 1'b0;
    endcase
end

always @(*) begin : comb_framing_error_o // Mealy
    case(state)
    READY_STATE:  framing_error_o <= (STOP_BIT != rx_sr_w[STOP_ADDR]) ||
                                     (START_BIT != rx_sr_w[START_ADDR]);
    default:      framing_error_o <= 1'b0;
    endcase
end

always @(*) begin : latch_noise_flag_o // Mealy
    // Reset the flag on transition from READY to WAIT_FOR_START.
    // In all other states it can only be set, not cleared.
    case(state)
    // Only care about flag for start bit.
    // WAIT_FOR_START_STATE:   noise_flag_o <= ( noise_flag_w ? 1'b1 : 1'b0 );
    WAIT_FOR_START_STATE:   noise_flag_o <= noise_flag_w;
    RESET_STATE:            noise_flag_o <= 1'b0;
    // Set if flag for this bit but don't clear if its not.
    // default:                noise_flag_o <= ( noise_flag_w ? 1'b1 : noise_flag_o );
    default:                noise_flag_o <= noise_flag_o ;
    endcase
end

generate // Conditionally do parity if requested.
    if( 0 != PARITY ) begin
        always @(*) begin : comb_parity_error // Mealy
            case(state)
            READY_STATE:     parity_error_o <= ( parity_w != rx_sr_w[PARITY_ADDR] );
            default:         parity_error_o <= 1'b0;
            endcase
        end
    end
    else begin // Default to NO PARITY
        always @(*)  parity_error_o = 1'b0;
    end
endgenerate

assign  debug_o = {
    state, // assume 3 bits.
    rxd_bit_shift_en,
    1'b0,
    baud_clk,
    1'b0
};


// always_comb $display("[%t] %s=rx_in.state",$time,state.name());
// always_comb $display("[%t] %s=rx_in.next_state",$time,next_state.name());

//****************************************************************************
//                             Procedural Blocks
//****************************************************************************

initial begin
    is_ready_o                  =  1'b0;
    framing_error_o             =  1'b0;
    noise_flag_o                =  1'b0;
    parity_error_o              =  1'b0;
    wait_for_start_state        =  1'b0;
    rxd_bit_shift_en            =  1'b0;
    reset_bit_count             =  1'b0;
    reset_sampler               =  1'b0;
    reset_rx_sr_data            =  1'b0;
    count_bits_en               =  1'b0;
end

//****************************************************************************
//                              Internal Modules
//****************************************************************************

counter_wreset #(
    .N(COUNTER_WIDTH)
) rx_count_bits (
    .clk(clk), 
    .enable_i(count_bits_en),
    .reset_i(reset_bit_count),
    .value_o(count_bits)
);

shift_reg_n #(
    .n(TOTAL_WIDTH)
) rx_sr (
    .clk(clk),
    .load(1'b0),
    .reset(reset_rx_sr_data),
    .value_i(),
    .shift(rxd_bit_shift_en),
    .s_i(rxd_i_checked),
    .s_o(),
    .value_o(rx_sr_w)
);


generate // Conditionally do parity if requested.
    if( 0 != PARITY ) begin
        parity #(
            .N(DATA_WIDTH),
            .TYPE(PARITY)
        ) rx_parity (
            .data_i(rx_data_o), // {stop [, parity], data, start} // Parity is only done on the data
            .parity_o(parity_w)
        );
    end
endgenerate

//****************************************************************************
// Start bit, voting, and baud clock generation.
// Option 1

// assign rxd_i_checked = rxd_i;
// assign noise_flag_w = 1'b0;
// pulse_generator #(
//     .CLK_FREQ_IN(CLK_FREQ)
//     ,.PULSE_FREQ_OUT(BAUD_RATE*SAMPLES_PER_BIT)
// ) gen_sample_pulse (
//     .clk_i(clk)
//     ,.reset_i(1'b0) // Do NOT Reset Sample Clock
//     ,.pulse_o(sample_clk)
// );
// pulse_generator #(
//     .CLK_FREQ_IN(CLK_FREQ)
//     ,.PULSE_FREQ_OUT(BAUD_RATE)
// ) gen_baud_pulse (
//     .clk_i(clk)
//     ,.reset_i(reset_sampler)
//     ,.pulse_o(baud_clk)
// );


// Option 2
data_recovery # (
    .CLK_FREQ(CLK_FREQ)
    ,.BAUD_RATE(BAUD_RATE)
) sampler (
    .clk(clk)
    ,.reset_i(reset_sampler)
    ,.wait_for_start_state_i(wait_for_start_state)
    ,.rxd_i(rxd_i)
    ,.rxd_o(rxd_i_checked)
    ,.noise_flag_o(noise_flag_w)
    ,.baud_enable_o(baud_clk)
);


// Option 3

// pulse_generator #(
//     .CLK_FREQ_IN(CLK_FREQ)
//     ,.PULSE_FREQ_OUT(BAUD_RATE*SAMPLES_PER_BIT)
// ) gen_sample_pulse (
//     .clk_i(clk)
//     ,.reset_i(1'b0) // Do NOT Reset Sample Clock
//     ,.pulse_o(sample_clk)
// );
// rx_sampler # () sampler (
//     .clk_i(clk),
//     .reset_i(reset_sampler),
//     .sample_i(sample_clk),
//     .sync_baud_now_i(1'b0),
//     .rxd_i(rxd_i),
//     .rxd_o(rxd_i_checked),
//     .noise_flag_o(noise_flag_w),
//     .is_ready_o(baud_clk)
// );
//****************************************************************************

endmodule

// rs232_in_deserializer #(
//     .DATA_WIDTH(8),
//     .CLK_FREQ(RS232_CLK_FREQ),
//     .BAUD_RATE(BAUD_RATE),
//     .PARITY(PARITY), // 0=none, 1=odd, 2=even
// ) rx_in (
//     .clk(rs232_clk),
//     .reset(reset),
//     // Data:
//     .rxd_i(loopback),
//     .rx_data_o(rx_data),// TODO Push to FIFO @ 
//     // Control:
//     .rx_enable_i(rx_enable),
//     // Status:
//     .is_ready_o(rx_is_ready),
//     .framing_error_o(framing_error_o), // FE
//     .noise_flag_o(noise_flag_w), // NF
//     .parity_error_o(parity_error_o) // PE
//     // Debug:
//     , .debug_o(rx_debug)
// );

`endif
