
// Demonstates the usage of UART (RS232) communication.
module rs232_top #(
    parameter NCLK=3,
    parameter CLK0_FREQ = (50) * (10**6), // 50MHz
    parameter NIN=8,
    parameter NOUT=24,
    parameter NSEGS=8
) (
    input wire [NCLK-1:0] clk,
    // clk[0] // LCD requires 50 MHz.
    // clk[1] // RS232 clk.
    // clk[2] // load wreg on negedge and use wreg on posedge.

    // I/O
    input  wire [NIN-1:0] in,
    output wire [NOUT-1:0] out,

    // Seven Segment Display
    output wire [(NSEGS*HEX_W)-1:0] hex_L,

    //************************************************************************
    // UART
    //************************************************************************
    output   wire UART_TXD, // UART Transmitter
    input    wire UART_RXD, // UART Receiver

    //************************************************************************
    // LCD Module 16X2
    //************************************************************************
    output [7:0] LCD_DATA, // LCD Data bus 8 bits
    output LCD_ON, // LCD Power ON/OFF
    output LCD_BLON, // LCD Back Light ON/OFF
    output LCD_RW, // LCD Read/Write Select, 0 = Write, 1 = Read
    output LCD_EN, // LCD Enable
    output LCD_RS // LCD Command/Data Select, 0 = Command, 1 = Data
);

//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

localparam HEX_W = 7; // For 7-Seg Displays
localparam PARITY = 1;// 0=none, 1=odd, 2=even // TODO: Make typedef enum
localparam RS232_DATA_WIDTH = 8; // Termite can communicate with 5,6,7,8 bit-data 
localparam BAUD_RATE = 9600; // Hz // 110 Hz -> 9600 -> 115200 Hz tested
localparam RS232_CLK_FREQ = CLK0_FREQ;
localparam RESET_COUNTER_WIDTH = 20; // up counter @ clk_i, power on resets until at maximum count 

// localparam BAUD_RATE = (1) * (16); // automated loopback in ~2sec transactions
// localparam RS232_CLK_FREQ = CLK0_FREQ; // automated loopback in ~2sec transactions

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

wire reset;
wire po_reset;
wire rxd_w;
wire rx_is_loopback;

wire rs232_clk = clk[0]; // TODO: ENABLE_EXT_CLK
// wire rs232_clk = clk[1];

wire tx_is_ready;
wire tx_enable;

wire tx_enable_once_sample;
wire tx_enable_once_now;
wire tx_enable_once_previous;
wire tx_enable_once_posedge;
wire wreg_clk_debounced;

wire rx_is_ready;
wire rx_enable;

wire framing_error_o;
wire noise_flag_o;
wire parity_error_o;

wire [RS232_DATA_WIDTH-1:0] rx_data;

// DEBUG
wire [7:0] status;
wire [7:0] rx_debug;
wire [7:0] tx_debug;

reg [7:0] wreg;
reg [7:0] control;
reg [RS232_DATA_WIDTH-1:0] tx_reg;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************
//****************************************************************************
//                             Sequential logic
//****************************************************************************

always @(negedge wreg_clk_debounced) begin : clk2n_wreg
    wreg <= in[7:0];
end

always @(posedge wreg_clk_debounced) begin : clk2_control
    case(in[7:0])
        0:       control <= wreg;
        default: control <= control;
    endcase
end

always @(posedge wreg_clk_debounced) begin : clk2_tx_reg
    case(in[7:0])
        1:       tx_reg <= wreg;
        default: tx_reg <= tx_reg;
    endcase
end


always @(posedge clk[0]) begin
    tx_enable_once_now <= control[2]; // toggle;
    tx_enable_once_previous <= tx_enable_once_now; // shift now to previous;
end


//****************************************************************************
//                            Combinational logic
//****************************************************************************

assign LCD_ON = 1'b1;
assign LCD_BLON = 1'b1;
assign status = {
    framing_error_o, noise_flag_o, parity_error_o, rx_is_loopback,
    UART_TXD, tx_is_ready, rxd_w, rx_is_ready
};

assign out [0 +: 8] = status;
assign out [8 +: 8] = tx_debug;
assign out [16 +: 8] = rx_debug;

assign rx_enable = control[0];
assign tx_enable = control[1] || tx_enable_once_posedge ;
assign rx_is_loopback = control[6];
assign reset = (control[7] || po_reset);
assign rxd_w = ( rx_is_loopback ? UART_TXD : UART_RXD);

assign tx_enable_once_posedge = (~tx_enable_once_previous)   & (tx_enable_once_now); 
//****************************************************************************
//                              Internal Modules
//****************************************************************************

// Debounce the button to clock in wreg
debounce #(
    .clk_freq_hz    (  (50) * (10** 6)), // Mhz  = 10^6 Hz
    .wait_time_sec  ((   4) * (10**-3))  // ms  = 10^-3 sec
) debounce_start (
    .in_sig(clk[2]),
    .clk(clk),           // Input Clock
    .out_sig(wreg_clk_debounced)
);


power_on_reset #(
    .COUNTER_WIDTH(RESET_COUNTER_WIDTH) // up counter @ clk_i, resets until at maximum count 
) po_rst (
    .clk_i(rs232_clk),
    .reset_o(po_reset)
);

rs232_inf #(
    .DATA_WIDTH(RS232_DATA_WIDTH)
) inf (
    .clk(rs232_clk),
    .reset(reset),

    // Data:
    .tx_data(tx_reg),
    .rx_data(rx_data),
    .tx_out(UART_TXD),
    .rx_in(rxd_w),

    // Control:
    .tx_enable(tx_enable),
    .rx_enable(rx_enable),

    // Status:
    .rx_is_ready(rx_is_ready),
    .tx_is_ready(tx_is_ready),

    .framing_error_o(framing_error_o),
    .noise_flag_o(noise_flag_o),
    .parity_error_o(parity_error_o)
);

rs232 #(
    .CLK_FREQ(RS232_CLK_FREQ),
    .PARITY(PARITY), // 0=none, 1=odd, 2=even
    .DATA_WIDTH(RS232_DATA_WIDTH),
    .BAUD_RATE(BAUD_RATE),
    .FIFO_ADDR_WIDTH(0)
) uart (
    .ports(inf)
    , .tx_debug_o(tx_debug)
    , .rx_debug_o(rx_debug)
);

display_on_hex #(
    .NSEGS(8)
) hex_display (
    .hex_L( hex_L ),
    .value( {control, wreg, tx_reg, rx_data} )
);

// LCD_TEST lcd_display (
//     // Host Side
//     .iCLK(clk[0]), // Assumes 50 MHz clock.
//     .iRST_N(reset),
//     // LCD Side
//     .LCD_DATA(LCD_DATA),
//     .LCD_RW(LCD_RW),
//     .LCD_EN(LCD_EN),
//     .LCD_RS(LCD_RS)
// );

endmodule
