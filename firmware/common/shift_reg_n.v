//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: N-Bit Shift Register
//Date Created : 2017-01-16

module shift_reg_n  #(
    parameter n = 8 // Assume we want to store 8-Bit #s...
    ,parameter SHIFT_DIRECTION = 0  // 0 = Shift right. s_i -> MSB. // 1 = Shift left. s_i -> LSB.
)(
    input wire clk,

    input wire reset, // Synchronous.

    input wire load, // Synchronous.
    input wire [n-1:0] value_i,

    input wire shift,  // Synchronous.
    input wire s_i,
    output wire s_o,

    output reg [n-1:0] value_o
);

generate
if(SHIFT_DIRECTION==0) begin
    // always @ (posedge clk or posedge reset or posedge load) begin // Async reset & load.
    always @ (posedge clk) begin : sync_value_shiftreg
        if(reset) begin
            value_o[n-1:0] <= 0;
        end
        else if(load) begin
            value_o[n-1:0] <= value_i[n-1:0];
        end
        else if(shift) begin
            // Shift right. s_i -> MSB.
            value_o[n-1:0] <= { s_i, value_o[n-1:1] };
        end
        else begin
            value_o[n-1:0] <= value_o[n-1:0]; // latch.
        end
    end
end else begin
    // always @ (posedge clk or posedge reset or posedge load) begin // Async reset & load.
    always @ (posedge clk) begin : sync_value_shiftreg
        if(reset) begin
            value_o[n-1:0] <= 0;
        end
        else if(load) begin
            value_o[n-1:0] <= value_i[n-1:0];
        end
        else if(shift) begin
            // Shift right. s_i -> LSB.
            value_o[n-1:0] <= { value_o[n-2:0], s_i };
        end
        else begin
            value_o[n-1:0] <= value_o[n-1:0]; // latch.
        end
    end
end
endgenerate

assign s_o = value_o[0];
endmodule

// shift_reg_n #(
//     .n(opperand_width)
//     ,.SHIFT_DIRECTION(0) // 0 = Shift right. s_i -> MSB.
// ) sr_q (
//     .clk(clk_shift_reg),
//     .load(load_lsb), // Load with Q at Start.
//     .reset(0),
//     .value_i(q),
//     .shift(shift_en),
//     .s_i(aq_w),
//     // .s_o(), // Throw away shift out of multiplier
//     .value_o(p[opperand_width-1:0])
// );
