module counter_wreset #(
    parameter N=8
) (
    input wire clk,
    input wire enable_i,
    input wire reset_i,
    output reg [N -1:0] value_o
);
always @(posedge clk) begin
    if(reset_i)       value_o <= {N {1'b0}};
    else if(enable_i) value_o <= value_o + {{N-1 {1'b0}}, 1'b1};
    else              value_o <= value_o;
end
endmodule

// counter_wreset #(
//     .N(8);
// ) counter_u0 (
//     .clk(clk),
//     .enable_i(enable_i),
//     .reset_i(reset_i),
//     .value_o(value_o)
// );
