//Author : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Monostable Multivibrator - Debounce a signal
//Date Created : 2017-01-16
//Reference URL: http://simplefpga.blogspot.com/2012/12/fifofirst-in-first-out-buffer-in-verilog.html

module mono_debounce (
    input wire  in_sig,
    input wire  clk,    // Input Clock
    output wire out_sig 
);
reg db1,db2;

always @ (posedge clk) db1 <= in; 
always @ (posedge clk) db2 <= db1;

// Use the propegation delay of two registers and an 'and' gate to debounce the input signal
assign out_sig = ~db1 & db2; 

endmodule