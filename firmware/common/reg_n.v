//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: N-Bit Register
//Date Created : 2017-01-16

module reg_n #(
    parameter n=32 // N-Bit
)(
    input  wire [n:0] D,     // Data
    input  wire C,           // Clock
    output reg [n:0] Q,      // Register Value
    output wire [n:0] nQ          // Not Register Value
);

always @(posedge C)
begin
    Q[n:0] <= D[n:0];
end

assign nQ = ~Q ;

endmodule
// reg_n #(
//     .n(32) // N-Bit
// ) rn (
//     .D(D[n:0]),     // Data
//     .C(C),           // Clock
//     .Q(Q[n:0]),      // Register Value
//     .nQ(nQ[n:0])          // Not Register Value
// );