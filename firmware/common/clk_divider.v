//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Clock divider
//Date Created : 2017-01-10

`define OFF_BY_ONE 1

module clk_divider #(
    parameter in_clk_freq_hz    = (  (50) * (10** 6) ), // Mhz = 10^6 Hz
    out_clk_freq_hz   = (  (1 ) * (10** 0) ), // Hz = 10^0 Hz
    integer total_wait_cycles  =  (in_clk_freq_hz / out_clk_freq_hz) - 2*`OFF_BY_ONE, // Cycles of in_clk for every out_clk
    real duty_cycle   = ( 0.5 ) , // Percent of time ON
    parameter sig_active_state  = ( 1'b1 )   // Input signal is ACTIVE HIGH
)(
    input wire  in_clk,    // Input Clock
    input wire  in_reset,  // WARNING: Asynchronous Reset
    output reg out_clk     // Output Clock
);
localparam OFF_BY_ONE = `OFF_BY_ONE; // Correct off By one error, caused by ceiling function implied by casting real as integer
localparam integer ON_WAIT_CYCLES  = duty_cycle*(total_wait_cycles); 
localparam integer OFF_WAIT_CYCLES = total_wait_cycles - ON_WAIT_CYCLES;
localparam CNTR_WIDTH = $clog2(OFF_WAIT_CYCLES + ON_WAIT_CYCLES) ; // $clog2 = round_up(log_2(WAIT_CYCLES))
localparam ON  = sig_active_state;
localparam OFF = ~(sig_active_state);

generate
if ( in_clk_freq_hz < out_clk_freq_hz ) begin
    // Error (12006): Node instance "assert_freq_in_less_than_out" instantiates undefined entity "EXCEPTION_CLK_DIVIDER"
    EXCEPTION_CLK_DIVIDER assert_freq_in_less_than_out();
end
endgenerate

// Local Reg
reg [CNTR_WIDTH-1:0] cntr;

// Counter
always@(posedge in_clk) begin

if(in_reset)        // Synchronous Reset
    begin
        out_clk<=OFF;
        cntr<=0;     // Reset Counter
    end
else begin
    case (out_clk)
    OFF:
        if(cntr<OFF_WAIT_CYCLES)        // Continue Waiting
            begin
                out_clk<=OFF; // Hold Output
                cntr<=cntr+1'b1; // Increment Counter
            end
        else if(cntr>=OFF_WAIT_CYCLES)    // Done Waiting
            begin
                out_clk<=ON; // Posedge out_clk
                cntr<=0;     // Reset Counter
            end
    ON:
        if(cntr<ON_WAIT_CYCLES)        // Continue Waiting
            begin
                out_clk<=ON; // Hold Output
                cntr<=cntr+1'b1; // Increment Counter
            end
        else if(cntr>=ON_WAIT_CYCLES)    // Done Waiting
            begin
                out_clk<=OFF;
                cntr<=0;     // Reset Counter
            end
    default :                           // Error = // Synchronous Reset
        begin
            out_clk<=OFF;
            cntr<=0;     // Reset Counter
        end
    
    endcase // out_clk
end // else


end // always

endmodule // clk_divider

// // clk_divider_50Mhz_downto_1Hz
// clk_divider #(
//     .in_clk_freq_hz(  (50) * (10** 6) ),  // Mhz = 10^6 Hz
//     .out_clk_freq_hz( (1 ) * (10** 0) ), // Hz = 10^0 Hz
//     //.total_wait_cycles(),               // total_wait_cycles =  in_clk_freq_hz / out_clk_freq_hz ;
//     .duty_cycle( 0.5 ) ,                  // Percent of time ON
//     .sig_active_state( 1'b1 )             // Input signal is ACTIVE HIGH
// ) cd_1 (
//     .in_clk(cd_2_in_clk),    // Input Clock
//     .out_clk(cd_2_out_clk)     // Output Clock
// );
