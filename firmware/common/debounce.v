//Author : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Debounce a signal
//Date Created : 2017-01-16

module debounce #(
    parameter clk_freq_hz   = (  (50) * (10** 6) ), // Mhz = 10^6 Hz
    wait_time_sec           = ((   4) * (10**-3) ), // ms  = 10^-3 sec
    integer wait_cycles       =  clk_freq_hz * wait_time_sec // Overide auto calculated # of cycles to wait
)(
    input wire  in_sig,
    input wire  clk,    // Input Clock
    output reg out_sig
);
localparam CNTR_WIDTH = $clog2(wait_cycles) ; // $clog2(wait_cycles)=round_up(log_2(wait_cycles))

// Local Reg
reg [CNTR_WIDTH-1:0] cntr;
reg last_state;

// Counter
always@(posedge clk)
begin
    if((in_sig==last_state) && (cntr<=wait_cycles))   // Signal is "Bouncing"
    begin
    	cntr<=cntr+1; // Increment Counter
        out_sig<=out_sig; // Hold Output
    end
    else if((in_sig==last_state) && (cntr>wait_cycles))    // Signal Deemed "Done Bouncing"
    begin
        cntr<=cntr;  // Hold Counter
        out_sig<=in_sig; // Shift Input -> Output
    end
    else                                  // Default
    begin
        cntr<=0;     // Reset Counter
        out_sig<=out_sig;
    end
    last_state <= in_sig;
end

endmodule // debounce

// debounce #(
//     .clk_freq_hz    (  (50) * (10** 6)), // Mhz  = 10^6 Hz
//     .wait_time_sec  ((   4) * (10**-3))  // ms  = 10^-3 sec
//     //, .wait_cycles(250000)       //=  clk_freq_hz * wait_time_sec // Overide auto calculated # of cycles to wait
// ) debounce_start (
//     .in_sig(start),
//     .clk(CLOCK_50),           // Input Clock
//     .out_sig(start_debounced)
// );
