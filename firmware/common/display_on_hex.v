//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Automatically instantiate NSEGS number of seven segment displays and display binary as Hex
//Date Created : 2017-01-16

module display_on_hex #(
    parameter NSEGS=6
)(
    output wire [(7*NSEGS)-1:0] hex_L,
    input  wire [(4*NSEGS)-1:0] value
);

generate
    genvar iseg;
    for( iseg=0; iseg<NSEGS; iseg=iseg+1) begin: for_each_7seg
        HexOn7Seg seven_seg (
            .nIn(value[(iseg+1)*4-1:iseg*4]),
            .ssOut_L(hex_L[(iseg+1)*7-1:iseg*7])
        );
    end
endgenerate

endmodule
// display_on_hex #(
//     .NSEGS(4)
// ) hex3_0 (
//     .hex_L( {HEX3,HEX2,HEX1,HEX0} ),
//     .value( p )
// );
