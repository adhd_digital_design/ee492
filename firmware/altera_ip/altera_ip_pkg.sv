
`ifndef altera_ip_pkg_
`define altera_ip_pkg_

// ALTERA DE2 DEFAULT Source Files
`include "LCD_Controller.v"
`include "LCD_TEST.v"
`include "Reset_Delay.v"
`include "SEG7_LUT.v"
`include "SEG7_LUT_8.v"

`endif