//Author : Jordan D. Ulmer 
//Purpose: exclusive or
//Lab 1 - How to create a .bdf and wire up modules
//Date Created : 2017-01-13
//Date Modified: 2017-01-13

module exor(
    input wire  x,    // Input signal
    input wire  y,    
    output wire z      // Output signal
    );

assign z = (~x|y)|(x|~y) ; // xor with intended error
// assign z = (~x&y)|(x&~y) ; // correct xor

endmodule // exor


