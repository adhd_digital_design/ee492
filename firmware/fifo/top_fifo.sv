//Authors : Jordan D. Ulmer , Nathan Genetzky
// DE10 has 10 LEDR. // DE10 has 10 SW. // DE10 has 6 HEX.
// DE1 has 10 LEDR. // DE1 has 10 SW. // DE1 has 6 HEX.
// DE2 has 8 HEX.

module top_fifo #(
    parameter NCLK=4,
    parameter NIN=8,
    parameter NOUT=10,
    parameter NSEGS=6
)(
    input  wire [NCLK-1:0] clk,
    input  wire [NIN-1:0] in,
    output wire [NOUT-1:0] out,
    output wire [(NSEGS*HEX_W)-1:0] hex_L
);


//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

localparam HEX_W = 7;
localparam FIFO_DATA_WIDTH = 8;
localparam FIFO_ADDR_WIDTH = 10;
localparam DISPLAY_N_POPS = NSEGS/2; // 2 Segs per 1 Byte


//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

// Fifo Wires
wire [FIFO_DATA_WIDTH-1:0] r_data;
wire [FIFO_DATA_WIDTH-1:0] w_data;
wire [DISPLAY_N_POPS*FIFO_DATA_WIDTH-1:0] dequeue_w;
wire pop_front_next_clk;
wire fifo_is_empty_w;


//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************



//****************************************************************************
//                             Sequential logic
//****************************************************************************



//****************************************************************************
//                            Combinational logic
//****************************************************************************

// Assign External Drivers
assign w_data[FIFO_DATA_WIDTH-1:0]=in[FIFO_DATA_WIDTH-1:0];

// Assign External Indicators
// assign out[NOUT-1:2]=w_data[FIFO_DATA_WIDTH-1:0]; // show write value on leds
assign out[0]=fifo_is_empty_w;


//****************************************************************************
//                              Internal Modules
//****************************************************************************


// Display registers on Hex.
display_on_hex #( .NSEGS(NSEGS)) hex_displays_5_0 (
    .hex_L( hex_L[((NSEGS)*HEX_W)-1:((0)*HEX_W)] ),
    .value( {
             dequeue_w[DISPLAY_N_POPS*FIFO_DATA_WIDTH-1:0]
             // r_data[FIFO_DATA_WIDTH-1:0]
            } )
);


fifo
   #(
    .DATA_WIDTH(FIFO_DATA_WIDTH), // number of bits in a word
    .ADDR_WIDTH(FIFO_ADDR_WIDTH)  // number of address bits
   ) my_fifo (
    .clk(clk[0]),
    .reset(clk[1]),
    .rd(pop_front_next_clk), // .rd(clk[2]), //
    .wr(clk[3]),
    .w_data(w_data[FIFO_DATA_WIDTH-1:0]),
    .empty(fifo_is_empty_w),
    .full(out[1]),
    .r_data(r_data[FIFO_DATA_WIDTH-1:0])
   );


dequeue_fifo_n #(
    .FIFO_DATA_WIDTH(FIFO_DATA_WIDTH),                                // number of bits
    .FIFO_ADDR_WIDTH(FIFO_ADDR_WIDTH),                                // number of address bits
    .N(DISPLAY_N_POPS)                                                // number of items to dequeue "pop" from FIFO
) dequeue_my_fifo (           
    .clk_i(clk[0]),                                                   //    input wire clk_i,
    .reset_i(clk[1]),                                                 //    input wire reset_i,
                                                                      // Data:
    .dequeue_reg_o(dequeue_w[DISPLAY_N_POPS*FIFO_DATA_WIDTH-1:0]),    //    output reg [N*FIFO_DATA_WIDTH -1:0] dequeue_reg_o,
                                                                      // Status:
  //.done_reading_o(done_reading_o),                                  //    output reg done_reading_o,
                                                                      // Control: 
    .start_i(clk[2]),                                                 //    input wire start_i,
    .fifo_is_empty_i(fifo_is_empty_w),                                //  input wire fifo_is_empty_i,
                                                                      // Passthrough signals:
    .pop_front_next_clk_o(pop_front_next_clk),                        //    output wire pop_front_next_clk_o, // Read Data     // Dequeue // Pop
    .pop_data_i(r_data[FIFO_DATA_WIDTH-1:0])                          //    input  wire [FIFO_DATA_WIDTH -1:0]  pop_data_i,
                                                                      // Debug Only: 
 // ,.dequeue_state_w(out[NOUT-1:2])                                  //    ,output wire [DEQUEUE_STATE_WIDTH-1:0] dequeue_state_w
);


endmodule
