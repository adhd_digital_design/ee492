//Authors : Jordan D. Ulmer , Nathan Genetzky
//Purpose: Dequeue (pop) n values from First On First Out - FIFO
//Date Created : 2017-02-04

`define PRESERVE_STATES_M\
dequeue_counter         <= dequeue_counter; \
dequeue_reg_o           <= dequeue_reg_o; \
done_reading_o          <= done_reading_o; \
pop_front_next_clk_o    <= pop_front_next_clk_o; // Preserve Last State 


module dequeue_fifo_n #(
    parameter FIFO_DATA_WIDTH = 8,   // number of bits
              FIFO_ADDR_WIDTH = 10,  // number of address bits
              N = 6 // number of items to dequeue "pop" from FIFO
) (
    input wire clk_i,
    input wire reset_i,
    // Data:
    output reg [N*FIFO_DATA_WIDTH -1:0] dequeue_reg_o,
    // Status:
    output reg done_reading_o,
    // Control:
    input wire start_i,
    input wire fifo_is_empty_i,
    // Passthrough signals:
    output reg pop_front_next_clk_o, // Read Data     // Dequeue // Pop
    input  wire [FIFO_DATA_WIDTH -1:0]  pop_data_i
    // Debug Only:
    // ,output wire [DEQUEUE_STATE_WIDTH-1:0] dequeue_state_w
    // Use dual_port_ram to "peek" inside FIFO, without modifying FIFO
    // ,output wire [FIFO_DATA_WIDTH -1:0]  r_next_addr,
    // ,input  wire [FIFO_DATA_WIDTH -1:0]  r_data,
    // ,output wire rd // Read Data     // Dequeue // Pop
);

//****************************************************************************
//                        Local Parameter Declarations
//****************************************************************************

localparam DEQUEUE_COUNTER_WIDTH = $clog2(N) ; // $clog2 = round_up(log_2(N))
localparam DEQUEUE_STATE_WIDTH = 3;
// Have compilier optimize state values
typedef enum reg [DEQUEUE_STATE_WIDTH-1:0] \
            {IDLE,RESET,START,READING,ERROR} dequeue_state_t; 

// Choose state values for debug
// typedef enum reg [DEQUEUE_STATE_WIDTH-1:0] {IDLE=3'd0,RESET=3'd1,START=3'd2,READING=3'd3,ERROR=3'd4} dequeue_state_t;

//****************************************************************************
//                 Internal wires and registers Declarations
//****************************************************************************

dequeue_state_t dequeue_state_reg; // State Variable

reg [DEQUEUE_COUNTER_WIDTH-1:0] dequeue_counter ;

// reg reset_reg;

//****************************************************************************
//                         Finite State Machine(s)
//****************************************************************************

always @(posedge clk_i) begin : DEQUEUE_STATE_FSM
    case (dequeue_state_reg)
        ERROR:
            begin
            `PRESERVE_STATES_M
            dequeue_state_reg  <= RESET;
            end
        RESET:
            begin
            dequeue_counter         <= 0; // Point @ LSB of dequeue_reg
            dequeue_reg_o           <= 0; 
            done_reading_o          <= 0; // NOT Done Reading
            pop_front_next_clk_o    <= 0; // Do Not POP
            dequeue_state_reg  <= IDLE;
            end
        START:
            begin
            dequeue_counter         <= 0; // Point @ LSB of dequeue_reg
            dequeue_reg_o           <= dequeue_reg_o; // Preserve State
            done_reading_o          <= 0; // NOT Done Reading
            pop_front_next_clk_o    <= 1; // POP data from fifo on next clock
            dequeue_state_reg  <= READING;
            end
        READING:
            begin
            case (1)
                ~fifo_is_empty_i && inbetween(0,dequeue_counter,(N-1)-1):// Up to Last POP request // 2nd to Last Read    
                    begin
                    dequeue_counter         <= dequeue_counter+1; // Point to next dequeue_reg
                    dequeue_reg_o[dequeue_counter*FIFO_DATA_WIDTH+:FIFO_DATA_WIDTH] <= pop_data_i; // Load data from previous fifo POP
                    dequeue_reg_o[dequeue_counter*FIFO_DATA_WIDTH+:FIFO_DATA_WIDTH] <= pop_data_i; // Load data from previous fifo POP
                    dequeue_reg_o[dequeue_counter*FIFO_DATA_WIDTH+:FIFO_DATA_WIDTH] <= pop_data_i; // Load data from previous fifo POP
                    done_reading_o          <= 0; // NOT Done Reading
                    pop_front_next_clk_o    <= 1; // POP data from fifo on next
                    dequeue_state_reg  <= READING;
                    end

                fifo_is_empty_i || inbetween((N-1),dequeue_counter,(N-1)): //FIFO is Empty or // Last Read // Don't care if fifo_is_empty_i
                    begin
                    dequeue_counter         <= dequeue_counter; // Preserve State
                    dequeue_reg_o[dequeue_counter*FIFO_DATA_WIDTH+:FIFO_DATA_WIDTH] <= pop_data_i; // Load data from previous fifo POP
                    done_reading_o          <= 1; // Done Reading
                    pop_front_next_clk_o    <= 0; // Don't POP
                    dequeue_state_reg <= IDLE;
                    end
                default: // Should never get to this state
                    begin
                    `PRESERVE_STATES_M
                    dequeue_state_reg <= ERROR; 
                    end
            endcase // 1
            end
        IDLE:
            begin
            casex ({reset_i,start_i,fifo_is_empty_i})
                3'b1xx: // RESET has priority
                    begin
                    `PRESERVE_STATES_M
                    dequeue_state_reg <= RESET; 
                    end
                3'b010: // Only Start if NOT empty
                    begin
                    `PRESERVE_STATES_M
                    dequeue_state_reg <= START;
                    end
                3'b011: // IDLE if empty
                    begin
                    `PRESERVE_STATES_M
                    dequeue_state_reg <= IDLE;
                    end
                3'b00x: // do nothing 
                    begin
                    `PRESERVE_STATES_M
                    dequeue_state_reg <= IDLE;
                    end
                default:// Should never get to this state
                    begin
                    `PRESERVE_STATES_M
                    dequeue_state_reg <= ERROR; 
                    end
            endcase // {reset_i,start_i,fifo_is_empty_i}
            end
        default : // Should never get to this state
            begin
            `PRESERVE_STATES_M
            dequeue_state_reg <= ERROR; 
            end
    endcase // dequeue_state_reg
end // DEQUEUE_STATE_FSM


//****************************************************************************
//                             Sequential logic
//****************************************************************************

// Catch Asynchronous Reset, perform reset on clock  // This was a bad idea...
// always @(posedge reset_i ) begin
//     reset_reg <= reset_i;
// end

//****************************************************************************
//                            Combinational logic
//****************************************************************************

//http://www.alteraforum.com/forum/showthread.php?t=42182
function reg inbetween(input [DEQUEUE_COUNTER_WIDTH:0] low, value, high); 
    begin
      inbetween = value >= low && value <= high;
    end
endfunction

// assign dequeue_state_w = dequeue_state_reg;

//****************************************************************************
//                              Internal Modules
//****************************************************************************


endmodule

// Example Instantiation
// dequeue_fifo_n #(
//     .FIFO_DATA_WIDTH(FIFO_DATA_WIDTH),                                // number of bits
//     .FIFO_ADDR_WIDTH(FIFO_ADDR_WIDTH),                                // number of address bits
//     .N(DISPLAY_N_POPS)                                                // number of items to dequeue "pop" from FIFO
// ) dequeue_my_fifo (           
//     .clk_i(clk[0]),                                                   //    input wire clk_i,
//     .reset_i(clk[1]),                                                 //    input wire reset_i,
//                                                                       // Data:
//     .dequeue_reg_o(dequeue_w[DISPLAY_N_POPS*FIFO_DATA_WIDTH-1:0]),    //    output reg [N*FIFO_DATA_WIDTH -1:0] dequeue_reg_o,
//                                                                       // Status:
//   //.done_reading_o(done_reading_o),                                  //    output reg done_reading_o,
//                                                                       // Control: 
//     .start_i(clk[2]),                                                 //    input wire start_i,
//     .fifo_is_empty_i(fifo_is_empty_w),                                //  input wire fifo_is_empty_i,
//                                                                       // Passthrough signals:
//     .pop_front_next_clk_o(pop_front_next_clk),                        //    output wire pop_front_next_clk_o, // Read Data     // Dequeue // Pop
//     .pop_data_i(r_data[FIFO_DATA_WIDTH-1:0])                          //    input  wire [FIFO_DATA_WIDTH -1:0]  pop_data_i,
//                                                                       // Debug Only: 
//   //,.dequeue_state_w(out[NOUT-1:2])                                  //    ,output wire [DEQUEUE_STATE_WIDTH-1:0] dequeue_state_w
// );