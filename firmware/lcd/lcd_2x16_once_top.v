

// Modified from "LCD_TEST" from "DE2-Demonstration"
// http://www.terasic.com/downloads/cd-rom/de2/
module lcd_2x16_once_top (
    input wire [ (8*16*2) -1:0] char_data, // 8 bits/char * 16 char/line * 2 line/display
    // Host Side
    input wire clk,
    input wire reset_L,
    // LCD Side
    output wire [7:0] LCD_DATA,
    output wire LCD_RW,
    output wire LCD_EN,
    output wire LCD_RS
);

localparam  PROG_SIZE = 5+32+1;
localparam  STATE_SET_DATA  =  0;
localparam  STATE_START     =  1;
localparam  STATE_WAIT      =  2;
localparam  STATE_NEXT      =  3;


// Internal Wires/Registers
reg	[5:0]	mLCD_ST;
reg	[17:0]	mDLY=0;
reg			mLCD_Start=0;
reg	[7:0]	mLCD_DATA=0;
reg			mLCD_RS=0;
wire		mLCD_Done;

// LCD Program
reg	[5:0]	PROG_INDEX;
wire [8:0] PROG_DATA;

always@(posedge clk or negedge reset_L) begin : lcd_state_machine
    if(!reset_L) begin : reset_to_zero
        PROG_INDEX   <=  0;
        mLCD_ST     <=  0;
        mDLY        <=  0;
        mLCD_Start  <=  0;
        mLCD_DATA   <=  0;
        mLCD_RS     <=  0;
    end

    else begin
    if(PROG_INDEX<PROG_SIZE) begin
        case(mLCD_ST)
        STATE_SET_DATA: begin : lcd_set_data
                mLCD_DATA   <=  PROG_DATA[7:0];
                mLCD_RS     <=  PROG_DATA[8];
                mLCD_Start  <=  1;
                mLCD_ST     <=  STATE_START;
            end

        STATE_START: begin : lcd_start
                if(mLCD_Done) begin
                    mLCD_Start  <=  0;
                    mLCD_ST     <=  STATE_WAIT;
                end
            end

        STATE_WAIT: begin : lcd_wait
                // TODO: What is this delay when clk=27 MHz?
                if(mDLY<18'h3FFFE) begin
                    mDLY <= mDLY+1;
                end
                else begin
                    mDLY <= 0;
                    mLCD_ST <= STATE_NEXT;
                end
            end

        STATE_NEXT: begin : lcd_next
                PROG_INDEX <= PROG_INDEX+1;
                mLCD_ST <= STATE_SET_DATA;
            end

        endcase
    end
    end
end

lcd_prog_2x16_once #(
    .SIZE(PROG_SIZE)
) prog0 (
    .char_data(char_data),
    .index(PROG_INDEX),
    .data(PROG_DATA) // { 1'RS, 8'DATA }
);

LCD_Controller controller (
    // Host Side
    .iDATA(mLCD_DATA),
    .iRS(mLCD_RS),
    .iStart(mLCD_Start),
    .oDone(mLCD_Done),
    .iCLK(clk),
    .iRST_N(reset_L),
    // LCD Interface
    .LCD_DATA(LCD_DATA),
    .LCD_RW(LCD_RW),
    .LCD_EN(LCD_EN),
    .LCD_RS(LCD_RS)
);

endmodule
